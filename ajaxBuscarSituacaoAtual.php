<?php
require_once("helper_functions.php");
session_start(); // inicio a sessão

if($_SESSION['usuario']){ // verifico se usuario esta logado
    require("conexao.php");
    conexao();
    
    $post_situacoes = $_POST['situacoes'];
    $post_unidades  = $_POST['unidades'];
    
    $where  = NULL;

    $r['col'][] = array('nome' => 'unidade_nome',
                                  'label' => 'Unidade',
                                  'align' => 'left');
    if ((sizeOf($post_situacoes)+sizeOf($post_unidades))>0){
        $where = ' WHERE 1=1';
        if (in_array(1, $post_situacoes)) { 
            $r['col'][] = array('nome' => 'pendente',
                                'label' => 'Pendentes',
                                'align' => 'right');
        }
        if (in_array(2, $post_situacoes)) { 
            $r['col'][] = array('nome' => 'em_analise',
                                'label' => 'Em an&aacute;lise',
                                'align' => 'right');
        }
        if (in_array(3, $post_situacoes)) { 
            $r['col'][] = array('nome' => 'concluida',
                                'label' => 'Conclu&iacute;das',
                                'align' => 'right');
        }
        if (in_array(4, $post_situacoes)) { 
            $r['col'][] = array('nome' => 'publicada',
                                'label' => 'Publicadas',
                                'align' => 'right');
        }
        if (in_array(5, $post_situacoes)) { 
            $r['col'][] = array('nome' => 'cancelada',
                                'label' => 'Canceladas',
                                'align' => 'right');
        }
        
        for($i=0;$i<sizeOf($r['col']);$i++){
            $colunas[] = $r['col'][$i]['nome'];
        }
        
        $select = 'SELECT '.implode($colunas,',');
        
        if (sizeOf($post_unidades)>0){
            $where          .= ' AND unidade_id IN ('.implode($post_unidades,',').')';
        }
    }
    
    $sql = "$select FROM v_situacao_atual_analises $where" ;
    
    $exe = mysql_query($sql, $base) or die(mysql_error());
    
    $r['qtd']= mysql_num_rows($exe); // retorna se existe alguma linha no banco
    
    while ($row = mysql_fetch_array($exe, MYSQL_NUM)) {
        $r['res'][] = $row;
    }
    echo json_encode($r);
}