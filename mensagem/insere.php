<?php 
session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado
?>
<div id="chamada" class="chamada37">Cadastrar Mensagem</div>
<p>Aqui você deverá inserir os dados da mensagem.</p>
<form id="enviaDados" name="enviaDados" method="post" action="#" >
  <table id="tabGeral">
    <tr>
      <td align="right" valign="middle">Título* :</td>
      <td><input name="titulo" type="text" id="titulo" size="60" />
      </td>
    </tr>
    <tr>
      <td align="right" valign="top">Mensagem*:</td>
      <td>
      <textarea name="mensagem" id="mensagem" cols="60" rows="10"></textarea></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>* Campos Obrigatórios</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input name="envia" type="button" id="envia" onclick="mensagem_user('enviaDados', 'mensagem/gerencia.php?acao=1', 'conteudo');" value="Enviar" class="btnLogar"/>
        <input type="reset" name="limpar" id="limpar" value="Limpar" class="btnLogar" /></td>
    </tr>
  </table>
</form>
<?php } else { // se usuário não estiver logado?>
	<script language="JavaScript">
			window.location.href = "../index.php";
	</script>
<?php }?>