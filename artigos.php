<?php session_start(); // Inicio a Sessão?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="análise de falhas, indústria, RCA - Root Cause Analisys, ações de bloqueio, causa raiz da falha
" />
		<meta name="description" content="Realize análises de falhas de maneira descomplicada e contribua para a melhoria nos indicadores da indústria." />
		<meta name="rating" content="General" />
		<meta name="author" content="Agência GH" />
		<meta name="language" content="pt-br" />
		<meta name="ROBOTS" content="index,follow" />
		<title>TELIOS</title>
		<link href="../favicon.ico" rel="shortcut icon" />
		<link href="css/home.css" rel="stylesheet" type="text/css" media="screen" />
        <?php if(isset($_GET['a'])){
			$artigo = $_GET['a'];
		} else {
			$artigo = 1;	
		}?>
		
		<?php if($artigo == 1){?>
		<style>
	#btnFlexaArtigo{
	width:27px;
	height:54px;
	position:absolute;
	left: 0px;
	top: 143px;
}
        </style>
        <?php }?>
        
        <?php if($artigo == 2){?>
		<style>
	#btnFlexaArtigo{
	width:27px;
	height:54px;
	position:absolute;
	left: 0px;
	top: 236px;
}
        </style>
        <?php }?>
        
        <?php if($artigo == 3){?>
		<style>
	#btnFlexaArtigo{
	width:27px;
	height:54px;
	position:absolute;
	left: 0px;
	top: 333px;
}
        </style>
        <?php }?>
        
        <?php if($artigo == 4){?>
		<style>
	/*#btnFlexaArtigo{
	width:27px;
	height:54px;
	position:absolute;
	left: 0px;
	top: 425px;*/
}
        </style>
        <?php }?>
        
		<script language="javascript" src="js/post.js"></script>
		<script language="javascript" src="js/mascara.js"></script>
		<script type="text/javascript" src="swfobject/swfobject.js"></script>
        
        <link rel="stylesheet" href="css/colorbox.css" />
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.colorbox.js"></script>
		<script>
			$(document).ready(function(){
				
				$(".iframe").colorbox({iframe:true, width:"600px", height:"300px"});
			});
		</script>
        
		</head>
		<body>
<?php if (isset($_SESSION['usuario'])) { // Verifico se o Usuário ja esta logado?>
<script language="JavaScript">
			window.location.href = "logado.php";
		</script>
<?php } else { // Se o Usuário não estiver logado mostro form de login ?>

<div id="tudo">
          <div id="topo">
    <div id="topoInterno">
              <div id="linkHome"><a href="index.php"><img src="images/site/linkHome.gif" width="192" height="41" /></a></div>
              <a href="ferramenta.php"><div id="linkFerramenta"></div></a>
              <a href="rca.php"><div id="linkRca"></div></a>
      		  <a href="vantagens.php"><div id="linkVantagens"></div></a>
      		  <a href="artigos.php"><div id="linkArtigos"></div></a>
	  <a href="estatisticas.php" title="Estatísticas"><div id="linkEstatisticas"></div></a>
              <a href="busca_publicadas.php"><div id="linkBusca"></div></a>
              <a href="contato.php"><div id="linkContato"></div></a>
              <div id="formLogin">
        <div id="formLoginInterna">
                  <form id="log" name="log" method="post" action="logado.php" >
            <div id="inputFormLogin">
                      <input name="login" type="text" id="login"/>
                    </div>
            <div id="inputFormSenha">
                      <input name="senha" type="password" id="senha"/>
                    </div>
            <div id="inputFormEntrar">
                      <input name="envia" type="submit" id="envia" class="btnLogar" value=""/>
                    </div>
          </form>
                  <?php if(isset($_GET['deslogado'])){?>
                  <div id="erroLogin">Login ou Senha Inválido</div>
                  <?php }?>
<div id="esqueceu_senha"><a href="esqueceu_senha.php" class="group2 iframe">Esqueceu sua senha ? </a></div>
                </div>
      </div>
            </div>
  </div>
          <div id="conteudoArtigos">
          <div id="artigos">
          	<p class="fonte28">Artigos</p>
            <div id="textoArtigos">
            	<p>Leia os artigos relacionados a Engenharia de Manutenção e Análise da Causa Raíz.</p>
            </div>
            
            
<div id="linkArtigosTexto">
            
            	<p><a href="artigos.php?a=1" class="artigo16">Justifying Root<br />
				   Cause Analisys </a></p>
                <p><a href="artigos.php?a=1" class="artigo14">By Robert J. Latino<br />
                   Reliability Center, Inc.</a></p> 
        <div class="linhaArtigo"><img src="images/site/linhaArtigo.jpg"/></div>   
            </div>
            
<div id="linkArtigosTexto">
            
            	<p><a href="artigos.php?a=2" class="artigo16">Definition of Root<br />
Cause Analysis (RCA)  </a></p>
                <p><a href="artigos.php?a=2" class="artigo14">www.downtimecentral.com<br />
Posted 1-17-05  </a></p>  
<div class="linhaArtigo"><img src="images/site/linhaArtigo.jpg"/></div>     
            </div>
            
<div id="linkArtigosTexto">
            
            	<p><a href="artigos.php?a=3" class="artigo16">Getting Root Cause Analysis<br />
to Work for You </a></p>
                <p><a href="artigos.php?a=3" class="artigo14">Alexander Dunn <br />
Assetivity Pty Ltd </a></p>  
<div class="linhaArtigo"><img src="images/site/linhaArtigo.jpg"/></div>     
            </div>
            
<!--<div id="linkArtigosTexto">
            
            	<p><a href="artigos.php?a=4" class="artigo16">Manutenção em<br />
Tempo de Crise </a></p>
                <p><a href="artigos.php?a=4" class="artigo14">Mario Filho</a></p>
        <div class="linhaArtigo"><img src="images/site/linhaArtigo.jpg"/></div>       
            </div>-->
            
          </div>
          <div id="conteudoArtigo">
          <div id="btnFlexaArtigo"><img src="images/site/ARTIGOS_06.jpg" width="27" height="54" /></div>
          	<?php if($artigo == 1){?>
            
            <p class="fonte18">Justifying Root Cause Analysis </p>
            <p class="negrito">
            
            Make the business case with a significant calculated return on investment.</p>
            <p>
How often have we heard that we do not have time to do root cause analysis (RCA)? This is certainly the paradigm from those closest to the work, especially if they operate in a reactive culture. What about when we hear that RCA is too expensive? This is generally the paradigm from management, or those farthest from the work.</p>
<p>
What is common about these two perspectives? Both perceptions represent reality because if that is what we believe, then our decisions will be made on that basis. So how do we overcome this hurdle of letting our paradigms prevent us from taking advantage of opportunities?</p>
<p class="negrito">
Opposing paradigms: operations vs. finance</p>
<p>
Let's explore this issue from two different perspectives: operational and financial. The operational people are those who are closest to the work and are responsible for maximizing the output of the organization. In this world, a reactive culture usually dominates. So whether we are making paper, processing patients, or dealing with customer complaints, we are likely dealing with the moment and handling one fire at a time.</p>
<p>
In this world it is difficult to listen to people who advocate an activity like RCA because as it stands now, there does not seem to be enough time in a day to do our current job. Now someone wants us to perform another task, RCA, when our plates are already full. Let's face it: this is the reality when working at this level. We do not see RCA as a solution to our already overburdened work schedule. We see RCA as a nuisance to being able to fight fires in the short-term.</p>
<p>
Contrast this perspective against the financial one. Management level people are typically the ones that are charged with fiscal responsibility. So their world is one of numbers, statistics, and ultimately dollars. When people approach them about the concept of RCA, the first issue in their mind is: "How much is this going to cost?" Again, this is their world.</p>
<p>
Usually in this world the first question is not "What value proposition does RCA bring to the table?" In the financial world we are dictated to by the budget, and no matter how attractive the opportunity, the cost in relation to the budget will be one of the major deciding factors. Sometimes our performance evaluations will reward us for staying within the budget, so there is a personal incentive to view everything from the cost standpoint versus the value standpoint.</p>
<p>
What happens when these two worlds collide? We become risk aversive in our decision-making and our operations. When this happens we hang out in the safety zone and if we are lucky, we make marginal improvements over time. Creativity is stifled and we become human robots doing nothing more or less than we are told. </p>

<?php } if($artigo == 2){?>


<p class="fonte18">Definition of Root Cause Analysis (RCA)</p>

<p>One of the lean manufacturing principles explained in this refreshing article.
From the newsletter "Process Plant and Equipment UP-TIME" by www.feedforward.com.au – Leaders in Plant Maintenance Continuous Improvement</p>
<p>
<span class="negrito">What you will learn from this article.</span></p>
<ul id="artigos_ul">
	<li>An explanation of root causes analysis (RCA).</li> 
	<li>An overview of how it is applied at your work place.</li> 
	<li>See the result of a sample RCA.</li> 
</ul>
<p><span class="negrito">ABSTRACT -</span> Root cause analysis answers your problem. Root Cause Analysis (RCA) is a step by step method that leads to the discovery of a fault's first or root cause. Every equipment failure happens for a number of reasons. There is a definite progression of actions and consequences that lead to a failure. An RCA investigation traces the cause and effect trail from the end failure back to the root cause. Much like a detective solving a crime. Keywords: failure mode, fault finding, fault tree, cause tree, events. </p>

<p class="negrito">OVERVIEW OF THE RCA PROCESS</p>
<p>The method brings a team of 3 to 6 knowledgeable people together to investigate the failure using evidence left behind from the fault. The team brainstorms to find as many causes of the fault as possible. By using what evidence remained after the fault, and through discussions with people involved in the incident, all the non-contributing causes are removed and the contributing causes retained.</p>
<p> A fault tree is constructed starting with the final failure and progressively tracing each cause that led to the previous cause. This continues till the trail can be traced back no further. Each result of a cause must clearly flow from its predecessor (the one before it). If it is clear that a step is missing between causes it is added in and evidence looked for to support its presence.
Once the fault tree is completed and checked for logical flow the team then determines what changes to make to prevent the sequence of causes and consequences from again occurring.</p>

<p class="negrito">PREVENTING REOCCURRENCE OF THE FAILUR</p>
<p>It is not necessary to prevent the first, or root cause, from happening. It is merely necessary to break the chain of events at any point and the final failure cannot occur. Often the fault tree leads to an initial design problem. In such a case redesign is necessary. Where the fault tree leads back to a failure of procedures it is necessary to either address the procedural weakness or to install a method to protect against the damage caused by the procedural failure. Below is a sample fault tree for the moral story of the kingdom lost because of a missing horseshoe nail.</p>

<p class="negrito">AN RCA EXAMPLE WITH CAUSE TREE</p>
<p>The story is told that before an important battle a king sent his horse with a groomsman to the blacksmith for shoeing. But the blacksmith had used all the nails shoeing the knight's horses for battle and was one short. The groomsman tells the blacksmith to do as good a job as he can. But the blacksmith warns him that the missing nail may allow the shoe to come off. The king rides into battle not knowing of the missing horseshoe nail. In the midst of the battle he rides toward the enemy. As he approaches them the horseshoe comes off the horse's hoof causing it to stumble and the king falls to the ground. The enemy is quickly onto him and kills him. The king's troops see the death, give up the fight and retreat. The enemy surges onto the city and captures the kingdom. The kingdom is lost because of a missing horseshoe nail.</p>
<p>The cause tree explains step-by-step how the events leading to the king's death unfolded. Notice that two separate event 'branches' had to occur together for the sequence to continue to the fateful end. If any of the causes could have been prevented then the kingdom would have been safe.</p>


<?php } if($artigo == 3){?>
<p class="fonte18">Getting Root Cause Analysis to Work for You</p>
<p class="negrito">Summary</p> 
<p>This paper provides several tips regarding how to make the most of Root Cause Analysis to progressively eliminate failures. Specific topics that are covered include:</p>
<ul id="artigos_ul"> 
<li>The need for training – why Root Cause Analysis is not just "common sense" </li>
<li>The benefits of a team-based approach to Root Cause Analysis</li> 
<li>RCA Software – is it necessary for effective Root Cause Analysis? </li>
<li>Creating the organisational environment for RCA success </li>
</ul>
<p>The paper is based on the author's experience in providing RCA training and consulting assistance at several mining organisations in Australia and overseas</p> 
<p class="negrito">Keywords </p>
<p>Root Cause Analysis, RCA, Software, Organisational Culture, Implementation, Team-based Problem Solving</p> 

<p class="negrito">1 INTRODUCTION</p> 
<p>Root Cause Analysis (RCA) is rapidly becoming another one of those "flavour of the month" TLAs (Three Letter Acronyms). Like all TLAs, it is easy to get carried away with the hype surrounding the approach. Inevitably, then, the reality doesn't live up to the expectations created by the hype. But nevertheless, the appropriate application of Root Cause Analysis techniques can yield significant organisational and individual benefits. This paper discusses some of the practical issues surrounding the implementation of Root Cause Analysis processes within organisations, and in doing so, attempts to give some guidance to those wishing to obtain success from their Root Cause Analysis program. </p>
<p><a href="Getting_Root_Cause_Analysis_to_Work_for_You.docx" target="_blank">Baixe o artigo completo</a></p>

<?php } if($artigo == 4){?>

<?php }?>
            
          </div>
          <div class="clear"></div>
          </div>
          
          <div id="rodape">
  	<div id="rodape_interno">
    	<a href="http://www.agenciagh.com.br" title="Agência GH" target="_blank"><div id="marca_gh"></div></a>
        <div id="menu_inferior">
        	<div class="email_contato"><span class="negrito">E-mail:</span><a href="mailto:contato@telios.eng.br" class="group2">contato@telios.eng.br</a></div>
            
            <div class="menu_inferior_link"><a href="contato.php" title="CONTATO" class="group2">Contato</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="busca_publicadas.php" title="BUSCA ANÁLISE" class="group2">Busca Análises</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="estatisticas.php" title="RANKING DO CONHECIMENTO" class="group2">Ranking do Conhecimento</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="artigos.php" title="ARTIGOS" class="group2">Artigos</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="rca.php" title="RCA" class="group2">RCA</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="vantagens.php" title="VANTAGENS" class="group2">Vantagens</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="ferramenta.php" title="A FERRAMENTA" class="group2">A Ferramenta</a></div>
        </div>
        
        <div class="clear"></div>
    </div>
  </div>
          
        </div>

<!--<form id="enviaLogin" name="enviaLogin" method="post" action="logado.php" >
  <table cellspacing="0" id="tabGeral">
    <tr>
      <td align="right" valign="middle">Usuário:</td>
      <td><input name="login" type="text" id="login" size="30"/></td>
    </tr>
    <tr>
      <td align="right" valign="middle">Senha:</td>
      <td><input name="senha" type="password" id="senha" size="30"/></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><a href="#" onclick="recuperaSenha('senha/recuperaSenha.php');">Esqueci Minha Senha.</a></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input name="envia" type="submit" id="envia" value="Logar" class="btnLogar"/></td>
    </tr>
  </table>
</form>-->
<?php }?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29818024-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>