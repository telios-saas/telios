<?php

/**
 * BaseController
 *
 * @author fmontezuma
 */
abstract class BaseController {
    
    const NUM_REG_PAG = 10;
    
    static function iniciaSessaoEConexao() {
        session_start(); // inicio a sessão
        if(!$_SESSION['usuario'])
            throw new Exception("Usuário não logado!");
        
        require_once("../../conexao.php");
        conexao();        
    }
    
    static function obterPaginaAtual() {
        $p = 1;
        if(isset($_GET['p'])){ // Pega a página atual da paginação
            $p = $_GET["p"];
        }
        return $p;
    }
}
