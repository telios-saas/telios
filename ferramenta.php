<?php session_start(); // Inicio a Sessão?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="análise de falhas, indústria, RCA - Root Cause Analisys, ações de bloqueio, causa raiz da falha
" />
		<meta name="description" content="Realize análises de falhas de maneira descomplicada e contribua para a melhoria nos indicadores da indústria." /><meta name="rating" content="General" />
<meta name="author" content="Agência GH" />
<meta name="language" content="pt-br" />
<meta name="ROBOTS" content="index,follow" />
<title>TELIOS</title>
<link href="../favicon.ico" rel="shortcut icon" />
<link href="css/home.css" rel="stylesheet" type="text/css" media="screen" />
<script language="javascript" src="js/post.js"></script>
<script language="javascript" src="js/mascara.js"></script>
<script type="text/javascript" src="swfobject/swfobject.js"></script>

<link rel="stylesheet" href="css/colorbox.css" />
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.colorbox.js"></script>
		<script>
			$(document).ready(function(){
				$(".group1").colorbox({rel:'group1'});
				$(".iframe").colorbox({iframe:true, width:"600px", height:"300px"});
			});
		</script>

</head>
<body>
<?php if (isset($_SESSION['usuario'])) { // Verifico se o Usuário ja esta logado?>
<script language="JavaScript">
			window.location.href = "logado.php";
		</script>
<?php } else { // Se o Usuário não estiver logado mostro form de login ?>
<div id="tudo">
  <div id="topo">
    <div id="topoInterno">
              <div id="linkHome"><a href="index.php"><img src="images/site/linkHome.gif" width="192" height="41" /></a></div>
              <a href="ferramenta.php"><div id="linkFerramenta"></div></a>
              <a href="rca.php"><div id="linkRca"></div></a>
      		  <a href="vantagens.php"><div id="linkVantagens"></div></a>
      		  <a href="artigos.php"><div id="linkArtigos"></div></a>
	  <a href="estatisticas.php" title="Estatísticas"><div id="linkEstatisticas"></div></a>
              <a href="busca_publicadas.php"><div id="linkBusca"></div></a>
              <a href="contato.php"><div id="linkContato"></div></a>
              <div id="formLogin">
        <div id="formLoginInterna">
                  <form id="log" name="log" method="post" action="logado.php" >
            <div id="inputFormLogin">
                      <input name="login" type="text" id="login"/>
                    </div>
            <div id="inputFormSenha">
                      <input name="senha" type="password" id="senha"/>
                    </div>
            <div id="inputFormEntrar">
                      <input name="envia" type="submit" id="envia" class="btnLogar" value=""/>
                    </div>
          </form>
                  <?php if(isset($_GET['deslogado'])){?>
          <div id="erroLogin">Login ou Senha Inválido</div>
                  <?php }?>
<div id="esqueceu_senha"><a href="esqueceu_senha.php" class="group2 iframe">Esqueceu sua senha ? </a></div>
                </div>
      </div>
            </div>
  </div>
  <div id="conteudo2"> 
  <!--<div id="imagemFerramenta1">
    <p class="fonte28">A Ferramenta</p> 
  	<div id="dadosFerramenta1Esq">
    	<p><img src="images/site/A-FERRAMENTA_09.jpg" class="imgDir"/>Desenvolvida para rodar na internet sobre uma plataforma robusta de gerenciamento de banco de dados, esta ferramenta de RCA - Root Cause Analisys propicia fácil gerenciamento das informações sem a necessidade de instalar software específico no seu computador.</p>
<p>Em sua interface, o usuário pode inserir todos os dados de uma ANÁLISE DE FALHA PADRÃO, priorizar o evento, inserir AÇÕES DE BLOQUEIO atreladas às pessoas da equipe e comunicar a todos automaticamente. Além  disso pode inserir anexos, gerar relatórios padronizados das análises e manter a sua base de dados atualizada de uma maneira prática. </p>
    </div>
    <div id="dadosFerramenta1Dir">
    <p>Após o cadastramento, com apenas algumas informações básicas necessárias para manter a rastreabilidade das análises, o usuário pode iniciar uma avaliação de falha seguindo uma metodologia simples e eficaz.<img src="images/site/A-FERRAMENTA_06.jpg" class="imgEsq"/> </p>
    <p>Todas as informações armazenadas podem ser acessadas a partir de qualquer computador que tiver conexão com a internet, facilitando a consulta das informações e o gerenciamento das pendências geradas durante os processos de investigação.</p>
    </div>
    <div class="clear"></div>
  </div>-->
 <div id="imagemFerramenta2">
  <p class="fonte28">Saiba como utilizar a Télios</p> 
  <div id="nomePasso"><img src="images/site/A-FERRAMENTA_15.jpg" width="128" height="127"/></div>
  <div id="dadosPasso">
  	<div id="topDadosPassos"></div>
    <div id="conteudoPasso">
    	<div id="dadosConteudoEsqInter">
        	<div id="imgUser1"><img src="images/site/A-FERRAMENTA_23.jpg" width="62" height="71"/></div>
            <div id="imgUser2">
           	  <p><img src="images/site/A-FERRAMENTA_18.jpg"/></p>
              <p>Este usuário tem acesso ao gerenciamento geral da Télios.</p>
            </div>
            <div class="clear"></div>
        </div>
        <div id="dadosConteudoDirInter">
        <div id="imgUser1"><img src="images/site/A-FERRAMENTA_26.jpg" width="62" height="71"/></div>
        <div id="imgUserDir2">
        	<p><img src="images/site/A-FERRAMENTA_20.jpg"/></p>
            <p>Este usuário tem acesso limitado às suas responsablidades nas AÇÕES DE BLOQUEIO e cadastro de NOVAS ANÁLISES.</p>
        </div>
        <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
  </div>
  
  <div class="clear"></div>
  
  <div id="nomePasso"><img src="images/site/A-FERRAMENTA_33.jpg" width="128" height="127"/></div>
  <div id="dadosPasso1">
  <div id="texto_passo_1">
  	<p>Após fazer o login e entrar na
Página Inicial, navegue pela aba
"Dados de Cadastro". Nesta
aba você deve cadastrar:</p>

<ul>
	<li>Equipe</li>
    <li>Equipamentos</li>
    <li>Linha de Produção</li>
    <li>Unidade Fabril</li>
</ul>

  </div>
  
  <div id="img_clic_1"><a href="images/site/Passo_1_figura_1.jpg" class="group1"><img src="images/site/Passo_1_figura_1_THUMB.jpg"/></a></div>
  <div id="img_clic_2"><a href="images/site/Passo1_figura_2.JPG" class="group1"><img src="images/site/Passo1_figura_2_thumb.jpg"/></a></div>
  
  
  <div id="clique_ampliar"><a href="images/site/Passo_1_figura_1.jpg" class="group1">Clique para Ampliar</a></div>
  <div id="clique_ampliar_2"><a href="images/site/Passo1_figura_2.JPG" class="group1">Clique para Ampliar</a></div>
  </div>
 
  <div id="dadosPasso2">
  <div id="texto_passo_1">
  	<p>Agora clique a aba "Cadastrar Nova Análise".</p>
    <p>

Nesse momento, no campo "Título", você deve nomear o problema . Após situe-o dentro de sua empresa através das opções nos filtros abaixo.</p>
  </div>
  
  <div id="img_clic_1"><a href="images/site/Passo_2_figura_1.jpg" class="group1"><img src="images/site/Passo_2_figura_1_thumb.jpg"/></a></div>
  <div id="img_clic_2"><a href="images/site/Passo_2_figura_2.jpg" class="group1"><img src="images/site/Passo_2_figura_2_thumb.png"/></a></div>
  
  <div id="clique_ampliar"><a href="images/site/Passo_2_figura_1.jpg" class="group1">Clique para Ampliar</a></div>
  <div id="clique_ampliar_2"><a href="images/site/Passo_2_figura_2.jpg" class="group1">Clique para Ampliar</a></div>
  </div>
  
  <div id="dadosPasso3">
  	<div id="texto_passo_2">
    	<p>A próxima etapa é o cadastro
e alimentação de toda a
metodologia de análise da
Télios. Este processo é composto por 6 PASSOS.
</p>
<p>
Preencha com o máximo de informações possíveis, pois nestes passos que você fara a Gestão do seu Conhecimento e trará subsídios para a solução de seus problemas.</p>
    </div>
    <div id="img_clic_1"><a href="images/site/Passo_3_Figura_1.jpg" class="group1"><img src="images/site/Passo_3_Figura_1_thumb.png"/></a></div>
    
    <div id="clique_ampliar"><a href="images/site/Passo_3_Figura_1.jpg" class="group1">Clique para Ampliar</a></div>
    <div id="texto_passo_3"><p>Ao final deste processo você terá disponibilidade de gerar um relatório em PDF com todas as informações da análise.</p></div>
  </div>
  
  <div class="clear"></div>
  <div id="textoCenter"><p class="fonte14Cinza">OBS:É importante que todos os dados sejam preenchidos para melhor situar a sua análise dentro de sua empresa.</p></div>
  
  <div id="nomePasso"><img src="images/site/A-FERRAMENTA_52.jpg" width="128" height="127"/></div>
  <div id="gerenciamento">
  <div id="texto_analise">
  	<p>Todas as suas ANÁLISES ficarão disponíveis na página inicial do Sistema Télios. Poderão ser acessadas pelos filtros de busca.</p>
  </div>
  <div id="acoes_ferramentas">
  	<p>Todas as suas AÇÕES DE BLOQUEIO ficarão disponíveis na aba com este nome. Neste
local você poderá fazer o acompanhamento
do andamento, fazendo novas anotações e
acionando a equipe responsável.</p>
  </div>
  	 <div id="img_clic_3"><a href="images/site/Analise_de_falhas_Figura_1.jpg" class="group1"><img src="images/site/Analise_de_falhas_Figura_1_thumb.jpg"/></a></div>
     <div id="img_clic_4"><a href="images/site/Acoes_de_Bloqueio_Figura_1.jpg" class="group1"><img src="images/site/Acoes_de_Bloqueio_Figura_1_thumb.jpg"/></a></div>
     
     <div id="clique_ampliar_3"><a href="images/site/Analise_de_falhas_Figura_1.jpg" class="group1">Clique para Ampliar</a></div>
  <div id="clique_ampliar_4"><a href="images/site/Acoes_de_Bloqueio_Figura_1.jpg" class="group1">Clique para Ampliar</a></div>
  </div>
  <div class="clear"></div>
  </div>
  
  </div>
  <div id="rodape">
  	<div id="rodape_interno">
    	<a href="http://www.agenciagh.com.br" title="Agência GH" target="_blank"><div id="marca_gh"></div></a>
        <div id="menu_inferior">
        	<div class="email_contato"><span class="negrito">E-mail:</span><a href="mailto:contato@telios.eng.br" class="group2">contato@telios.eng.br</a></div>
            
            <div class="menu_inferior_link"><a href="contato.php" title="CONTATO" class="group2">Contato</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="busca_publicadas.php" title="BUSCA ANÁLISE" class="group2">Busca Análises</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="estatisticas.php" title="RANKING DO CONHECIMENTO" class="group2">Ranking do Conhecimento</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="artigos.php" title="ARTIGOS" class="group2">Artigos</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="rca.php" title="RCA" class="group2">RCA</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="vantagens.php" title="VANTAGENS" class="group2">Vantagens</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="ferramenta.php" title="A FERRAMENTA" class="group2">A Ferramenta</a></div>
        </div>
        
        <div class="clear"></div>
    </div>
  </div>
</div>

<!--<form id="enviaLogin" name="enviaLogin" method="post" action="logado.php" >
  <table cellspacing="0" id="tabGeral">
    <tr>
      <td align="right" valign="middle">Usuário:</td>
      <td><input name="login" type="text" id="login" size="30"/></td>
    </tr>
    <tr>
      <td align="right" valign="middle">Senha:</td>
      <td><input name="senha" type="password" id="senha" size="30"/></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><a href="#" onclick="recuperaSenha('senha/recuperaSenha.php');">Esqueci Minha Senha.</a></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input name="envia" type="submit" id="envia" value="Logar" class="btnLogar"/></td>
    </tr>
  </table>
</form>-->
<?php }?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29818024-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>