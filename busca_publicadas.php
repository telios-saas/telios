<?php session_start(); 
require('conexao.php');
conexao();
if($_SERVER['REQUEST_METHOD']=='POST'){
	$busca = mysql_escape_string(addslashes($_POST['busca_analise']));
} else {
	$busca = "";	
}

if($busca != ""){
	
	
	// OR modo_falha LIKE '%$busca%', OR familia_equipamento LIKE '%$busca%', OR solucao LIKE '%$busca%'
	
	$select_busca = "SELECT d.*, u.*, u.nome AS unidade_fabril FROM dados_basico_analise AS d INNER JOIN unidade_fabril AS u ON u.id_unidade = d.id_unidade WHERE d.id_situacao = 4 AND (d.titulo LIKE '%$busca%' OR d.equipamento LIKE '%$busca%' OR d.linha_producao LIKE '%$busca%' OR u.nome LIKE '%$busca%' OR d.descricao_evento LIKE '%$busca%' OR d.evidencias LIKE '%$busca%' OR d.causa_basica LIKE '%$busca%' OR d.componente_falhou LIKE '%$busca%' OR d.causa_raiz LIKE '%$busca%' OR d.observacoes LIKE '%$busca%' OR d.modo_falha LIKE '%$busca%' OR d.familia_equipamento LIKE '%$busca%' OR d.solucao LIKE '%$busca%') ORDER BY d.titulo ASC ";
	$query_busca = mysql_query($select_busca, $base) or die(mysql_error()); 
	$total_resultados = mysql_num_rows($query_busca);
}else{
	$total_resultados = 0;
}
// Inicio a Sessão?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="análise de falhas, indústria, RCA - Root Cause Analisys, ações de bloqueio, causa raiz da falha" />
		<meta name="description" content="Realize análises de falhas de maneira descomplicada e contribua para a melhoria nos indicadores da indústria." />
		<meta name="rating" content="General" />
		<meta name="author" content="Agência GH" />
		<meta name="language" content="pt-br" />
		<meta name="ROBOTS" content="index,follow" />
		<title>TELIOS</title>
		<link href="../favicon.ico" rel="shortcut icon" />
		<link href="css/home.css" rel="stylesheet" type="text/css" media="screen" />
		<script language="javascript" src="js/post.js"></script>
		<script language="javascript" src="js/mascara.js"></script>
		<script type="text/javascript" src="swfobject/swfobject.js"></script>
		<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
		<script src="SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
		<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
        <link href="SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
        
        <link rel="stylesheet" href="css/colorbox.css" />
		
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.colorbox.js"></script>
		
		<script>
			$(document).ready(function(){
				$(".iframe").colorbox({iframe:true, width:"600px", height:"300px"});
				$(".iframe2").colorbox({iframe:true, width:"750px", height:"550px"});
			});
		</script>
        
</head>
<body>
<div id="tudo">
    <div id="topo">
        <div id="topoInterno">
              <div id="linkHome"><a href="index.php"><img src="images/site/linkHome.gif" width="192" height="41" /></a></div>
              <a href="ferramenta.php"><div id="linkFerramenta"></div></a>
              <a href="rca.php"><div id="linkRca"></div></a>
      		  <a href="vantagens.php"><div id="linkVantagens"></div></a>
      		  <a href="artigos.php"><div id="linkArtigos"></div></a>
	  <a href="estatisticas.php" title="Estatísticas"><div id="linkEstatisticas"></div></a>
              <a href="busca_publicadas.php"><div id="linkBusca"></div></a>
              <a href="contato.php"><div id="linkContato"></div></a>
              <div id="formLogin">
        <div id="formLoginInterna">
                  <form id="log" name="log" method="post" action="logado.php" >
            <div id="inputFormLogin">
                      <input name="login" type="text" id="login"/>
                    </div>
            <div id="inputFormSenha">
                      <input name="senha" type="password" id="senha"/>
                    </div>
            <div id="inputFormEntrar">
                      <input name="envia" type="submit" id="envia" class="btnLogar" value=""/>
                    </div>
          </form>
                  <?php if(isset($_GET['deslogado'])){?>
                  <div id="erroLogin">Login ou Senha Inválido</div>
                  <?php }?>
<div id="esqueceu_senha"><a href="esqueceu_senha.php" class="group2 iframe">Esqueceu sua senha ? </a></div>
                </div>
      </div>
            </div>
    </div>
    <div id="conteudo2">
    	<div id="busca_analise_int">
        	<p class="fonte28">Busca análise de falha</p>
            
            <form id="form1" name="form1" method="post" action="busca_publicadas.php">
                
                <div id="campo_busca">
                
                    <span id="sprytextfield1">
                    
                        <label for="busca_analise"></label>
                        
                        <input type="text" name="busca_analise" id="busca_analise" />
                        
                        <span class="textfieldRequiredMsg">
        
                            <div class="aviso_busca">Campo Obrigatório</div>
                        
                        </span>
                    
                    </span>
                
                </div>
                
                <div id="envia_busca_int"><input type="submit" name="envia_busca2" id="envia_busca2" value="" /></div>
                
                <div class="clear"></div>
                
            </form>
            
          	<?php if($_SERVER['REQUEST_METHOD']=='POST'){?>
         
          		<div id="resultados"><?php echo $total_resultados;?> resultado(s) encontrado(s) para " <?php echo $busca;?> "</div>
           
			<?php }?>
           
           
            <?php if($total_resultados > 0){?>
            <div id="tabela_busca">
                <table width="100%" cellspacing="0" id="tab_busca">
                    <tr>
                        <td bgcolor="#bfc7d7"><span class="fonte_16">Título da Análise</span></td>
                        <td colspan="2" bgcolor="#bfc7d7"><span class="fonte_16">Opções</span></td>
                    </tr>
                    <?php while($reg_busca = mysql_fetch_assoc($query_busca)){
						$apontador_1 = $reg_busca['quem_cadastro'];
						$select_autor = "SELECT * FROM  usuarios WHERE id_usuario = '$apontador_1'";
						$query_autor = mysql_query($select_autor, $base) or die(mysql_error());
						$reg_autor = mysql_fetch_assoc($query_autor);
						?>
                    <tr>
                        <td>
                        	<p><span class="fonte14Azul">Análise "<?php echo $reg_busca['titulo'];?>"</span></p>
                        	<p><span class="negrito">Autor: </span><?php echo $reg_autor['nome'];?> . <span class="negrito">Familia Equipamento:</span> <?php echo $reg_busca['familia_equipamento'];?>. <span class="negrito">Causa Raiz:</span> <?php echo $reg_busca['causa_raiz'];?>. </p>
                            <p><span class="negrito">Soluções Encontradas: </span> <?php echo $reg_busca['solucao'];?></p>
                            
                        </td>
                        <td width="24"><a href="geraPDF2.php?id_analise=<?php echo $reg_busca['id_analise'];?>&amp;apontador=<?php echo $reg_busca['apontador'];?>&amp;autor=<?php echo $reg_autor['nome'];?>" title="Gerar PDF" target="_blank"><img src="images/site/imprimir.png" width="24" height="23" /></a></td>
                        <td width="24"><a href="envia_email_publico.php?id_analise=<?php echo $reg_busca['id_analise'];?>&amp;apontador=<?php echo $apontador_1;?>" class="iframe2" title="Entre em Contato com o Autor"><img src="images/site/comentario.png" width="24" height="23" /></a></td>
                    </tr>
                    
                    <?php }?>
                </table>
            </div>
            <?php }?>
        </div>   
    </div>
    <div id="rodape">
  	<div id="rodape_interno">
    	<a href="http://www.agenciagh.com.br" title="Agência GH" target="_blank"><div id="marca_gh"></div></a>
        <div id="menu_inferior">
        	<div class="email_contato"><span class="negrito">E-mail:</span><a href="mailto:contato@telios.eng.br" class="group2">contato@telios.eng.br</a></div>
            
            <div class="menu_inferior_link"><a href="contato.php" title="CONTATO" class="group2">Contato</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="busca_publicadas.php" title="BUSCA ANÁLISE" class="group2">Busca Análises</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="estatisticas.php" title="RANKING DO CONHECIMENTO" class="group2">Ranking do Conhecimento</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="artigos.php" title="ARTIGOS" class="group2">Artigos</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="rca.php" title="RCA" class="group2">RCA</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="vantagens.php" title="VANTAGENS" class="group2">Vantagens</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="ferramenta.php" title="A FERRAMENTA" class="group2">A Ferramenta</a></div>
        </div>
        
        <div class="clear"></div>
    </div>
  </div>
</div>
<script type="text/javascript">
var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29818024-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
</script>
</body>
</html>