<?php
require_once("../helper_functions.php");
session_start(); // inicio a sessão

if($_SESSION['usuario']){ // verifico se usuario esta logado
    require("../conexao.php");
    conexao();
    
    $situacao_sql = "SELECT id_situacao, descricao FROM situacao WHERE status = 'A' AND id_situacao <= 5 order by id_situacao ASC";
    $situacao_exe = mysql_query($situacao_sql, $base) or die(mysql_error());

    while ($row = mysql_fetch_assoc($situacao_exe)) {
        $grafico[$row['id_situacao']] = new stdClass();
        $grafico[$row['id_situacao']]->descricao = $row['descricao'];
        $grafico[$row['id_situacao']]->valor     = 0;
    }

    $sql = 'SELECT * FROM v_gerenciar_analises WHERE 1=1';

    $sql .= (empty($_POST['unidade_fabril']))    ? NULL : ' AND unidade_id = '.$_POST['unidade_fabril'];
    $sql .= (empty($_POST['linha_producao']))    ? NULL : ' AND linha_producao_descricao = \''.mysql_real_escape_string($_POST['linha_producao']).'\'';
    $sql .= (empty($_POST['equipamento']))       ? NULL : ' AND equipamento_descricao = \''.mysql_real_escape_string($_POST['equipamento']).'\'';
    $sql .= (empty($_POST['situacao']))          ? NULL : ' AND situacao_id = '.$_POST['situacao'];
    $sql .= (empty($_POST['prioridade']))        ? NULL : ' AND prioridade = \''.$_POST['prioridade'].'\'';
    $sql .= (empty($_POST['titulo']))            ? NULL : ' AND titulo like \'%'.$_POST['titulo'].'%\'';
    $sql .= (empty($_SESSION['apontador']))      ? NULL : ' AND apontador = '.$_SESSION['apontador'];

    $exe = mysql_query($sql, $base) or die(mysql_error());
    $r['qtd'] = mysql_num_rows($exe);    
    while ($row = mysql_fetch_assoc($exe)) {
        $r['res'][] = $row;
        
        $grafico[$row['situacao_id']]->valor += 1;
    }
    
    foreach ($grafico as $k=>$v){
        $r['grafico'][] = array($v->descricao, $v->valor);
    }
    
    echo json_encode($r);
}