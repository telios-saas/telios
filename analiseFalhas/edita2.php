<?php 
session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado
require("../conexao.php");
require("funcao.php");
conexao();

$id_usuario = $_SESSION['id_usuario'];

// Dados Via Formulario
$titulo         = mysql_escape_string(addslashes($_POST['titulo']));
$id_analise     = mysql_escape_string(addslashes($_GET['id']));
$situacao       = mysql_escape_string(addslashes($_POST['situacao']));
$equipamento    = mysql_escape_string(addslashes($_POST['equipamento']));
$linha_producao = mysql_escape_string(addslashes($_POST['linha_producao']));
$unidade_fabril = mysql_escape_string(addslashes($_POST['id_unidade']));	
$data_evento    = mysql_escape_string(addslashes($_POST['data_evento']));
$num_sap        = mysql_escape_string(addslashes($_POST['num_sap']));   

$select_equipamento = "SELECT * FROM equipamento WHERE nome = '$equipamento'";
$query_equipamento = mysql_query($select_equipamento, $base) or die(mysql_error());
$reg_equipamento = mysql_fetch_assoc($query_equipamento);
$familia_equipamento = $reg_equipamento['familia'];



$select_basico1 = "SELECT * FROM dados_basico_analise WHERE id_analise = '$id_analise'";
$query_basico1 = mysql_query($select_basico1, $base) or die(mysql_error());
$reg_basico1 = mysql_fetch_assoc($query_basico1);
$quem_cadastro = $reg_basico1['quem_cadastro'];

$select_pontuacao1 = "SELECT * FROM usuarios WHERE id_usuario = '$quem_cadastro'";
$query_pontuacao1 = mysql_query($select_pontuacao1, $base) or die(mysql_error());
$reg_pontuacao1 = mysql_fetch_assoc($query_pontuacao1);
$pontos = $reg_pontuacao1['pontuacao'];

$pontuacao_geral = calcular_pontos_por_situacao($reg_basico1['id_situacao'],$situacao,$pontos);

$update_pontuacao = "UPDATE usuarios SET pontuacao = '$pontuacao_geral' WHERE id_usuario = '$quem_cadastro'";
$executa_pontuacao = mysql_query($update_pontuacao, $base) or die(mysql_error());


// Fim Dados

$update_analise = "UPDATE dados_basico_analise SET titulo = '$titulo', id_situacao = '$situacao', equipamento = '$equipamento', linha_producao = '$linha_producao', id_unidade = '$unidade_fabril' , familia_equipamento = '$familia_equipamento', num_sap = '$num_sap', data = '$data_evento'  WHERE id_analise = '$id_analise'";
$executa_update = mysql_query($update_analise, $base) or die(mysql_error());

$update_analise2 = "UPDATE analise_falhas SET titulo = '$titulo' WHERE id_analise = '$id_analise'";
$executa_update2 = mysql_query($update_analise2, $base) or die(mysql_error());

$select_analise = "SELECT * FROM analise_falhas WHERE id_analise = '$id_analise'";
$query_analise = mysql_query($select_analise, $base) or die(mysql_error());
$reg_analise = mysql_fetch_assoc($query_analise);
$criticidade = $reg_analise['prioridade'];


$select_basico = "SELECT d.*, u.*, u.nome AS unidade_fabril, s.descricao as situacao FROM dados_basico_analise AS d INNER JOIN unidade_fabril AS u ON u.id_unidade = d.id_unidade left join situacao as s on d.id_situacao = s.id_situacao WHERE id_analise = '$id_analise'";
$query_basico = mysql_query($select_basico, $base) or die(mysql_error());
$reg_basico = mysql_fetch_assoc($query_basico);


?>
<div id="internaTopDados"></div>
<div id="internaInterDados">
<div id="falhaMenu" class="fonte14Negrito">FALHA</div>
 <?php if($criticidade == "A"){?>
        <div id="criticidadeVermelha"><img src="images/PriA.png" width="128" height="31" /></div>
        <?php }?>
        <?php if($criticidade == "B"){?>
        <div id="criticidadeVermelha"><img src="images/PriB.png" width="128" height="31" /></div>
        <?php }?>
        <?php if($criticidade == "C"){?>
        <div id="criticidadeVermelha"><img src="images/PriC.png" width="128" height="32" /></div>
        <?php }?>
        <?php if($criticidade == "D"){?>
        <div id="criticidadeVermelha"><img src="images/PriD.png" width="128" height="32" /></div>
        <?php }?>
        <?php if($criticidade == "E"){?>
        <div id="criticidadeVermelha"><img src="images/PriE.png" width="128" height="32" /></div>
        <?php }?>
<div class="clear"></div>
<div id="monstraTituloAnalise"><?php echo $reg_basico['titulo'];?></div>
<p><span class="fonte14">Unidade Fabril</span><br>
  <span class="fonte14Negrito"><?php echo $reg_basico['unidade_fabril'];?></span></p>
<p>
<div class="linhaPassos"></div>
</p>

<p><span class="fonte14">N&deg;. Ordem Serviço:</span><br>
  <span class="fonte14Negrito"><?php echo $reg_basico['num_sap'];?></span></p>
<p>
<div class="linhaPassos"></div>
</p>

<p><span class="fonte14">Linha de Produção</span><br>
  <span class="fonte14Negrito"><?php echo $reg_basico['linha_producao'];?></span></p>
<p>
<div class="linhaPassos"></div>
</p>
<p><span class="fonte14">Equipamento</span><br>
  <span class="fonte14Negrito"><?php echo $reg_basico['equipamento'];?></span></p>
<p>
<div class="linhaPassos"></div>
</p>
<p><span class="fonte14">Data do Evento</span><br>
  <span class="fonte14Negrito"><?php echo $reg_basico['data'];?></span></p>
<p>
<div class="linhaPassos"></div>
</p>
<p><span class="fonte14">Situação</span><br>
  <span class="fonte14Negrito"><?php echo $reg_basico['situacao'];?></span></p>
  <div id="btnAlterarAnalise"> <a href="#" onclick="menuLat('analiseFalhas/edita1.php?id=<?php echo $id_analise;?>');">
        <img src="images/Editar_dados.png" width="128" height="26" /> </a>
          <div class="clear"></div>
        </div>
</div>
</div>
<div id="rodapeInternaDados"></div>
<?php } else { // se usuário não estiver logado?>
<script language="JavaScript">
	window.location.href = "http://www.telios.eng.br/index.php?deslogado=erro";
</script>
<?php }?>
