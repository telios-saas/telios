<?php 
session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado
require("../conexao.php");
conexao();
$apontador = $_SESSION['apontador'];
$id_analise = $_GET['id'];
$acao = $_GET['acao'];

if($acao == 2){ // Salva os dados
  // Dados Via Formulario
  $saude_seguranca = $_POST['saude_seguranca'];
  $meio_ambiente = $_POST['meio_ambiente'];
  $producao = $_POST['producao'];
  $custo_manutencao = $_POST['custo_manutencao'];
  $frequencia_evento = $_POST['frequencia_evento'];

  // Fim Dados
  $risco = ($saude_seguranca + $meio_ambiente + $producao + $custo_manutencao)*$frequencia_evento;// CALCULA O RISCO 
  ### Calculco Prioridade da Analise ##########
    if ($risco <  500 ){ // Risco menor q 500 
      if ($saude_seguranca < 30) {// Se a Saude e segurança for menor q 30
        if ($meio_ambiente < 30){ // Se Meio Ambiente for menor q 30
          if ($producao < 30){ // Se Produção for menor que 30
            if ($risco <=  100 ){ // Se risco menor igual á 100
              if ($saude_seguranca < 20){ // Se Saude e Segurança menor q 20
                if ($meio_ambiente < 20){ // Se Meio Ambiente menor q 20
                  if ($producao < 20){ // Se produção menor que 20
                    if ($risco <= 40){ // Se Risco for menor igual á 40
                      if ($risco < 29){ // Se risco for menor que 29 
                        $criticidade = "E";
                      } else { // Se risco for maior que 29 e menor igual á 40
                        $criticidade = "D";
                      } 
                    } // Fim Risco menor igual á 40
                    else { // Se risco for maior que 40 e menor igual á 100
                      $criticidade = "C";
                    }
                  } else {// Se produção maior que 20
                    $criticidade = "B";
                  }
              } else { // Se Meio Ambiente Maior que 20
                $criticidade = "B";
              }
            } else { // Se Saude Maior que 20
              $criticidade = "B";
            }
          } else {// Se risco maior que 100
            $criticidade = "B";
          }
        } else {// Se Produção Maior que 30
          $criticidade = "A";
        }
      } else {// Se Meio Ambiente Maior que 30
        $criticidade = "A";
      }
      }else {// Se Saude Segurança Maior que 30
        $criticidade = "A";
      }
    } else {// Se soma for maior que 500 
      $criticidade = "A";
    }

  $update_priorizacao = "UPDATE priorizacao_falhas SET saude_seguranca = '$saude_seguranca', meio_ambiente = '$meio_ambiente', producao = '$producao', custo_manutencao = '$custo_manutencao', frequencia_evento = '$frequencia_evento' WHERE id_analise = '$id_analise'";
  $executa_update_priorizacao = mysql_query($update_priorizacao, $base) or die(mysql_error());

  // Faço UPDATE na Tabela Analise para Atualizar a Prioridade 
  $update_analise = "UPDATE analise_falhas SET prioridade = '$criticidade' WHERE id_analise = '$id_analise'";
  $executa_update_analise = mysql_query($update_analise, $base) or die(mysql_error());

  $update_dados_basicos = "UPDATE dados_basico_analise SET prioridade = '$criticidade' WHERE id_analise = '$id_analise'";
  $executa_update_dados_basicos = mysql_query($update_dados_basicos, $base) or die(mysql_error());

}

// Seleciono os dados basico
$select_basico = "SELECT d.*, u.*, u.nome AS unidade_fabril, s.descricao as situacao FROM dados_basico_analise AS d INNER JOIN unidade_fabril AS u ON u.id_unidade = d.id_unidade left join situacao as s on d.id_situacao = s.id_situacao WHERE id_analise = '$id_analise'";
$query_basico = mysql_query($select_basico, $base) or die(mysql_error());
$reg_basico = mysql_fetch_assoc($query_basico);

$select_analise = "SELECT * FROM analise_falhas WHERE id_analise = '$id_analise'";
$query_analise = mysql_query($select_analise, $base) or die(mysql_error());
$reg_analise = mysql_fetch_assoc($query_analise);
$criticidade = $reg_analise['prioridade'];


// Select na Priorização
$select_priorizacao = "SELECT * FROM priorizacao_falhas WHERE id_analise = '$id_analise'";
$query_priorizacao = mysql_query($select_priorizacao, $base) or die(mysql_error());
$reg_priorizacao = mysql_fetch_assoc($query_priorizacao);

?>
<?php 
    $menuAtivo = 'analise2';
    include '../menu_top.php'; 
?>
<!-- FIM MENU TOP -->
<div id="geral">
  <div id="menuLateralPassos">
    <div id="internaMenuPassos">
      <div id="internaTopDados"></div>
      <div id="internaInterDados">
        <div id="falhaMenu" class="fonte14Negrito">FALHA</div>
        <?php if($criticidade == "A"){?>
        <div id="criticidadeVermelha"><img src="images/PriA.png" width="128" height="31" /></div>
        <?php }?>
        <?php if($criticidade == "B"){?>
        <div id="criticidadeVermelha"><img src="images/PriB.png" width="128" height="31" /></div>
        <?php }?>
        <?php if($criticidade == "C"){?>
        <div id="criticidadeVermelha"><img src="images/PriC.png" width="128" height="32" /></div>
        <?php }?>
        <?php if($criticidade == "D"){?>
        <div id="criticidadeVermelha"><img src="images/PriD.png" width="128" height="32" /></div>
        <?php }?>
        <?php if($criticidade == "E"){?>
        <div id="criticidadeVermelha"><img src="images/PriE.png" width="128" height="32" /></div>
        <?php }?>
        <div class="clear"></div>
        <div id="monstraTituloAnalise"><?php echo $reg_basico['titulo'];?></div>
        <p><span class="fonte14">Unidade Fabril</span><br>
          <span class="fonte14Negrito"><?php echo $reg_basico['unidade_fabril'];?></span></p>
        <p>
        <div class="linhaPassos"></div>
        </p>

        <p><span class="fonte14">N&deg;. Ordem Serviço:</span><br>
        <span class="fonte14Negrito"><?php echo $reg_basico['num_sap'];?></span></p>
        <p>
        <div class="linhaPassos"></div>
        </p>

        <p><span class="fonte14">Área</span><br>
          <span class="fonte14Negrito"><?php echo $reg_basico['linha_producao'];?></span></p>
        <p>
        <div class="linhaPassos"></div>
        </p>
        <p><span class="fonte14">Função no Processo</span><br>
          <span class="fonte14Negrito"><?php echo $reg_basico['equipamento'];?></span></p>
        <p>
        <div class="linhaPassos"></div>
        </p>
        <p><span class="fonte14">Data do Evento</span><br>
          <span class="fonte14Negrito"><?php echo $reg_basico['data'];?></span></p>
        <p>
        <div class="linhaPassos"></div>
        </p>
        <p><span class="fonte14">Situação</span><br>
          <span class="fonte14Negrito"><?php echo $reg_basico['situacao'];?></span></p>
        

              <div id="btnAlterarAnalise"> 
                <a href="#" onclick="menuLat('analiseFalhas/edita1.php?id=<?php echo $id_analise;?>');">
                 <img src="images/Editar_dados.png" width="128" height="26" /> 
                </a>
                <div class="clear"></div>
              </div>

          
      </div>
      <div id="rodapeInternaDados"></div>
    </div>
  </div>
  <div id="formAnaliseDir">
    <div id="passo_passo"> <a href="#" onclick="passo_link('analiseFalhas/edita_passo1.php?id=<?php echo $id_analise?>&amp;acao=1');" class="btnPassoLink">
      <div id="passo1Ativo"> 
        <!--<p><span class="fonte15Branca">Passo 1 &raquo;</span></p>
      <p><span class="branca">Análise da Prioridade</span></p>--> 
      </div>
      </a> <a href="#" onclick="passo_link('analiseFalhas/edita_passo2.php?id=<?php echo $id_analise?>&amp;acao=1');" class="btnPassoLink">
      <div id="passo2"> 
        <!-- <p><span class="fonte15Passo">Passo 2 &raquo;</span></p>
      <p><span class="cinza">Informações Básicas</span></p>--> 
      </div>
      </a> <a href="#" onclick="menu('analiseFalhas/edita_passo3.php?id=<?php echo $id_analise?>&amp;acao=1');" class="btnPassoLink">
      <div id="passo3"> 
        <!--<p><span class="fonte15Passo">Passo 3 &raquo;</span></p>
      <p><span class="cinza">Diagrama Causa e Efeito</span></p>--> 
      </div>
      </a> <a href="#" onclick="menu('analiseFalhas/edita_passo5.php?id=<?php echo $id_analise?>&amp;acao=5');" class="btnPassoLink">
      <div id="passo4"> 
        <!-- <p><span class="fonte15Passo">Passo 4 &raquo;</span></p>
      <p><span class="cinza">5 Porquês</span></p>--> 
      </div>
      </a> <a href="#" onclick="menu('analiseFalhas/edita_passo6.php?id=<?php echo $id_analise?>&amp;acao=1');" class="btnPassoLink">
      <div id="passo5"> 
        <!--<p><span class="fonte15Passo">Passo 5 &raquo;</span></p>
      <p><span class="cinza">Ações</span></p>--> 
      </div>
      </a> </div>
    <div class="clear"></div>
    <div id="dadosExplicativos">
      <div id="chamadaPagina"><span class="fonte37">Análise da Prioridade</span><a href="#" class="dcontexto"><span>
        
        <p>Requisito básico para o cadastramento de um evento, esta etapa cria uma regra para definir qual a ordem de
          prioridade dentre todas as nossas demandas de estudo.</p>
        <p>Se o impacto for elevado (por exemplo: um acidente com afastamento, um incêndio de difícil combate ou uma grande perda por lucro cessante) o evento terá prioridade 'A', mesmo que a sua frequência seja baixa. Os eventos de baixo impacto têm sua prioridade dada pela soma dos impactos multiplicado pela frequência que ocorrem.</p>
        </span><img src="images/AjudaG.gif" width="16" height="16" class="btnDuvida"/></a></div>
       
    </div>
    <div id="alertaDeletaAnalise">
    <form id="enviaDados" name="enviaDados" method="post" action="#" >
      <div id="formAnalise">
        <div id="formTopAnalise"></div>
        <div id="internoForm">
          <p>Ao preencher as informações, considerar o impacto do evento e a possibilidade de impactos maiores.</p>
          <table id="tabGeral">
            <tr>
              <td align="right">Saúde e Segurança:</td>
              <td><select name="saude_seguranca" id="saude_seguranca">
                  <?php $saude_seg = $reg_priorizacao['saude_seguranca'];?>
                  <?php if($saude_seg == 40){?>
                  <option value='40'>Acidente Fatal</option>
                  <?php }?>
                  <?php if($saude_seg == 30){?>
                  <option value='30'>Acidente Com Afastamento</option>
                  <?php }?>
                  <?php if($saude_seg == 20){?>
                  <option value='20' >Acidente Sem Afastamento</option>
                  <?php }?>
                  <?php if($saude_seg == 10){?>
                  <option value='10'>Ocorrencia Ambulatorial</option>
                  <?php }?>
                  <?php if($saude_seg == 0){?>
                  <option value='0'>Sem impactos</option>
                  <?php }?>
                   <?php if($saude_seg != 40){?><option value='40'>Acidente Fatal</option><?php }?>
                   <?php if($saude_seg != 30){?><option value='30'>Acidente Com Afastamento</option><?php }?>
                   <?php if($saude_seg != 20){?><option value='20' >Acidente Sem Afastamento</option><?php }?>
                   <?php if($saude_seg != 10){?><option value='10'>Ocorrencia Ambulatorial</option><?php }?>
                   <?php if($saude_seg != 0){?><option value='0'>Sem impactos</option><?php }?>
                </select></td>
            </tr>
            <tr>
              <td align="right">Meio Ambiente:</td>
              <td><select name="meio_ambiente" id="meio_ambiente">
                  <?php $meio_am = $reg_priorizacao['meio_ambiente'];?>
                  <?php if($meio_am == 40){?>
                  <option value='40'>Dificil combate, externa a area da empresa</option>
                  <?php }?>
                  <?php if($meio_am == 30){?>
                  <option value='30'>Dificil combate, restrita a area da empresa</option>
                  <?php }?>
                  <?php if($meio_am == 20){?>
                  <option value='20'>Combativel com auxilio externo</option>
                  <?php }?>
                  <?php if($meio_am == 10){?>
                  <option value='10'>Combativel com recursos proprios</option>
                  <?php }?>
                  <?php if($meio_am == 0){?>
                  <option value='0'>Sem impactos</option>
                  <?php }?>
                    <?php if($meio_am != 40){?><option value='40'>Dificil combate, externa a area da empresa</option><?php }?>
                    <?php if($meio_am != 30){?><option value='30'>Dificil combate, restrita a area da empresa</option><?php }?>
                    <?php if($meio_am != 20){?><option value='20'>Combativel com auxilio externo</option><?php }?>
                    <?php if($meio_am != 10){?><option value='10'>Combativel com recursos proprios</option><?php }?>
                    <?php if($meio_am != 0){?><option value='0'>Sem impactos</option><?php }?>
                </select></td>
            </tr>
            <tr>
              <td align="right">Produção:</td>
              <td><select name="producao" id="producao">
                  <?php $producao_2 = $reg_priorizacao['producao'];?>
                  <?php if($producao_2 == 30){?>
                  <option value='30'>Grande perda por lucro cessante</option>
                  <?php }?>
                  <?php if($producao_2 == 20){?>
                  <option value='20'>Consider&aacute;vel perda de por lucro cessante</option>
                  <?php }?>
                  <?php if($producao_2 == 10){?>
                  <option value='10'>Pequena perda por lucro cessante</option>
                  <?php }?>
                  <?php if($producao_2 == 0){?>
                  <option value='0'>Sem perdas por lucro cessante</option>
                  <?php }?>
                  <?php if($producao_2 != 30){?><option value='30'>Grande perda por lucro cessante</option><?php }?>
                  <?php if($producao_2 != 20){?><option value='20'>Consider&aacute;vel perda de por lucro cessante</option><?php }?>
                  <?php if($producao_2 != 10){?><option value='10'>Pequena perda por lucro cessante</option><?php }?>
                  <?php if($producao_2 != 0){?><option value='0'>Sem perdas por lucro cessante</option><?php }?>
                </select></td>
            </tr>
            <tr>
              <td align="right">Custo de Manutenção</td>
              <td><select name="custo_manutencao" id="custo_manutencao">
                  <?php $custo_manu = $reg_priorizacao['custo_manutencao'];?>
                  <?php if($custo_manu == 20){?>
                  <option value='20'>Alto</option>
                  <?php }?>
                  <?php if($custo_manu == 10){?>
                  <option value='10'>Medio</option>
                  <?php }?>
                  <?php if($custo_manu == 5){?>
                  <option value='5'>Baixo</option>
                  <?php }?>
                  <?php if($custo_manu == 0){?>
                  <option value='0'>Sem custo</option>
                  <?php }?>
                  <?php if($custo_manu != 20){?><option value='20'>Alto</option><?php }?>
                  <?php if($custo_manu != 10){?><option value='10'>Medio</option><?php }?>
                  <?php if($custo_manu != 5){?><option value='5'>Baixo</option><?php }?>
                  <?php if($custo_manu != 0){?><option value='0'>Sem custo</option><?php } ?>
                </select></td>
            </tr>
            <tr>
              <td align="right">Frequência do Evento:</td>
              <td><select name="frequencia_evento" id="frequencia_evento">
                  <?php $frequencia_even = $reg_priorizacao['frequencia_evento'];?>
                  <?php if($frequencia_even == 10){?>
                  <option value='10'>Varios eventos em menos de um mês </option>
                  <?php }?>
                  <?php if($frequencia_even == 8){?>
                  <option value='8'>Um evento mensal</option>
                  <?php }?>
                  <?php if($frequencia_even == 6){?>
                  <option value='6'>Um evento trimenstral</option>
                  <?php }?>
                  <?php if($frequencia_even == 4){?>
                  <option value='4'>Um evento anual</option>
                  <?php }?>
                  <?php if($frequencia_even == 2){?>
                  <option value='2'>Um evento bianual</option>
                  <?php }?>
                  <?php if($frequencia_even == 1){?>
                  <option value='1'>Mais de 5 anos para cada evento</option>
                  <?php }?>
                  <?php if($frequencia_even != 10){?><option value='10'>Varios eventos em menos de um mês</option><?php }?>
                  <?php if($frequencia_even != 8){?><option value='8'>Um evento mensal</option><?php }?>
                  <?php if($frequencia_even != 6){?><option value='6'>Um evento trimenstral</option><?php }?>
                  <?php if($frequencia_even != 4){?><option value='4'>Um evento anual</option><?php }?>
                  <?php if($frequencia_even != 2){?><option value='2'>Um evento bianual</option><?php }?>
                  <?php if($frequencia_even != 1){?><option value='1'>Mais de 5 anos para cada evento</option><?php }?>
                </select></td>
            </tr>
          </table>
          <?php if($acao == 2){?>
          <div id="gravadoSucesso">Seus dados foram salvos com sucesso!</div>
          <?php }?>
        </div>
        <div id="rodapeFormAnalise"></div>
      </div>
      <p>
      
      <div id="btnsEsquerda">
        <?php
          if (($_SESSION['nivel'] == "1") || ($_SESSION['nivel'] == "3")) {
        ?>
            <input name="envia" type="button" id="envia" onclick="geral('enviaDados', 'analiseFalhas/edita_passo1.php?id=<?php echo $id_analise;?>&amp;acao=2', 'alterar');" value="" class="btnGravar"/>
        <?php
          }
        ?>
        <input name="envia" type="button" id="envia" onclick="geral('enviaDados', 'analiseFalhas/edita_passo2.php?id=<?php echo $id_analise;?>&amp;acao=3', 'alterar');" value="" class="btnProsseguir"/>
      </div>
      
        <div id="btnsDireita">
        <div id="btnRelatorio"><a href="geraPDF.php?id=<?php echo $id_analise;?>" target="_blank"><div class="btnRelatorio"></div></a></div>
        
  <?php if(($_SESSION['nivel'] == "1") || ($_SESSION['nivel'] == "3")) : ?> 
      <div id="btnExcluirAnalise"><a href="#" onclick="deleta_analise('analiseFalhas/deletaAnalise.php?id=<?php echo $id_analise;?>&amp;acao=1');"><div class="btnExcluiAnalise"></div></a></div>
        <?php endif; ?>
  
  <div class="clear"></div>
      </div>
        <div class="clear"></div>
      
      </p>
    </form>
    
    </div>
  </div>
  <div class="clear"></div>
</div>
<?php } else { // se usuário não estiver logado?>
<script language="JavaScript">
  window.location.href = "http://www.telios.eng.br/index.php?deslogado=erro";
</script>
<?php }?>
