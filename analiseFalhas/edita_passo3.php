<?php 
session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado
require("../conexao.php");
conexao();
$id_analise = $_GET['id'];
$acao = $_GET['acao'];
if($acao == 3){
	$data_avaliacao = $_POST['data_avaliacao'];
	$equipe_avaliacao = $_POST['equipe_avaliacao'];
	$descricao_evento = $_POST['descricao_evento'];
	$evidencias = $_POST['evidencias'];
	$componente_falhou = $_POST['componente_falhou'];
	$funcao = $_POST['funcao'];
	$modo_falha = $_POST['modo_falha'];
	$causa_raiz = $_POST['causa_raiz'];
	$causa_basica = $_POST['causa_basica'];
	$observacoes = $_POST['observacoes'];
	$solucao = $_POST['solucao'];
	
	$update_basico = "UPDATE dados_basico_analise SET data_avaliacao = '$data_avaliacao', equipe_avaliacao = '$equipe_avaliacao', descricao_evento = '$descricao_evento', evidencias = '$evidencias', componente_falhou = '$componente_falhou', funcao = '$funcao', modo_falha = '$modo_falha', causa_raiz = '$causa_raiz', observacoes = '$observacoes', causa_basica = '$causa_basica', solucao = '$solucao' WHERE id_analise = '$id_analise'";
	$executa_update = mysql_query($update_basico, $base) or die(mysql_error());
}
if($acao == 2){
	
	$maquina = $_POST['maquina'];
	$meio_ambiente = $_POST['meio_ambiente'];
	$metodo = $_POST['metodo'];
	$falha = $_POST['falha'];
	$medida = $_POST['medida'];
	$mao_obra = $_POST['mao_obra'];
	$materia_prima = $_POST['materia_prima'];
	
	$update_causa = "UPDATE causa_efeito SET maquina = '$maquina', meio_ambiente = '$meio_ambiente', metodo = '$metodo', falha = '$falha', medida = '$medida', mao_obra = '$mao_obra', materia_prima = '$materia_prima' WHERE id_analise = '$id_analise'";
	$executa_causa = mysql_query($update_causa, $base) or die(mysql_error());
}
// Select na Dados Diagrama
$select_diagrama = "SELECT * FROM causa_efeito WHERE id_analise = '$id_analise'";
$query_diagrama = mysql_query($select_diagrama, $base) or die(mysql_error());
$reg_diagrama = mysql_fetch_assoc($query_diagrama);

// Seleciono os dados basico
$select_basico = "SELECT d.* FROM dados_basico_analise  d WHERE id_analise = '$id_analise'";
$query_basico = mysql_query($select_basico, $base) or die(mysql_error());
$reg_basico = mysql_fetch_assoc($query_basico);
?>

<div id="passo_passo"> <a href="#" onclick="passo_link('analiseFalhas/edita_passo1.php?id=<?php echo $id_analise?>&amp;acao=1');" class="btnPassoLink">
  <div id="passo1"> </div>
  </a> <a href="#" onclick="passo_link('analiseFalhas/edita_passo2.php?id=<?php echo $id_analise?>&amp;acao=1');" class="btnPassoLink">
  <div id="passo2"> </div>
  </a> <a href="#" onclick="menu('analiseFalhas/edita_passo3.php?id=<?php echo $id_analise?>&amp;acao=1');" class="btnPassoLink">
  <div id="passo3Ativo"> </div>
  </a> <a href="#" onclick="menu('analiseFalhas/edita_passo5.php?id=<?php echo $id_analise?>&amp;acao=1');" class="btnPassoLink">
  <div id="passo4"> </div>
  </a> <a href="#" onclick="menu('analiseFalhas/edita_passo6.php?id=<?php echo $id_analise?>&amp;acao=1');" class="btnPassoLink">
  <div id="passo5"> </div>
  </a> </div>
<div class="clear"></div>
<div id="dadosExplicativos">
  <div id="chamadaPagina2"><span class="fonte37">Diagrama Causa e Efeito</span><a href="#" class="dcontexto"><span>
    <p>É uma forma de elencar as hipóteses segundo um processo organizado de brainstorming. Não se preocupe muito com onde elencar uma hipótese (ex. Máquina ou Método), mas sim que ela esteja lá. </p>
    <p>Inicie inserindo a Falha que está sendo investigada e prossiga pelos demais M's. </p>
    <p>Valide as hipóteses que foram elencadas e crie ações de bloqueio para elas. </p>
    </span><img src="images/AjudaG.gif" width="16" height="16" class="btnDuvida"/></a></div>
    
    
    <div id="link_5_pq"><a href="#" onclick="menu('analiseFalhas/edita_passo4.php?id=<?php echo $id_analise?>&amp;acao=1');" class="evidencia_diagramas">5 Porquês </a><a href="#" class="dcontexto" ><span>Se você deseja utilizar esta tecnica clique aqui.</span><img src="images/AjudaP.gif" width="12" height="12" /></a></div>
    
    <div class="clear"></div>
    
</div>
<form id="enviaDados" name="enviaDados" method="post" action="#" >
  <div id="formAnalise">
  <div id="formTopAnalise"></div>
        <div id="internoForm">
  
    <p>Diagrama de Causa e Efeito (Ishikawa, Espinha de Peixe ou 6M's).</p>
    
    <div id="causaEfeitoDiagrama">
    
      <div id="ajudaCausa1"><a href="#" class="dcontexto"><span>Situação dos equipamentos utilizados para realizar a função.</span><img src="images/AjudaP.gif" width="12" height="12" /></a></div>
      
      <div id="ajudaCausa2"><a href="#" class="dcontexto"><span>Características do ambiente (vibração, temperatura, atmosfera explosiva, etc). </span><img src="images/AjudaP.gif" width="12" height="12" /></a></div>
      
      <div id="ajudaCausa3"><a href="#" class="dcontexto"><span>A forma como o processo é realizado - se existem procedimentos ou se eles são adequados. </span><img src="images/AjudaP.gif" width="12" height="12" /></a></div>
      
      <div id="ajudaCausa4"><a href="#" class="dcontexto"><span>Erros na forma como o processo é medido. </span><img src="images/AjudaP.gif" width="12" height="12" /></a></div>
      
      <div id="ajudaCausa5"><a href="#" class="dcontexto"><span>O envolvimento das pessoas com a falha. </span><img src="images/AjudaP.gif" width="12" height="12" /></a></div>
      
      <div id="ajudaCausa6"><a href="#" class="dcontexto"><span>Falha nos insumos necessários para realizar o trabalho com sucesso. </span><img src="images/AjudaP.gif" width="12" height="12" /></a></div>
      
      <div id="inputCausa1">
        <textarea name="maquina" id="maquina"><?php echo $reg_diagrama['maquina'];?></textarea>
      </div>
      
      <div id="inputCausa2">
        <textarea name="meio_ambiente" id="meio_ambiente"><?php echo $reg_diagrama['meio_ambiente'];?></textarea>
      </div>
      
      <div id="inputCausa3">
        <textarea name="metodo" id="metodo"><?php echo $reg_diagrama['metodo'];?></textarea>
      </div>
      
      <div id="inputCausa4">
        <textarea name="falha" id="falha"><?php echo trim($reg_diagrama['falha'])=='' ? $reg_basico['titulo']:$reg_diagrama['falha'];?></textarea>
      </div>
      
      <div id="inputCausa5">
        <textarea name="medida" id="medida"><?php echo $reg_diagrama['medida'];?></textarea>
      </div>
     
      <div id="inputCausa6">
        <textarea name="mao_obra" id="maa_obra"><?php echo $reg_diagrama['mao_obra'];?></textarea>
      </div>
      
      <div id="inputCausa7">
        <textarea name="materia_prima" id="materia_prima"><?php echo $reg_diagrama['materia_prima'];?></textarea>
      </div>
      
    </div>
    
    
    <?php if($acao == 2){?>
    <div id="gravadoSucesso">Seus dados foram salvos com sucesso!</div>
    <?php }?>
  </div>
   <div id="rodapeFormAnalise"></div>
  </div>
  
  <p>
   <div id="btnsEsquerda">
    <input name="envia" type="button" id="envia" onclick="geral('enviaDados', 'analiseFalhas/edita_passo3.php?id=<?php echo $id_analise;?>&amp;acao=2', 'formAnaliseDir');" value="" class="btnGravar"/>
    <input name="envia" type="button" id="envia" onclick="geral('enviaDados', 'analiseFalhas/edita_passo5.php?id=<?php echo $id_analise;?>&amp;acao=2', 'formAnaliseDir');" value="" class="btnProsseguir"/>
    </div>
    
    <div id="btnsDireita">
         <div id="btnRelatorio"><a href="geraPDF.php?id=<?php echo $id_analise;?>" target="_blank"><div class="btnRelatorio"></div></a></div>
        <div id="btnExcluirAnalise"><a href="#" onclick="deleta_analise('analiseFalhas/deletaAnalise.php?id=<?php echo $id_analise?>&amp;acao=1');"><div class="btnExcluiAnalise"></div></a></div>
        <div class="clear"></div>
      </div>
     <div class="clear"></div>
  </p>
</form>








































<?php } else { // se usuário não estiver logado?>
<script language="JavaScript">
	window.location.href = "http://www.telios.eng.br/index.php?deslogado=erro";
</script>
<?php }?>
