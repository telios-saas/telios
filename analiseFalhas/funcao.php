<?php 
// C = Cadastrada = 5 
// P = Pendente = 15
// Con = Concluida = 25
// Pub = Publicada = 100 

// A var $status é o status atual da análise
// A var $status_2 é o status desejado que fique


function status($status){
	
	if($status == 1){
		return "C";
	}
	
	if($status == 2){
		return "P";
	}
	
	if($status == 3){
		return "Con";
	}
	
	if($status == 4){
		return "Pub";
	}
	
	if($status == 5){
		return "Can";
	}
	
}

function geral_pontos($status, $status_2, $pontos){
        //Em análise -> Pendente
    	if($status == "P" and $status_2 == "C"){
		$pontos = $pontos - 10;
		return $pontos;
	}
        //Em análise -> Concluida
        if($status == "P" and $status_2 == "Con"){
		$pontos = $pontos + 10;
		return $pontos;
	}
        //Em análise -> Publicada
	if($status == "P" and $status_2 == "Pub"){
		$pontos = $pontos + 85;
		return $pontos;
	}
        //Em análise -> Cancelada
	if($status == "P" and $status_2 == "Can"){
		$pontos = $pontos - 15;
		return $pontos;
	}
        
        //Pendente -> Em análise
	if($status == "C" and $status_2 == "P"){
		$pontos = $pontos + 10;
		return $pontos;
	}
	//Pendente -> Concluida
	if($status == "C" and $status_2 == "Con"){
		$pontos = $pontos + 20;
		return $pontos;
	}
        //Pendente -> Publicada
	if($status == "C" and $status_2 == "Pub"){
		$pontos = $pontos + 95;
		return $pontos;
	}
        //Pendente -> Cancelada
	if($status == "C" and $status_2 == "Can"){
		$pontos = $pontos - 5;
		return $pontos;
	}
	
        //Concluida -> Em análise
        if($status == "Con" and $status_2 == "P"){
		$pontos = $pontos - 10;
		return $pontos;
	}
        //Concluida -> Pendente
	if($status == "Con" and $status_2 == "C"){
		$pontos = $pontos - 20;
		return $pontos;
	}
        //Concluida -> Publicada
	if($status == "Con" and $status_2 == "Pub"){
		$pontos = $pontos + 75;
		return $pontos;
	}
        //Concluida -> Cancelada
	if($status == "Con" and $status_2 == "Can"){
		$pontos = $pontos - 25;
		return $pontos;
	}
	
        //Publicada -> Em analise
	if($status == "Pub" and $status_2 == "P"){
		$pontos = $pontos - 85;
		return $pontos;
	}
        //Publicada -> Pendente
	if($status == "Pub" and $status_2 == "C"){
		$pontos = $pontos - 95;
		return $pontos;
	}
        //Publicada -> Concluida
	if($status == "Pub" and $status_2 == "Con"){
		$pontos = $pontos - 75;
		return $pontos;
	}
        //Publicada -> Cancelada
	if($status == "Pub" and $status_2 == "Can"){
		$pontos = $pontos - 100;
		return $pontos;
	}

	//Cancelada -> Em analise
	if($status == "Can" and $status_2 == "P"){
		$pontos = $pontos + 15;
		return $pontos;
	}
	//Cancelada -> Pendente
	if($status == "Can" and $status_2 == "C"){
		$pontos = $pontos + 5;
		return $pontos;
	}
	//Cancelada -> Concluida
	if($status == "Can" and $status_2 == "Con"){
		$pontos = $pontos + 25;
		return $pontos;
	}
	//Cancelada -> Publicada
	if($status == "Can" and $status_2 == "Pub"){
		$pontos = $pontos + 100;
		return $pontos;
	}
        
	if($status == $status_2){
		return $pontos;
	}
}

function calcular_pontos_por_situacao($situacao_old, $situacao_new, $pontos){
    if ($situacao_old != $situacao_new){
        switch($situacao_old){
            //Pendente
            case 1:
                switch($situacao_new){
                    //Em Analise
                    case 2:
                        $pontos += 10;
                        break; 
                    //Concluída
                    case 3:
                        $pontos += 20;
                        break; 
                    //Publicada
                    case 4:
                        $pontos += 95;
                        break; 
                    //Cancelada
                    case 5:
                        $pontos -= 5;
                        break;  
                }
                break;
            //Em Analise
            case 2:
                switch($situacao_new){
                    //Pendente
                    case 1:
                        $pontos -= 10;
                        break;
                    //Concluída
                    case 3:
                        $pontos += 10;
                        break;  
                    //Publicada
                    case 4:
                        $pontos += 85;
                        break;  
                    //Cancelada
                    case 5:
                        $pontos -= 15;
                        break;  
                }
                break;
            //Concluída
            case 3:
                switch($situacao_new){
                    //Pendente
                    case 1:
                        $pontos -= 20;
                        break;
                    //Em Analise
                    case 2:
                        $pontos -= 10;
                        break;  
                    //Publicada
                    case 4:
                        $pontos += 75;
                        break;  
                    //Cancelada
                    case 5:
                        $pontos -= 25;
                        break;  
                }
                break;
            //Publicada
            case 4:
                switch($situacao_new){
                    //Pendente
                    case 1:
                        $pontos -= 95;
                        break;
                    //Em Analise
                    case 2:
                        $pontos -= 85;
                        break;  
                    //Concluída
                    case 3:
                        $pontos -= 75;
                        break;  
                    //Cancelada
                    case 5:
                        $pontos -= 100;
                        break;  
                }
                break;
            //Cancelada
            case 5:
                switch($situacao_new){
                    //Pendente
                    case 1:
                        $pontos += 5;
                        break;
                    //Em Analise
                    case 2:
                        $pontos += 15;
                        break;  
                    //Concluída
                    case 3:
                        $pontos += 25;
                        break;  
                    //Publicada
                    case 4:
                        $pontos += 100;
                        break;  
                }
                break;
        }
    }
    
    return $pontos;
}
?>