<?php
session_start();

if($_SESSION['usuario']) { // verifico se usuario esta logado
    require("../conexao.php");
    conexao();
    $nome_usuario = $_SESSION['nome'];
    $apontador = $_SESSION['apontador'];
    $empresa = $_SESSION['empresa'];
    
    // Seleciono os equipamentos
    $select_equipamento = "SELECT * FROM equipamento WHERE apontador = '$apontador'";
    $query_equipamento = mysql_query($select_equipamento, $base) or die(mysql_error());
    $linhas_equipamento = mysql_num_rows($query_equipamento);

    // Seleciono as linhas de produção
    $select_linha = "SELECT * FROM linha_producao WHERE apontador = '$apontador'";
    $query_linha = mysql_query($select_linha, $base) or die(mysql_error());
    $linhas_linha = mysql_num_rows($query_linha);

    // Seleciono as unidades Fabris
    $select_unidade = "SELECT * FROM unidade_fabril WHERE apontador = '$apontador'";
    $query_unidade = mysql_query($select_unidade, $base) or die(mysql_error());
    $linhas_unidade = mysql_num_rows($query_unidade);
    
    // Seleciono as situações
    $select_situacao = "SELECT id_situacao, descricao FROM situacao WHERE status = 'A' AND id_situacao IN(1,3,5,6) order by id_situacao ASC";
    $query_situacao = mysql_query($select_situacao, $base) or die(mysql_error());
    $linhas_situacao = mysql_num_rows($query_situacao);
    
    // Seleciono as analises
    $select_analise = "SELECT * FROM analise_falhas WHERE apontador = '$apontador' ORDER BY prioridade ASC";
    $query_analise = mysql_query($select_analise, $base) or die(mysql_error());
    $linhas_analise = mysql_num_rows($query_analise);
    
    // Seleciono as responsaveis
    $select_responsaveis = "SELECT * FROM user_clientes WHERE id_empresa = '$apontador'";
    if ($_SESSION['nivel'] != 1){
        $select_responsaveis .= " AND id_user =".$_SESSION['id_usuario'];
    }
    $select_responsaveis .= " ORDER BY nome ASC";
    
    $query_responsaveis = mysql_query($select_responsaveis, $base) or die(mysql_error());
    $linhas_responsaveis = mysql_num_rows($query_responsaveis);

if($_SESSION['nivel'] == 2){
	$select_acao = "SELECT p.id_analise, f.titulo, p.responsavel, p.descricao, p.prazo, p.id_situacao, p.id_acao, s.descricao as situacao_nome FROM acoes p, analise_falhas f, situacao s  WHERE p.id_analise = f.id_analise AND s.id_situacao = p.id_situacao AND p.responsavel = '$nome_usuario' ORDER BY id_acao ASC";
} else {
	$select_acao = "SELECT p.id_analise, f.titulo, p.responsavel, p.descricao, p.prazo, p.id_situacao, p.id_acao, f.apontador, s.descricao as situacao_nome FROM acoes p, analise_falhas f, situacao s WHERE p.id_analise = f.id_analise AND s.id_situacao = p.id_situacao AND f.apontador = '$apontador' ORDER BY id_acao ASC";
}
$exe_acao = mysql_query($select_acao, $base) or die(mysql_error());
//$reg_acao = mysql_fetch_array($exe_acao, MYSQL_ASSOC);
$linha_acao = mysql_num_rows($exe_acao); // retorna se existe alguma linha no banco


if($_SESSION['nivel'] == 2){
	$select_acao2 = "SELECT p.id_analise, f.titulo, p.responsavel, p.descricao, p.prazo, p.id_situacao, p.id_acao, f.apontador FROM acoes p, analise_falhas f WHERE p.id_analise = f.id_analise AND p.responsavel = '$nome_usuario' AND p.id_situacao = 1 ORDER BY id_acao ASC";
} else {
	$select_acao2 = "SELECT p.id_analise, f.titulo, p.responsavel, p.descricao, p.prazo, p.id_situacao, p.id_acao, f.apontador FROM acoes p, analise_falhas f WHERE p.id_analise = f.id_analise AND f.apontador = '$apontador' AND p.id_situacao = 1 ORDER BY id_acao ASC";
}
$exe_acao2 = mysql_query($select_acao2, $base) or die(mysql_error());
//$reg_acao = mysql_fetch_array($exe_acao, MYSQL_ASSOC);
$linha_acao2 = mysql_num_rows($exe_acao2); // retorna se existe alguma linha no banco

if($_SESSION['nivel'] == 2){
	$select_acao3 = "SELECT p.id_analise, f.titulo, p.responsavel, p.descricao, p.prazo, p.id_situacao, p.id_acao, f.apontador FROM acoes p, analise_falhas f WHERE p.id_analise = f.id_analise AND p.responsavel = '$nome_usuario' AND p.id_situacao = 6 ORDER BY id_acao ASC";
} else {
	$select_acao3 = "SELECT p.id_analise, f.titulo, p.responsavel, p.descricao, p.prazo, p.id_situacao, p.id_acao, f.apontador FROM acoes p, analise_falhas f WHERE p.id_analise = f.id_analise AND f.apontador = '$apontador' AND p.id_situacao = 6 ORDER BY id_acao ASC";
}
$exe_acao3 = mysql_query($select_acao3, $base) or die(mysql_error());
//$reg_acao = mysql_fetch_array($exe_acao, MYSQL_ASSOC);
$linha_acao3 = mysql_num_rows($exe_acao3); // retorna se existe alguma linha no banco

if($_SESSION['nivel'] == 2){
	$select_acao4 = "SELECT p.id_analise, f.titulo, p.responsavel, p.descricao, p.prazo, p.id_situacao, p.id_acao, f.apontador FROM acoes p, analise_falhas f WHERE p.id_analise = f.id_analise AND p.responsavel = '$nome_usuario' AND p.id_situacao = 3 ORDER BY id_acao ASC";
} else {
	$select_acao4 = "SELECT p.id_analise, f.titulo, p.responsavel, p.descricao, p.prazo, p.id_situacao, p.id_acao, f.apontador FROM acoes p, analise_falhas f WHERE p.id_analise = f.id_analise AND f.apontador = '$apontador' AND p.id_situacao = 3 ORDER BY id_acao ASC";
}
$exe_acao4 = mysql_query($select_acao4, $base) or die(mysql_error());
//$reg_acao = mysql_fetch_array($exe_acao, MYSQL_ASSOC);
$linha_acao4 = mysql_num_rows($exe_acao4); // retorna se existe alguma linha no banco

if($_SESSION['nivel'] == 2){
	$select_acao5 = "SELECT p.id_analise, f.titulo, p.responsavel, p.descricao, p.prazo, p.id_situacao, p.id_acao, f.apontador FROM acoes p, analise_falhas f WHERE p.id_analise = f.id_analise AND p.responsavel = '$nome_usuario' AND p.id_situacao = 5 ORDER BY id_acao ASC";
} else {
	$select_acao5 = "SELECT p.id_analise, f.titulo, p.responsavel, p.descricao, p.prazo, p.id_situacao, p.id_acao, f.apontador FROM acoes p, analise_falhas f WHERE p.id_analise = f.id_analise AND f.apontador = '$apontador' AND p.id_situacao = 5 ORDER BY id_acao ASC";
}
$exe_acao5 = mysql_query($select_acao5, $base) or die(mysql_error());
//$reg_acao = mysql_fetch_array($exe_acao, MYSQL_ASSOC);
$linha_acao5 = mysql_num_rows($exe_acao5); // retorna se existe alguma linha no banco

// Seleciono os equipamentos
	$select_equipamento = "SELECT * FROM equipamento WHERE apontador = '$apontador'";
	$query_equipamento = mysql_query($select_equipamento, $base) or die(mysql_error());
	$linhas_equipamento = mysql_num_rows($query_equipamento);
	
	// Seleciono as linhas de produção
	$select_linha = "SELECT * FROM linha_producao WHERE apontador = '$apontador'";
	$query_linha = mysql_query($select_linha, $base) or die(mysql_error());
	$linhas_linha = mysql_num_rows($query_linha);
	
	// Seleciono as unidades Fabris
	$select_unidade = "SELECT * FROM unidade_fabril WHERE apontador = '$apontador'";
	$query_unidade = mysql_query($select_unidade, $base) or die(mysql_error());
	$linhas_unidade = mysql_num_rows($query_unidade);
	
	// Seleciono as analises
	$select_analise = "SELECT * FROM analise_falhas WHERE apontador = '$apontador' ORDER BY prioridade ASC";
	$query_analise = mysql_query($select_analise, $base) or die(mysql_error());
	$linhas_analise = mysql_num_rows($query_analise);

        // Seleciono as responsaveis
	$select_responsaveis = "SELECT * FROM user_clientes WHERE id_empresa = '$apontador' ORDER BY nome ASC";
	$query_responsaveis = mysql_query($select_responsaveis, $base) or die(mysql_error());
	$linhas_responsaveis = mysql_num_rows($query_responsaveis);
?>
<?php 
    $menuAtivo = 'acoes';
    include '../menu_top.php'; 
?>
<!-- FIM MENU TOP -->
<div id="geral">
<div id="chamadaInternaAcoes"><p class="fonte37">Ações de Bloqueio</p></div>
    <div id="menuLat">
        <div class="fundoChamadaBox">Situação das Ações de Bloqueio</div>
        <div id="dadosGerais">
            <div id="espacoGrafico"></div>
            <div id="graficoAcoesBloqueio">
                <!-- GRAFICO DINAMIZADO -->
            </div>
            <div class="clear"></div>
            <div id="resultadoTotal"><span class="fonte12Azul">Total : </span><span id="resultadoTotalQtd" class="fonte17">0</span></div>
        </div>
        <div id="dadosGeraisBotton"></div>
        <div id="fundoChamadaAviso">Avisos</div>
        <div id="avisosImportantes">
            <div id="conteudoAviso">
                <p>Fique atento aos avisos que
                  aparecerão em vermelho no topo das
                  páginas, ao ser incluido em novas
                  ações, ou durante o processo de
                  cadastramento de falhas. </p>
            </div>
        </div>
        <div id="dadosGeraisBotton"></div>
    </div>
    <div id="conteudo">
    <div id="dvAcoesBloqueio" class="boxConteudoDireitoSemMargin bAll">
        <div class="tituloConteudoDireito bTop">Buscar A&ccedil;&atilde;o de Bloqueio por:</div>
        <div class="conteudo">
            <div class="campo-6-12">
                <label for="ddlUnidadeFabril" class="campo-label">Unidade Fabril</label>
                <select id="ddlUnidadeFabril" name="ddlUnidadeFabril" style="width: 100%;">
                    <option value="" selected>Todos</option>
<?php 
                if($linhas_unidade > 0){ 
		    while($reg_unidade = mysql_fetch_assoc($query_unidade)) { 
?>
			<option value="<?php echo $reg_unidade['id_unidade'];?>"><?php echo $reg_unidade['nome'];?></option>
<?php	
		    }
		} 
?>
                </select>
            </div>
            <div class="campo-6-12">
                <label for="ddlLinhaProducao" class="campo-label">Linha de Produ&ccedil;&atilde;o</label>    
                <select id="ddlLinhaProducao" name="ddlLinhaProducao" style="width: 100%;">
                    <option value="" selected>Todos</option>
<?php 
                if ($linhas_linha > 0) { while ($reg_linha = mysql_fetch_assoc($query_linha)) {
?>
                    <option value="<?php echo htmlspecialchars($reg_linha['nome']); ?>"><?php echo $reg_linha['nome']; ?></option>
<?php	
                }} 
?>
                </select>
            </div>
            <div class="clear"></div>
            <div class="campo-6-12">
                <label for="ddlEquipamento" class="campo-label">Equipamento</label>
                <select id="ddlEquipamento" name="ddlEquipamento" style="width: 100%;">
                    <option value="" selected>Todos</option>
<?php 
                if ($linhas_equipamento > 0) { while ($reg_equipamento = mysql_fetch_assoc($query_equipamento)) {
?>
                    <option value="<?php echo htmlspecialchars($reg_equipamento['nome']);?>"><?php echo $reg_equipamento['nome'];?></option>
<?php	
                }} 
?>
                </select>
            </div>
            <div class="campo-6-12">
                <label for="ddlTitulo" class="campo-label">T&iacute;tulo</label>
                <select id="ddlTitulo" name="ddlTitulo" style="width: 100%;">
                    <option value="" selected>Todos</option>
<?php 
                if($linhas_analise > 0){ while($reg_analise = mysql_fetch_assoc($query_analise)) { 
?>
                    <option value="<?php echo $reg_analise['titulo'];?>"><?php echo $reg_analise['titulo'];?></option>
<?php	
                }} 
?>
                </select>
            </div>
            <div class="clear"></div>
            <div class="campo-6-12">
                <label for="ddlResponsavel" class="campo-label">Respons&aacute;vel</label>
<?php 
                if($linhas_responsaveis > 0){
                    $responsavel_disabled   = ($linhas_responsaveis === 1 && $_SESSION['nivel'] != 1) ? 'disabled="disabled"' : NULL;
                    $firstoption_selected   = ($linhas_responsaveis > 1 && $_SESSION['nivel'] == 1) ? 'selected' : NULL;
                    $registro_selected      = ($linhas_responsaveis === 1 && $_SESSION['nivel'] != 1) ? 'selected' : NULL;
                }
?>
		<select id="ddlResponsavel" name="ddlResponsavel" style="width: 100%;" <?php echo @$responsavel_disabled; ?>>
                    <option value="" <?php echo @$firstoption_selected; ?>>Todos</option>
<?php 
                if($linhas_responsaveis > 0){ while($reg_responsaveis = mysql_fetch_assoc($query_responsaveis)) { 
?>
                    <option value="<?php echo $reg_responsaveis['nome'];?>" <?php echo $registro_selected; ?>><?php echo $reg_responsaveis['nome'];?></option>
<?php	

                }} 
?>
                </select>
            </div>
            <div class="campo-3-12">
                <label for="ddlResponsavel" class="campo-label">Situa&ccedil;&atilde;o</label>
                <select id="ddlSituacao" name="ddlSituacao" style="width: 100%;">
                    <option value="" selected>Todos</option>
                <?php 
                if($linhas_situacao > 0){ while($red_situacao = mysql_fetch_assoc($query_situacao)) { 
                ?>
                    <option value="<?php echo $red_situacao['id_situacao'];?>"><?php echo $red_situacao['descricao'];?></option>
                <?php	
                }} 
                ?>
                </select>
            </div>

            <div style="float: left; padding: 0 5px; width: 96%;">
                <label for="txtDescAcao" class="campo-label">Descrição da Ação</label>
                <input type="text" name="txtDescAcao" id="txtDescAcao" style="width:100%"/>
            </div>
        </div>
    </div>

        <div id="dvGerenciarAnalises" class="boxConteudoDireito bAll">
            <div class="tituloConteudoDireito bTop">Gerenciamento das A&ccedil;&otilde;es de Bloqueio Cadastradas</div>
                <div id="conteudoPesquisa" class="conteudo">
                    <!-- AJAX GERENCIAR ANALISES -->
                </div>
        </div>
    </div>
</div>
    <div class="clear"></div>
</div>
<!-- FIM GERAL -->
<?php 
} else { // se usuário não estiver logado?>
    <script language="JavaScript">
            window.location.href = "http://www.telios.eng.br/index.php?deslogado=erro";
    </script>
<?php 
}
?>