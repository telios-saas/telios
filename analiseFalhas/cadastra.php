<?php session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado
require("../conexao.php");
conexao();
$apontador = $_SESSION['apontador'];
$data = date("d/m/Y");
// Seleciono os equipamentos
$select_equipamento = "SELECT * FROM equipamento WHERE apontador = '$apontador'";
$query_equipamento = mysql_query($select_equipamento, $base) or die(mysql_error());
$linhas_equipamento = mysql_num_rows($query_equipamento);

// Seleciono as linhas de produção
$select_linha = "SELECT * FROM linha_producao WHERE apontador = '$apontador'";
$query_linha = mysql_query($select_linha, $base) or die(mysql_error());
$linhas_linha = mysql_num_rows($query_linha);

// Seleciono as unidades Fabris
$select_unidade = "SELECT * FROM unidade_fabril WHERE apontador = '$apontador'";
$query_unidade = mysql_query($select_unidade, $base) or die(mysql_error());
$linhas_unidade = mysql_num_rows($query_unidade);

?>
<?php 
    $menuAtivo = 'analise';
    include '../menu_top.php'; 
?>
<!-- FIM MENU TOP -->
<div id="geral">
  <div id="textoAnalise">
    <p class="fonte37">Nova Análise de Falhas</p>
    <div id="textoExplicativoAnalise">
      <p>Para cadastrar uma análise, é necessário preencher as informações básicas.</p>
    </div>
  </div>
  <div id="formAnaliseDir">
    <form id="enviaDados" name="enviaDados" method="post" action="#" >
      <div id="formAnalise">
        <div id="formTopAnalise"></div>
        <div id="internoForm">
          <table cellspacing="0" id="tabGeral" width="100%">
            <tr>
              <td align="right">Título:</td>
              <td><input name="titulo" type="text" id="titulo" size="45"></td>
            </tr>
            <tr>
              <td align="right">N&deg;. Ordem Serviço:</td>
              <td><input name="num_sap" type="text" id="num_sap" size="45"></td>
            </tr>
            <tr>
              <td align="right">Unidade Fabril:</td>
              <td><select name="id_unidade" id="id_unidade">
                  <?php if($linhas_unidade > 0){
			  while($reg_unidade = mysql_fetch_assoc($query_unidade)){?>
                  <option value="<?php echo $reg_unidade['id_unidade'];?>"><?php echo $reg_unidade['nome'];?></option>
                  <?php } } else {?>
                  <option value="">NENHUMA UNIDADE...</option>
                  <?php }?>
                </select></td>
            </tr>
            <tr>
              <td align="right">Linha de Produção:</td>
              <td><select name="linha_producao" id="linha_producao">
                  <?php if($linhas_linha > 0){
			  while($reg_linha = mysql_fetch_assoc($query_linha)){?>
                  <option value="<?php echo $reg_linha['nome'];?>"><?php echo $reg_linha['nome'];?></option>
                  <?php } } else {?>
                  <option value="">NENHUMA LINHAS CADASTRADA...</option>
                  <?php }?>
                </select></td>
            </tr>
            <tr>
              <td align="right">Equipamento:</td>
              <td><select name="equipamento" id="equipamento">
                  <?php if($linhas_equipamento > 0){
			  while($reg_equipamento = mysql_fetch_assoc($query_equipamento)){?>
                  <option value="<?php echo $reg_equipamento['nome'];?>"><?php echo $reg_equipamento['nome'];?></option>
                  <?php } } else {?>
                  <option value="">NENHUM EQUIPAMENTO...</option>
                  <?php }?>
                </select></td>
            </tr>
            <tr>
              <td align="right">Data do Evento:</td>
              <td><input name="data_evento" type="text" id="data_evento" size="8" maxlength="10" onKeyPress="Mascara('DATA',this,event);" value="<?php echo $data;?>"></td>
            </tr>
            <tr>
              <td align="right">Situação:</td>
                <td><select name="id_situacao" id="id_situacao">
                        <option value="1">Pendente</option>
                        <option value="2">Em análise</option>
                        <option value="3">Concluída</option>
                        <option value="5">Cancelada</option>
                        <option value="4">Publicada</option>
                </select></td>
            </tr>
          </table>
        </div>
        <div id="rodapeFormAnalise"></div>
      </div>
      <p>
        <input name="envia" type="button" id="envia" onclick="basico_falha('enviaDados', 'analiseFalhas/passo1.php?acao=1', 'geral');" value="" class="btnGravaProsseguir"/>
      </p>
    </form>
    <div class="clear"></div>
  </div>
</div>
<!-- FIM GERAL -->
<?php } else { // se usuário não estiver logado?>
<script language="JavaScript">
	window.location.href = "http://www.telios.eng.br/index.php?deslogado=erro";
</script>
<?php }?>
