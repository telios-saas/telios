<?php 
session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado
require("../../conexao.php");
conexao();
$id_analise = $_GET['id'];
$acao = $_GET['acao'];

$data_avaliacao = $_POST['data_avaliacao'];
$equipe_avaliacao = $_POST['equipe_avaliacao'];
$descricao_evento = $_POST['descricao_evento'];
$evidencias = $_POST['evidencias'];
$componente_falhou = $_POST['componente_falhou'];
$funcao = $_POST['funcao'];
$modo_falha = $_POST['modo_falha'];
$causa_raiz = $_POST['causa_raiz'];
$causa_basica = $_POST['causa_basica'];
$observacoes = $_POST['observacoes'];

$update_basico = "UPDATE dados_basico_analise SET data_avaliacao = '$data_avaliacao', equipe_avaliacao = '$equipe_avaliacao', descricao_evento = '$descricao_evento', evidencias = '$evidencias', componente_falhou = '$componente_falhou', funcao = '$funcao', modo_falha = '$modo_falha', causa_raiz = '$causa_raiz', observacoes = '$observacoes', causa_basica = '$causa_basica' WHERE id_analise = '$id_analise'";
$executa_update = mysql_query($update_basico, $base) or die(mysql_error());

?>
  <div id="passo_passo">
    <div id="passo1">
      <p><span class="fonte15Passo">Passo 1 &raquo;</span></p>
      <p><span class="cinza">Análise da Prioridade</span></p>
    </div>
    <div id="passo2Ativo">
      <p><span class="fonte15Branca">Passo 2 &raquo;</span></p>
      <p><span class="branca">Informações Básicas</span></p>
    </div>
    <div id="passo3">
      <p><span class="fonte15Passo">Passo 3 &raquo;</span></p>
      <p><span class="cinza">Diagrama Causa e Efeito</span></p>
    </div>
    <div id="passo4">
      <p><span class="fonte15Passo">Passo 4 &raquo;</span></p>
      <p><span class="cinza">5 Porquês</span></p>
    </div>
    <div id="passo5">
      <p><span class="fonte15Passo">Passo 5 &raquo;</span></p>
      <p><span class="cinza">Ações</span></p>
    </div>
    <div id="passo6">
      <p><span class="fonte15Passo">Passo 6 &raquo;</span></p>
      <p><span class="cinza">Anexos</span></p>
    </div>
  </div>
  <div class="clear"></div>
  <div id="dadosExplicativos">
    <div id="chamadaPagina"><span class="fonte37">Informações Básicas</span></div>
    <p>Esta etapa situa as pessoas na análise, informando os dados que servirão de base para o processo de busca da causa raiz da falha.</p>
  </div>
  <form id="enviaDados" name="enviaDados" method="post" action="#" >
    <div id="formAnalise">
      <table id="tabGeral">
        <tr>
          <td align="right">Data Avaliação:</td>
          <td>
            <input type="text" name="data_avaliacao" id="data_avaliacao" size="8" maxlength="10" onKeyPress="Mascara('DATA',this,event);" value="<?php echo $data_avaliacao;?>"></td>
        </tr>
        <tr>
          <td align="right">Equipe de Avaliação:</td>
          <td>
            <input name="equipe_avaliacao" type="text" id="equipe_avaliacao" size="60" value="<?php echo $equipe_avaliacao;?>"></td>
        </tr>
        <tr>
          <td align="right" valign="top">Descrição do Evento:</td>
          <td>
            <textarea name="descricao_evento" id="descricao_evento" cols="55" rows="1"><?php echo $descricao_evento;?></textarea></td>
        </tr>
        <tr>
          <td align="right" valign="top">Evidências:</td>
          <td>
            <textarea name="evidencias" id="evidencias" cols="55" rows="1"><?php echo $evidencias;?></textarea></td>
        </tr>
        <tr>
          <td align="right" valign="top">Causas Básicas:</td>
          <td>
          <textarea name="causa_basica" id="causa_basica" cols="55" rows="1"><?php echo $causa_basica;?></textarea></td>
        </tr>
        <tr>
          <td align="right">Item que Falhou:</td>
          <td>
            <input name="componente_falhou" type="text" id="componente_falhou" size="60" value="<?php echo $componente_falhou;?>"></td>
        </tr>
        <tr>
          <td align="right">Função:</td>
          <td>
            <input name="funcao" type="text" id="funcao" size="60" value="<?php echo $funcao;?>"></td>
        </tr>
        <tr>
          <td align="right" valign="top">Modo de Falha:</td>
          <td>
            <textarea name="modo_falha" id="modo_falha" cols="55" rows="1"><?php echo $modo_falha;?></textarea></td>
        </tr>
        <tr>
          <td align="right" valign="top">Causa Raiz:</td>
          <td>
            <textarea name="causa_raiz" id="causa_raiz" cols="55" rows="1"><?php echo $causa_raiz;?></textarea></td>
        </tr>
        <tr>
          <td align="right" valign="top">Observações:</td>
          <td>
            <textarea name="observacoes" id="observacoes" cols="55" rows="1"><?php echo $observacoes;?></textarea></td>
        </tr>
      </table>
      <div id="gravadoSucesso">Seus dados foram salvos com sucesso!</div>
    </div>
    <p>
      <input name="envia" type="button" id="envia" onclick="geral('enviaDados', 'analiseFalhas/gravaPasso2.php?id=<?php echo $id_analise;?>&amp;acao=2', 'formAnaliseDir');" value="" class="btnGravar"/>
      <input name="envia" type="button" id="envia" onclick="geral('enviaDados', 'analiseFalhas/passo3.php?id=<?php echo $id_analise;?>&amp;acao=1', 'formAnaliseDir');" value="" class="btnProsseguir"/>
    </p>
  </form>
<?php } else { // se usuário não estiver logado?>
<script language="JavaScript">
	window.location.href = "../index.php";
</script>
<?php }?>
