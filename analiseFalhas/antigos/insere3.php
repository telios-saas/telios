<?php 
session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado

$apontador = $_SESSION['apontador'];

require("../../conexao.php");
conexao();

if(isset($_GET['acao'])){ // Pego a ação do meu sistema
	$acao = $_GET['acao'];
}else{
	$acao = 0;	
}

if($acao == 1){ 
	// Dados Via Formulario
	$saude_seguranca = $_POST['saude_seguranca'];
	$meio_ambiente = $_POST['meio_ambiente'];
	$producao = $_POST['producao'];
	$custo_manutencao = $_POST['custo_manutencao'];
	$frequencia_evento = $_POST['frequencia_evento'];
	$id_analise = $_GET['id_analise'];
	// Fim Dados
	$risco = ($saude_seguranca + $meio_ambiente + $producao + $custo_manutencao)*$frequencia_evento;// CALCULA O RISCO 
### Calculco Prioridade da Analise ##########
	if ($risco <  500 ){ // Risco menor q 500 
		if ($saude_seguranca < 30) {// Se a Saude e segurança for menor q 30
			if ($meio_ambiente < 30){ // Se Meio Ambiente for menor q 30
				if ($producao < 30){ // Se Produção for menor que 30
					if ($risco <=  100 ){ // Se risco menor igual á 100
						if ($saude_seguranca < 20){ // Se Saude e Segurança menor q 20
							if ($meio_ambiente < 20){ // Se Meio Ambiente menor q 20
								if ($producao < 20){ // Se produção menor que 20
									if ($risco <= 40){ // Se Risco for menor igual á 40
										if ($risco < 29){ // Se risco for menor que 29 
											$criticidade = "E";
										} else { // Se risco for maior que 29 e menor igual á 40
											$criticidade = "D";
										} 
									} // Fim Risco menor igual á 40
									else { // Se risco for maior que 40 e menor igual á 100
										$criticidade = "C";
									}
								} else {// Se produção maior que 20
									$criticidade = "B";
								}
						} else { // Se Meio Ambiente Maior que 20
							$criticidade = "B";
						}
					} else { // Se Saude Maior que 20
						$criticidade = "B";
					}
				} else {// Se risco maior que 100
					$criticidade = "B";
				}
			} else {// Se Produção Maior que 30
				$criticidade = "A";
			}
		} else {// Se Meio Ambiente Maior que 30
			$criticidade = "A";
		}
	}else {// Se Saude Segurança Maior que 30
		$criticidade = "A";
	}
} else {// Se soma for maior que 500 
	$criticidade = "A";
}
###### Fim do Calculo da Prioridade ######	

	// Inserir na tabela de Priorizacao
	$grava = "INSERT INTO priorizacao_falhas (id_analise, saude_seguranca, meio_ambiente, producao, custo_manutencao, frequencia_evento) VALUES ('$id_analise', '$saude_seguranca', '$meio_ambiente', '$producao', '$custo_manutencao', '$frequencia_evento')";
	$executa = mysql_query($grava, $base) or die(mysql_error());
   // Faço UPDATE na Tabela Analise para Atualizar a Prioridade 
    $update = "UPDATE analise_falhas SET prioridade = '$criticidade' WHERE id_analise = '$id_analise'";
    $executa_update = mysql_query($update, $base) or die(mysql_error());	
}
?>

<div id="chamada" class="chamada37">Cadastrar Análise Falhas</div>
<div class="passos">1° Passo &raquo; 2° Passo &raquo; <span class="passoAtivo">3° Passo &raquo;</span> 4° Passo &raquo; 5° Passo &raquo; 6° Passo</div>
<?php if($acao != 0){?>
<div id="acerto">
  <?php if($acao == 1){?>
  <p>2° Etapa concluida com sucesso.</p>
  <p>A Prioridade desta Análise é <?php echo $criticidade;?></p>
  <?php }?>
</div>
<?php }?>
<p class="negrito">Diagrama Causa e Efeito</p>
<br>
<form id="enviaDados" name="enviaDados" method="post" action="#" >
  <div id="imgDiagramaCausaEfeito">
    <div class="camposEsq">
      <div class="campoEsq1">
        <p>Máquina</p>
        <textarea name="maquina" id="maquina"></textarea>
      </div>
      <div class="campoEsq2">
      	<p>Meio Ambiente</p>
        <textarea name="meio_ambiente" id="meio_ambiente"></textarea>
      </div>
      <div class="clear"></div>
    </div>
    <div class="camposDir">
    	<p>Método</p>
        <textarea name="metodo" id="metodo"></textarea>
    </div>
    <div class="clear"></div>
    <img src="../images/diagrama.jpg" width="566" height="84">
    <div class="camposEsq">
      <div class="campoEsq1">
      	<p>Medida</p>
        <textarea name="medida" id="medida"></textarea>
      </div>
      <div class="campoEsq2">
      	<p>Mão de Obra</p>
        <textarea name="mao_obra" id="mao_obra"></textarea>
      </div>
      <div class="clear"></div>
    </div>
    <div class="camposDir">
    	<p>Matéria Prima</p>
        <textarea name="materia_prima" id="materia_prima"></textarea>
    </div>
    <div class="clear"></div>
  </div>
  <div id="campoFalha">
  <p>Falha</p>
        <textarea name="falha" id="falha"></textarea>
  </div>
  <div class="clear"></div>
  <input name="envia" type="button" id="envia" onclick="geral('enviaDados', 'analiseFalhas/insere4.php?acao=1&amp;id_analise=<?php echo $id_analise;?>', 'conteudo');" value="Proximo" class="btnLogar"/>
  <input type="reset" name="limpar" id="limpar" value="Limpar" class="btnLogar" />
</form>
<?php } else { // se usuário não estiver logado?>
<script language="JavaScript">
			window.location.href = "../index.php";
	</script>
<?php }?>
