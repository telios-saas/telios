<?php 
session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado
require("../../conexao.php");
conexao();
$id_analise = $_GET['id'];
$acao = $_GET['acao'];

$maquina = $_POST['maquina'];
$meio_ambiente = $_POST['meio_ambiente'];
$metodo = $_POST['metodo'];
$falha = $_POST['falha'];
$medida = $_POST['medida'];
$mao_obra = $_POST['mao_obra'];
$materia_prima = $_POST['materia_prima'];

if($acao == 2){
	$update_causa = "UPDATE causa_efeito SET maquina = '$maquina', meio_ambiente = '$meio_ambiente', metodo = '$metodo', falha = '$falha', medida = '$medida',  mao_falha = '$mao_falha', materia_prima = '$materia_prima' WHERE id_analise = '$id_analise'";
	$executa_causa = mysql_query($update_causa, $base) or die(mysql_error());
} else {
	$update_causa = "UPDATE causa_efeito SET maquina = '$maquina', meio_ambiente = '$meio_ambiente', metodo = '$metodo', falha = '$falha', medida = '$medida', mao_obra = '$mao_obra', materia_prima = '$materia_prima' WHERE id_analise = '$id_analise'";
	$executa_causa = mysql_query($update_causa, $base) or die(mysql_error());
}
?>
  <div id="passo_passo">
    <div id="passo1">
      <p><span class="fonte15Passo">Passo 1 &raquo;</span></p>
      <p><span class="cinza">Análise da Prioridade</span></p>
    </div>
    <div id="passo2">
      <p><span class="fonte15Passo">Passo 2 &raquo;</span></p>
      <p><span class="cinza">Informações Básicas</span></p>
    </div>
    <div id="passo3Ativo">
      <p><span class="fonte15Branca">Passo 3 &raquo;</span></p>
      <p><span class="branca">Diagrama Causa e Efeito</span></p>
    </div>
    <div id="passo4">
      <p><span class="fonte15Passo">Passo 4 &raquo;</span></p>
      <p><span class="cinza">5 Porquês</span></p>
    </div>
    <div id="passo5">
      <p><span class="fonte15Passo">Passo 5 &raquo;</span></p>
      <p><span class="cinza">Ações</span></p>
    </div>
    <div id="passo6">
      <p><span class="fonte15Passo">Passo 6 &raquo;</span></p>
      <p><span class="cinza">Anexos</span></p>
    </div>
  </div>
  <div class="clear"></div>
  <div id="dadosExplicativos">
    <div id="chamadaPagina"><span class="fonte37">Diagrama Causa e Efeito</span></div>
    <p>É uma forma de elencar as hipóteses segundo um processo organizado de brainstorming. Não se preocupe muito com onde elencar uma hipótese (ex. Máquina ou Método), mas sim que ela esteja lá. </p>
<p>Inicie inserindo a Falha que está sendo investigada e prossiga pelos demais M's. </p>
<p>Valide as hipóteses que foram elencadas e crie ações de bloqueio para elas. </p>
  </div>
  <form id="enviaDados" name="enviaDados" method="post" action="#" >
    <div id="formAnalise">
    	<div id="causaEfeitoDiagrama">
    	<div id="inputCausa1">
    	  <textarea name="maquina" id="maquina"><?php echo $maquina;?></textarea>
    	</div>
        <div id="inputCausa2">
        	<textarea name="meio_ambiente" id="meio_ambiente"><?php echo $meio_ambiente;?></textarea>
        </div>
        <div id="inputCausa3">
        	<textarea name="metodo" id="metodo"><?php echo $metodo;?></textarea>
        </div>
        <div id="inputCausa4">
        	<textarea name="falha" id="falha"><?php echo $falha;?></textarea>
        </div>
        <div id="inputCausa5">
        	<textarea name="medida" id="medida"><?php echo $medida;?></textarea>
        </div>
        <div id="inputCausa6">
        	<textarea name="mao_obra" id="maa_obra"><?php echo $mao_obra;?></textarea>
        </div>
        <div id="inputCausa7">
        	<textarea name="materia_prima" id="materia_prima"><?php echo $materia_prima;?></textarea>
        </div>
    </div> 
    <div id="gravadoSucesso">Seus dados foram salvos com sucesso!</div>
    </div>
    <p>
      <input name="envia" type="button" id="envia" onclick="geral('enviaDados', 'analiseFalhas/gravaPasso3.php?id=<?php echo $id_analise;?>&amp;acao=2', 'formAnaliseDir');" value="" class="btnGravar"/>
      <input name="envia" type="button" id="envia" onclick="geral('enviaDados', 'analiseFalhas/passo4.php?id=<?php echo $id_analise;?>&amp;acao=1', 'formAnaliseDir');" value="" class="btnProsseguir"/>
    </p>
  </form>
<?php } else { // se usuário não estiver logado?>
<script language="JavaScript">
	window.location.href = "../index.php";
</script>
<?php }?>
