<?php 
session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado
require("../../conexao.php");
conexao();
$id_analise = $_GET['id'];
$acao = $_GET['acao'];

$data_avaliacao = $_POST['data_avaliacao'];
$equipe_avaliacao = $_POST['equipe_avaliacao'];
$descricao_evento = $_POST['descricao_evento'];
$evidencias = $_POST['evidencias'];
$componente_falhou = $_POST['componente_falhou'];
$funcao = $_POST['funcao'];
$modo_falha = $_POST['modo_falha'];
$causa_raiz = $_POST['causa_raiz'];
$causa_basica = $_POST['causa_basica'];
$observacoes = $_POST['observacoes'];

$update_basico = "UPDATE dados_basico_analise SET data_avaliacao = '$data_avaliacao', equipe_avaliacao = '$equipe_avaliacao', descricao_evento = '$descricao_evento', evidencias = '$evidencias', componente_falhou = 'componente_falhou', funcao = '$funcao', modo_falha = '$modo_falha', causa_raiz = '$causa_raiz', observacoes = '$observacoes', causa_basica = '$causa_basica' WHERE id_analise = '$id_analise'";
$executa_update = mysql_query($update_basico, $base) or die(mysql_error());

?>
  <div id="passo_passo">
    <div id="passo1">
      <p><span class="fonte15Passo">Passo 1 &raquo;</span></p>
      <p><span class="cinza">Análise da Prioridade</span></p>
    </div>
    <div id="passo2">
      <p><span class="fonte15Passo">Passo 2 &raquo;</span></p>
      <p><span class="cinza">Informações Básicas</span></p>
    </div>
    <div id="passo3Ativo">
      <p><span class="fonte15Branca">Passo 3 &raquo;</span></p>
      <p><span class="branca">Diagrama Causa e Efeito</span></p>
    </div>
    <div id="passo4">
      <p><span class="fonte15Passo">Passo 4 &raquo;</span></p>
      <p><span class="cinza">5 Porquês</span></p>
    </div>
    <div id="passo5">
      <p><span class="fonte15Passo">Passo 5 &raquo;</span></p>
      <p><span class="cinza">Ações</span></p>
    </div>
    <div id="passo6">
      <p><span class="fonte15Passo">Passo 6 &raquo;</span></p>
      <p><span class="cinza">Anexos</span></p>
    </div>
  </div>
  <div class="clear"></div>
  <div id="dadosExplicativos">
    <div id="chamadaPagina"><span class="fonte37">Diagrama Causa e Efeito</span></div>
    <p>É uma forma de elencar as hipóteses segundo um processo organizado de brainstorming. Não se preocupe muito com onde elencar uma hipótese (ex. Máquina ou Método), mas sim que ela esteja lá. </p>
<p>Inicie inserindo a Falha que está sendo investigada e prossiga pelos demais M's. </p>
<p>Valide as hipóteses que foram elencadas e crie ações de bloqueio para elas. </p>
  </div>
  <form id="enviaDados" name="enviaDados" method="post" action="#" >
    <div id="formAnalise">
    <p>Diagrama de Causa e Efeito (Ishikawa, Espinha de Peixe ou 6M's).</p> 
    <div id="causaEfeitoDiagrama">
    	<div id="inputCausa1">
    	  <textarea name="maquina" id="maquina"></textarea>
    	</div>
        <div id="inputCausa2">
        	<textarea name="meio_ambiente" id="meio_ambiente"></textarea>
        </div>
        <div id="inputCausa3">
        	<textarea name="metodo" id="metodo"></textarea>
        </div>
        <div id="inputCausa4">
        	<textarea name="falha" id="falha"></textarea>
        </div>
        <div id="inputCausa5">
        	<textarea name="medida" id="medida"></textarea>
        </div>
        <div id="inputCausa6">
        	<textarea name="mao_obra" id="maa_obra"></textarea>
        </div>
        <div id="inputCausa7">
        	<textarea name="materia_prima" id="materia_prima"></textarea>
        </div>
    </div> 
    </div>
    <p>
      <input name="envia" type="button" id="envia" onclick="geral('enviaDados', 'analiseFalhas/gravaPasso3.php?id=<?php echo $id_analise;?>&amp;acao=1', 'formAnaliseDir');" value="" class="btnGravar"/>
      <input name="envia" type="button" id="envia" onclick="geral('enviaDados', 'analiseFalhas/passo4.php?id=<?php echo $id_analise;?>&amp;acao=1', 'formAnaliseDir');" value="" class="btnProsseguir"/>
    </p>
  </form>
<?php } else { // se usuário não estiver logado?>
<script language="JavaScript">
	window.location.href = "../index.php";
</script>
<?php }?>
