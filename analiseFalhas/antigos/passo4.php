<?php 
session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado
require("../../conexao.php");
conexao();
$id_analise = $_GET['id'];
$acao = $_GET['acao'];

$maquina = $_POST['maquina'];
$meio_ambiente = $_POST['meio_ambiente'];
$metodo = $_POST['metodo'];
$falha = $_POST['falha'];
$medida = $_POST['medida'];
$mao_obra = $_POST['mao_obra'];
$materia_prima = $_POST['materia_prima'];

$grava_causa = "INSERT INTO causa_efeito (id_analise, maquina, meio_ambiente, metodo, falha, medida, mao_obra, materia_prima) VALUES ('$id_analise', '$maquina', '$meio_ambiente', '$metodo', '$falha', '$medida', '$mao_obra', '$materia_prima')";
$executa_causa = mysql_query($grava_causa, $base) or die(mysql_error());
?>
  <div id="passo_passo">
    <div id="passo1">
      <p><span class="fonte15Passo">Passo 1 &raquo;</span></p>
      <p><span class="cinza">Análise da Prioridade</span></p>
    </div>
    <div id="passo2">
      <p><span class="fonte15Passo">Passo 2 &raquo;</span></p>
      <p><span class="cinza">Informações Básicas</span></p>
    </div>
    <div id="passo3">
      <p><span class="fonte15Passo">Passo 3 &raquo;</span></p>
      <p><span class="cinza">Diagrama Causa e Efeito</span></p>
    </div>
    <div id="passo4Ativo">
      <p><span class="fonte15Branca">Passo 4 &raquo;</span></p>
      <p><span class="branca">5 Porquês</span></p>
    </div>
    <div id="passo5">
      <p><span class="fonte15Passo">Passo 5 &raquo;</span></p>
      <p><span class="cinza">Ações</span></p>
    </div>
    <div id="passo6">
      <p><span class="fonte15Passo">Passo 6 &raquo;</span></p>
      <p><span class="cinza">Anexos</span></p>
    </div>
  </div>
  <div class="clear"></div>
  <div id="dadosExplicativos">
    <div id="chamadaPagina"><span class="fonte37">5 Porquês</span></div>
    <p>É uma técnica japonesa simples e eficaz, muito utilizada na área da qualidade.</p> 
<p>Consiste em expor o problema e perguntar por que ele ocorreu. Aqui podemos elencar até 3 hipóteses; ao identificar a verdadeira, marque na caixinha ao lado e prossiga a investigação perguntando por que esta hipótese ocorreu. </p>
<p>Ao chegar na causa raiz, relacione idéias de melhoria para evitar a recorrência do problema. Estas idéias devem ser elencadas como ações de bloqueio, com responsáveis e prazos para conclusão.  Para ver um exemplo, clique aqui.</p>
  </div>
  <form id="enviaDados" name="enviaDados" method="post" action="#" >
    <div id="formAnalise2">
    
      <table width="100%" cellspacing="0" id="tabPQ">
        <tr>
          <td width="18%" align="right" bgcolor="#f2f6ff">Descricao do Desvio:</td>
          <td width="82%" bgcolor="#f2f6ff">
          <input name="descricao_desvio" type="text" id="descricao_desvio" size="60"></td>
        </tr>
        <tr>
          <td colspan="2" bgcolor="#dfe8fa">
          <div class="round">1 ROUND</div>
          <div class="campoRound"><input type="text" name="1_round" id="1_round" size="75"></div>
          <div class="clear"></div>
          <div class="hipoteseEsq">
          	<div class="hipotese1">
          	<input name="hipotese_v_1_1" type="checkbox" id="hipotese_v_1_1" value="S">
       	    <textarea name="hipotese_1_1" id="hipotese_1_1" cols="20" rows="2"></textarea></div>
            <div class="hipotese2">
            <input name="hipotese_v_1_2" type="checkbox" id="hipotese_v_1_2" value="S">
            <textarea name="hipotese_1_2" id="hipotese_1_2" cols="20" rows="2"></textarea></div>
            <div class="clear"></div>
          </div>
          <div class="hipoteseDir">
          <input name="hipotese_v_1_3" type="checkbox" id="hipotese_v_1_3" value="S">
          <textarea name="hipotese_1_3" id="hipotese_1_3" cols="20" rows="2"></textarea></div>
          <div class="clear"></div>
          </td>
        </tr>
        <tr>
          <td colspan="2" bgcolor="#f2f6ff">
          <div class="round">2 ROUND</div>
          <div class="campoRound"><input type="text" name="2_round" id="2_round" size="85"></div>
          <div class="clear"></div>
          <div class="hipoteseEsq">
          	<div class="hipotese1">
          	<input name="hipotese_v_2_1" type="checkbox" id="hipotese_v_2_1" value="S">
       	    <textarea name="hipotese_2_1" id="hipotese_2_1" cols="20" rows="2"></textarea></div>
            <div class="hipotese2">
            <input name="hipotese_v_2_2" type="checkbox" id="hipotese_v_2_2" value="S">
            <textarea name="hipotese_2_2" id="hipotese_2_2" cols="20" rows="2"></textarea></div>
            <div class="clear"></div>
          </div>
          <div class="hipoteseDir">
          <input name="hipotese_v_2_3" type="checkbox" id="hipotese_v_2_3" value="S">
          <textarea name="hipotese_2_3" id="hipotese_2_3" cols="20" rows="2"></textarea></div>
          <div class="clear"></div>
          </td>
        </tr>
        <tr>
          <td colspan="2" bgcolor="#dfe8fa">
          <div class="round">3 ROUND</div>
          <div class="campoRound"><input type="text" name="3_round" id="3_round" size="85"></div>
          <div class="clear"></div>
          <div class="hipoteseEsq">
          	<div class="hipotese1">
          	<input name="hipotese_v_3_1" type="checkbox" id="hipotese_v_3_1" value="S">
       	    <textarea name="hipotese_3_1" id="hipotese_3_1" cols="20" rows="2"></textarea></div>
            <div class="hipotese2">
            <input name="hipotese_v_3_2" type="checkbox" id="hipotese_v_3_2" value="S">
            <textarea name="hipotese_3_2" id="hipotese_3_2" cols="20" rows="2"></textarea></div>
            <div class="clear"></div>
          </div>
          <div class="hipoteseDir">
          <input name="hipotese_v_3_3" type="checkbox" id="hipotese_v_3_3" value="S">
          <textarea name="hipotese_3_3" id="hipotese_3_3" cols="20" rows="2"></textarea></div>
          <div class="clear"></div>
          </td>
        </tr>
        <tr>
          <td colspan="2" bgcolor="#f2f6ff">
          <div class="round">4 ROUND</div>
          <div class="campoRound"><input type="text" name="4_round" id="4_round" size="85"></div>
          <div class="clear"></div>
          <div class="hipoteseEsq">
          	<div class="hipotese1">
          	<input name="hipotese_v_4_1" type="checkbox" id="hipotese_v_4_1" value="S">
       	    <textarea name="hipotese_4_1" id="hipotese_4_1" cols="20" rows="2"></textarea></div>
            <div class="hipotese2">
            <input name="hipotese_v_4_2" type="checkbox" id="hipotese_v_4_2" value="S">
            <textarea name="hipotese_4_2" id="hipotese_4_2" cols="20" rows="2"></textarea></div>
            <div class="clear"></div>
          </div>
          <div class="hipoteseDir">
          <input name="hipotese_v_4_3" type="checkbox" id="hipotese_v_4_3" value="S">
          <textarea name="hipotese_4_3" id="hipotese_4_3" cols="20" rows="2"></textarea></div>
          <div class="clear"></div>
          </td>
        </tr>
        <tr>
          <td colspan="2" bgcolor="#dfe8fa">
          <div class="round">5 ROUND</div>
          <div class="campoRound"><input type="text" name="5_round" id="5_round" size="85"></div>
          <div class="clear"></div>
          <div class="hipoteseEsq">
          	<div class="hipotese1">
          	<input name="hipotese_v_5_1" type="checkbox" id="hipotese_v_5_1" value="S">
       	    <textarea name="hipotese_5_1" id="hipotese_5_1" cols="20" rows="2"></textarea></div>
            <div class="hipotese2">
            <input name="hipotese_v_5_2" type="checkbox" id="hipotese_v_5_2" value="S">
            <textarea name="hipotese_5_2" id="hipotese_5_2" cols="20" rows="2"></textarea></div>
            <div class="clear"></div>
          </div>
          <div class="hipoteseDir">
          <input name="hipotese_v_5_3" type="checkbox" id="hipotese_v_5_3" value="S">
          <textarea name="hipotese_5_3" id="hipotese_5_3" cols="20" rows="2"></textarea></div>
          <div class="clear"></div>
          </td>
        </tr>
        <tr>
          <td align="right" valign="top" bgcolor="#f2f6ff">Ideias de Melhorias:</td>
          <td bgcolor="#f2f6ff"><label for="ideia_melhora"></label>
          <textarea name="ideia_melhora" id="ideia_melhora" cols="70" rows="2"></textarea></td>
        </tr>
      </table>
    </div>
    <p>
      <input name="envia" type="button" id="envia" onclick="geral('enviaDados', 'analiseFalhas/gravaPasso4.php?id=<?php echo $id_analise;?>&amp;acao=1', 'formAnaliseDir');" value="" class="btnGravar"/>
      <input name="envia" type="button" id="envia" onclick="geral('enviaDados', 'analiseFalhas/passo5.php?id=<?php echo $id_analise;?>&amp;acao=1', 'formAnaliseDir');" value="" class="btnProsseguir"/>
    </p>
  </form>
<?php } else { // se usuário não estiver logado?>
<script language="JavaScript">
	window.location.href = "../index.php";
</script>
<?php }?>
