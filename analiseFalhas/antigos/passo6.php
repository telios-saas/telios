<?php 
session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado
require("../../conexao.php");
conexao();
$id_analise = $_GET['id'];
$acao = $_GET['acao'];
?>

<div id="passo_passo">
  <div id="passo1">
    <p><span class="fonte15Passo">Passo 1 &raquo;</span></p>
    <p><span class="cinza">Análise da Prioridade</span></p>
  </div>
  <div id="passo2">
    <p><span class="fonte15Passo">Passo 2 &raquo;</span></p>
    <p><span class="cinza">Informações Básicas</span></p>
  </div>
  <div id="passo3">
    <p><span class="fonte15Passo">Passo 3 &raquo;</span></p>
    <p><span class="cinza">Diagrama Causa e Efeito</span></p>
  </div>
  <div id="passo4">
    <p><span class="fonte15Passo">Passo 4 &raquo;</span></p>
    <p><span class="cinza">5 Porquês</span></p>
  </div>
  <div id="passo5">
    <p><span class="fonte15Passo">Passo 5 &raquo;</span></p>
    <p><span class="cinza">Ações</span></p>
  </div>
  <div id="passo6Ativo">
    <p><span class="fonte15Branca">Passo 6 &raquo;</span></p>
    <p><span class="branca">Anexos</span></p>
  </div>
</div>
<div class="clear"></div>
<div id="dadosExplicativos">
  <div id="chamadaPagina"><span class="fonte37">Anexos</span></div>
  <p>Para complementar a sua análise, inclua fotos e figuras do evento.</p>
<p>Lembre-se, uma imagem vale mais que mil palavras. </p>
<p>Para armazenar os arquivos no banco de dados, clique em Procurar...,
selecione o arquivo e clique em Abrir (ou ok dependendo do seu windows), depois clique em Enviar.</p> 
<p>Seus anexos serão automaticamente inseridos no relatório impresso da ferramenta. </p>
</div>
<div id="formAnalise"></div>
<div id="listaAcoes">
  <div class="fundoChamadaBox">Anexos desta Análise</div>
</div>

<?php } else { // se usuário não estiver logado?>
<script language="JavaScript">
	window.location.href = "../index.php";
</script>
<?php }?>
