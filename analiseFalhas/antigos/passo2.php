<?php 
session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado
require("../../conexao.php");
conexao();
$apontador = $_SESSION['apontador'];
$id_analise = $_GET['id'];
$acao = $_GET['acao'];
// Seleciono os dados basico
$select_basico = "SELECT d.*, u.*, u.nome AS unidade_fabril, s.descricao as situacao FROM dados_basico_analise AS d INNER JOIN unidade_fabril AS u ON u.id_unidade = d.id_unidade left join situacao as s on d.id_situacao = s.id_situacao WHERE id_analise = '$id_analise'";
$query_basico = mysql_query($select_basico, $base) or die(mysql_error());
$reg_basico = mysql_fetch_assoc($query_basico);

// Dados Via Formulario
$saude_seguranca = $_POST['saude_seguranca'];
$meio_ambiente = $_POST['meio_ambiente'];
$producao = $_POST['producao'];
$custo_manutencao = $_POST['custo_manutencao'];
$frequencia_evento = $_POST['frequencia_evento'];

// Fim Dados
$risco = ($saude_seguranca + $meio_ambiente + $producao + $custo_manutencao)*$frequencia_evento;// CALCULA O RISCO 
### Calculco Prioridade da Analise ##########
	if ($risco <  500 ){ // Risco menor q 500 
		if ($saude_seguranca < 30) {// Se a Saude e segurança for menor q 30
			if ($meio_ambiente < 30){ // Se Meio Ambiente for menor q 30
				if ($producao < 30){ // Se Produção for menor que 30
					if ($risco <=  100 ){ // Se risco menor igual á 100
						if ($saude_seguranca < 20){ // Se Saude e Segurança menor q 20
							if ($meio_ambiente < 20){ // Se Meio Ambiente menor q 20
								if ($producao < 20){ // Se produção menor que 20
									if ($risco <= 40){ // Se Risco for menor igual á 40
										if ($risco < 29){ // Se risco for menor que 29 
											$criticidade = "E";
										} else { // Se risco for maior que 29 e menor igual á 40
											$criticidade = "D";
										} 
									} // Fim Risco menor igual á 40
									else { // Se risco for maior que 40 e menor igual á 100
										$criticidade = "C";
									}
								} else {// Se produção maior que 20
									$criticidade = "B";
								}
						} else { // Se Meio Ambiente Maior que 20
							$criticidade = "B";
						}
					} else { // Se Saude Maior que 20
						$criticidade = "B";
					}
				} else {// Se risco maior que 100
					$criticidade = "B";
				}
			} else {// Se Produção Maior que 30
				$criticidade = "A";
			}
		} else {// Se Meio Ambiente Maior que 30
			$criticidade = "A";
		}
	}else {// Se Saude Segurança Maior que 30
		$criticidade = "A";
	}
} else {// Se soma for maior que 500 
	$criticidade = "A";
}
###### Fim do Calculo da Prioridade ######	
if($acao == 1){
	// Inserir na tabela de Priorizacao
	$grava_priorizacao = "INSERT INTO priorizacao_falhas (id_analise, saude_seguranca, meio_ambiente, producao, custo_manutencao, frequencia_evento) VALUES ('$id_analise', '$saude_seguranca', '$meio_ambiente', '$producao', '$custo_manutencao', '$frequencia_evento')";
	$executa_priorizacao = mysql_query($grava_priorizacao, $base) or die(mysql_error());
}
// Select na Priorização
$select_priorizacao = "SELECT * FROM priorizacao_falhas WHERE id_analise = '$id_analise'";
$query_priorizacao = mysql_query($select_priorizacao, $base) or die(mysql_error());
$reg_priorizacao = mysql_fetch_assoc($query_priorizacao);

// Faço UPDATE na Tabela Analise para Atualizar a Prioridade 
$update_analise = "UPDATE analise_falhas SET prioridade = '$criticidade' WHERE id_analise = '$id_analise'";
$executa_update_analise = mysql_query($update_analise, $base) or die(mysql_error());
?>

<div id="menuLateralPassos">
  <div id="internaMenuPassos">
    <div id="falhaMenu" class="fonte14Negrito">FALHA</div>
    <?php if($criticidade == "A"){?>
    <div id="criticidadeVermelha">Prioridade <span class="negrito"><?php echo $criticidade;?></span></div>
    <?php }?>
    <?php if($criticidade == "B"){?>
    <div id="criticidadeLaranja">Prioridade <span class="negrito"><?php echo $criticidade;?></span></div>
    <?php }?>
    <?php if($criticidade == "C" or $criticidade == "D" or $criticidade == "E"){?>
    <div id="criticidadeAmarela">Prioridade <span class="negrito"><?php echo $criticidade;?></span></div>
    <?php }?>
    <div class="clear"></div>
    <div id="monstraTituloAnalise"><?php echo $reg_basico['titulo'];?></div>
    <p><span class="fonte14">Unidade Fabril</span><br>
      <span class="fonte14Negrito"><?php echo $reg_basico['unidade_fabril'];?></span></p>
    <p>
    <div class="linhaPassos"></div>
    </p>
    <p><span class="fonte14">Linha de Produção</span><br>
      <span class="fonte14Negrito"><?php echo $reg_basico['linha_producao'];?></span></p>
    <p>
    <div class="linhaPassos"></div>
    </p>
    <p><span class="fonte14">Equipamento</span><br>
      <span class="fonte14Negrito"><?php echo $reg_basico['equipamento'];?></span></p>
    <p>
    <div class="linhaPassos"></div>
    </p>
    <p><span class="fonte14">Data do Evento</span><br>
      <span class="fonte14Negrito"><?php echo $reg_basico['data'];?></span></p>
    <p>
    <div class="linhaPassos"></div>
    </p>
    <p><span class="fonte14">Situação</span><br>
      <span class="fonte14Negrito"><?php echo $reg_basico['situacao'];?></span></p>
  </div>
</div>
<div id="formAnaliseDir">
  <div id="passo_passo">
    <div id="passo1">
      <p><span class="fonte15Passo">Passo 1 &raquo;</span></p>
      <p><span class="cinza">Análise da Prioridade</span></p>
    </div>
    <div id="passo2Ativo">
      <p><span class="fonte15Branca">Passo 2 &raquo;</span></p>
      <p><span class="branca">Informações Básicas</span></p>
    </div>
    <div id="passo3">
      <p><span class="fonte15Passo">Passo 3 &raquo;</span></p>
      <p><span class="cinza">Diagrama Causa e Efeito</span></p>
    </div>
    <div id="passo4">
      <p><span class="fonte15Passo">Passo 4 &raquo;</span></p>
      <p><span class="cinza">5 Porquês</span></p>
    </div>
    <div id="passo5">
      <p><span class="fonte15Passo">Passo 5 &raquo;</span></p>
      <p><span class="cinza">Ações</span></p>
    </div>
    <div id="passo6">
      <p><span class="fonte15Passo">Passo 6 &raquo;</span></p>
      <p><span class="cinza">Anexos</span></p>
    </div>
  </div>
  <div class="clear"></div>
  <div id="dadosExplicativos">
    <div id="chamadaPagina"><span class="fonte37">Informações Básicas</span></div>
    <p>Esta etapa situa as pessoas na análise, informando os dados que servirão de base para o processo de busca da causa raiz da falha.</p>
  </div>
  <form id="enviaDados" name="enviaDados" method="post" action="#" >
    <div id="formAnalise">
      <table id="tabGeral">
        <tr>
          <td align="right">Data Avaliação:</td>
          <td><label for="data_avaliacao"></label>
            <input type="text" name="data_avaliacao" id="data_avaliacao" size="8" maxlength="10" onKeyPress="Mascara('DATA',this,event);"></td>
        </tr>
        <tr>
          <td align="right">Equipe de Avaliação:</td>
          <td><label for="equipe_avaliacao"></label>
            <input name="equipe_avaliacao" type="text" id="equipe_avaliacao" size="60"></td>
        </tr>
        <tr>
          <td align="right" valign="top">Descrição do Evento:</td>
          <td><label for="descricao_evento"></label>
            <textarea name="descricao_evento" id="descricao_evento" cols="55" rows="1"></textarea></td>
        </tr>
        <tr>
          <td align="right" valign="top">Evidências:</td>
          <td><label for="evidencias"></label>
            <textarea name="evidencias" id="evidencias" cols="55" rows="1"></textarea></td>
        </tr>
        <tr>
          <td align="right" valign="top">Causas Básicas:</td>
          <td><label for="causa_basica"></label>
          <textarea name="causa_basica" id="causa_basica" cols="55" rows="1"></textarea></td>
        </tr>
        <tr>
          <td align="right">Item que Falhou:</td>
          <td><label for="componente_falhou"></label>
            <input name="componente_falhou" type="text" id="componente_falhou" size="60"></td>
        </tr>
        <tr>
          <td align="right">Função:</td>
          <td><label for="funcao"></label>
            <input name="funcao" type="text" id="funcao" size="60"></td>
        </tr>
        <tr>
          <td align="right" valign="top">Modo de Falha:</td>
          <td><label for="modo_falha"></label>
            <textarea name="modo_falha" id="modo_falha" cols="55" rows="1"></textarea></td>
        </tr>
        <tr>
          <td align="right" valign="top">Causa Raiz:</td>
          <td><label for="causa_raiz"></label>
            <textarea name="causa_raiz" id="causa_raiz" cols="55" rows="1"></textarea></td>
        </tr>
        <tr>
          <td align="right" valign="top">Observações:</td>
          <td><label for="observacoes"></label>
            <textarea name="observacoes" id="observacoes" cols="55" rows="1"></textarea></td>
        </tr>
      </table>
    </div>
    <p>
      <input name="envia" type="button" id="envia" onclick="geral('enviaDados', 'analiseFalhas/gravaPasso2.php?id=<?php echo $id_analise;?>&amp;acao=1', 'formAnaliseDir');" value="" class="btnGravar"/>
      <input name="envia" type="button" id="envia" onclick="geral('enviaDados', 'analiseFalhas/passo3.php?id=<?php echo $id_analise;?>&amp;acao=1', 'formAnaliseDir');" value="" class="btnProsseguir"/>
    </p>
  </form>
</div>
<div class="clear"></div>
<?php } else { // se usuário não estiver logado?>
<script language="JavaScript">
	window.location.href = "../index.php";
</script>
<?php }?>
