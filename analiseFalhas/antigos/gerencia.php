<?php 
session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado

require("../../conexao.php");
conexao();
if(isset($_GET['p'])){ // Pega a página atual da paginação
	$p = $_GET["p"];
}else {
	$p = 1;
}
if(isset($_GET['acao'])){ // Pego a ação do meu sistema
	$acao = $_GET['acao'];
}else{
	$acao = 0;	
}
/* AÇÕES */
if($acao == 1){ 
} // FIM DA AÇÃO 1
if($acao == 2){
	
} // FIM AÇÃO 2
/* FIM DAS AÇÕES*/

$qnt = 20; // Defina aqui a quantidade máxima de registros por página.
// O sistema calcula o início da seleção calculando: 
// (página atual * quantidade por página) - quantidade por página
$inicio = ($p*$qnt) - $qnt;
// Seleciona no banco de dados com o LIMIT indicado pelos números acima
$sql_select = "SELECT * FROM analise_falhas ORDER BY id_analise DESC LIMIT $inicio, $qnt";
// Executa o Query
$sql_query = mysql_query($sql_select, $base) or die(mysql_error());
$linhas = mysql_num_rows($sql_query);
?>

<div id="chamada" class="chamada37">Análise de Falhas</div>
<div id="menuAjuda">
	<ul>
    	<li><a href="#" onClick="menu('analiseFalhas/insere.php');" title="CADASTRAR UMA NOVA ANALISE">Nova Analise</a></li>
    </ul>
</div>
<?php if($acao != 0){?>
    <div id="acerto">
        <?php if($acao == 1){?>Cadastrado com sucesso!
 				<?php }?>
         <?php if($acao == 2){?>Dados alterados com sucesso.<?php }?>
    </div>
<?php }?>
<?php if($linhas > 0){?>
<p>Abaixo segue todas analises cadastradas no sistema</p><br />
<form id="enviaDados" name="enviaDados" method="post" action="#" >
<table width="100%" cellspacing="0" id="tabGerencia">
  <tr>
    <td bgcolor="#BBBBBB"><span class="negrito">Título</span></td>
    <td bgcolor="#BBBBBB"><span class="negrito">Data Evento</span></td>
    <td bgcolor="#BBBBBB"><span class="negrito">Prioridade</span></td>
    <td colspan="2" bgcolor="#BBBBBB"><span class="negrito">Ações</span></td>
  </tr>
  <?php 
  $num_linha = 0;
  while($reg = mysql_fetch_array($sql_query, MYSQL_ASSOC)){?>
      <tr <?php if($num_linha % 2){?>bgcolor="#F5F5F5"<?php } else {?> <?php }?>>
        <td><a href="#" onclick="menu('analiseFalhas/edita.php?id=<?php echo $reg['id_analise'];?>');" title="EDITAR OS DADOS DESTA ANALISE"><?php echo $reg['titulo'];?></a></td>
        <td><?php echo $reg['data_evento'];?></td>
        <td><?php echo $reg['prioridade'];?></td>
        <td><a href="#" onclick="menu('analiseFalhas/edita.php?id=<?php echo $reg['id_analise'];?>');" title="EDITAR OS DADOS DESTA ANALISE">Editar</a></td>
      </tr>
  <?php 
  $num_linha++;
  }?>
</table>
</form>
<div id="paginacao">
  <?php 
// Faz uma nova seleção no banco de dados, desta vez sem LIMIT, para pegarmos o número total de registros
$sql_select_all = "SELECT * FROM analise_falhas ORDER BY id_analise DESC";
$sql_query_all = mysql_query($sql_select_all); // Executa o query da seleção acimas
$total_registros = mysql_num_rows($sql_query_all); // Gera uma variável com o número total de registros no banco de dados
// Gera outra variável, desta vez com o número de páginas que será precisa. 
// O comando ceil() arredonda 'para cima' o valor
$pags = ceil($total_registros/$qnt);
// Número máximos de botões de paginação
$max_links = 5;
// Exibe o primeiro link 'primeira página', que não entra na contagem acima(3) ?>
  <a href='#' onclick="menu('analiseFalhas/gerencia.php');" class="paginacao"> &laquo; </a>
  <?php 
// Cria um for() para exibir os 3 links antes da página atual
for($i = $p-$max_links; $i <= $p-1; $i++) {
// Se o número da página for menor ou igual a zero, não faz nada
// (afinal, não existe página 0, -1, -2..)
	if($i <=0) {
	//faz nada
	} else { // Se estiver tudo OK, cria o link para outra página ?>
  <a href="#" onclick="menu('analiseFalhas/gerencia.php?p=<?php echo $i; ?>');" class="paginacao"><?php echo $i;?></a>
  <?php
	}
}
// Exibe a página atual, sem link, apenas o número ?>
  <span class="branco"><?php echo $p; ?></span>
  <?php
// Cria outro for(), desta vez para exibir 3 links após a página atual
for($i = $p+1; $i <= $p+$max_links; $i++) {
// Verifica se a página atual é maior do que a última página. Se for, não faz nada.
	if($i > $pags){
	//faz nada
	} else { // Se tiver tudo Ok gera os links. ?>
  <a href="#" onclick="menu('analiseFalhas/gerencia.php?p=<?php echo $i;?>');" class="paginacao"><?php echo $i;?></a>
  <?php
	}
}
// Exibe o link "última página" ?>
  <a href="#" onclick="menu('analiseFalhas/gerencia.php?p=<?php echo $pags;?>');" class="paginacao"> &raquo;</a> 
</div>
<?php } else {?>
	<p class="negrito">Nenhuma analise cadastrada</p>
<?php }?>
<?php } else { // se usuario não estiver logado ?>
	<script language="JavaScript">
			window.location.href = "../index.php";
	</script>
<?php }?>