<?php 
session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado

require("../../conexao.php");
conexao();

$data = date("m/d/Y");
$apontador = $_SESSION['apontador'];

// Seleciono os equipamentos
$select_equipamento = "SELECT * FROM equipamento WHERE apontador = '$apontador'";
$query_equipamento = mysql_query($select_equipamento, $base) or die(mysql_error());
$linhas_equipamento = mysql_num_rows($query_equipamento);

// Seleciono as linhas de produção
$select_linha = "SELECT * FROM linha_producao WHERE apontador = '$apontador'";
$query_linha = mysql_query($select_linha, $base) or die(mysql_error());
$linhas_linha = mysql_num_rows($query_linha);

// Seleciono as unidades Fabris
$select_unidade = "SELECT * FROM unidade_fabril WHERE apontador = '$apontador'";
$query_unidade = mysql_query($select_unidade, $base) or die(mysql_error());
$linhas_unidade = mysql_num_rows($query_unidade);

?>

<div id="box">
  <div id="menuAnalise">
    <ul>
      <li class="menuAnaliseAtivo">Análise da Prioridade</li>
      <li>Informações Básicas</li>
      <li>Diagrama Causa e Efeito</li>
      <li>5 Porquês</li>
      <li>Ações</li>
      <li>Anexos</li>
    </ul>
  </div>
  <div id="barraMenuAnalise"></div>
  <div id="informacaoPasso">
    <p><span class="fonte13">PASSO 1</span> Requisito básico para o cadastramento de um evento, esta etapa cria uma regra para definir qual a ordem de prioridade dentre todas as nossas demandas de estudo.</p>
    <p>Se o impacto for elevado (por exemplo um acidente com afastamento, um incêndio de difícil combate ou uma grande perda por lucro cessante) o evento terá prioridade 'A' mesmo que a sua frequência seja baixa. Os eventos de baixo impacto têm sua prioridade dada pela soma dos impactos multiplicado pela frequência que ocorrem.</p>
  </div>
</div>
<form id="enviaDados" name="enviaDados" method="post" action="#" >
  <div id="box">
    <div id="inputTitulo"><span class="fonte15" id="fonte15">Título </span>
      <input name="titulo" type="text" id="titulo" size="45">
    </div>
    <div id="inputData"><span class="fonte15" id="fonte15">Data</span>
      <input name="data_evento" type="text" id="data_evento" size="8" maxlength="10" onKeyPress="Mascara('DATA',this,event);" value="<?php echo $data;?>">
    </div>
    <div class="clear"></div>
    <div id="inputLadoEsq">
      <div id="inputUnidade"> <span class="fonte15" id="fonte15_2">Unidade</span><br />
        <select name="id_unidade" id="id_unidade">
          <?php if($linhas_unidade > 0){
			  while($reg_unidade = mysql_fetch_assoc($query_unidade)){?>
          <option value="<?php echo $reg_unidade['nome'];?>"><?php echo $reg_unidade['nome'];?></option>
          <?php } } else {?>
          <option value="">NENHUMA UNIDADE...</option>
          <?php }?>
        </select>
      </div>
      <div id="inputProducao"> <span class="fonte15" id="fonte15_2">Produção</span><br />
        <select name="linha_producao" id="linha_producao">
          <?php if($linhas_linha > 0){
			  while($reg_linha = mysql_fetch_assoc($query_linha)){?>
          <option value="<?php echo $reg_linha['nome'];?>"><?php echo $reg_linha['nome'];?></option>
          <?php } } else {?>
          <option value="">NENHUMA LINHAS CADASTRADA...</option>
          <?php }?>
        </select>
      </div>
      <div class="clear"></div>
    </div>
    <div id="inputLadoDir">
      <div id="inputEquipamento"> <span class="fonte15">Equipamento</span><br />
        <select name="equipamento" id="equipamento">
          <?php if($linhas_equipamento > 0){
			  while($reg_equipamento = mysql_fetch_assoc($query_equipamento)){?>
          <option value="<?php echo $reg_equipamento['nome'];?>"><?php echo $reg_equipamento['nome'];?></option>
          <?php } } else {?>
          <option value="">NENHUM EQUIPAMENTO...</option>
          <?php }?>
        </select>
      </div>
      <div id="inputSituacao"> <span class="fonte15">Situação</span><br />
        <label for="situacao"></label>
        <select name="situacao" id="situacao">
          <option value="Aberta">Aberta</option>
          <option value="Em análise">Em análise</option>
          <option value="Concluída">Concluída</option>
          <option value="Cancelada">Cancelada</option>
        </select>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <div id="box">
    <p>Ao preencher as informacoes, considerar o impacto do evento e a possibilidade de impactos maiores.</p>
    <div id="formataBox">
      <div id="nomesCampos">
        <p><span class="fonte15">Saúde e Segurança </span></p>
        <p><span class="fonte15">Meio Ambiente</span></p>
        <p><span class="fonte15">Segurança</span></p>
        <p><span class="fonte15">Custo de Manutenção</span></p>
        <p><span class="fonte15">Frequência do Evento</span></p>
      </div>
      <div id="camposInput">
        <p>
          <select name="saude_seguranca" id="saude_seguranca">
            <option value='40'>Acidente Fatal</option>
            <option value='30'>Acidente Com Afastamento</option>
            <option value='20' >Acidente Sem Afastamento</option>
            <option value='10'>Ocorrencia Ambulatorial</option>
            <option value='0'>Sem impactos</option>
          </select>
        </p>
        <p><select name="meio_ambiente" id="meio_ambiente">
        <option value='40'>Dificil combate, externa a area da empresa</option>
          <option value='30'>Dificil combate, restrita a area da empresa</option>
          <option value='20'>Combativel com auxilio externo</option>
          <option value='10'>Combativel com recursos proprios</option>
          <option value='0'>Sem impactos</option>
      </select></p>
      <p><select name="producao" id="producao">
        <option value='30'>Grande perda por lucro cessante</option>
          <option value='20'>Consider&aacute;vel perda de por lucro cessante</option>
          <option value='10'>Pequena perda por lucro cessante</option>
          <option value='0'>Sem perdas por lucro cessante</option>
      </select></p>
      <p> <select name="custo_manutencao" id="custo_manutencao">
        <option value='20'>Alto</option>
        <option value='10'>Medio</option>
        <option value='5'>Baixo</option>
        <option value='0'>Sem custo</option>
      </select></p>
      <p> <select name="frequencia_evento" id="frequencia_evento">
        <option value='10'>Varios eventos em menos de um m&ecircs </option>
        <option value='8'>Um evento mensal</option>
        <option value='6'>Um evento trimenstral</option>
        <option value='4'>Um evento anual</option>
        <option value='2'>Um evento bianual</option>
        <option value='1'>Mais de 5 anos para cada evento</option>
      </select></p>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  <input name="envia" type="button" id="envia" onclick="basico_falha('enviaDados', 'analiseFalhas/insere2.php?acao=1', 'conteudo');" value="Gravar" class="btnAzul"/>
  <input name="envia" type="button" id="envia" onclick="basico_falha('enviaDados', 'analiseFalhas/insere2.php?acao=2', 'conteudo');" value="Prosseguir &raquo;" class="btnAzul"/>
</form>
<?php } else { // se usuário não estiver logado?>
<script language="JavaScript">
	window.location.href = "../index.php";
</script>
<?php }?>
