<?php 
session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado
require("../conexao.php");
conexao();
$apontador = $_SESSION['apontador'];
$data = date("d/m/Y");
$id_analise = $_GET['id'];

// Seleciona análise
$select_basico = "SELECT d.*, u.*, u.nome AS unidade_fabril, s.descricao as situacao FROM dados_basico_analise AS d INNER JOIN unidade_fabril AS u ON u.id_unidade = d.id_unidade left join situacao as s on d.id_situacao = s.id_situacao WHERE id_analise = '$id_analise'";
$query_basico = mysql_query($select_basico, $base) or die(mysql_error());
$linhas_basico = mysql_num_rows($query_basico);
$reg_basico = mysql_fetch_assoc($query_basico);

// Seleciono as analises
$select_analise = "SELECT * FROM analise_falhas WHERE id_analise = '$id_analise'";
$query_analise = mysql_query($select_analise, $base) or die(mysql_error());
$linhas_analise = mysql_num_rows($query_analise);
$reg_analise = mysql_fetch_assoc($query_analise);
$criticidade = $reg_analise['prioridade'];

// Seleciono os equipamentos
$select_equipamento = "SELECT * FROM equipamento WHERE apontador = '$apontador'";
$query_equipamento = mysql_query($select_equipamento, $base) or die(mysql_error());
$linhas_equipamento = mysql_num_rows($query_equipamento);

// Seleciono as linhas de produção
$select_linha = "SELECT * FROM linha_producao WHERE apontador = '$apontador'";
$query_linha = mysql_query($select_linha, $base) or die(mysql_error());
$linhas_linha = mysql_num_rows($query_linha);

// Seleciono as unidades Fabris
$select_unidade = "SELECT * FROM unidade_fabril WHERE apontador = '$apontador'";
$query_unidade = mysql_query($select_unidade, $base) or die(mysql_error());
$linhas_unidade = mysql_num_rows($query_unidade);

// Seleciono as ações
$select_acao    = "SELECT id_analise, id_situacao FROM acoes WHERE apontador = '$apontador' AND id_analise = '$id_analise' AND (id_situacao = 1 OR id_situacao = 6)";
$query_acao     = mysql_query($select_acao, $base) or die(mysql_error());
$linhas_acao    = mysql_num_rows($query_acao);

// Verificacao de pendências
if($linhas_acao > 0) {
  $select_situacao = "SELECT id_situacao, descricao FROM situacao WHERE status = 'A' AND id_situacao <= 5 AND id_situacao <> 3 AND id_situacao <> 4 order by id_situacao ASC";
} 
else {
  $select_situacao = "SELECT id_situacao, descricao FROM situacao WHERE status = 'A' AND id_situacao <= 5 order by id_situacao ASC";
}
// Seleciono as situações
$query_situacao = mysql_query($select_situacao, $base) or die(mysql_error());
$linhas_situacao = mysql_num_rows($query_situacao);
?>


<div id="internaTopDados"></div>
<div id="internaInterDados">
  <div id="falhaMenu" class="fonte14Negrito">FALHA</div>
  <?php if($criticidade == "A"){?>
  <div id="criticidadeVermelha"><img src="images/PriA.png" width="128" height="31" /></div>
  <?php }?>
  <?php if($criticidade == "B"){?>
  <div id="criticidadeVermelha"><img src="images/PriB.png" width="128" height="31" /></div>
  <?php }?>
  <?php if($criticidade == "C"){?>
  <div id="criticidadeVermelha"><img src="images/PriC.png" width="128" height="32" /></div>
  <?php }?>
  <?php if($criticidade == "D"){?>
  <div id="criticidadeVermelha"><img src="images/PriD.png" width="128" height="32" /></div>
  <?php }?>
  <?php if($criticidade == "E"){?>
  <div id="criticidadeVermelha"><img src="images/PriE.png" width="128" height="32" /></div>
  <?php }?>
  <div class="clear"></div>
  <form id="enviaDados" name="enviaDados" method="post" action="#" >
    <p><div class="espacoLinhas"><span class="fonte14">Título</span></div>
      <input name="titulo" type="text" id="titulo" value="<?php echo $reg_basico['titulo'];?>" />
    </p>
    <div class="linhaPassos"></div>
    <p><div class="espacoLinhas"><span class="fonte14">Unidade Fabril</span> </div>
      <select name="id_unidade" id="id_unidade">
        <option value="<?php echo $reg_basico['id_unidade'];?>"><?php echo $reg_basico['unidade_fabril'];?></option>
        <?php if($linhas_unidade > 0){
			  while($reg_unidade = mysql_fetch_assoc($query_unidade)){?>
       <?php if($reg_basico['unidade_fabril'] != $reg_unidade['nome']){?> <option value="<?php echo $reg_unidade['id_unidade'];?>"><?php echo $reg_unidade['nome'];?></option><?php }?>
        <?php } } else {?>
        <option value="">NENHUMA UNIDADE...</option>
        <?php }?>
      </select>
    </p>
    <div class="linhaPassos"></div>

    <p><span class="fonte14">N&deg;. Ordem Serviço:</span><br>
    <input name="num_sap" type="text" id="num_sap" value="<?php echo $reg_basico['num_sap'];?>" />
    </p>
    <p><div class="linhaPassos"></div></p>
    
    <p><div class="espacoLinhas"><span class="fonte14">Linha de Produção</span></div>
      <select name="linha_producao" id="linha_producao">
        
        
       <option value="<?php echo $reg_basico['linha_producao'];?>"><?php echo $reg_basico['linha_producao'];?></option>
		
		<?php if($linhas_linha > 0){
			  while($reg_linha = mysql_fetch_assoc($query_linha)){?>
        <?php if($reg_basico['linha_producao'] != $reg_linha['nome']){?><option value="<?php echo $reg_linha['nome'];?>"><?php echo $reg_linha['nome'];?></option><?php }?>
        <?php } } else {?>
        <option value="">NENHUMA LINHAS CADASTRADA...</option>
        <?php }?>
      </select>
    </p>
    <div class="linhaPassos"></div>
    <p><div class="espacoLinhas"><span class="fonte14">Equipamento</span></div>
      <select name="equipamento" id="equipamento">
        <option value="<?php echo $reg_basico['equipamento'];?>"><?php echo $reg_basico['equipamento'];?></option>
		<?php if($linhas_equipamento > 0){
			  while($reg_equipamento = mysql_fetch_assoc($query_equipamento)){?>
        <?php if($reg_basico['equipamento'] != $reg_equipamento['nome'] ){?><option value="<?php echo $reg_equipamento['nome'];?>"><?php echo $reg_equipamento['nome'];?></option><?php }?>
        <?php } } else {?>
        <option value="">NENHUM EQUIPAMENTO...</option>
        <?php }?>
      </select>
    </p>
    <div class="linhaPassos"></div>
    <p><div class="espacoLinhas"><span class="fonte14">Data de Evento</span></div>
      <?php
        if (($_SESSION['nivel'] == "1") || ($_SESSION['nivel'] == "3")) {
      ?>
          <input value="<?php echo $reg_basico['data']; ?>" name="data_evento" type="text" id="data_evento" size="12" maxlength="10" onKeyPress="Mascara('DATA',this,event);" onblur="VerificaData(this.value);"/>
      <?php
        } else {
      ?>  
          <input type="hidden" value="<?php echo $reg_basico['data']; ?>" name="data_evento">

          <span class="fonte14Negrito"><?php echo $reg_basico['data'];?></span>
      <?php
        }
      ?>
    </p>
    <div class="linhaPassos"></div>
    <p><div class="espacoLinhas"><span class="fonte14">Situação</span></div>
      <select name="situacao" id="situacao">
<?php 
                    if($linhas_situacao > 0){ while($red_situacao = mysql_fetch_assoc($query_situacao)) { 
                        $selected = ($red_situacao['id_situacao'] === $reg_basico['id_situacao']) ? 'selected' : NULL;
?>
                        <option value="<?php echo $red_situacao['id_situacao'];?>" <?php echo $selected; ?>><?php echo $red_situacao['descricao'];?></option>
<?php	
                    }} 
?>
      </select>
    </p>
    <div class="linhaPassos"></div>
    <p>
      <input name="envia" type="button" id="envia" onclick="geral('enviaDados', 'analiseFalhas/edita2.php?id=<?php echo $id_analise;?>&amp;acao=1', 'internaMenuPassos');" value="" class="btnGravar"/>
    </p>
  </form>
</div>
<div id="rodapeInternaDados"></div>
<?php } else { // se usuário não estiver logado?>
<script language="JavaScript">
	window.location.href = "http://www.telios.eng.br/index.php?deslogado=erro";
</script>
<?php }?>
