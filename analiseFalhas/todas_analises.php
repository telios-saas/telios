<?php
session_start();
require("../conexao.php");
conexao();

$post = new stdClass();
$post->login = isset($_POST['login']) ? $_POST['login'] : null;
$post->senha = isset($_POST['senha']) ? $_POST['senha'] : null;

$executar = TRUE;
if (isset($post->login) && !empty($post->login)) {
    $usuario = mysql_escape_string(addslashes($post->login));
    $senha_md5 = md5(mysql_escape_string(addslashes($post->senha)));
} else if (isset($_SESSION['usuario']) && !empty($_SESSION['usuario'])) {
    $usuario = $_SESSION['usuario'];
    $senha_md5 = $_SESSION['senha_geral'];
} else {
    $executar = FALSE; // Atribuo o valor 0 para a variavel "dizendo que teve ZERO resultados na busca no banco"
}

if ($executar) {
    $select = "SELECT * FROM usuarios WHERE usuario = '$usuario' AND senha = '$senha_md5' AND bloqueado = 'N'";
    $exe = mysql_query($select, $base) or die(mysql_error());
    $reg = mysql_fetch_array($exe, MYSQL_ASSOC);

    $_SESSION['usuario'] = $reg['usuario'];
    $_SESSION['id_usuario'] = $reg['id_usuario'];
    $_SESSION['senha_geral'] = $reg['senha'];
    $_SESSION['nome'] = $reg['nome'];
    $_SESSION['nivel'] = $reg['nivel'];
    $_SESSION['apontador'] = $reg['apontador'];
    $_SESSION['empresa'] = $reg['empresa'];

    // Seleciono os dados do Usuario 
    $id_empresa = $reg['apontador'];
    $select_user = "SELECT * FROM clientes WHERE id_clientes = '$id_empresa'";
    $exe_user = mysql_query($select_user, $base) or die(mysql_error());
    $reg_user = mysql_fetch_array($exe_user, MYSQL_ASSOC);

    $apontador = $_SESSION['apontador'];
    $_SESSION['marca_empresa'] = $reg_user['marca_empresa'];
    
    // Seleciono os equipamentos
    $select_equipamento = "SELECT * FROM equipamento WHERE apontador = '$apontador'";
    $query_equipamento = mysql_query($select_equipamento, $base) or die(mysql_error());
    $linhas_equipamento = mysql_num_rows($query_equipamento);

    // Seleciono as linhas de produção
    $select_linha = "SELECT * FROM linha_producao WHERE apontador = '$apontador'";
    $query_linha = mysql_query($select_linha, $base) or die(mysql_error());
    $linhas_linha = mysql_num_rows($query_linha);

    // Seleciono as unidades Fabris
    $select_unidade = "SELECT * FROM unidade_fabril WHERE apontador = '$apontador'";
    $query_unidade = mysql_query($select_unidade, $base) or die(mysql_error());
    $linhas_unidade = mysql_num_rows($query_unidade);
    
    // Seleciono as situações
    $select_situacao = "SELECT id_situacao, descricao FROM situacao WHERE status = 'A' AND id_situacao <= 5 order by id_situacao ASC";
    $query_situacao = mysql_query($select_situacao, $base) or die(mysql_error());
    $linhas_situacao = mysql_num_rows($query_situacao);

    if (isset($_GET['deleta_analise']) && !empty($_GET['deleta_analise'])) {
        $deleta2 = "ok";
        $id_analise = $_GET['id_analise'];


        $select_analise = "SELECT * FROM dados_basico_analise WHERE id_analise = '$id_analise'";
        $exe_analise = mysql_query($select_analise, $base) or die(mysql_error());
        $reg_analise = mysql_fetch_array($exe_analise, MYSQL_ASSOC);

        $quem_cadastro = $reg_analise['quem_cadastro'];

        $select_pontuacao = "SELECT * FROM usuarios WHERE id_usuario = '$quem_cadastro'";
        $exe_pontuacao = mysql_query($select_pontuacao, $base) or die(mysql_error());
        $reg_pontuacao = mysql_fetch_array($exe_pontuacao, MYSQL_ASSOC);


        $situacao_analise = $reg_analise['id_situacao'];

        $pontuacao = $reg_pontuacao['pontuacao'];

        if ($situacao_analise == 1) {
            $pontuacao_geral = $pontuacao - 5;
        }

        if ($situacao_analise == 2) {
            $pontuacao_geral = $pontuacao - 15;
        }

        if ($situacao_analise == 3) {
            $pontuacao_geral = $pontuacao - 25;
        }

        if ($situacao_analise == 4) {
            $pontuacao_geral = $pontuacao - 100;
        }

        if ($situacao_analise == 5) {
            $pontuacao_geral = $pontuacao;
        }

        $pontuacao_geral;

        $update_pontuacao = "UPDATE usuarios SET pontuacao = '$pontuacao_geral' WHERE id_usuario = '$quem_cadastro'";
        $executa_pontuacao = mysql_query($update_pontuacao, $base) or die(mysql_error());
        
        $deleta_1 = "DELETE FROM analise_falhas WHERE id_analise = '$id_analise'";
        $executa_del_1 = mysql_query($deleta_1, $base) or die(mysql_error());

        $deleta_2 = "DELETE FROM 5_porques WHERE id_analise = '$id_analise'";
        $executa_del_2 = mysql_query($deleta_2, $base) or die(mysql_error());

        $deleta_3 = "DELETE FROM acoes WHERE id_analise = '$id_analise'";
        $executa_del_3 = mysql_query($deleta_3, $base) or die(mysql_error());

        $deleta_4 = "DELETE FROM arquivos_analise WHERE id_analise = '$id_analise'";
        $executa_del_4 = mysql_query($deleta_4, $base) or die(mysql_error());

        $deleta_5 = "DELETE FROM causa_efeito WHERE id_analise = '$id_analise'";
        $executa_del_5 = mysql_query($deleta_5, $base) or die(mysql_error());

        $deleta_6 = "DELETE FROM dados_basico_analise WHERE id_analise = '$id_analise'";
        $executa_del_6 = mysql_query($deleta_6, $base) or die(mysql_error());

        $deleta_7 = "DELETE FROM priorizacao_falhas WHERE id_analise = '$id_analise'";
        $executa_del_7 = mysql_query($deleta_7, $base) or die(mysql_error());
    }

    $menuAtivo = 'analise2';
    include '../menu_top.php';
    ?>
    <!-- FIM MENU TOP -->
    <div id="geral">
        <div id="chamadaInternaAcoes"><p class="fonte37">Gerenciar Análises</p></div>
        <div id="menuLat">
            <div class="fundoChamadaBox">Situa&ccedil;&atilde;o das An&aacute;lises </div>
            <div id="dadosGerais">
                <div id="espacoGrafico"></div>
                <div id="graficoTodasAnalises">
                    <!-- GRAFICO DINAMIZADO -->
                </div>
                <div class="clear"></div>
                <div id="resultadoTotal"><span class="fonte12Azul">Total : </span><span id="resultadoTotalQtd" class="fonte17">0</span></div>
            </div>
            <div id="dadosGeraisBotton"></div>
            <div id="fundoChamadaAviso">Avisos</div>
            <div id="avisosImportantes">
                <div id="conteudoAviso">
                    <p>Fique atento aos avisos que
                        aparecer&atilde;o em vermelho no topo das
                        p&aacute;ginas. Ao ser incluido em novas
                        a&ccedil;&otilde;es ou durante o processo de
                        cadastramento de falhas, voc&ecirc; receber&aacute; estes alertas. </p>
                </div>
            </div>
            <div id="dadosGeraisBotton"></div>
        </div>
        <div id="conteudo">
            <?php

            if ($_SESSION['nivel'] == "3") {
                $nome_acoes = $_SESSION['nome'];
                $select_acoes = "SELECT * FROM acoes WHERE responsavel = '$nome_acoes'";
                $query_acoes = mysql_query($select_acoes, $base) or die(mysql_error());
                $linhas_acoes = mysql_num_rows($query_acoes);
                if ($linhas_acoes > 0) {
                    ?>
                    <div id="avisoNovasAcoes">Ol&aacute; <?php echo $nome_acoes; ?>, voc&ecirc; foi inclu&iacute;do em <?php echo $linhas_acoes; ?> novas a&ccedil;&otilde;es, clique aqui para visualiz&aacute;-las</div>
                    <?php
                }
            }
            if (isset($deleta2)) {
                $deleta2 = "ok";
                if ($deleta == 'ok') {
                    ?>
                    <div id="acerto2">
                        Análise deletada com Sucesso!
                    </div>

                    <p>&nbsp; </p>
                    <?php 
                }
            } 
            ?>        
        <div id="dvGerenciarAnalises" class="boxConteudoDireitoSemMargin bAll">
            <div class="tituloConteudoDireito bTop">Buscar An&aacute;lises por:</div>
                <div class="conteudo">
                    <div class="campo-6-12">
                        <label for="ddlUnidadeFabril" class="campo-label">Unidade Fabril</label>    
                        <select id="ddlUnidadeFabril" name="ddlUnidadeFabril" style="width: 100%;">
                            <option value="" selected>Todos</option>
                            <?php 
                            if($linhas_unidade > 0){ 
                                while($reg_unidade = mysql_fetch_assoc($query_unidade)) { 
                                ?>
                                <option value="<?php echo $reg_unidade['id_unidade'];?>"><?php echo $reg_unidade['nome'];?></option>
                                <?php
                                }
                            } 
                            ?>
                        </select>
                    </div>
                    <div class="campo-6-12">
                        <label for="ddlLinhaProducao" class="campo-label">Linha de Produ&ccedil;&atilde;o</label> 
                        <select id="ddlLinhaProducao" name="ddlLinhaProducao" style="width: 100%;">
                            <option value="" selected>Todos</option>
                            <?php 
                            if ($linhas_linha > 0) { 
                                while ($reg_linha = mysql_fetch_assoc($query_linha)) {
                                ?>
                                <option value="<?php echo htmlspecialchars($reg_linha['nome']); ?>"><?php echo $reg_linha['nome']; ?></option>
                                <?php	
                                }
                            } 
                            ?>
                        </select>
                    </div>
                    <div class="clear"></div>
                    <div class="campo-6-12">
                        <label for="ddlEquipamento" class="campo-label">Equipamento</label>
                        <select id="ddlEquipamento" name="ddlEquipamento" style="width: 100%;">
                            <option value="" selected>Todos</option>
                            <?php 
                            if ($linhas_equipamento > 0) { 
                                while ($reg_equipamento = mysql_fetch_assoc($query_equipamento)) {
                                ?>
                                <option value="<?php echo htmlspecialchars($reg_equipamento['nome']);?>"><?php echo $reg_equipamento['nome'];?></option>
                                <?php	
                                }
                            } 
                            ?>
                        </select>
                    </div>
                    <div class="campo-3-12">
                        <label for="ddlSituacao" class="campo-label">Situa&ccedil;&atilde;o da An&aacute;lise</label>
                        <select id="ddlSituacao" name="ddlSituacao" style="width: 100%;">
                            <option value="" selected>Todos</option>
                            <?php 
                            if($linhas_situacao > 0){ 
                                while($red_situacao = mysql_fetch_assoc($query_situacao)) { 
                                    ?>
                                    <option value="<?php echo $red_situacao['id_situacao'];?>"><?php echo $red_situacao['descricao'];?></option>
                                    <?php	
                                }
                            } 
                            ?>
                        </select>
                    </div>
                    <div class="campo-3-12">
                        <label for="ddlPrioridade" class="campo-label">Prioridade</label>
                        <select id="ddlPrioridade" name="ddlPrioridade" style="width: 100%;">
                            <option value="" selected>Todos</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                            <option value="E">E</option>
                        </select>
                    </div>

                    <div style="float: left; padding: 0 5px; width: 96%;">
                      <label for="txtTituloAnalise" class="campo-label">Título da Análise</label>
                      <input type="text" name="txtTituloAnalise" id="txtTituloAnalise" style="width:100%"/>
                    </div>
                </div>
            </div>
        <div id="dvGerenciarAnalises" class="boxConteudoDireito bAll">
            <div class="tituloConteudoDireito bTop">Gerenciamento das An&aacute;lises Cadastradas</div>
            <div  id="conteudoPesquisa" class="conteudo">
                <!-- AJAX GERENCIAR ANALISES -->
            </div>
        </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<?php 
    } else { 
?>
        <script language="JavaScript">
            window.location.href = "index.php?acao=erro";
            //window.location.href = "index.php;
        </script>
<?php 
    } 
?>