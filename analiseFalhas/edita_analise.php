<?php 
session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado
require("../conexao.php");
conexao();
$apontador = $_SESSION['apontador'];
$id_analise = mysql_escape_string(addslashes($_GET['id']));
$acao = mysql_escape_string(addslashes($_GET['acao']));
$id_acao = mysql_escape_string(addslashes($_GET['id_acao']));

// Seleciono os dados basico
$select_basico = "SELECT d.*, u.*, u.nome AS unidade_fabril, s.descricao as situacao FROM dados_basico_analise AS d INNER JOIN unidade_fabril AS u ON u.id_unidade = d.id_unidade left join situacao as s on d.id_situacao = s.id_situacao WHERE id_analise = '$id_analise'";
$query_basico = mysql_query($select_basico, $base) or die(mysql_error());
$reg_basico = mysql_fetch_assoc($query_basico);

$select_analise = "SELECT * FROM analise_falhas WHERE id_analise = '$id_analise'";
$query_analise = mysql_query($select_analise, $base) or die(mysql_error());
$reg_analise = mysql_fetch_assoc($query_analise);
$criticidade = $reg_analise['prioridade'];

// Select na Priorização
$select_priorizacao = "SELECT * FROM priorizacao_falhas WHERE id_analise = '$id_analise'";
$query_priorizacao = mysql_query($select_priorizacao, $base) or die(mysql_error());
$reg_priorizacao = mysql_fetch_assoc($query_priorizacao);

// Seleciono a Ação
$select_acoes = "SELECT * FROM acoes WHERE id_acao = '$id_acao'";
$query_acoes = mysql_query($select_acoes, $base) or die(mysql_error());
$reg_acoes = mysql_fetch_assoc($query_acoes);

$nome_empresa = $_SESSION['empresa'];

// Seleciono os usuarios
  $id_unidade_fabril = $reg_basico['id_unidade'];
  //$select_usuario = "SELECT * FROM user_clientes WHERE empresa = '$nome_empresa'";
  $select_usuario = "SELECT * FROM user_clientes AS uc INNER JOIN usuarios as u ON uc.id_login = u.id_usuario 
  WHERE uc.empresa = '$nome_empresa' and u.bloqueado = 'N'";
  /*$select_usuario = "SELECT 
                      * 
                   FROM 
                      user_clientes AS uc INNER JOIN usuarios AS u ON uc.id_login = u.id_usuario 
                   WHERE 
                      (uc.id_empresa = '$apontador' AND u.bloqueado = 'N' AND uc.id_unidade_fabril = '$id_unidade_fabril') OR u.id_usuario = '".$reg_basico['id_usuario']."'
                   ORDER BY 
                      u.nome";
    */               

$query_usuario = mysql_query($select_usuario, $base) or die(mysql_error());
$linhas_usuario = mysql_num_rows($query_usuario);

// Seleciono as situações
$select_situacao = "SELECT id_situacao, descricao FROM situacao WHERE status = 'A' AND id_situacao IN(1,3,5,6) order by id_situacao ASC";
$query_situacao = mysql_query($select_situacao, $base) or die(mysql_error());
$linhas_situacao = mysql_num_rows($query_situacao);
?>

<?php 
    $menuAtivo = 'acoes';
    include '../menu_top.php'; 
?>
<!-- FIM MENU TOP -->
<div id="geral">
  <div id="menuLateralPassos">
    <div id="internaMenuPassos">
      <div id="internaTopDados"></div>
      <div id="internaInterDados">
        <div id="falhaMenu" class="fonte14Negrito">FALHA</div>
        <?php if($criticidade == "A"){?>
        <div id="criticidadeVermelha"><img src="images/PriA.png" width="128" height="31" /></div>
        <?php }?>
        <?php if($criticidade == "B"){?>
        <div id="criticidadeVermelha"><img src="images/PriB.png" width="128" height="31" /></div>
        <?php }?>
        <?php if($criticidade == "C"){?>
        <div id="criticidadeVermelha"><img src="images/PriC.png" width="128" height="32" /></div>
        <?php }?>
        <?php if($criticidade == "D"){?>
        <div id="criticidadeVermelha"><img src="images/PriD.png" width="128" height="32" /></div>
        <?php }?>
        <?php if($criticidade == "E"){?>
        <div id="criticidadeVermelha"><img src="images/PriE.png" width="128" height="32" /></div>
        <?php }?>
        <div class="clear"></div>
        <div id="monstraTituloAnalise"><?php echo $reg_basico['titulo'];?></div>
        <p><span class="fonte14">Unidade Fabril</span><br>
          <span class="fonte14Negrito"><?php echo $reg_basico['unidade_fabril'];?></span></p>
        <p>
        <div class="linhaPassos"></div>
        </p>
        <p><span class="fonte14">Linha de Produção</span><br>
          <span class="fonte14Negrito"><?php echo $reg_basico['linha_producao'];?></span></p>
        <p>
        <div class="linhaPassos"></div>
        </p>
        <p><span class="fonte14">Equipamento</span><br>
          <span class="fonte14Negrito"><?php echo $reg_basico['equipamento'];?></span></p>
        <p>
        <div class="linhaPassos"></div>
        </p>
        <p><span class="fonte14">Data do Evento</span><br>
          <span class="fonte14Negrito"><?php echo $reg_basico['data'];?></span></p>
        <p>
        <div class="linhaPassos"></div>
        </p>
        <p><span class="fonte14">Situação</span><br>
          <span class="fonte14Negrito"><?php echo $reg_basico['situacao'];?></span></p>
        <div id="btnAlterarAnalise"> <a href="#" onclick="menuLat('analiseFalhas/edita1.php?id=<?php echo $id_analise;?>');"> <img src="images/Editar_dados.png" width="128" height="26" /> </a>
          <div class="clear"></div>
        </div>
      </div>
      <div id="rodapeInternaDados"></div>
    </div>
  </div>
  <div id="formAnaliseDir">
    <div id="passo_passo"> <a href="#" onclick="passo_link('analiseFalhas/edita_passo1.php?id=<?php echo $id_analise?>&amp;acao=1');" class="btnPassoLink">
      <div id="passo1"> </div>
      </a> <a href="#" onclick="passo_link('analiseFalhas/edita_passo2.php?id=<?php echo $id_analise?>&amp;acao=1');" class="btnPassoLink">
      <div id="passo2"> </div>
      </a> <a href="#" onclick="menu('analiseFalhas/edita_passo3.php?id=<?php echo $id_analise?>&amp;acao=1');" class="btnPassoLink">
      <div id="passo3"> </div>
      </a>
      <div id="passo4Ativo"> </div>
      <a href="#" onclick="menu('analiseFalhas/edita_passo6.php?id=<?php echo $id_analise?>&amp;acao=1');" class="btnPassoLink">
      <div id="passo5"> </div>
      </a> </div>
    <div class="clear"></div>
    <div id="dadosExplicativos">
      <div id="chamadaPagina"><span class="fonte37">Ações de Bloqueio</span> <a href="#" class="dcontexto"><span>
        <p>É Para evitar recorrência de eventos, temos que romper a cadeia da falha de alguma forma e quanto mais próximo da raiz do problema, mais econômico se torna a AÇÃO DE BLOQUEIO.</p>
        <p>Esta etapa representa uma grande vantagem deste processo integrado de RCA, ela permite que o usuário crie sua lista de pendências referentes ao evento analisado e acompanhe a sua implementação. </p>
        <p>As ações aqui elencadas ficarão relacionadas no banco de dados e serão facilmente gerenciadas pelo sistema. </p>
        </span><img src="images/AjudaG.gif" width="16" height="16" class="btnDuvida"/></a></div>
    </div>
    <div id="loadingAcao">
      <div id="formAnalise">
        <div id="formTopAnalise"></div>
        <div id="internoForm">
          <form id="enviaDados" name="enviaDados" method="post" action="#" >
            <table id="tabGeral">
              <tr>
                <td align="right" valign="top">Descrição*:</td>
                <td><textarea name="descricao" id="descricao" cols="65" rows="2"><?php echo $reg_acoes['descricao'];?></textarea></td>
              </tr>
              <tr>
                <td align="right" valign="top">Resposta*:</td>
                <td><textarea name="resposta" id="resposta" cols="65" rows="2"><?php echo $reg_acoes['resposta'];?></textarea></td>
              </tr>
              <tr>
                <td align="right">Responsável*:</td>
                <td><select name="responsavel" id="responsavel">
                    
                    <?php if($linhas_usuario > 0){
			  while($reg_usuario = mysql_fetch_assoc($query_usuario)){?>
                    <option value="<?php echo $reg_usuario['nome'];?>" <?php if($reg_acoes['responsavel'] == $reg_usuario['nome']){?> selected="selected" <?php }?> ><?php echo $reg_usuario['nome'];?></option>
                    <?php } } else {?>
                    <option value="">NENHUM RESPONSÁVEL CADASTRADO...</option>
                    <?php }?>
                  </select></td>
              </tr>
              <tr>
                <td align="right">Prazo*:</td>
                <td><input name="prazo" type="text" id="prazo" size="12" maxlength="10" onKeyPress="Mascara('DATA',this,event);"  onblur="VerificaData(this.value);"value="<?php echo $reg_acoes['prazo'];?>"/></td>
              </tr>
              <tr>
                <td align="right">Situação*:</td>
                <td>
                    <select name="situacao" id="situacao">
                  <?php 
                    if($linhas_situacao > 0){ while($red_situacao = mysql_fetch_assoc($query_situacao)) { 
                        $selected = ($red_situacao['id_situacao'] === $reg_acoes['id_situacao']) ? 'selected' : NULL;
                  ?>
                        <option value="<?php echo $red_situacao['id_situacao'];?>" <?php echo $selected; ?>><?php echo $red_situacao['descricao'];?></option>
                  <?php	
                    }} 
                  ?>
                    </select>
                  </select></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>* Campos Obrigatórios</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><input name="envia" type="button" id="envia" onclick="cadastra_acoes('enviaDados', 'analiseFalhas/gravaAcao.php?id=<?php echo $id_analise;?>&amp;acao=2&amp;id_acao=<?php echo $id_acao;?>', 'loadingAcao');" value="" class="btnGravar"/></td>
              </tr>
            </table>
          </form>
        </div>
        <div id="rodapeFormAnalise"></div>
      </div>
      <div id="listaAcoes">
        <div id="formTopAnalise"></div>
        <div id="internoForm">
          <?php 
          $sql_tabela = "SELECT a.id_acao AS acao_id,
                                a.descricao AS acao_descricao,
                                a.resposta AS acao_resposta,
                                a.responsavel AS acao_responsavel,
                                a.prazo AS acao_prazo,
                                s.id_situacao AS situacao_id,
                                s.descricao AS situacao_descricao 
            FROM acoes a left join situacao s on s.id_situacao = a.id_situacao WHERE id_analise = '$id_analise'";
	           /*$sql_tabela = "SELECT   a.id_acao AS acao_id,
                                a.descricao AS acao_descricao,
                                a.resposta AS acao_resposta,
                                a.responsavel AS acao_responsavel,
                                a.prazo AS acao_prazo,
                                s.id_situacao AS situacao_id,
                                s.descricao AS situacao_descricao
                        FROM acoes AS a
                        INNER JOIN situacao AS s
                                ON a.id_situacao = s.id_situacao
                        WHERE id_analise = '$id_analise'";
	          if($_SESSION['nivel'] == "2"){
              $sql_tabela .=  "AND responsavel = '".$_SESSION['nome']."'";
	          }*/
          	$query_acoes2 = mysql_query($sql_tabela, $base) or die(mysql_error());
          	$linhas_acoes2 = mysql_num_rows($query_acoes2);
          	
          	if($linhas_acoes2){
        	?>
          <table width="100%" cellspacing="0" id="tabAnalise">
            <tr>
                <td bgcolor="#dfe8fa" class="negrito">Descrição da Ação </td>
                <td bgcolor="#dfe8fa" class="negrito">Resposta </td>
                <td width="170" bgcolor="#dfe8fa" class="negrito">Responsável</td>
                <td width="85" bgcolor="#dfe8fa" class="negrito">Prazo</td>
                <td colspan="3" bgcolor="#dfe8fa" class="negrito">Situação</td>
            </tr>
            <?php while($reg_acoes2 = mysql_fetch_assoc($query_acoes2)){?>
            <tr>
              <td><a href="#" onclick="edita_acao('analiseFalhas/editaAcao.php?id_acao=<?php echo $reg_acoes2['acao_id'];?>&amp;id_analise=<?php echo $id_analise;?>');"><?php echo $reg_acoes2['acao_descricao'];?></a></td>
              <td><?php echo $reg_acoes2['acao_resposta'];?></td>
              <td><?php echo $reg_acoes2['acao_responsavel'];?></td>
              <td><?php echo $reg_acoes2['acao_prazo'];?></td>
              <td width="100"><?php echo $reg_acoes2['situacao_descricao'];?></td>
              <td width="15"><a href="#" onclick="edita_acao('analiseFalhas/editaAcao.php?id_acao=<?php echo $reg_acoes2['acao_id'];?>&amp;id_analise=<?php echo $id_analise;?>');"><img src="images/btnEditar.jpg" width="15" height="16"></a></td>
              <td width="15"><a href="#" onclick="edita_acao('analiseFalhas/deletaAcao.php?id_acao=<?php echo $reg_acoes2['acao_id'];?>&amp;id_analise=<?php echo $id_analise;?>');"><img src="images/btnDeletar.gif" width="14" height="15"></a></td>
            </tr>
            <?php }?>
          </table>
          <?php } else {?>
          <p class="negrito">Nenhuma ação cadastrada</p>
          <?php }?>
        </div>
        <div id="rodapeFormAnalise"></div>
      </div>
    </div>
    </form>
  </div>
  <div class="clear"></div>
</div>
<?php } else { // se usuário não estiver logado?>
<script language="JavaScript">
	window.location.href = "http://www.telios.eng.br/index.php?deslogado=erro";
</script>
<?php }?>
