<?php 
session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado
require("../../conexao.php");
conexao();
$id_analise = $_GET['id'];
$acao = $_GET['acao'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/reset.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../../css/padrao.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript">
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}
</script>
</head>

<body>
<div id="formAnalise">
  <div id="formTopAnalise"></div>
  <div id="internoForm">
  <div id="mostraLoader">
  <p><img src="../../images/upload/uploading.gif"/></p>
  <p>Aguarde seu arquivo está sendo enviado ao servidor.</p>
  </div>
    <form action="enviaAnexo.php?id=<?php echo $id_analise;?>" method="post" enctype="multipart/form-data" onsubmit="MM_showHideLayers('mostraLoader','','show')">
      <table cellspacing="0" id="tabGeral">
        <tr>
          <td align="right">Arquivo:</td>
          <td><input type="file" name="arquivo" /></td>
        </tr>
        <tr>
          <td align="right" valign="top">Descrição:</td>
          <td><label for="descricao"></label>
            <textarea name="descricao" id="descricao" cols="60" rows="2"></textarea></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input type="submit" value="" class="btnGravar" /></td>
        </tr>
      </table>
    </form>
  </div>
  <div id="rodapeFormAnalise"></div>
</div>
<div class="fundoChamadaBoxG">Anexo desta Análises</div>
<div id="boxPesquisa2">
  <div id="conteudoPesquisa2">
    <?php
// Pasta onde o arquivo vai ser salvo
$_UP['pasta'] = '../uploads/';

// Tamanho máximo do arquivo (em Bytes)
$_UP['tamanho'] = 1024 * 1024 * 20; // 20Mb

// Array com as extensões permitidas
$_UP['extensoes'] = array('jpg', 'png', 'gif', 'doc', 'pdf', 'rar', 'docx', 'xls', 'xlsx', 'bmp');

// Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
$_UP['renomeia'] = false;

// Array com os tipos de erros de upload do PHP
$_UP['erros'][0] = 'Não houve erro';
$_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
$_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
$_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
$_UP['erros'][4] = 'Não foi feito o upload do arquivo';

// Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
if ($_FILES['arquivo']['error'] != 0) {
	die("Não foi possível fazer o upload, erro:<br />" . $_UP['erros'][$_FILES['arquivo']['error']]);
	exit; // Para a execução do script
}

// Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar

// Faz a verificação da extensão do arquivo
$extensao = strtolower(end(explode('.', $_FILES['arquivo']['name'])));
$tamanho_arquivo = $_FILES['arquivo']['size'];

if (array_search($extensao, $_UP['extensoes']) === false) { ?>
    <div id="avisoNovasAcoes">Por favor, envie arquivos com as seguintes extensões: jpg, png, gif.</div>
    <?php }

// Faz a verificação do tamanho do arquivo
else if ($_UP['tamanho'] < $_FILES['arquivo']['size']) { ?>
    <div id="avisoNovasAcoes">O arquivo enviado é muito grande, envie arquivos de até 20 Mb.</div>
    <?php }

// O arquivo passou em todas as verificações, hora de tentar movê-lo para a pasta
else {
// Primeiro verifica se deve trocar o nome do arquivo
if ($_UP['renomeia'] == true) {
// Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .jpg
	$nome_final = time().'.jpg';
} else {
// Mantém o nome original do arquivo
	$nome_final = $_FILES['arquivo']['name'];
	$verefica_jpg = substr($nome_final,-3);
	if($verefica_jpg == "JPG" or $verefica_jpg == "jpg" or $verefica_jpg == "GIF" or $verefica_jpg == "gif" or $verefica_jpg == "PNG" or $verefica_jpg == "png" or $verefica_jpg == "BMP" or $verefica_jpg == "bmp"){
		 $nome_final = time().'.jpg';
	}
}

// Depois verifica se é possível mover o arquivo para a pasta escolhida

if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $_UP['pasta'] . $nome_final)) { // UPLOAD ARQUIVO COM SUCESSO 
	
	$verefica_jpg = substr($nome_final,-3);
	$nome_atual = substr($nome_final,0,-4);
	$tipo_arquivo = 0;

	if($verefica_jpg == "JPG" or $verefica_jpg == "jpg" or $verefica_jpg == "GIF" or $verefica_jpg == "gif" or $verefica_jpg == "PNG" or $verefica_jpg == "png" or $verefica_jpg == "BMP" or $verefica_jpg == "bmp"){ // MOATRA SE É UMA IMAGEM
   		
		$tipo_arquivo = 1;
		$size = getimagesize($nome_final);	
   		if($size[0] >= 480 and $size[1] >= 400){ // VEREFICA O TAMANHO DA IMAGEM EM PX

			include('../m2brimagem.class.0.6.2.php'); 
			$oImg = new m2brimagem();
			$oImg->carrega($nome_final);
			
			$nome_final = time().'.jpg';
			$nome_final = $nome_atual.'.jpg';
			$valida = $oImg->valida();  
            if ($valida == 'OK') {  
               $oImg->redimensiona(400); 
               $oImg->grava($nome_final,true);
			   
            } 
			
			$oImg2 = new m2brimagem();
			$oImg2->carrega($nome_final);
			$nome_final = time().'.jpg';
			$nome_final = $nome_atual.'.jpg';
				if ($valida == 'OK') {  
					$oImg2->redimensiona(400, 300); 
            		$oImg2->grava($nome_final,true); 
			
				}
			
		    //$nome_final = $nome_atual.'.jpg';
					    
			$data = date("d/m/Y");
		    $descricao = $_POST['descricao'];
		    $grava = "INSERT INTO arquivos_analise (id_analise, nome, data, descricao) VALUES ('$id_analise', '$nome_final', '$data', '$descricao')";
		    $executa = mysql_query($grava, $base) or die(mysql_error()); ?>
			<div id="gravadoSucesso">Arquivo anexado com sucesso!</div>
	   <?php }else {   ?> 
			<div id="erroSistema">Atenção! Sua imagem é menor que 450px X 350px.</div>
		<?php } // FIM DA VERIFICAÇÂO DO TAMANHO
	}  
	
	if($tipo_arquivo == 0){ // se o arquivo não for imagem aqui grava e mostra a mensagem
	
		$data = date("d/m/Y");
		$descricao = $_POST['descricao'];
		$grava = "INSERT INTO arquivos_analise (id_analise, nome, data, descricao) VALUES ('$id_analise', '$nome_final', '$data', '$descricao')";
		$executa = mysql_query($grava, $base) or die(mysql_error());
	
	 ?>
    <div id="gravadoSucesso">Arquivo anexado com sucesso!</div>
<?php  }
} else { ?>
    <div id="erroSistema">Não foi possível enviar o arquivo, tente novamente</div>
<?php }
}

$select = "SELECT * FROM arquivos_analise WHERE id_analise = '$id_analise'";
$query = mysql_query($select, $base) or die(mysql_error());
$linhas = mysql_num_rows($query);

?>
    <?php if($linhas > 0){?>
    <table width="100%" cellspacing="0" id="tabAnexo_2">
      <tr>
        <td width="200" bgcolor="#dfe8fa" class="negrito" id="tabAnexo_2">Arquivo</td>
        <td bgcolor="#dfe8fa" class="negrito" id="tabAnexo_2">Descrição</td>
        <td width="80" align="center" bgcolor="#dfe8fa" class="negrito">Data</td>
        <td width="50" align="center" bgcolor="#dfe8fa" class="negrito">Ações</td>
      </tr>
      <?php while($reg = mysql_fetch_assoc($query)){?>
      <tr>
        <td id="tabAnexo_2"><a href="../uploads/<?php echo $reg['nome'];?>" target="_blank"><?php echo $reg['nome'];?></a></td>
        <td id="tabAnexo_2"><?php echo nl2br($reg['descricao']);?></td>
        <td><?php echo $reg['data'];?></td>
        <td align="center"><a href="../insereArquivo.php?id=<?php echo $id_analise;?>&amp;acao=2&amp;id_arquivo=<?php echo $reg['id_arquivo'];?>"><img src="../../images/btnDeletar.gif" width="14" height="15" /></a></td>
      </tr>
      <?php }?>
    </table>
    <?php } else {?>
    <p class="negrito">Nenhum arquivo cadastrado!</p>
    <?php }?>
  </div>
</div>
<div id="boxPesquisaBotton"></div>

<p> 
<div id="btnsDireita">
         <div id="btnRelatorio"><a href="../../geraPDF.php?id=<?php echo $id_analise;?>" target="_blank"><div class="btnRelatorio"></div></a></div>
        <div id="btnExcluirAnalise"><a href="../deleta.php?id=<?php echo $id_analise;?>"><div class="btnExcluiAnalise"></div></a></div>
        <div class="clear"></div>
      </div>
     <div class="clear"></div>
</p>


<!--<p class="btnSalvarAnalise"><a href="passo6.php?id=<?php //echo $id_analise;?>"><img src="../images/btnSalvarAnalise.jpg" width="121" height="30" /></a></p>-->
</body>
</html>
<?php } else { // se usuário não estiver logado?>
<script language="JavaScript">
	window.location.href = "http://www.telios.eng.br/index.php?deslogado=erro";
</script>
<?php }?>
