<?php
session_start();
if(isset($_SESSION['empresa'])){
	
include "config.inc";
include ('src/class.ezpdf.php');
$_SESSION['id_analise'] = mysql_escape_string(addslashes($_GET['id']));

function troca($x){
   $x = str_replace('ç','&ccedil',$x);
   $x = str_replace('Ç','&Ccedil',$x);
   $x = str_replace('á','&aacute',$x);
   $x = str_replace('é','&eacute',$x);
   $x = str_replace('í','&iacute',$x);
   $x = str_replace('ó','&oacute',$x);
   $x = str_replace('ú','&uacute',$x);
   $x = str_replace('Á','&Aacute',$x);
   $x = str_replace('É','&Eacute',$x);
   $x = str_replace('Í','&Iacute',$x);
   $x = str_replace('Ó','&Oacute',$x);
   $x = str_replace('Ú','&Uacute',$x);
   $x = str_replace('ã','&atilde',$x);
   $x = str_replace('õ','&otilde',$x);
   $x = str_replace('Ã','&Atilde',$x);
   $x = str_replace('Õ','&Otilde',$x);
   $x = str_replace('à','&agrave',$x);
   $x = str_replace('À','&Agrave',$x);
   $x = str_replace('â','&acirc',$x);
   $x = str_replace('ê','&ecirc',$x);
   $x = str_replace('ô','&ocirc',$x);
   $x = str_replace('Â','&Acirc',$x);
   $x = str_replace('Ê','&Ecirc',$x);
   $x = str_replace('Ô','&Ocirc',$x);
   $x = str_replace('ä','&auml',$x);
   $x = str_replace('ë','&euml',$x);
   $x = str_replace('ï','&iuml',$x);
   $x = str_replace('ö','&ouml',$x);
   $x = str_replace('ü','&uuml',$x);
   $x = str_replace('Ä','&Auml',$x);
   $x = str_replace('Ë','&Euml',$x);
   $x = str_replace('Ï','&Iuml',$x);
   $x = str_replace('Ö','&Ouml',$x);
   $x = str_replace('Ü','&Uuml',$x);
   $x = str_replace('ñ','&ntilde',$x);
   $x = str_replace('Ñ','&Ntilde',$x);
   $x = str_replace('~~~',' ',$x);
   $x = str_replace('percentpercentpercent','%',$x);
   return $x;
}

function getTextWidth($size,$text,$pdf){
// this function should not change any of the settings, though it will need to
// track any directives which change during calculation, so copy them at the start
// and put them back at the end.
  $store_currentTextState = $pdf->currentTextState;
  if (!$pdf->numFonts){
    $pdf->selectFont('src/fonts/Helvetica');
  }

  // converts a number or a float to a string so it can get the width
  $text = "$text";

  // hmm, this is where it all starts to get tricky - use the font information to
  // calculate the width of each character, add them up and convert to user units
  $w=0;
  $len=strlen($text);
  $cf = $pdf->currentFont;
  for ($i=0;$i<$len;$i++){
    $f=1;
    $directive = $pdf->PRVTcheckTextDirective($text,$i,$f);
    if ($directive){
      if ($f){
        $pdf->setCurrentFont();
        $cf = $pdf->currentFont;
      }
      $i=$i+$directive-1;
    } else {
      $char=ord($text[$i]);
      if (isset($pdf->fonts[$cf]['differences'][$char])){
        // then this character is being replaced by another
        $name = $pdf->fonts[$cf]['differences'][$char];
        if (isset($pdf->fonts[$cf]['C'][$name]['WX'])){
          $w+=$pdf->fonts[$cf]['C'][$name]['WX'];
        }
      } else if (isset($pdf->fonts[$cf]['C'][$char]['WX'])){
        $w+=$pdf->fonts[$cf]['C'][$char]['WX'];
      }
    }
  }
 
  $pdf->currentTextState = $store_currentTextState;
  $pdf->setCurrentFont();

  return $w*$size/1000;
}

function Cabecalho($pdf){
	$pdf->selectFont('src/fonts/Helvetica.afm');

	$id_user = $_SESSION['apontador'];
	$id_analise = $_SESSION['id_analise'];

	$sql_img = mysql_query("select * from clientes where id_clientes='$id_user'"); 
	$reg_img = mysql_fetch_row($sql_img); 
	
		$sql = mysql_query("select * from dados_basico_analise where id_analise='$id_analise'"); 
		$reg_dados_basicos = mysql_fetch_row($sql); 
		$unidade	= utf8_decode($reg_dados_basicos[6]);
		$linha		= utf8_decode($reg_dados_basicos[5]);
		$equipamento	= utf8_decode($reg_dados_basicos[4]);
		$titulo		= utf8_decode($reg_dados_basicos[2]);
		$datadoevento	= utf8_decode($reg_dados_basicos[3]);
		$prioridade	= utf8_decode($reg_dados_basicos[18]);
		$situacao	= utf8_decode($reg_dados_basicos[16]);
		$quem_cadastro = $reg_dados_basicos[22];
		
		
	$sql_cadastro = mysql_query("SELECT * FROM usuarios WHERE id_usuario='$quem_cadastro'"); 
	$reg_cadastro = mysql_fetch_row($sql_cadastro);
	
	$nome_cadastro = $reg_cadastro[4];
		
	$espacamento=6;	
	
	$data_atual= date("d/m/Y");
	$dados_rodape = "www.telios.eng.br | <b>".$titulo."</b> - ".$data_atual;
	$pdf->addText(30,15,8,$dados_rodape);
	
	$pdf->setColor(0.11,0.33,0.71);
	//Relatório RCA
	$pdf->addJpegFromFile('images/logo_telios.jpg',485,800); 
	$chamada_relatorio = utf8_decode("<b>Relatório de Análise de Causa Raiz de Falha</b>");
	$pdf->addText(30,805,16,$chamada_relatorio);

	// Empresa
	
	$tamanhofonte=12;
	$empresa =utf8_decode("<b>".$_SESSION['empresa']."</b>");
	$pdf->setColor(0,0,0);
	$width = getTextWidth($tamanhofonte,$empresa,$pdf);
	$pdf->addText(30,775,$tamanhofonte,$empresa);

	$tamanhofonte=10;
	$nome_gerador =utf8_decode($quem_cadastro);
	$nome_gerador_2 =utf8_decode("<b>Responsável pela Análise: </b>");
	$pdf->setColor(0,0,0);
	$width = getTextWidth($tamanhofonte,$nome_gerador,$pdf);
	$pdf->addText(30,757,$tamanhofonte,$nome_gerador_2);	
	$pdf->addText(135,757,$tamanhofonte,$nome_gerador);	
	
	//Unidade
	$tamanhofonte=10;
	$pdf->setColor(0,0,0);
	$width = getTextWidth($tamanhofonte,$unidade,$pdf);
	$unidade_relatorio = utf8_decode("<b>Unidade Fabril: </b>");
	$pdf->addText(30,742,10,$unidade_relatorio);
	$pdf->addText(105,742,$tamanhofonte,$unidade);

	$width = getTextWidth($tamanhofonte,$linha,$pdf);
	$linha_relatorio = utf8_decode("<b>Linha de Produção: </b>");
	$pdf->addText(30,727,10,$linha_relatorio);
	$pdf->addText(125,727,$tamanhofonte,$linha);

	$width = getTextWidth($tamanhofonte,$equipamento,$pdf);
	$equipamento_relatorio = utf8_decode("<b>Equipamento: </b>");
	$pdf->addText(30,712,10,$equipamento_relatorio);
	$pdf -> addText(100, 712, $tamanhofonte, $equipamento);

	//Texto Prioridade

	$x=473;
	$y2=800;
	$dy=18;
	$w=84;

	$pdf ->setcolor(0,0,0);

	if ($prioridade=="A"){
		$pdf->addJpegFromFile('images/pdf/prioridadeA.jpg',465,750);
	}else if ($prioridade=="B"){
		$pdf->addJpegFromFile('images/pdf/prioridadeB.jpg',465,750);
	}else if ($prioridade=="C"){
		$pdf->addJpegFromFile('images/pdf/prioridadeC.jpg',465,750);
	}else if($prioridade=="D"){
		$pdf->addJpegFromFile('images/pdf/prioridadeD.jpg',465,750);
	}else if($prioridade=="E"){
		$pdf->addJpegFromFile('images/pdf/prioridadeE.jpg',465,750);
	}

	//Texto Data do eventto
	$pdf->setColor(0,0,0);
	$pdf->addText(477,735,11,"Data do evento");
	$data=$datadoevento;

	$pdf->setColor(0,0,0);
	$pdf->addText(487,720,11,$data);
	$pdf->setColor(0,0,0);
	
	$pdf->addJpegFromFile('images/pdf/titulo.jpg',30,670); 
	$a = $pdf->addTextWrap(30,678,537.4, 16,$titulo,'center');

	$pdf->setColor(0,0,0);
	return $pdf;
}

$pdf =& new Cezpdf();
$pdf->setColor(0,0,0);
$pdf = Cabecalho($pdf);
$pdf->setColor(0,0,0);

$dadosbasicos	= "on";
$ishikawa	= "on";
$porques	= "on";
$acoes		= "on";
//$anexos		= "on"; 

if ($dadosbasicos=="on"){
	//Dados Básicos
	$pdf =& new Cezpdf();
	$pdf = Cabecalho($pdf);

	$pagina = utf8_decode("Página 1");
	$pdf->addText(530,15,8,$pagina);
	
	
	$pdf->selectFont('./src/fonts/Helvetica.afm');
	$pdf ->ezSetY(665);
	$pdf->setColor(0.11,0.33,0.71);
	$dados_basicos = utf8_decode("<b>Dados Básicos</b>");
	$y = 	$pdf->ezText($dados_basicos,14);
	
	$pdf->setColor(0,0,0);
	$id_user = $_SESSION['apontador'];
	$id_analise = $_SESSION['id_analise'];
	
	$sql_2 = mysql_query("SELECT * from dados_basico_analise WHERE id_analise = '$id_analise'"); 
	$reg_2 = mysql_fetch_row($sql_2); 

	$datadeavaliacao	= $reg_2[7];
	$equipedeavaliacao	= utf8_decode($reg_2[8]);
	$observacoes		= utf8_decode($reg_2[15]);;
	$descricaodoevento	= utf8_decode($reg_2[9]);
	$evidencias		= utf8_decode($reg_2[10]);
	$causabasica		= utf8_decode($reg_2[11]);
	$componente		= utf8_decode($reg_2[12]);
	$funcao			= utf8_decode($reg_2[13]);
	$mododefalha		= utf8_decode($reg_2[15]);
	$causaraiz		= utf8_decode($reg_2[14]);

	//Texto Data Avaliação
	if($datadeavaliacao != ""){
		$pdf ->ezSetDY(-15);
		$data_avaliacao = utf8_decode("<b>Data da Avaliação:  </b>");
		$y = $pdf->ezText($data_avaliacao,10);

		//Valor Data Avaliação
		$data=$datadeavaliacao;
		$pdf ->ezSetDY(-1);
		$y = $pdf->ezText($data,10);
	}
	//Texto Equipe de Avaliação
	$pdf ->ezSetDY(-12);
	$equipe_avaliacao = utf8_decode("<b>Equipe de Avaliação: </b>");
	$y= $pdf->ezText($equipe_avaliacao,10);
	
	//Valor Equipe de Avaliação
	$pdf ->ezSetDY(-1);
	$y = $pdf->ezText($equipedeavaliacao,10);

	//Texto Descrição do evento
	$pdf ->ezSetDY(-12);
	$descricao_evento = utf8_decode("<b>Descrição do Evento: </b>");
	$y= $pdf->ezText($descricao_evento,10);
	
	//Valor Descrição do Evento
	$pdf ->ezSetDY(-1);
	$y = $pdf->ezText($descricaodoevento,10);

	//Texto Evidências
	$pdf ->ezSetDY(-12);
	$evidencias_new = utf8_decode("<b>Evidências: </b>");
	$y= $pdf->ezText($evidencias_new,10);
	
	//Valor Evidências
	$pdf ->ezSetDY(-1);
	$y = $pdf->ezText($evidencias,10);

	//Texto Causa Básica
	$pdf ->ezSetDY(-12);
	$causa_basica = utf8_decode("<b>Causa Básica: </b>");
	$y = 	$pdf->ezText($causa_basica,10);
	
	//Valor Causa básica
	$pdf ->ezSetDY(-1);
	$y = $pdf->ezText($causabasica,10);

	//Texto Componente que falhou
	$pdf ->ezSetDY(-12);
	$y = 	$pdf->ezText("<b>Componente que Falhou: </b>",10);
	
	//Valor Componente que falhou
	$pdf ->ezSetDY(-1);
	$y = $pdf->ezText($componente,10);

	//Texto Função componente que falhou
	$pdf ->ezSetDY(-12);
	$funcao_componente = utf8_decode("<b>Função do Componente</b>");
	$y = 	$pdf->ezText($funcao_componente,10);
	
	//Valor função componente que falhou
	$pdf ->ezSetDY(-1);
	$y = $pdf->ezText($funcao,10);

	//Texto Modo de falha
	$pdf ->ezSetDY(-12);
	$y = 	$pdf->ezText("<b>Modo de Falha</b>",10);
	
	//Valor Modo de falha
	$pdf ->ezSetDY(-1);
	$y = $pdf->ezText($mododefalha,10);

	//Texto Causa Raiz
	$pdf ->ezSetDY(-12);
	$y = 	$pdf->ezText("<b>Causa Raiz: </b>",10);
	
	//Valor Causa raiz
	$pdf ->ezSetDY(-1);
	$y = $pdf->ezText($causaraiz,10);

}

if ($ishikawa=="on"){

	//Ishikawa
	if ($dadosbasicos=="on"){
		$pdf -> ezNewPage();
		$pdf->setColor(0,0,0);
		$pdf = Cabecalho($pdf);
		//$pdf = Rodape($pdf);
		$pdf->setColor(0,0,0);
	}
	else{
		$pdf =& new Cezpdf();
		$pdf->setColor(0,0,0);
		$pdf = Cabecalho($pdf);
		$pdf = Rodape($pdf);
		$pdf->setColor(0,0,0);
	}

	$pagina = utf8_decode("Página 2");
	$pdf->addText(530,15,8,$pagina);
	
	$pdf->selectFont('./src/fonts/Helvetica.afm');
	$pdf ->ezSetY(665);
	$pdf->setColor(0.11,0.33,0.71);
	$y = 	$pdf->ezText("<b>Diagrama de Causa e Efeito - Ishikawa </b>",14);

	$sql = mysql_query("select * from causa_efeito where id_analise ='$id_analise'"); 
	$row = mysql_fetch_row($sql);

	$maquina	= utf8_decode($row[2]);
	$meioambiente	= utf8_decode($row[3]);
	$metodo		= utf8_decode($row[4]);
	$medida		= utf8_decode($row[5]);
	$maodeobra	= utf8_decode($row[6]);
	$materiaprima	= utf8_decode($row[7]);
	$falha		= utf8_decode($row[8]);

	//Ishikawa
	$pdf->setColor(0,0,0);
	$pdf -> ezSetY(630);
	$maquina_ = utf8_decode("<b>Máquina</b>");
	$metodo_ = utf8_decode("<b>Método</b>");
	$mao_obra_ = utf8_decode("<b>Mão-de-obra</b>");
	$materia_prima_ = utf8_decode("<b>Matéria-prima</b>");
	
	//$data = array(array('maquina'=>$maquina,'vazio1'=>'','meioambiente'=>$meioambiente,'vazio2'=>'','metodo'=>$metodo));
	$data = array(array('maquina'=>$maquina,'meioambiente'=>$meioambiente,'metodo'=>$metodo));
	//$cols = array('maquina'=>"             ".$maquina_,'vazio1'=>'','meioambiente'=>"        <b>Meio Ambiente</b>",'vazio2'=>'','metodo'=>"              ".$metodo_);
	$cols = array('maquina'=>"             ".$maquina_,'meioambiente'=>"        <b>Meio Ambiente</b>",'metodo'=>"              ".$metodo_);
	$options = array('innerLineThickness' =>0.4, 'outerLineThickness' => 0.8,'xPos'=>40,'xOrientation'=>'right','showLines'=>1,'shaded'=>0, 'cols'=>array('maquina'=>array('width'=>120),'vazio1'=>array('width'=>10),'meioambiente'=>array('width'=>120),'vazio2'=>array('width'=>10),'metodo'=>array('width'=>120,)));
	$y = $pdf->ezTable($data,$cols,'',$options);
	$y1=$y;

	$pdf->addJpegFromFile('images/ishikawa.jpg',40,$y-75,400,70);

	$pdf -> ezSetY($y-20);
	$data = array(array('Falha'=>$falha));
	$cols = array('Falha'=>"           <b>Falha</b>");
	$options = array('innerLineThickness' =>0.4, 'outerLineThickness' => 0.8,'lineCol'=>array(0,0,0), 'xPos'=>450,'xOrientation'=>'right','showLines'=>1, 'shaded'=>0, 'cols'=>array('Falha'=>array('width'=>120)));
	$y = $pdf->ezTable($data,$cols,'',$options);

	$pdf -> ezSetY($y1-80);
	//$data = array(array('Medida'=>$medida,'vazio1'=>'','Mao-de-obra'=>$maodeobra,'vazio2'=>'','Materia-prima'=>$materiaprima));
	$data = array(array('Medida'=>$medida,'Mao-de-obra'=>$maodeobra,'Materia-prima'=>$materiaprima));
	//$cols = array('Medida'=>"             <b>Medida</b>",'vazio1'=>'','Mao-de-obra'=>"        ".$mao_obra_,'vazio2'=>'','Materia-prima'=>"        ".$materia_prima_);
	$cols = array('Medida'=>"             <b>Medida</b>",'Mao-de-obra'=>"        ".$mao_obra_,'Materia-prima'=>"        ".$materia_prima_);
	$options = array('innerLineThickness' =>0.4, 'outerLineThickness' => 0.8,'xPos'=>40,'xOrientation'=>'right','showLines'=>1,'shaded'=>0, 'cols'=>array('Medida'=>array('width'=>120),'vazio1'=>array('width'=>10),'Mao-de-obra'=>array('width'=>120),'vazio2'=>array('width'=>10),'Materia-prima'=>array('width'=>120)));
	$y = $pdf->ezTable($data,$cols,'',$options);
}

if ($porques=="on"){
	//5porques

	if ($dadosbasicos=="on" | $ishikawa=="on"){
		$pdf -> ezNewPage();
		$pdf->setColor(0,0,0);
		$pdf = Cabecalho($pdf);
		$pdf->setColor(0,0,0);
	}
	else{
		$pdf =& new Cezpdf();
		$pdf->setColor(0,0,0);
		$pdf = Cabecalho($pdf);
		$pdf->setColor(0,0,0);
	}
	
	$pagina = utf8_decode("Página 3");
	$pdf->addText(530,15,8,$pagina);
	
	$pdf->selectFont('./src/fonts/Helvetica.afm');
	$pdf ->ezSetY(665);
	$pdf->setColor(0.11,0.33,0.71);
	$titulo_5_pqs = utf8_decode("<b>Análise dos 5 Porquês</b>");
	$y = 	$pdf->ezText($titulo_5_pqs,14);

	$pdf->setColor(0,0,0);
	$pdf ->ezSetDY(-20);

	$sql = mysql_query("SELECT * FROM 5_porques WHERE id_analise ='$id_analise'"); 
	$row = mysql_fetch_row($sql);
	
	$descricao	= utf8_decode($row[2]);
	$titulo_descricao = utf8_decode("<b>Descrição do desvio</b>");
	$y = 	$pdf->ezText($titulo_descricao,10);
	$pdf ->ezSetDY(-1);
	$y = 	$pdf->ezText($descricao,10);
	$pdf ->ezSetDY(-10);
	
    $pdf->setColor(0.11,0.33,0.71);
	$y =	$pdf->ezText("<b>Round 1</b>",10);
	$pdf ->ezSetDy(-5);
	
	$pdf->setColor(0,0,0);
	$hipotese_1 = utf8_decode($row[3]);
	$y =	$pdf->ezText($hipotese_1,10);
	$pdf ->ezSetDy(-5);

	if($row[5]=="S"){$row14="<b>Verdadeiro</b>";}else{$row14="Falso";}
	if($row[7]=="S"){$row15="<b>Verdadeiro</b>";}else{$row15="Falso";}
	if($row[9]=="S"){$row16="<b>Verdadeiro</b>";}else{$row16="Falso";}
	$hipotese = utf8_decode("<b>Hipótese</b>");
	
	
	$dados_hipotese_1_13 = utf8_decode($row[4]);
	$dados_hipotese_1_14 = utf8_decode($row[6]);
	$dados_hipotese_1_15 = utf8_decode($row[8]);
	
	$data = array(array('a'=> $hipotese.' 1: ','b'=>$dados_hipotese_1_13,  'c'=>$row14), 
                      array('a'=>$hipotese.' 2: ','b'=>$dados_hipotese_1_14, 'c'=>$row15),
                      array('a'=>$hipotese.' 3: ','b'=>$dados_hipotese_1_15, 'c'=>$row16));

	$y = $pdf->ezTable($data,'','',array('xPos'=>90,'xOrientation'=>'right','width'=>400,'showHeadings'=>0,'shaded'=>0,'showLines'=>0,
					     'cols'=>array('a'=>array('width'=>80),'b'=>array('width'=>250,'justification'=>'left'),'c'=>array('width'=>70))));

	$pdf ->ezSetDY(-10);

	$pdf->setColor(0.11,0.33,0.71);
	$y =	$pdf->ezText("<b>Round 2</b>",10);
	 $pdf->setColor(0,0,0);
	$pdf ->ezSetDy(-5);

	$hipotese_2 = utf8_decode($row[10]);
	$y =	$pdf->ezText($hipotese_2,10);
	$pdf ->ezSetDy(-5);

	if($row[12]=="S"){$row29="<b>Verdadeiro</b>";}else{$row29="Falso";}
	if($row[14]=="S"){$row30="<b>Verdadeiro</b>";}else{$row30="Falso";}
	if($row[16]=="S"){$row31="<b>Verdadeiro</b>";}else{$row31="Falso";}

	$dados_hipotese_1_10 = utf8_decode($row[11]);
	$dados_hipotese_1_11 = utf8_decode($row[13]);
	$dados_hipotese_1_12 = utf8_decode($row[15]);
	
	$data = array(array('a'=>$hipotese.' 1: ','b'=>$dados_hipotese_1_10, 'c'=>$row29), 
                      array('a'=>$hipotese.' 2: ','b'=>$dados_hipotese_1_11, 'c'=>$row30),
                      array('a'=>$hipotese.' 3: ','b'=>$dados_hipotese_1_12, 'c'=>$row31));

	$y = $pdf->ezTable($data,'','',array('xPos'=>90,'xOrientation'=>'right','width'=>400,'showHeadings'=>0,'shaded'=>0,'showLines'=>0,
					     'cols'=>array('a'=>array('width'=>80),'b'=>array('width'=>250,'justification'=>'left'),'c'=>array('width'=>70))));

	$pdf ->ezSetDY(-10);

	 $pdf->setColor(0.11,0.33,0.71);
	$y =	$pdf->ezText("<b>Round 3</b>",10);
	$pdf ->ezSetDy(-5);

    $pdf->setColor(0,0,0);
	$hipotese_3 = utf8_decode($row[17]);
	$y =	$pdf->ezText($hipotese_3,10);
	$pdf ->ezSetDy(-5);

	if($row[19]=="S"){$row44="<b>Verdadeiro</b>";}else{$row44="Falso";}
	if($row[21]=="S"){$row45="<b>Verdadeiro</b>";}else{$row45="Falso";}
	if($row[23]=="S"){$row46="<b>Verdadeiro</b>";}else{$row46="Falso";}

	$dados_hipotese_1_7 = utf8_decode($row[18]);
	$dados_hipotese_1_8 = utf8_decode($row[20]);
	$dados_hipotese_1_9 = utf8_decode($row[22]);
	
	$data = array(array('a'=>$hipotese.' 1: ','b'=>$dados_hipotese_1_7, 'c'=>$row44), 
                      array('a'=>$hipotese.' 2: ','b'=>$dados_hipotese_1_8, 'c'=>$row45),
                      array('a'=>$hipotese.' 3: ','b'=>$dados_hipotese_1_9, 'c'=>$row46));

	$y = $pdf->ezTable($data,'','',array('xPos'=>90,'xOrientation'=>'right','width'=>400,'showHeadings'=>0,'shaded'=>0,'showLines'=>0,
					     'cols'=>array('a'=>array('width'=>80),'b'=>array('width'=>250,'justification'=>'left'),'c'=>array('width'=>70))));

	$pdf ->ezSetDY(-10);

	 $pdf->setColor(0.11,0.33,0.71);
	$y =	$pdf->ezText("<b>Round 4</b>",10);
	$pdf ->ezSetDy(-5);
	 $pdf->setColor(0,0,0);
	$hipotese_4 = utf8_decode($row[24]);
	$y =	$pdf->ezText($hipotese_4,10);
	$pdf ->ezSetDy(-5);

	if($row[26]=="S"){$row59="<b>Verdadeiro</b>";}else{$row59="Falso";}
	if($row[28]=="S"){$row60="<b>Verdadeiro</b>";}else{$row60="Falso";}
	if($row[30]=="S"){$row61="<b>Verdadeiro</b>";}else{$row61="Falso";}

	$dados_hipotese_1_1 = utf8_decode($row[25]);
	$dados_hipotese_1_2 = utf8_decode($row[27]);
	$dados_hipotese_1_3 = utf8_decode($row[29]);
	$data = array(array('a'=>$hipotese.' 1: ','b'=>$dados_hipotese_1_1, 'c'=>$row59), 
                      array('a'=>$hipotese.' 2: ','b'=>$dados_hipotese_1_2, 'c'=>$row60),
                      array('a'=>$hipotese.' 3: ','b'=>$dados_hipotese_1_3, 'c'=>$row61));

	$y = $pdf->ezTable($data,'','',array('xPos'=>90,'xOrientation'=>'right','width'=>400,'showHeadings'=>0,'shaded'=>0,'showLines'=>0,
					     'cols'=>array('a'=>array('width'=>80),'b'=>array('width'=>250,'justification'=>'left'),'c'=>array('width'=>70))));

	$pdf ->ezSetDY(-10);

	 $pdf->setColor(0.11,0.33,0.71);
	$y =	$pdf->ezText("<b>Round 5</b>",10);
	$pdf ->ezSetDy(-5);
	 $pdf->setColor(0,0,0);

	$hipotese_5 = utf8_decode($row[31]);
	$y =	$pdf->ezText($hipotese_5,10);
	$pdf ->ezSetDy(-5);

	if($row[33]=="S"){$row74="<b>Verdadeiro</b>";}else{$row74="Falso";}
	if($row[35]=="S"){$row75="<b>Verdadeiro</b>";}else{$row75="Falso";}
	if($row[37]=="S"){$row76="<b>Verdadeiro</b>";}else{$row76="Falso";}

	
	$dados_hipotese_1_4 = utf8_decode($row[32]);
	$dados_hipotese_1_5 = utf8_decode($row[34]);
	$dados_hipotese_1_6 = utf8_decode($row[36]);
	
	$data = array(array('a'=>$hipotese.' 1: ','b'=>$dados_hipotese_1_4, 'c'=>$row74), 
                      array('a'=>$hipotese.' 2: ','b'=>$dados_hipotese_1_5, 'c'=>$row75),
                      array('a'=>$hipotese.' 3: ','b'=>$dados_hipotese_1_6, 'c'=>$row76));

	$y = $pdf->ezTable($data,'','',array('xPos'=>90,'xOrientation'=>'right','width'=>400,'showHeadings'=>0,'shaded'=>0,'showLines'=>0,
					     'cols'=>array('a'=>array('width'=>80),'b'=>array('width'=>250,'justification'=>'left'),'c'=>array('width'=>70))));

	$pdf ->ezSetDY(-10);
	$ideias_melhoria = utf8_decode("<b> Idéias de Melhoria </b>");
	$ideias_2 = utf8_decode($row[38]);
	$y =	$pdf->ezText($ideias_melhoria,10);
	$pdf ->ezSetDy(-1);

	$y =	$pdf->ezText($ideias_2,10);
}

if ($acoes=="on"){
	//Ações
	if ($dadosbasicos=="on" | $ishikawa=="on" | $porques=="on"){
		$pdf -> ezNewPage();
		$pdf = Cabecalho($pdf);
	}
	else{
		$pdf =& new Cezpdf();
		$pdf = Cabecalho($pdf);
	}

	$pagina = utf8_decode("Página 4");
	$pdf->addText(530,15,8,$pagina);
	
	$pdf->selectFont('./src/fonts/Helvetica.afm');
	$pdf ->ezSetY(665);
	 $pdf->setColor(0.11,0.33,0.71);
	$acao_bloqueio = utf8_decode("<b>Ações de Bloqueio</b>");
	$y = 	$pdf->ezText($acao_bloqueio,14);
     $pdf->setColor(0,0,0);
	$pdf ->ezSetY(630);


	$sql = mysql_query("select * from acoes where id_analise ='$id_analise'"); 
	while($rowacoes = mysql_fetch_row($sql)){

		$responsavel	= utf8_decode($rowacoes[2]);
		$descricao	= utf8_decode($rowacoes[3]);
		$prazo		= $rowacoes[4];
		$situacao	= utf8_decode($rowacoes[5]);
		//$prazobr = strftime("%d/%m/%Y",strtotime($prazo));
		$prazobr = $prazo;
		//$responsavel = troca($responsavel);
		$pdf->setColor(0.11,0.33,0.71);
		$responsavel_ = utf8_decode("<b>Responsável</b>");
		$descricao_ = utf8_decode("<b>Descrição</b>");
		$situacao_ = utf8_decode("<b>Situação</b>");
		$prazo = utf8_decode("<b>Prazo</b>");
		
		$pdf->setColor(0,0,0);
		$data1[] = array('Responsavel'=>$responsavel,'Descricao'=>$descricao,'Prazo'=>$prazobr,'Situacao'=>$situacao);
	}
  
	$cols = array('Responsavel'=>"      ".$responsavel_,'Descricao'=>"                                         ".$descricao_,'Prazo'=>$prazo,'Situacao'=>$situacao_);
	$options = array('colGap' => 2,'rowGap' => 5,'innerLineThickness' =>0.4, 'outerLineThickness' => 0.8,'xPos'=>40,'xOrientation'=>'right','showLines'=>2, 'shaded'=>0,'cols'=>array('Responsavel'=>array('width'=>103),'Descricao'=>array('width'=>270),'Prazo'=>array('justification'=>'centre','width'=>80),'Situacao'=>array('justification'=>'centre','width'=>80)));

	$y = $pdf->ezTable($data1,$cols,'',$options);

}

//$sql = mysql_query("select * from arquivos_analise where id_analise ='$id_analise' order by id_arquivo asc"); 
//$linhas = mysql_num_rows($sql);

/*if($linhas > 0){*/
	$anexos="on";
/* }	*/
if ($anexos=="on"){

		//Anexos
	if ($dadosbasicos=="on" | $ishikawa=="on" | $porques=="on" | $acoes=="on")
	{
		$pdf -> ezNewPage();
		$pdf->setColor(0,0,0);
		$pdf = Cabecalho($pdf);
		$pdf->setColor(0,0,0);
	}
	else
	{
		$pdf =& new Cezpdf();
/*		$pdf->setColor(0,0,0);
*/		$pdf = Cabecalho($pdf);
		$pdf->setColor(0,0,0);
	}

	$pdf->selectFont('src/fonts/Helvetica.afm');

$sql = mysql_query("select * from arquivos_analise where id_analise ='$id_analise' order by id_arquivo asc"); 
	$linhas = mysql_num_rows($sql);

	$i=1;

	while($rowanexos = mysql_fetch_row($sql))
	{
		if ($i==1)
		{
			$pdf	-> setColor(0,0,0);
			$pdf	-> addText(30,900,12,"Anexo ".$i.":  ".$rowanexos[4]);
			$pdf -> addJpegFromFile("analiseFalhas/uploads/".$rowanexos[2],30,400,150);
			
		}
		else if ($i==2)
		{
			$pdf	-> setColor(0,0,0);
			$pdf	-> addText(310,900,12,"Anexo ".$i.":  ".$rowanexos[4]);
			
				$pdf -> addJpegFromFile("analiseFalhas/uploads/".$rowanexos[2],310,400,150);
			
		}
		else if ($i==3)
		{
			$pdf	-> setColor(0,0,0);
			$pdf	-> addText(30,470,12,"Anexo ".$i.":  ".$rowanexos[4]);
			
				$pdf -> addJpegFromFile("analiseFalhas/uploads/".$rowanexos[2],30,280,250);
			
		}
		else if ($i==4)
		{
			$pdf	-> setColor(0,0,0);
			$pdf	-> addText(310,470,12,"Anexo ".$i.":  ".$rowanexos[4]);
			
				$pdf -> addJpegFromFile("analiseFalhas/uploads/".$rowanexos[2],310,280,250);
			
		}
		else if ($i==5)
		{
			$pdf	-> setColor(0,0,0);
			$pdf	-> addText(30,250,12,"Anexo ".$i.":  ".$rowanexos[4]);
			
				$pdf -> addJpegFromFile("analiseFalhas/uploads/".$rowanexos[2],30,60,250);
			
		}
		else if ($i==6)
		{
			$pdf	-> setColor(0,0,0);
			$pdf	-> addText(310,250,12,"Anexo ".$i.":  ".$rowanexos[4]);
			
				$pdf -> addJpegFromFile("analiseFalhas/uploads/".$rowanexos[2],310,60,250);
			
		}
		else if ($i==7)
		{
			$pdf -> ezNewPage();
			$pdf->setColor(0,0,0);
			$pdf = Cabecalho($pdf);
			$pdf->setColor(0,0,0);
			$pdf->selectFont('src/fonts/Helvetica.afm');

			$pdf	-> setColor(0,0,0);
			$pdf	-> addText(30,690,12,"Anexo ".$i.":  ".$rowanexos[4]);
			
				$pdf -> addJpegFromFile("analiseFalhas/uploads/".$rowanexos[2],30,500,250);
			
		}
		else if ($i==8)
		{
			$pdf	-> setColor(0,0,0);
			$pdf	-> addText(310,690,12,"Anexo ".$i.":  ".$rowanexos[4]);
			
				$pdf -> addJpegFromFile("analiseFalhas/uploads/".$rowanexos[2],310,500,250);
			
		}
		else if ($i==9)
		{
			$pdf	-> setColor(0,0,0);
			$pdf	-> addText(30,470,12,"Anexo ".$i.":  ".$rowanexos[4]);
			
				$pdf -> addJpegFromFile("analiseFalhas/uploads/".$rowanexos[2],30,280,250);
			
		}
		else if ($i==10)
		{
			$pdf	-> setColor(0,0,0);
			$pdf	-> addText(310,470,12,"Anexo ".$i.":  ".$rowanexos[4]);
			
				$pdf -> addJpegFromFile("analiseFalhas/uploads/".$rowanexos[2],310,280,250);
			
		}
		else if ($i==11)
		{
			$pdf	-> setColor(0,0,0);
			$pdf	-> addText(30,250,12,"Anexo ".$i.":  ".$rowanexos[4]);
			
				$pdf -> addJpegFromFile("analiseFalhas/uploads/".$rowanexos[2],30,60,250);
			
		}
		else if ($i==12)
		{
			$pdf	-> setColor(0,0,0);
			$pdf	-> addText(310,250,12,"Anexo ".$i.":  ".$rowanexos[4]);
			
				$pdf -> addJpegFromFile("analiseFalhas/uploads/".$rowanexos[2],310,60,250);
			
		}

		$i++;

	}
	
	
	
	
	
	
	
	/*if ($dadosbasicos=="on" | $ishikawa=="on" | $porques=="on" | $acoes=="on"){
		$pdf -> ezNewPage();
		$pdf->setColor(0,0,0);
		$pdf = Cabecalho($pdf);
		$pdf->setColor(0,0,0);
	}else{
		$pdf =& new Cezpdf();
		$pdf->setColor(0,0,0);
		$pdf = Cabecalho($pdf);
		$pdf->setColor(0,0,0);
	}

	$pagina = utf8_decode("Página 5");
	$pdf->addText(530,15,8,$pagina);
	
	$pdf ->ezSetY(665);
	$pdf->setColor(0.11,0.33,0.71);
	$anexos_titulo = utf8_decode("<b>Anexos</b>");
	$y = 	$pdf->ezText($anexos_titulo,14);
	
	$pdf->selectFont('./src/fonts/Helvetica.afm');
	

	$i=1;
	
		$altura_titulo = 610;
		$altura_imagem = 370;
		$pagina_nova = 6;
		$pagina_titulo = utf8_encode("Página ");
		$contem = 0;
		while($rowanexos = mysql_fetch_row($sql)){
			
				$pdf	-> setColor(0,0,0);	 
				$descricao_alterada = utf8_decode($rowanexos[4]);
				$verefica_jpg = substr($rowanexos[2],-3);
				if ($verefica_jpg == 'jpg' or $verefica_jpg == 'JPG' or $verefica_jpg == 'png'){
					$contem = 1;
					if($i == 1){ // Mostra primeira imagem
						$pdf	-> addText(30,$altura_titulo,10,"<b>Anexo:</b> ".$i.":  ".$descricao_alterada);
						if($verefica_jpg == 'jpg'){
							$pdf -> addJpegFromFile("analiseFalhas/uploads/".$rowanexos[2],30,$altura_imagem,300);
						}
					}else { // Mostra a partir da segunda imagem
						if($altura_titulo == 610){ // ta na primeira posição dos anexos, entao o proximo vai ser abaixo dele
							$altura_titulo = 340;
							$altura_imagem = 100;
							$pdf	-> addText(30,$altura_titulo,10,"<b>Anexo:</b> ".$i.":  ".$descricao_alterada);
							if($verefica_jpg == 'jpg'){
							$pdf -> addJpegFromFile("analiseFalhas/uploads/".$rowanexos[2],30,$altura_imagem,300);
						}
						}else{ // crio uma nova pagina 
							$pdf -> ezNewPage();
							$pdf->setColor(0,0,0);
							$pdf = Cabecalho($pdf);
							$pdf->setColor(0,0,0);
							$altura_titulo = 610;
							$altura_imagem = 370;
							$pdf	-> addText(30,$altura_titulo,10,"<b>Anexo:</b> ".$i.":  ".$descricao_alterada);
							if($verefica_jpg == 'jpg'){
							$pdf -> addJpegFromFile("analiseFalhas/uploads/".$rowanexos[2],30,$altura_imagem,300);
						}
							$pagina = utf8_decode("Página 5");
							$pdf->addText(530,15,8,$pagina_titulo.$pagina_nova);
							$pagina_nova++;
						}
					}
					$i++;
				}else{	
				}
		}
		
		if($contem == 0){
			$erro = utf8_decode("Esta análise contém anexo que não é suportado pelo PDF.");
			$pdf ->ezSetDY(-7);
			$y = 	$pdf->ezText($erro,11);	
		}*/
		
} 


$pdf->ezStream();

} else {
	echo "SUA SESSÃO EXPIROU!";	
}
?>