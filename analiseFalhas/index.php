<?php 
session_start(); // Inicio a Sessão?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="análise de falhas, indústria, RCA - Root Cause Analisys, ações de bloqueio, causa raiz da falha
" />
		<meta name="description" content="Realize análises de falhas de maneira descomplicada e contribua para a melhoria nos indicadores da indústria." />
		<meta name="rating" content="General" />
		<meta name="author" content="Agência GH" />
		<meta name="language" content="pt-br" />
		<meta name="ROBOTS" content="index,follow" />
		<title>TÉLIOS</title>
		<link href="../favicon.ico" rel="shortcut icon" />
		<link href="css/home.css" rel="stylesheet" type="text/css" media="screen" />
        <!--[if IE]>
        <link href="css/ie.css" rel="stylesheet" type="text/css" media="screen" />
		<![endif]-->
        <!-- [if gte IE 8]
        <link rel="stylesheet" href="css/ie8.css" type="text/css" />
        <! [endif] -->
       
		<script language="javascript" src="js/post.js"></script>
		<script language="javascript" src="js/mascara.js"></script>
		<script type="text/javascript" src="swfobject/swfobject.js"></script>
		<script type="text/javascript">
			swfobject.registerObject("content", "10.0.0");
		</script>
		</head>
		<body>
<?php if (isset($_SESSION['usuario'])) {
	
 if(isset($_GET['deslogado'])){ 
	session_start();
	session_unset();
	session_destroy(); ?>	
     <script language="JavaScript">
			window.location.href = "index.php";
		</script>
 <?php }
	 // Verifico se o Usuário ja esta logado?>
<script language="JavaScript">
			window.location.href = "logado.php";
		</script>
<?php } else { // Se o Usuário não estiver logado mostro form de login ?>
<div id="tudo">
          <div id="topo">
    <div id="topoInterno">
              <div id="linkHome"><a href="index.php"><img src="images/site/linkHome.gif" width="192" height="41" /></a></div>
              <div id="linkFerramenta"><a href="ferramenta.php"><img src="images/site/linkFerramentas.gif" width="111" height="24" /></a></div>
              <div id="linkRca"><a href="rca.php"><img src="images/site/linkArtigos.gif" width="65" height="24" /></a></div>
              <div id="linkVantagens"><a href="vantagens.php"><img src="images/site/linkVantagens.gif" width="89" height="24" /></a></div>
              <div id="linkArtigos"><a href="artigos.php"><img src="images/site/linkArtigos.gif" width="65" height="24" /></a></div>
              <div id="linkContato"><a href="contato.php"><img src="images/site/linkRCA.gif" width="72" height="24" /></a></div>
              <div id="formLogin">
        <div id="formLoginInterna">
                  <form id="log" name="log" method="post" action="logado.php" >
            <div id="inputFormLogin">
                      <input name="login" type="text" id="login"/>
                    </div>
            <div id="inputFormSenha">
                      <input name="senha" type="password" id="senha"/>
                    </div>
            <div id="inputFormEntrar">
                      <input name="envia" type="submit" id="envia" class="btnLogar" value=""/>
                    </div>
          </form>
                  <?php if(isset($_GET['deslogado'])){?>
                  <div id="erroLogin">Login ou Senha Inválido</div>
                  <?php }?>
                </div>
      </div>
            </div>
  </div>
          <div id="banner">
    <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="300" id="content">
              <param name="movie" value="banner.swf">
              <param name="wmode" value="transparent" />
              <!--[if !IE]>-->
              <object type="application/x-shockwave-flash" data="banner.swf" width="100%" height="300">
        <param name="wmode" value="transparent" />
        
        <!--<![endif]--> 
        <a href="http://www.adobe.com/go/getflashplayer"> <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Baixar Flash Player"> </a> 
        <!--[if !IE]>-->
      </object>
              <!--<![endif]-->
            </object>
  </div>
          <div id="fundoSite">
    <div id="fundoSiteInterna">
              <div class="ladoEsq">
        <p><img src="images/site/layoute_03.png" width="402" height="27" /></p>
        <div id="textoEsq">
                  <ul>
            <li>Com dados básicos para manter a rastreabilidade das informações, cadastre e priorize suas análises, gere ações de bloqueio e mantenha tudo sob controle.</li>
            <li>A ferramenta disponibiliza filtros rápidos para resgate de análises e ações cadastradas, facilitando a atualização das informações.</li>
          </ul>
                </div>
        <div id="textoDir">
                  <ul>
            <li>Os relatórios podem ser gerados em formato pdf padrão e as ações de bloqueio pendentes podem ser enviadas por email para os responsáveis em apenas um clique.</li>
            <li>Explore a ferramenta e contribua para a constante melhoria dos indicadores de manutenção de sua empresa. </li>
          </ul>
                </div>
        <div class="clear"></div>
      </div>
              <div id="ladoDir">
        <!--<div id="chamadaCadastro"><img src="images/site/layoute_06.png" width="86" height="14" /></div>-->
        <form id="enviaDados2" name="enviaDados2" method="post" onsubmit="return false;">
                  <div id="inputNome">
            <input type="text" name="nome_cadastra" id="nome_cadastra" onfocus="this.value=''" value="NOME" onkeypress="enviar8(event);"/>
          </div>
                  
                  <div id="inputEmail">
            <input type="text" name="email_cadastra" id="email_cadastra" onfocus="this.value=''" value="E-MAIL" onkeypress="enviar8(event);"/>
          </div>
                  <div id="prosseguir">
            <input name="envia" type="button" id="envia" onclick="envia_cadastro('enviaDados2', 'cadastra.php?acao=1','tudo');" value="" class="btnProsseguir"/>
          </div>
                </form>
      </div>
              <div class="clear"></div>
            </div>
  </div>
          <div id="conteudo2">
    <div id="dadosConteudoEsq">
              <div id="conteudoDadosEsq">
        <p><img src="images/site/layoute_11.png" width="35" height="15" /></p>
        <p class="fonteCinza">A Análise de Causa Raiz (Root Cause Analysis - RCA) é uma metodologia de resolução de problemas que proporciona valiosa contribuição na melhoria da disponibilidade dos ativos industriais, diminuição dos custos de manutenção e na formação de equipes de alta performance. </p>
        <p><a href="rca.php"><img src="images/site/btnLeiaMais.gif" width="79" height="32" /></a></p>
      </div>
              <div id="conteudoDadosDir">
        <p><img src="images/site/layoute_13.png" width="63" height="20" /></p>
        <ul>
                  <li class="fonteCinza">Justifying Root Cause Analisys<br />
            <span class="italico">By Robert J. Latino -  Reliability Center, Inc.</span></li>
                  <li class="fonteCinza">Getting Root Cause Analysis to Work for You<br />
            <span class="italico">Alexander Dunn - Assetivity Pty Ltd</span></li>
                  <!--<li class="fonteCinza">Manutenção em Tempo de Crise<br />
            <span class="italico">Mario Filho</span> </li>-->
                </ul>
        <p><a href="artigos.php"><img src="images/site/btnLeiaMais.gif" width="79" height="32" /></a></p>
      </div>
              <div class="clear"></div>
            </div>
    <!--<div id="dadosConteudoDir">
              <p><img src="images/site/layoute_15.png" width="70" height="15" /></p>
              <p>Em caso de dúvidas, críticas ou sujestões, entre em contato.</p>
              <p>Telefone: <span class="negrito">+ 51 (55) 3333 - 3333</span><br />
        Fax: <span class="negrito">+ 51 (55) 3333 - 3331</span><br />
        E-mail: <span class="negrito">contato@telios.com.br</span><br />
        MSN: <span class="negrito">contato@hotmail.com</span><br />
        Skipe: <span class="negrito">telios</span></p>
            </div>-->
    <div class="clear"></div>
  </div>
        </div>

<!--<form id="enviaLogin" name="enviaLogin" method="post" action="logado.php" >
  <table cellspacing="0" id="tabGeral">
    <tr>
      <td align="right" valign="middle">Usuário:</td>
      <td><input name="login" type="text" id="login" size="30"/></td>
    </tr>
    <tr>
      <td align="right" valign="middle">Senha:</td>
      <td><input name="senha" type="password" id="senha" size="30"/></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><a href="#" onclick="recuperaSenha('senha/recuperaSenha.php');">Esqueci Minha Senha.</a></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input name="envia" type="submit" id="envia" value="Logar" class="btnLogar"/></td>
    </tr>
  </table>
</form>-->
<?php }?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29818024-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>