<?php 
		
	require("../conexao.php");
	conexao();	
	
	$apontador = $_GET['apontador'];	
		
		$select_analise1 = "SELECT p.id_analise, f.titulo, p.id_situacao, p.id_basico, f.apontador FROM dados_basico_analise p, analise_falhas f WHERE p.id_analise = f.id_analise AND p.id_situacao = 1 AND f.apontador = '$apontador'   ORDER BY id_basico ASC";
	
	$exe_analise1 = mysql_query($select_analise1, $base) or die(mysql_error());
	$linha_analise1 = mysql_num_rows($exe_analise1);
	
	$select_analise2 = "SELECT p.id_analise, f.titulo, p.id_situacao, p.id_basico, f.apontador FROM dados_basico_analise p, analise_falhas f WHERE p.id_analise = f.id_analise AND p.id_situacao = 3 AND f.apontador = '$apontador' ORDER BY id_basico ASC";
	
	$exe_analise2 = mysql_query($select_analise2, $base) or die(mysql_error());
	$linha_analise2 = mysql_num_rows($exe_analise2);
	
	$select_analise3 = "SELECT p.id_analise, f.titulo, p.id_situacao, p.id_basico, f.apontador FROM dados_basico_analise p, analise_falhas f WHERE p.id_analise = f.id_analise AND p.id_situacao = 5 AND f.apontador = '$apontador' ORDER BY id_basico ASC";
	
	$exe_analise3 = mysql_query($select_analise3, $base) or die(mysql_error());
	$linha_analise3 = mysql_num_rows($exe_analise3);
	
	$select_analise4 = "SELECT p.id_analise, f.titulo, p.id_situacao, p.id_basico, f.apontador FROM dados_basico_analise p, analise_falhas f WHERE p.id_analise = f.id_analise AND p.id_situacao = 2 AND f.apontador = '$apontador' ORDER BY id_basico ASC";
	
	$exe_analise4 = mysql_query($select_analise4, $base) or die(mysql_error());
	$linha_analise4 = mysql_num_rows($exe_analise4);
	
	$select_analise5 = "SELECT p.id_analise, f.titulo, p.id_situacao, p.id_basico, f.apontador FROM dados_basico_analise p, analise_falhas f WHERE p.id_analise = f.id_analise AND p.id_situacao = 4 AND f.apontador = '$apontador' ORDER BY id_basico ASC";
	
	$exe_analise5 = mysql_query($select_analise5, $base) or die(mysql_error());
	$linha_analise5 = mysql_num_rows($exe_analise5);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title>

<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="excanvas.min.js"></script><![endif]-->

<script language="javascript" type="text/javascript" src="jquery.js"></script>
<script language="javascript" type="text/javascript" src="jquery.flot.js"></script>
<script language="javascript" type="text/javascript" src="jquery.flot.pie.js"></script>
<script type="text/javascript">

$(function () {

	// data

	var data = [
		{ label: "Pendente",  data: <?php echo $linha_analise1;?>},
		{ label: "Em Análise",  data: <?php echo $linha_analise4;?>},
		{ label: "Concluída",  data: <?php echo $linha_analise2;?>},
		{ label: "Cancelada",  data: <?php echo $linha_analise3;?>},
		{ label: "Publicada",  data: <?php echo $linha_analise5;?>}
	];

	

	//var data = [];

	//var series = Math.floor(Math.random()*10)+1;

	/*for( var i = 0; i<series; i++)

	{

		data[i] = { label: "Series"+(i+1), data: Math.floor(Math.random()*100)+1 }

	}*/

	// GRAPH 3

		$.plot($("#graph3"), data, 
	{
		series: {
			pie: { 
				show: true,
				radius: 1,
				label: {
					show: true,
					radius: 3/4,
					formatter: function(label, series){
						return '';
						
					},
					background: { 
						opacity: 0.5,
						color: '#000'
					}
				}
			}
		},
		legend: {
			show: false
		}
	});
	
});

</script>
<style type="text/css">
* {
	padding:0;
	margin:0;
	border:0;
}
body {
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	color:#000;
}
div.graph {
	width: 200px;
	height: 160px;
	text-align:left;
}

</style>
</head>

<body>
<div id="graph3" class="graph"></div>
<!--<div id="graph4" class="graph"></div>-->
</body>
</html>
