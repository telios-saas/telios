<?php session_start(); // Inicio a Sessão?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="análise de falhas, indústria, RCA - Root Cause Analisys, ações de bloqueio, causa raiz da falha
" />
		<meta name="description" content="Realize análises de falhas de maneira descomplicada e contribua para a melhoria nos indicadores da indústria." />
		<meta name="rating" content="General" />
		<meta name="author" content="Agência GH" />
		<meta name="language" content="pt-br" />
		<meta name="ROBOTS" content="index,follow" />
		<title>TELIOS</title>
		<link href="../favicon.ico" rel="shortcut icon" />
		<link href="css/home.css" rel="stylesheet" type="text/css" media="screen" />
		<script language="javascript" src="js/post.js"></script>
		<script language="javascript" src="js/mascara.js"></script>
		<script type="text/javascript" src="swfobject/swfobject.js"></script>
		<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
		<script src="SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
		<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
        <link href="SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
        
        <link rel="stylesheet" href="css/colorbox.css" />
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.colorbox.js"></script>
		<script>
			$(document).ready(function(){
				
				$(".iframe").colorbox({iframe:true, width:"600px", height:"300px"});
			});
		</script>
</head>
		<body>
<?php if (isset($_SESSION['usuario'])) { // Verifico se o Usuário ja esta logado?>
<script language="JavaScript">
			window.location.href = "logado.php";
		</script>
<?php } else { // Se o Usuário não estiver logado mostro form de login ?>
<div id="tudo">
          <div id="topo">
    <div id="topoInterno">
              <div id="linkHome"><a href="index.php"><img src="images/site/linkHome.gif" width="192" height="41" /></a></div>
              <a href="ferramenta.php"><div id="linkFerramenta"></div></a>
              <a href="rca.php"><div id="linkRca"></div></a>
      		  <a href="vantagens.php"><div id="linkVantagens"></div></a>
      		  <a href="artigos.php"><div id="linkArtigos"></div></a>
	  <a href="estatisticas.php" title="Estatísticas"><div id="linkEstatisticas"></div></a>
              <a href="busca_publicadas.php"><div id="linkBusca"></div></a>
              <a href="contato.php"><div id="linkContato"></div></a>
              <div id="formLogin">
        <div id="formLoginInterna">
                  <form id="log" name="log" method="post" action="logado.php" >
            <div id="inputFormLogin">
                      <input name="login" type="text" id="login"/>
                    </div>
            <div id="inputFormSenha">
                      <input name="senha" type="password" id="senha"/>
                    </div>
            <div id="inputFormEntrar">
                      <input name="envia" type="submit" id="envia" class="btnLogar" value=""/>
                    </div>
          </form>
                  <?php if(isset($_GET['deslogado'])){?>
                  <div id="erroLogin">Login ou Senha Inválido</div>
                  <?php }?>
<div id="esqueceu_senha"><a href="esqueceu_senha.php" class="group2 iframe">Esqueceu sua senha ? </a></div>
                </div>
      </div>
            </div>
  </div>
          <div id="conteudo2">
          	<div id="contato1">
            	 <p class="fonte28">Contato</p>
              <div id="textoContato"><p>Você tem alguma duvida? Sinta-se a vontade em nos contatar.  Será um prazer atendê-lo!</p> </div>
                <img src="images/CONTATO_email.jpg" width="213" height="54" /><br>
                <div id="textoTelefone">
                  <img src="images/logo_telefone.png"  /><p><b>Telefone:</b> (11) 3280-1922</p>
              </div>
              </div>              
            <div id="contato2">
           	  <p class="fonte18">Fale Conosco</p>
              <form id="form1" name="form1" method="post" action="enviaContato.php">
                <table id="tab_contato">
                  <tr>
                    <td><span id="sprytextfield1">
                      <label for="nome"></label>
                      <input name="nome" type="text" id="nome" size="50" />
                    <span class="textfieldRequiredMsg">*</span></span></td>
                  </tr>
                  <tr>
                    <td><span id="sprytextfield2">
                    <label for="email"></label>
                    <input name="email" type="text" id="email" size="15" maxlength="14" />
                    <span class="textfieldRequiredMsg">*</span><span class="textfieldInvalidFormatMsg">*</span></span></td>
                  </tr>
                  <tr>
                    <td><span id="sprytextfield3">
                    <label for="telefone"></label>
                    <input name="telefone" type="text" id="telefone" size="50" />
                    <span class="textfieldRequiredMsg">*</span><span class="textfieldInvalidFormatMsg">*</span></span></td>
                  </tr>
                  <tr>
                    <td><span id="sprytextarea1">
                      <label for="mensagem"></label>
                      <textarea name="mensagem" id="mensagem" cols="55" rows="10"></textarea>
                    <span class="textareaRequiredMsg">*</span></span></td>
                  </tr>
                  <tr>
                    <td><input type="submit" name="enviar" id="enviar" value="" class="btnEnviar" /></td>
                  </tr>
                </table>
              </form>
            </div>
            <div class="clear"></div>
          </div>
          <div id="rodape">
  	<div id="rodape_interno">
    	<a href="http://www.agenciagh.com.br" title="Agência GH" target="_blank"><div id="marca_gh"></div></a>
        <div id="menu_inferior">
        	<div class="email_contato"><span class="negrito">E-mail:</span><a href="mailto:contato@telios.eng.br" class="group2">contato@telios.eng.br</a></div>
            
            <div class="menu_inferior_link"><a href="contato.php" title="CONTATO" class="group2">Contato</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="busca_publicadas.php" title="BUSCA ANÁLISE" class="group2">Busca Análises</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="estatisticas.php" title="RANKING DO CONHECIMENTO" class="group2">Ranking do Conhecimento</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="artigos.php" title="ARTIGOS" class="group2">Artigos</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="rca.php" title="RCA" class="group2">RCA</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="vantagens.php" title="VANTAGENS" class="group2">Vantagens</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="ferramenta.php" title="A FERRAMENTA" class="group2">A Ferramenta</a></div>
        </div>
        
        <div class="clear"></div>
    </div>
  </div>
        </div>

<!--<form id="enviaLogin" name="enviaLogin" method="post" action="logado.php" >
  <table cellspacing="0" id="tabGeral">
    <tr>
      <td align="right" valign="middle">Usuário:</td>
      <td><input name="login" type="text" id="login" size="30"/></td>
    </tr>
    <tr>
      <td align="right" valign="middle">Senha:</td>
      <td><input name="senha" type="password" id="senha" size="30"/></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><a href="#" onclick="recuperaSenha('senha/recuperaSenha.php');">Esqueci Minha Senha.</a></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input name="envia" type="submit" id="envia" value="Logar" class="btnLogar"/></td>
    </tr>
  </table>
</form>-->
<?php }?>
<script type="text/javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none", {hint:"Nome"});
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "phone_number", {format:"phone_custom", hint:"(00) 0000-0000", useCharacterMasking:true, validateOn:["blur"]});
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3", "email", {useCharacterMasking:true, validateOn:["blur"], hint:"E-mail"});
var sprytextarea1 = new Spry.Widget.ValidationTextarea("sprytextarea1", {hint:"Coment\xE1rio"});
</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29818024-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>