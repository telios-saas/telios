<?php
$nome = mysql_escape_string(addslashes($_POST['nome_cadastra']));
//$cnpj = $_POST['cnpj_cadastra'];
$email = mysql_escape_string(addslashes($_POST['email_cadastra'])); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="rating" content="General" />
<meta name="author" content="Agência GH" />
<meta name="language" content="pt-br" />
<meta name="ROBOTS" content="index,follow" />
<title>TELIOS</title>
</head>
<body>
<style>
body{
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	color:#000;
	line-height:15px;	
	background-image:url(images/site/bg3.gif);
}
input, select {
	padding:5px;
	background-color:#FFF;
	border:1px solid #898989;
}
textarea {
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	color:#000;
	padding:5px;
	background-color:#FFF;
	border:1px solid #898989;
}
</style>
<div id="topoDados">
	<div id="topoDadosInterno"></div>
</div>
<div id="conteudo2">
	<div id="dadosEsq">
    	<p class="fonte37">Cadastro</p>
        <p>Para ser um cliente Télios, é
necessário preencher corretamente
o formulário ao lado com os
respectivos dados cadastrais.</p>
<p>
Após preencher todo o formulário,
clique em "Prosseguir".
Em caso de dúvidas, entre
em contato conosco,
será um prazer atendê-lo.</p>
    </div>
    <div id="dadosDir">
    <form id="enviaDados" name="enviaDados" method="post" onsubmit="return false;" >
    <div id="dadosDirInterna">
    <div id="ieForm">
    <p></p>
    <table id="tabGeral">
    <tr>
      <td align="right" valign="middle">Login*</td>
      <td><input name="responsavel" type="text" id="responsavel" size="60" value="<?php echo $nome;?>" onkeypress="enviar7(event);"/></td>
    </tr>
    <tr>
      <td align="right" valign="middle">E-mail*</td>
      <td><input name="email_empresa" type="text" id="email_empresa" size="60" value="<?php echo $email;?>" onkeypress="enviar7(event);"/></td>
    </tr>
    <tr>
      <td width="155" align="right" valign="middle">Empresa:</td>
      <td><input name="razao_social" type="text" id="razao_social" size="60" onkeypress="enviar7(event);"/>
       </td>
    </tr>
    <tr>
      <td align="right">Endereço:</td>
      <td>
        <input name="endereco" type="text" id="endereco" size="60" onkeypress="enviar7(event);"/></td>
    </tr>
    <tr>
      <td align="right">Telefone:</td>
      <td>
        <input name="telefone" type="text" id="telefone" onKeyPress="Mascara('TEL',this,event);" size="20" maxlength="14" onkeypress="enviar7(event);"/></td>
    </tr>
    <tr>
      <td align="right">UF:</td>
      <td><select name="estado" id="estado">
        
          <option value="RS">RS</option>
          <option value="SC">SC</option>
          <option value="PR">PR</option>
          <option value="SP">SP</option>
          <option value="MS">MS</option>
          <option value="RJ">RJ</option>
          <option value="ES">ES</option>
          <option value="MG">MG</option>
          <option value="GO">GO</option>
          <option value="DF">DF</option>
          <option value="MT">MT</option>
          <option value="RO">RO</option>
          <option value="AC">AC</option>
          <option value="BA">BA</option>
          <option value="TO">TO</option>
          <option value="PA">PA</option>
          <option value="AM">AM</option>
          <option value="RR">RR</option>
          <option value="AP">AP</option>
          <option value="MA">MA</option>
          <option value="PI">PI</option>
          <option value="CE">CE</option>
          <option value="PB">PB</option>
          <option value="PE">PE</option>
          <option value="AL">AL</option>
          <option value="SE">SE</option>
        </select></td>
    </tr>
    <tr>
      <td align="right">CEP:</td>
      <td><label for="estado"></label>
        <label for="cep"></label>
        <input name="cep" type="text" id="cep" onKeyPress="Mascara('CEP',this,event);" size="15" maxlength="9" onkeypress="enviar7(event);"/></td>
    </tr>
    <tr>
      <td align="right">País:</td>
      <td><label for="pais"></label>
      <input name="pais" type="text" id="pais" value="Brasil" size="40" onkeypress="enviar7(event);"/></td>
    </tr>
    <tr>
      <td align="right">Ramo Atividade*</td>
      <td><input name="ramo_atividade" type="text" id="ramo_atividade" size="35" onkeypress="enviar7(event);"/></td>
    </tr>
    <tr>
      <td align="right">Senha*</td>
      <td><input name="senha1" type="password" id="senha1" size="30" onkeypress="enviar7(event);"/></td>
    </tr>
    <tr>
      <td align="right">Confirmar Senha*</td>
      <td><input name="senha2" type="password" id="senha2" size="30" onblur="validarSenha()" onkeypress="enviar7(event);"/></td>
    </tr>
    <tr>
      <td align="right">&nbsp;</td>
      <td>* Campos Obrigatórios</td>
    </tr>
    </table>    
    </div>
    
    </div>
   <input name="envia" type="button" id="envia" onclick="cliente_cadastra('enviaDados', 'cadastra2.php', 'dadosDir');" value="" class="btnProsseguir_2"/>
    </form>
    </div>
    <div class="clear"></div>
</div>

</body>
</html>