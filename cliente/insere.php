<?php 
session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado
?>
<div id="chamada" class="chamada37">Cadastrar Clientes</div>
<div class="passos"><span class="passoAtivo">1° Passo</span> &raquo; 2° Passo</div>
<p>Aqui você deverá inserir os dados do seu cliente.</p>
<form id="enviaDados" name="enviaDados" method="post" action="#" >
  <table id="tabGeral">
    <tr>
      <td align="right" valign="middle">Razão Social* :</td>
      <td colspan="3"><input name="razao_social" type="text" id="razao_social" size="60" />
        <label for="razao_social"></label></td>
    </tr>
    <tr>
      <td align="right" valign="middle">CNPJ*:</td>
      <td colspan="3"><input name="cnpj" type="text" id="video2" onKeyPress="Mascara('CNPJ',this,event);" size="20" maxlength="18" />
        <label for="cnpj"></label>
        <label for="video2"></label></td>
    </tr>
    <tr>
      <td align="right">Ins. Estadual*</td>
      <td colspan="3"><input name="ins_estadual" type="text" id="ins_estadual" size="20" maxlength="15" onKeyPress="Mascara('INSCRI',this,event);"/>
        <label for="ins_estadual"></label></td>
    </tr>
    <tr>
      <td align="right">Endereço*:</td>
      <td colspan="3"><label for="endereco"></label>
        <input name="endereco" type="text" id="endereco" size="60" /></td>
    </tr>
    <tr>
      <td align="right">Telefone*:</td>
      <td colspan="3"><label for="telefone"></label>
        <input name="telefone" type="text" id="telefone" onKeyPress="Mascara('TEL',this,event);" size="20" maxlength="14"/></td>
    </tr>
    <tr>
      <td align="right">E-mail da Empresa*:</td>
      <td colspan="3"><label for="email_empresa"></label>
        <input name="email_empresa" type="text" id="email_empresa" size="60" /></td>
    </tr>
    <tr>
      <td align="right">Estado:</td>
      <td><label for="estado"></label>
        <select name="estado" id="estado">
          <option value="RS">RS</option>
          <option value="SC">SC</option>
          <option value="PR">PR</option>
          <option value="SP">SP</option>
          <option value="MS">MS</option>
          <option value="RJ">RJ</option>
          <option value="ES">ES</option>
          <option value="MG">MG</option>
          <option value="GO">GO</option>
          <option value="DF">DF</option>
          <option value="MT">MT</option>
          <option value="RO">RO</option>
          <option value="AC">AC</option>
          <option value="BA">BA</option>
          <option value="TO">TO</option>
          <option value="PA">PA</option>
          <option value="AM">AM</option>
          <option value="RR">RR</option>
          <option value="AP">AP</option>
          <option value="MA">MA</option>
          <option value="PI">PI</option>
          <option value="CE">CE</option>
          <option value="PB">PB</option>
          <option value="PE">PE</option>
          <option value="AL">AL</option>
          <option value="SE">SE</option>
        </select></td>
      <td align="right">CEP:</td>
      <td><label for="cep"></label>
        <input name="cep" type="text" id="cep" onKeyPress="Mascara('CEP',this,event);" size="15" maxlength="9"/></td>
    </tr>
    <tr>
      <td align="right">País:</td>
      <td colspan="3"><label for="pais"></label>
      <input name="pais" type="text" id="pais" size="40" /></td>
    </tr>
    <tr>
      <td align="right">Ramo Atividade*:</td>
      <td colspan="3"><input name="ramo_atividade" type="text" id="ramo_atividade" size="35" /></td>
    </tr>
    <tr>
      <td align="right">Responsável Técnico*:</td>
      <td colspan="3"><label for="responsavel"></label>
      <input name="responsavel" type="text" id="responsavel" size="60" /></td>
    </tr>
    <tr>
      <td align="right">CPF do Responsável*:</td>
      <td colspan="3"><label for="cpf_responsavel"></label>
      <input name="cpf_responsavel" type="text" id="cpf_responsavel" onKeyPress="Mascara('CPF',this,event);" size="20" maxlength="14"/></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="3">* Campos Obrigatórios</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="3"><input name="envia" type="button" id="envia" onclick="cliente('enviaDados', 'cliente/insere2.php', 'conteudo');" value="Enviar" class="btnLogar"/>
        <input type="reset" name="limpar" id="limpar" value="Limpar" class="btnLogar" /></td>
    </tr>
  </table>
</form>
<?php } else { // se usuário não estiver logado?>
	<script language="JavaScript">
			window.location.href = "../index.php";
	</script>
<?php }?>