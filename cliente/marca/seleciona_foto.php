<?php 
session_start();
if (isset($_SESSION['usuario'])) { ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- UPLOAD IMAGEM -->
<script type="text/javascript" src="../../js/upload/mootools-trunk.js"></script>
<script type="text/javascript" src="../../js/upload/Swiff.Uploader.js"></script>
<script type="text/javascript" src="../../js/upload/Fx.ProgressBar.js"></script>
<script type="text/javascript" src="../../js/upload/FancyUpload2.js"></script>
<script type="text/javascript">
window.addEvent('load', function() {
	var swiffy = new FancyUpload2($('status'), $('images-list'), {
		url: $('form_imagens').action,
		fieldName: 'photoupload',
		path: 'Swiff.Uploader.swf',
		onLoad: function() {
			$('loading').destroy();
			$('status').removeClass('hide');			
			this.options.typeFilter = {'Imagens (*.jpg, *.jpeg, *.gif, *.png)': '*.jpg; *.jpeg; *.gif; *.png'};
		},
		limitFiles: 1,
		debug: true,
		target: 'add-image'
	});
	/**
	 * Various interactions
	 */
 
	$('add-image').addEvent('click', function() {
		swiffy.browse();
		return false;
	});
 
	$('clear-list').addEvent('click', function() {
		swiffy.removeFile();
		return false;
	}); 
 
	$('upload-images').addEvent('click', function() {
		swiffy.upload();
		return false;
	});
 
});
</script>
<link href="../../css/upload.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<?php $id = $_GET['id'];?>
<div id="uploadImg">
  <form action="upload.php?id=<?php echo $id;?>" method="post" enctype="multipart/form-data" id="form_imagens">
    <div id="uploadEsq">
      <div id="loading"> <img src="../../images/upload/uploading.gif" alt="" /><br />
        Carregando... </div>
      <div id="status" class="hide">
        <div> <strong class="overall-title">Progresso Total</strong><br />
          <img src="../../images/upload/bar.gif" class="progress overall-progress" /> </div>
        <div> <strong class="current-title">Arquivo atual</strong><br />
          <img src="../../images/upload/bar.gif" class="progress current-progress" /> </div>
        <div class="current-text"></div>
      </div>
      <table width="100%">
        <tr>
          <td><a href="#" id="add-image" title="ADICIONAR IMAGEM"><img src="../../images/upload/selecionaFotos.gif" /> </a></td>
        </tr>
        <tr>
        <td><a href="#" id="clear-list" > </a><a href="#" id="upload-images" title="ENVIAR IMAGEM"><img src="../../images/upload/btnsalvaFotos.gif" /> </a></td>
        </tr>
      </table>
    </div>
    <div id="images-list"></div>
    <div class="clear"></div>
  </form>
</div>
</body>
</html>
<?php } else {?>
<script language="JavaScript">
	window.location.href = "http://www.telios.eng.br/index.php?deslogado=erro";
</script>
<?php }?>
