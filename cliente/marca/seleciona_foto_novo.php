
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<script type="text/javascript" src="../../js/get.js" /></script>
<script type="text/javascript" src="../../js/jquery.js" /></script>
<script type="text/javascript" src="../../js/jquery.MultiFile.js" /></script>
<style>
*{
	margin:0;
	padding:0;
	border:0;	
}
body{
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	line-height:15px;
	color:#000;
	background-color:#FFF;
}
#ulpload_img_capa p{
	padding:5px 0;		
}
input{
	border:1px solid #CCC;
	background-color:#FFF;
	width:200px;
	padding:5px;
}
.btnGravar {
	width:83px;
	height:30px;
	background-image:url(../../images/B_gravar.jpg);
	background-repeat:no-repeat;
	background-color:transparent;
	cursor:pointer;
	background-position: 0 0;
	margin-bottom:30px;
	border:none;
	padding:0;
	margin:0;
}
.btnGravar:hover {
	width:83px;
	height:30px;
	background-image:url(../../images/B_gravar.jpg);
	cursor:pointer;
	background-position:0 -30px;
	margin-bottom:30px;
	border:none;
	padding:0;
	margin:0;
}
#acerto_img{
	background-color:#d9f998;
	border:1px solid #abe55d;
	padding:10px;	
	font-size:12px;
	font-weight:bold;
	margin-top:5px;
}
.fonteNOrmal{
	color:#666;
	font-weight:normal;
}
#acerto_img p{
	padding-bottom:10px;
}
#erro_img{
	background-color:#f8c2c2;
	border:1px solid #ec9595;
	font-size:12px;
	font-weight:bold;
	padding:10px;
	margin-top:5px;	
}
.MultiFile-label{
	border:1px solid #CCC;
	margin-top:15px;
	padding:10px;
	font-weight:bold;	
}
.MultiFile-label a{
	color:#666;
	padding-right:5px;	
}

</style>
</head>

<body>

<?php 
require("../../conexao.php"); // incluo a conexão
conexao();// inicio a conexão
include('m2brimagem.class.0.6.2.php');  
// instancia objeto m2brimagem  
$oImg = new m2brimagem();

$id_cliente = $_GET['id'];// Pego o ID do cliente

if(isset($_POST['upload'])){
 $pasta = 'fotos/g/';
 foreach($_FILES["img"]["error"] as $key => $error){

 if($error == UPLOAD_ERR_OK){
 	$tmp_name = $_FILES["img"]["tmp_name"][$key];
 	$cod = date('dmy') . '-' . $_FILES["img"]["name"][$key];
 	$nome = $_FILES["img"]["name"][$key];
	$uploadfile = $pasta . basename($cod);

 if(move_uploaded_file($tmp_name, $uploadfile)){
 	$acerto = 1;
	$arquivo = $cod;// Pego o nome da foto  
    $diretorio = 'fotos/g/';  
    $dir_thumbs = 'fotos/p/';
    $oImg->carrega($diretorio.$arquivo);
           $valida = $oImg->valida();  
           if ($valida == 'OK') {  
               // redimensiona  
               $oImg->redimensiona(85,85,'crop'); 
               // salva no diretório das miniaturas  
               $oImg->grava($dir_thumbs.$arquivo,true);       
           } 
		   
	// Grava informações no banco
	$update = "UPDATE clientes SET marca_empresa = '$arquivo' WHERE id_clientes = '$id_cliente'";
	$exeAltera = mysql_query($update, $base) or die(mysql_error());	   	
 }else{
 	$erro = 1;
	
	//echo "Erro ao enviar o arquivo " . $nome . "! Por favor tente outra vez!";
 } } } } ?>

<form name="upload_files" action="" enctype="multipart/form-data" method="post">
<div id="ulpload_img_capa">
	<p>Faça upload de sua imagem.<br />
(.JPEG, .JPG, .PNG ou .GIF)</p>
    <p><input name="img[]" type="file" class="multi" size="5" maxlength="1" accept="jpeg|jpg|png|gif" /></p>
	<p><input type="submit" name="upload" value="" class="btnGravar" /></p>
</div>
</form>
<?php if(isset($acerto)){?>
<div id="acerto_img">
	<p>Imagem foi enviada com sucesso!</p>
    <p class="fonteNOrmal">Para atualizar sua imagem clique no link abaixo.</p>
</div>
<?php } 
if(isset($erro)){?>
<div id="erro_img">
	<p>Arquivo não permitido ou campo vazio!</p>
</div>
<?php }?>

</body>
</html>
