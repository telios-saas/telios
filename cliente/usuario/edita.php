<?php 
session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado
	require("../../conexao.php");
	conexao();
	$id = $_GET['id'];
	// Seleciona dados do usuario
	$select = "SELECT * FROM user_clientes WHERE id_user = '$id'";
	$query = mysql_query($select, $base) or die(mysql_error());
	$reg_usuario = mysql_fetch_assoc($query);
	$id_usuario = $reg_usuario['id_login'];
	// Seleciona dados do login do usuario
	$select_login = "SELECT * FROM usuarios WHERE id_usuario = '$id_usuario'";
	$query_login = mysql_query($select_login, $base) or die(mysql_error());
	$reg_login = mysql_fetch_assoc($query_login);
	//Seleciono as empresas cadastradas
	$select_empresa = "SELECT * FROM clientes ORDER BY razao_social ASC";
	$query_empresa = mysql_query($select_empresa, $base) or die(mysql_error());
	$linhas_empresa = mysql_num_rows($query_empresa);
?>
<div id="chamadaInput">Aqui você deverá editar os dados do seu cliente.</div>
<form id="enviaDados" name="enviaDados" method="post" action="#" >
  <table id="tabGeral">
    <tr>
      <td align="right" valign="middle">Nome* :</td>
      <td><input name="nome" type="text" id="nome" size="60" value="<?php echo $reg_usuario['nome'];?>"/>
        </td>
    </tr>
    <tr>
      <td align="right" valign="middle">Área de trabalho*:</td>
      <td><input name="area_trabalho" type="text" id="area_trabalho"  size="60" value="<?php echo $reg_usuario['area'];?>"/></td>
    </tr>
    <tr>
      <td align="right">Empresa*</td>
      <td><select name="empresa" id="empresa">
          <option value="<?php echo $reg_usuario['empresa'];?>"><?php echo $reg_usuario['empresa'];?></option>
          <?php if($linhas_empresa > 0){
			  while($reg_empresa = mysql_fetch_assoc($query_empresa)){?>
              <option value="<?php echo $reg_empresa['id_clientes'];?>"><?php echo $reg_empresa['razao_social'];?></option>
          <?php } } else {?>
          	<option value="">NENHUMA EMPRESA CADASTRADA...</option>
          <?php }?>
      </select></td>
    </tr>
    <tr>
      <td align="right">E-mail / Login*:</td>
      <td><label for="email"></label>
      <input name="email" type="text" id="email" size="60" value="<?php echo $reg_login['usuario'];?>"/></td>
    </tr>
    <tr>
      <td align="right">Bloquear:</td>
      <td><input name="bloquear" type="checkbox" id="bloquear" value="S" <?php if($reg_login['bloqueado'] == "S"){?>checked="checked"<?php }?> />
      <label for="bloquear">Sim, desejo bloquear este usuário</label></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>* Campos Obrigatórios</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input name="envia" type="button" id="envia" onclick="usuario_edita('enviaDados', 'cliente/usuario/gerencia.php?id=<?php echo $id;?>&amp;acao=2', 'alterar');" value="" class="btnGravar"/></td>
    </tr>
  </table>
</form>
<?php } else { // se usuário não estiver logado?>
<script language="JavaScript">
			window.location.href = "../index.php";
	</script>
<?php }?>