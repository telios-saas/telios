<?php 
session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado
?>
<?php 
    $menuAtivo = '';
    include 'menu_top.php'; 
?>
<!-- FIM MENU TOP -->
<div id="geral">
  
  <div id="sairSistema">
  	<p>Você tem certeza que deseja sair do sistema ? <a href="logout.php" class="clicSim">SIM</a> | <a href="index.php" class="clicNao">NÃO</a></p>
  </div>
</div>
<!-- FIM GERAL -->
<?php } else { // se usuário não estiver logado?>
<script language="JavaScript">
	window.location.href = "../index.php";
</script>
<?php }?>
