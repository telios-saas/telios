<?php
session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado
    require("conexao.php");
    require("src/email.php");
    conexao();
    $apontador = $_SESSION['apontador'];
    $id_acao_email = isset($_POST['acao_email']) ? $_POST['acao_email'] : null;
    $sql = "SELECT 
                a.id_acao AS acao_id,
                a.responsavel AS acao_reponsavel, 
                a.prazo AS acao_prazo,
		a.resposta AS resposta,
                af.id_analise AS analise_id,
                af.titulo AS analise_titulo,
                a.descricao AS acao_descricao,
                s.descricao AS situacao_descricao 
            FROM acoes AS a 
            INNER JOIN analise_falhas AS af
                ON a.id_analise = af.id_analise
            INNER JOIN situacao AS s 
                ON s.id_situacao = a.id_situacao";
    if(isset($id_acao_email) && !empty($id_acao_email)){
        $select_acao_email = $sql." WHERE id_acao = '$id_acao_email'";
        $query_acao_email       = mysql_query($select_acao_email, $base) or die(mysql_error()); 
        $reg_acao_email         = mysql_fetch_assoc($query_acao_email);

        $nome_user_acao         = $reg_acao_email['acao_reponsavel'];

        $select_user_email      = "SELECT * FROM usuarios WHERE nome = '$nome_user_acao' and apontador = '$apontador'";
        $query_user_email       = mysql_query($select_user_email, $base) or die(mysql_error()); 
        $reg_user_email         = mysql_fetch_assoc($query_user_email);

        $email_user = $reg_user_email['usuario'];
        $name_user = $reg_user_email['nome'];

	$enviaEmail = new Email();
	$retorno = $enviaEmail->emailAcoesBloqueioEspecifico($email_user, $name_user, $reg_acao_email);
	
    } else {
        $select_atrasada    = $sql." WHERE s.id_situacao = 6 AND a.apontador = '$apontador'";
        $query_atrasada     = mysql_query($select_atrasada, $base) or die(mysql_error()); 
        $linhas_atrasada    = mysql_num_rows($query_atrasada);

        if($linhas_atrasada > 0){
            while ($reg_atrasada = mysql_fetch_assoc($query_atrasada)){
                $nome_user_acao         = $reg_atrasada['acao_reponsavel'];

                $select_user_email      = "SELECT * FROM usuarios WHERE nome = '$nome_user_acao' and apontador = '$apontador'";
                $query_user_email       = mysql_query($select_user_email, $base) or die(mysql_error()); 
                $reg_user_email         = mysql_fetch_assoc($query_user_email);

                $email_user = $reg_user_email['usuario'];
                $name_user = $reg_user_email['nome'];

                $enviaEmail = new Email();
		$enviaEmail->emailAcoesBloqueioEspecifico($email_user, $name_user, $reg_atrasada);
            }
        }

        $select_pendente = $sql." WHERE s.id_situacao = 1 AND a.apontador = '$apontador'";
        $query_pendente = mysql_query($select_pendente, $base) or die(mysql_error()); 
        $linhas_pendente = mysql_num_rows($query_pendente);
        if($linhas_pendente > 0){
            while ($reg_pendente = mysql_fetch_assoc($query_pendente)){
                $nome_user_acao = $reg_pendente['acao_reponsavel'];

                $select_user_email = "SELECT * FROM usuarios WHERE nome = '$nome_user_acao' and apontador = '$apontador'";
                $query_user_email = mysql_query($select_user_email, $base) or die(mysql_error()); 
                $reg_user_email = mysql_fetch_assoc($query_user_email);

                $email_user = $reg_user_email['usuario'];
                $name_user = $reg_user_email['nome'];

                $enviaEmail = new Email();
		$enviaEmail->emailAcoesBloqueioEspecifico($email_user, $name_user, $reg_pendente);
            }
        }
	$retorno = "E-mails enviados com sucesso!";
    }
    
    echo json_encode($retorno);
}
?>