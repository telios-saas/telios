<?php
class Email {
    
    public $base_conexao_conexao;
    
    // carregar o arquivo conexao.php anteriormente
    public function __construct() {
        //require("../conexao.php");
        $this->base_conexao = conexao(true);
    }
    
    private function cabecalho($assunto){
        $cabecalho   = array();
        $cabecalho[] = "MIME-Version: 1.0";
	$cabecalho[] = "X-Priority: 3";
        $cabecalho[] = "Content-type: text/html; charset=UTF-8";
        $cabecalho[] = utf8_decode("From: Télios <suporte@telios.eng.br>");
        //$cabecalho[] = "Bcc: Télios <telios@telios.eng.br>";
        //$cabecalho[] = "Reply-To: Télios <telios@telios.eng.br>";
        //$cabecalho[] = "Subject: $assunto";
        //$cabecalho[] = "X-Mailer: PHP/".phpversion();
        return $cabecalho;
    }
    
    private function enviar($parametros){
	return mail(implode(",", $parametros->destinatarios), $parametros->assunto, $parametros->mensagem, implode("\n", $parametros->cabecalho));
    }  
    
    public function emailAcoesBloqueioEspecifico($email_user, $name_user, $reg_acao_email) {
	$nome_user_acao         = $reg_acao_email['acao_reponsavel'];
        $prazo_acao             = $reg_acao_email['acao_prazo'];
        $situacao_acao          = $reg_acao_email['situacao_descricao'];
        $descricao_acao         = $reg_acao_email['acao_descricao'];
        $titulo_analise	        = $reg_acao_email['analise_titulo'];
	$resposta		= $reg_acao_email['resposta'];
	
	$email_conteudo = "Olá <b>$nome_user_acao</b>, este é uma aviso que a(s) ação(ões) abaixo em seu nome está(ão) pendente(s) ou atrasada(s).<br /><br />
	    <b>Análise: </b>$titulo_analise<br />
	    <b>Descrição da Ação: </b>$descricao_acao<br />
	    <b>Resposta: </b>$resposta<br />
	    <b>Prazo para resposta: </b>$prazo_acao<br />
	    <b>Situação: </b>$situacao_acao<br /><br />
	    O(s) líder(es) da(s) análise(s) pede(m) que você atualize a(s) mesma(s) na Télios (<a href='http://www.telios.eng.br'>http://www.telios.eng.br.</a>)";
	
	$emailParams = new stdClass();
	$emailParams->destinatarios = Array($email_user);
	$emailParams->assunto = "Notificação de situação de ação de bloqueio";
	$emailParams->cabecalho = $this->cabecalho($emailParams->assunto);
	$emailParams->mensagem = $email_conteudo;
	
	if ($this->enviar($emailParams)) {
	    $retorno = "Email da ação '$descricao_acao' enviado com sucesso para $name_user!";
	} else {
	    $retorno = "Ocorreu um erro no envio do email da ação '$descricao_acao' para $name_user!"; 
	}
	
	return $retorno;
    }
    
    public function emailUsuarioCadastradoAcao($email_user, $reg) {
	$responsavel = $reg["responsavel"];
	$titulo_analise = $reg["titulo_analise"];
	$descricao = $reg["descricao"];
	$resposta = $reg["resposta"];
	$prazo = $reg["prazo"];
	$situacao = $reg["situacao"];
	$quem_criou = $reg["quem_criou"];
	
	$email_conteudo = "Olá <b>$responsavel</b>, este é uma aviso que a ação abaixo foi criada em seu nome na Télios.<br /><br />
	    <b>Análise: </b>$titulo_analise<br />
	    <b>Descrição da Ação: </b>$descricao<br />
	    <b>Resposta: </b>$resposta<br />
	    <b>Prazo para resposta: </b>$prazo<br />
	    <b>Situação: </b>$situacao<br /><br />
	    O líder da análise (<b>$quem_criou</b>) pede que você atualize está pendência na Télios (<a href='http://www.telios.eng.br'>http://www.telios.eng.br.</a>)";
	
	$emailParams = new stdClass();
	$emailParams->destinatarios = Array($email_user);
	$emailParams->assunto = "Você foi cadastrado em uma ação de bloqueio";
	$emailParams->cabecalho = $this->cabecalho($emailParams->assunto);
	$emailParams->mensagem = $email_conteudo;
	
	$this->enviar($emailParams);
    }
    
    public function emailCadastroUsuarioNivel1($email_empresa, $usuarioId, $senha) {
	$email_conteudo = "Obrigado por se registrar no Sistema Télios. Sua conta foi criada e deve ser ativada antes de você usa-la.<br />
	    Para ativar sua conta, clique no link a seguir ou copie e cole no seu browser:<br />
	    <a href='http://www.telios.eng.br/ativacao.php?id=$usuarioId'>http://www.telios.eng.br/ativacao.php?id=$usuarioId</a><br />
	    Login: $email_empresa<br />
	    Senha: $senha";
	
	$emailParams = new stdClass();
	$emailParams->destinatarios = Array($email_empresa);
	$emailParams->assunto = "Cadastro na Télios";
	$emailParams->cabecalho = $this->cabecalho($emailParams->assunto);
	$emailParams->mensagem = $email_conteudo;
	
	$this->enviar($emailParams);
    }
    
    public function emailCadastroUsuarioNivel2($email, $nome, $empresa, $senha) {
	$email_conteudo = "$nome, você foi cadastrado no Sistema Télios pela empresa $empresa.<br />
	    Para ter acesso ao sistema acesse <a href='http://www.telios.eng.br'>http://www.telios.eng.br.</a><br />
	    Seus dados de acesso são:<br />
	    <b>Login:</b> $email<br />
	    <b>Senha:</b> $senha"; 
	
	$emailParams = new stdClass();
	$emailParams->destinatarios = Array($email);
	$emailParams->assunto = "Cadastro na Télios";
	$emailParams->cabecalho = $this->cabecalho($emailParams->assunto);
	$emailParams->mensagem = $email_conteudo;
	
	$this->enviar($emailParams);
    }
    
    public function emailEsquecimentoSenha($email, $idUsuario) {
	$email_conteudo = "Para alterar sua senha de acesso á ferramenta Télios.<br />
		Clique no link a seguir ou copie e cole no seu browser:<br />
		<a href='http://www.telios.eng.br/esqueceu_senha_2.php?id=$idUsuario'>http://www.telios.eng.br/alterar_senha.php</a>"; 
	
	$emailParams = new stdClass();
	$emailParams->destinatarios = Array($email);
	$emailParams->assunto = "Esquecimento de senha";
	$emailParams->cabecalho = $this->cabecalho($emailParams->assunto);
	$emailParams->mensagem = $email_conteudo;
	
	$this->enviar($emailParams);
    }
    
    public function dados_basicos_analise_cron(){
        $parametros = new stdClass();
        $parametros->assunto = 'Notificação de Ações Atrasadas';
        $parametros->cabecalho = $this->cabecalho($parametros->assunto);
        
        $select = "SELECT "
                    . "responsavel_email, "
                    . "responsavel_nome, "
                    . "analise_titulo, "
                    . "acao_descricao, "
                    . "acao_resposta, "
                    . "acao_prazo, "
                    . "criador_nome "
                    . "FROM "
                    . "v_email_acoes_atrasadas "
                . "WHERE STR_TO_DATE(acao_prazo, '%d/%m/%Y') = CURDATE() + INTERVAL 1 DAY";
        $query = mysql_query($select, $this->base_conexao) or die(mysql_error());

        while ($row = mysql_fetch_assoc($query)) {
            $parametros->destinatarios  = array($row['responsavel_email']);
            $parametros->mensagem       = '<html>
                                            <body>
                                                Olá <b>'.$row['responsavel_nome'].'</b>, este é uma aviso que existe uma ação cadastrada em seu nome na <b>Télios</b> cujo prazo de conclusão se encerra amanhã.<br/>
						<br/>
                                                <b>Análise:</b> '.$row['analise_titulo'].' <br/>
                                                <b>Descrição da Ação:</b> '.$row['acao_descricao'].' <br/>
                                                <b>Resposta:</b> '.$row['acao_resposta'].' <br/>
                                                <b>Prazo para resposta:</b> '.$row['acao_prazo'].' <br/>
						<br/>
                                                O líder da análise (<b>'.$row['criador_nome'].'</b>) pede que você atualize está pendência na Télios (http://www.telios.eng.br)
                                            </body>
                                           </html>';
            $this->enviar($parametros);                   
        }
    }
    
    public function dados_basicos_analise_cadastro($id){
        $parametros = new stdClass();
        $parametros->assunto = 'Notificação de Criação de Ação';
        $parametros->cabecalho = $this->cabecalho($parametros->assunto);
        
        $select = "SELECT "
                    . "responsavel_email, "
                    . "responsavel_nome, "
                    . "analise_titulo, "
                    . "acao_descricao, "
                    . "acao_resposta, "
                    . "acao_prazo, "
                    . "criador_nome "
                    . "FROM "
                    . "v_email_acoes_atrasadas "
                . "WHERE analise_id = $id";
        $query = mysql_query($select, $this->base_conexao) or die(mysql_error());

        while ($row = mysql_fetch_assoc($query)) {
            $parametros->destinatarios  = array($row['responsavel_email']);
            $parametros->mensagem       = '<html>
                                            <body>
                                                Olá <b>'.$row['responsavel_nome'].'</b>, este é uma aviso que a ação abaixo foi criada em seu nome na <b>Télios</b>.<br/>
						<br/>
                                                <b>Análise:</b> '.$row['analise_titulo'].'<br/>
                                                <b>Descrição da Ação:</b> '.$row['acao_descricao'].'<br/>
                                                <b>Resposta:</b> '.$row['acao_resposta'].'<br/>
                                                <b>Prazo para resposta:</b> '.$row['acao_prazo'].'<br/>
						<br/>
                                                O líder da análise (<b>'.$row['criador_nome'].'</b>) pede que você atualize esta pendência na Télios (http://www.telios.eng.br)
                                            </body>
                                           </html>';
            $this->enviar($parametros);
        }
    }
}
?>