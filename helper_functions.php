<?php
function imprimir($string){
    echo '<pre>';
    print_r($string);
    exit;
}

function formata_data_inserir($data) {
    if ($data <> ''){
        $data_replace = str_replace("/", "-", $data);
        $data_explode = explode('-',$data_replace);	    
        if(isset($data_explode[2])){
            $data = $data_explode[2].'-'.$data_explode[1].'-'.$data_explode[0];	
        } else {
            $data = $data_explode[1].'-'.$data_explode[0];
        }
    }
    return $data;
}
    
function formata_data_recuperar($data = NULL,$formato = 'd/m/Y', $delimiter = '/') {
    if ($formato == 'd/m/Y' && $data){
        $data_replace = str_replace("-", "/", $data);
        $data_explode = explode('/',$data_replace);
        $data = $data_explode[2].$delimiter.$data_explode[1].$delimiter.$data_explode[0];
    }
    return $data;
}

function retorna_mes($mes){ 
    switch ($mes) {  
        case 1 : $mes='Janeiro';    break; 
        case 2 : $mes='Fevereiro';  break; 
        case 3 : $mes='Março';      break; 
        case 4 : $mes='Abril';      break; 
        case 5 : $mes='Maio';       break; 
        case 6 : $mes='Junho';      break; 
        case 7 : $mes='Julho';      break; 
        case 8 : $mes='Agosto';     break; 
        case 9 : $mes='Setembro';   break; 
        case 10 : $mes='Outubro';   break; 
        case 11 : $mes='Novembro';  break; 
        case 12 : $mes='Dezembro';  break; 
    } 
    return $mes; 
} 