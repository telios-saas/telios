<?php
session_start(); 
require("conexao.php");
require_once "helper_functions.php";
conexao();

if(isset($_POST['login'])){ // Se o usuário digitar o login e senha
    // recebo login e senha
    $usuario = $_POST['login'];
    $senha = $_POST['senha'];
    // Codifico as variaveis com MD5
    $senha_cod = md5($senha);
    // verefico no banco se existe este login e senha
    $select = "SELECT * FROM usuarios WHERE usuario = '$usuario' AND senha = '$senha_cod' AND bloqueado = 'N'";
    $exe = mysql_query($select, $base) or die(mysql_error());
    $reg = mysql_fetch_array($exe, MYSQL_ASSOC);
    $linha = mysql_num_rows($exe); // retorna se existe alguma linha no banco
	
} else { // Se eu não tiver login e senha via POST 
	if(isset($_SESSION['usuario'])){ // Se o usuário já estiver logado
		$usuario = $_SESSION['usuario'];
		$senha_geral = $_SESSION['senha_geral'];	
		// verefico no banco se existe este login e senha
		$select = "SELECT * FROM usuarios WHERE usuario = '$usuario' AND senha = '$senha_geral' AND bloqueado = 'N'";
		$exe = mysql_query($select, $base) or die(mysql_error());
		$reg = mysql_fetch_array($exe, MYSQL_ASSOC);
		$linha = mysql_num_rows($exe); // retorna se existe alguma linha no banco
	} else {
		$linha = 0; // Atribuo o valor 0 para a variavel "dizendo que teve ZERO resultados na busca no banco"
	}
}

if($linha > 0){
    // Crio as sessões com os dados do usuário
    $_SESSION['usuario'] = $reg['usuario'];
    $_SESSION['id_usuario'] = $reg['id_usuario'];
    $_SESSION['senha_geral'] = $reg['senha'];
    $_SESSION['nome'] = $reg['nome'];
    $_SESSION['nivel'] = $reg['nivel'];
    $_SESSION['apontador'] = $reg['apontador'];
    $_SESSION['empresa'] = $reg['empresa'];
    $_SESSION['pontuacao'] = $reg['pontuacao'];

    $apontador = $_SESSION['apontador'];
    $data = date("d/m/Y");
    
    $id_empresa = $reg['apontador'];
    $select_user = "SELECT * FROM clientes WHERE id_clientes = '$id_empresa'";
    $exe_user = mysql_query($select_user, $base) or die(mysql_error());
    $reg_user = mysql_fetch_array($exe_user, MYSQL_ASSOC);
    $marca_empresa = $reg_user['marca_empresa'];
}
// Inicio a Sessão?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"lang="pt-br">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="rating" content="General" />
    <meta name="author" content="Agência GH" />
    <meta name="language" content="pt-br" />
    <meta name="ROBOTS" content="index,follow" />
    <title>TÉLIOS</title>
    <link href="../favicon.ico" rel="shortcut icon" />
    <link href="css/padrao.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="css/reset.css" rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/redmond/jquery-ui-1.10.3.custom.min.css" />
    <link rel="stylesheet" href="css/jquery.jqplot.min.css" />
    <!--[if lt IE 9]><script language="javascript" type="text/javascript" src="js/jqplot/excanvas.min.js"></script><![endif]-->
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-ui-1.10.3.custom.min.js"></script>
    <script language="javascript" src="js/jquery.blockUI.js"></script>
    <script language="javascript" src="js/get.js"  charset="utf-8"></script>
    <script language="javascript" src="js/post.js"></script>
    <script language="javascript" src="js/mascara.js"></script>
    <script language="javascript" src="js/update.js"></script>
    <script language="javascript" src="js/telios.js"></script>
    <script type="text/javascript" src="js/jqplot/jquery.jqplot.min.js"></script>
    <script type="text/javascript" src="js/jqplot/jqplot.canvasTextRenderer.min.js"></script>
    <script type="text/javascript" src="js/jqplot/jqplot.canvasAxisTickRenderer.min.js"></script>
    <script type="text/javascript" src="js/jqplot/jqplot.barRenderer.min.js"></script>
    <script type="text/javascript" src="js/jqplot/jqplot.categoryAxisRenderer.min.js"></script>
    <script type="text/javascript" src="js/jqplot/jqplot.pointLabels.min.js"></script>
    <script type="text/javascript" src="js/jqplot/jqplot.cursor.min.js"></script>
    <script type="text/javascript" src="js/jqplot/jqplot.dateAxisRenderer.min.js"></script>
    <script type="text/javascript" src="js/jqplot/jqplot.highlighter.min.js"></script>

<?php 
    if (!$_SESSION['usuario'] || $linha == 0) {
?>
        <script language="JavaScript">
            window.location.href = "index.php?deslogado=erro";
        </script>
<?php 
    } 
?>
</head>
    <body onload="LoadMyJs(Array('js/dashboard.js'));">
<div id="tudo">
    <div id="version">Versão: 1.1</div>
  <div id="topo">
    <div id="marcaEmpresa"><img src="cliente/marca/fotos/p/<?php echo $marca_empresa;?>" width="85" height="85" /></div>
    <div id="dadosEmpresa">
      <p><span class="fonte13">Nome:</span> <?php echo $_SESSION['nome'];?></p>
      <p><span class="fonte13">Perfil:</span> Usu&aacute;rio de Nivel <?php echo $_SESSION['nivel'];?></p>
      <p><span class="fonte13">Empresa:</span> <?php echo $_SESSION['empresa'];?></p>
    </div>
  </div>


<?php

if ($_SESSION['usuario']) { // verifico se usuario esta logado
    $apontador = $_SESSION['apontador'];
    $data = date("d/m/Y");
   
// Seleciono as situações
    $select_situacao = "SELECT id_situacao, descricao FROM situacao WHERE status = 'A' AND id_situacao <= 5 order by id_situacao ASC";
    $query_situacao = mysql_query($select_situacao, $base) or die(mysql_error());
    $linhas_situacao = mysql_num_rows($query_situacao);
    
    $select_situacao_acoes = "SELECT id_situacao, descricao FROM situacao WHERE status = 'A' AND id_situacao IN(1,3,5,6) order by id_situacao ASC";
    $query_situacao_acoes = mysql_query($select_situacao_acoes, $base) or die(mysql_error());
    $linhas_situacao_acoes = mysql_num_rows($query_situacao_acoes);

// Seleciono as unidades Fabris
    $select_unidade = "SELECT * FROM unidade_fabril WHERE apontador = '$apontador' order by nome ASC";
    $query_unidade = mysql_query($select_unidade, $base) or die(mysql_error());
    $linhas_unidade = mysql_num_rows($query_unidade);
    
    $view_select = "SELECT * FROM v_situacao_atual_analises order by unidade_nome ASC ";
    $view_query = mysql_query($view_select, $base) or die(mysql_error());
    $view_total = mysql_num_rows($view_query);
?>

    <div id="alterar">

    <?php 
        $menuAtivo = 'inicial';
        include 'menu_top.php'; 
        
    ?>
    <!-- FIM MENU TOP -->
    <div id="geral" style="min-width: 980px; width: 98%; max-width:1600px; margin: -70px auto;">
        <div class="menuLateral">
            <p class="fonte37" style="text-align:center">Painel de Controle</p>
        </div>
        <div id="dvSituacaoAtual" class="setor boxConteudoDireito bAll">
            <div class="tituloConteudoDireito bAll">Situa&ccedil;&atilde;o Atual das Análises</div>
            <div class="conteudo">
<?php 
            if ($linhas_situacao > 0 && $linhas_unidade > 0) {
?>
                <fieldset id="dvSituacaoAtual-fsSituacao">
                    <legend>Situa&ccedil;&atilde;o</legend>
<?php
                        $s = 0;
                        while ($situacao = mysql_fetch_assoc($query_situacao)) {
                            $reg_situacao[$s] = $situacao;
                            $s++;
?>
                            <div class="wdt-2-12 fleft">  
                                <input id="dvSituacaoAtual-chkSituacao<?php echo $s; ?>" type="checkbox" value="<?php echo $situacao['id_situacao']; ?>" checked="checked">
                                <label for="dvSituacaoAtual-chkSituacao<?php echo $s; ?>"><?php echo $situacao['descricao']; ?></label> 
                            </div>
<?php                          
                        }
                        
?>
                </fieldset>
                <fieldset id="dvSituacaoAtual-fsUnidade" style="margin-top:8px">
                    <legend>Unidade</legend>
<?php
                        $u = 0;
                        while ($unidade = mysql_fetch_assoc($query_unidade)) {
                            $reg_unidade[$u] = $unidade;
                            $u++;
?>
                            <div class="wdt-2-12 fleft">
                                <input id="dvSituacaoAtual-chkUnidade<?php echo $u; ?>" type="checkbox" value="<?php echo $unidade['id_unidade']; ?>" checked="checked">
                                <label for="dvSituacaoAtual-chkUnidade<?php echo $u; ?>"><?php echo $unidade['nome']; ?></label>                
                            </div>
<?php
                        }
?>    
                </fieldset>
                <br/>
                <div id="dvSituacaoAtual-div_graficoResultado" class="div_graficoResultado">
                    <!--Carregamento AJAX-->
                </div>
                <div id="dvSituacaoAtual-div_tabelaResultado" class="div_tabelaResultado">
                    <table id="dvSituacaoAtual-buscaSituacaoUnidade" class="tabelaResultado" cellpadding="1" style="font-size:0.9em;">
                        <!--Carregamento AJAX-->
                    </table>
                </div>
                <div id="dvSituacaoAtual-div_erroResultado" class="div_erroResultado">
                    <!--Erro de Resultado-->
                </div>
<?php 
            } else {
?>
                <center>N&atilde;o existem Unidades Fabris para o usu&aacute;rio logado ou Situa&ccedil;&otilde;es cadastradas no sistema.</center>
<?php 
            }
?>
            </div>
        </div>
        <div class="clear"></div>
        <div id="dvSituacaoMensal" class="setor boxConteudoDireito bAll">
            <div class="tituloConteudoDireito bAll">Evolu&ccedil;&atilde;o Mensal das Análises</div>
            <div class="conteudo">
<?php 
                if ($linhas_situacao > 0 && $linhas_unidade > 0) {
?>
                    <fieldset id="dvSituacaoMensal-fsSituacao">
                        <legend>Situa&ccedil;&atilde;o</legend>
<?php
                           foreach ($reg_situacao as $i=>$v ) {
?>
                                <div class="wdt-2-12 fleft">  
                                    <input id="dvSituacaoMensal-chkSituacao<?php echo $i; ?>" type="checkbox" value="<?php echo $v['id_situacao']; ?>" checked="checked">
                                    <label for="dvSituacaoMensal-chkSituacao<?php echo $i; ?>"><?php echo $v['descricao']; ?></label> 
                                </div>
<?php
                        }
?>
                </fieldset>
                
                <fieldset id="dvSituacaoMensal-fsUnidade" style="margin-top:8px">
                    <legend>Unidade</legend>
<?php
                        foreach ($reg_unidade as $i=>$v ) {
?>
                            <div class="wdt-2-12 fleft">  
                                <input id="dvSituacaoMensal-chkUnidade<?php echo $i; ?>" type="checkbox" value="<?php echo $v['id_unidade']; ?>" checked="checked">
                                <label for="dvSituacaoMensal-chkUnidade<?php echo $i; ?>"><?php echo $v['nome']; ?></label> 
                            </div>
<?php
                    }
?>
                </fieldset>
                
		<fieldset id="dvSituacaoMensal-fsMes" style="margin-top:8px">
                    <legend>M&ecirc;s</legend>            
<?php
            $data     = new DateTime();
            $mes_atual=  substr(str_pad($data->format('m'),1,'0',STR_PAD_LEFT),-2,2);
            $data_atual=$data->format('Y').$mes_atual;
		    
		    mysql_query("SET lc_time_names = 'pt_BR'", $base);
		    $query_meses = "SELECT DATE_FORMAT(data, '%Y%m') as numerico, DATE_FORMAT(data, '%M / %Y') as extenso FROM situacao_mensal group by numerico order by numerico;";
		    $exe_q_meses = mysql_query($query_meses, $base) or die(mysql_error());
		    while ($row = mysql_fetch_assoc($exe_q_meses)) {
			$row['extenso'] = ucfirst($row['extenso']);
			$meses[] = $row; 
		    }
            setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
            $meses[]= array('numerico'=>$data_atual,'extenso'=>ucfirst(strftime('%B',time())).' / '.strftime('%Y',time()));
?>
                    
                    <label for="DataInicialSituacaoMensal">De</label>
                    <select id="DataInicialSituacaoMensal" name="DataInicialSituacaoMensal" class="wdt-15-12">
<?php
			$numMeses = sizeof($meses);
                        $contador = 0;
			foreach ($meses as $mes) {
			    $contador++;
			    $selecionado = $numMeses >= 12 ? $numMeses-11 == $contador : $contador == 1;
?>			    
                        <option value="<?php echo $mes['numerico']; ?>" <?php echo ($selecionado) ? 'selected' : null; ?>><?php echo $mes['extenso']; ?></option>
<?php
                        }
?>
                        <!-- Carregamento pela DataInicialSituacaoMensal -->
                    </select>
                    <label for="DataFinalSituacaoMensal"> At&eacute; </label>
                    <select id="DataFinalSituacaoMensal" name="DataFinalSituacaoMensal" class="wdt-15-12">
<?php
                        $contador = 0;
			foreach ($meses as $mes) {
			    $contador++;
			    $selecionado = $contador == $numMeses;
?>
                        <option value="<?php echo $mes['numerico']; ?>" <?php echo ($selecionado) ? 'selected' : null; ?>><?php echo $mes['extenso']; ?></option>
<?php
                        }
?>
                    </select>

                </fieldset>
		
		<br/>
                <div id="dvSituacaoMensal-div_graficoResultado" class="div_graficoResultado">
                    <!--Carregamento AJAX-->
                </div>
                <div id="dvSituacaoMensal-div_tabelaResultado" class="div_tabelaResultado">
                    <table id="dvSituacaoMensal-buscaSituacaoUnidade" class="tabelaResultado" cellpadding="1" style="font-size:0.9em;">
                        <!--Carregamento AJAX-->
                    </table>
                </div>
                <div id="dvSituacaoMensal-div_erroResultado" class="div_erroResultado">
                    <!--Erro de Resultado-->
                </div>
<?php 
            } else {
?>
                <center>N&atilde;o existem Unidades Fabris para o usu&aacute;rio logado ou Situa&ccedil;&otilde;es cadastradas no sistema.</center>
<?php 
            }
?>
            </div>
        </div>
        <div class="clear"></div>
        <div id="dvEvolucaoMensal" class="setor boxConteudoDireito bAll">
            <div class="tituloConteudoDireito bAll">Evolu&ccedil;&atilde;o Mensal das Ações de Bloqueio</div>
            <div class="conteudo">
<?php 
                if ($linhas_situacao_acoes > 0 && $linhas_unidade > 0) {
?>
                    <fieldset id="dvEvolucaoMensal-fsSituacao">
                        <legend>Situa&ccedil;&atilde;o</legend>
<?php
                           $contador = 0;
                           while ($situacao_acoes = mysql_fetch_assoc($query_situacao_acoes)) {
?>
                                <div class="wdt-2-12 fleft">  
                                    <input id="dvEvolucaoMensal-chkSituacao<?php echo $contador; ?>" type="checkbox" value="<?php echo $situacao_acoes['id_situacao']; ?>" checked="checked">
                                    <label for="dvEvolucaoMensal-chkSituacao<?php echo $contador; ?>"><?php echo $situacao_acoes['descricao']; ?></label> 
                                </div>
<?php
                        $contador++;}
?>
                    </fieldset>
                    
                    <fieldset id="dvEvolucaoMensal-fsUnidade" style="margin-top:8px;">
                        <legend>Unidade</legend>
<?php
                           foreach ($reg_unidade as $i=>$v ) {
?>
                                <div class="wdt-2-12 fleft">  
                                    <input id="dvEvolucaoMensal-chkUnidade<?php echo $i; ?>" type="checkbox" value="<?php echo $v['id_unidade']; ?>" checked="checked">
                                    <label for="dvEvolucaoMensal-chkUnidade<?php echo $i; ?>"><?php echo $v['nome']; ?></label> 
                                </div>
<?php
                        }
?>
                    </fieldset>
		
		    <fieldset id="dvEvolucaoMensal-fsMes" style="margin-top:8px">
                        <legend>M&ecirc;s</legend>            
<?php
                        $data           = new DateTime();
                        $mes_atual=  substr(str_pad($data->format('m'),1,'0',STR_PAD_LEFT),-2,2);           
                        $data_atual=$data->format('Y').$mes_atual;
			
			mysql_query("SET lc_time_names = 'pt_BR'", $base);
			$query_meses = "SELECT DATE_FORMAT(data, '%Y%m') as numerico, DATE_FORMAT(data, '%M / %Y') as extenso FROM evolucao_mensal group by numerico order by numerico;";
			$exe_q_meses = mysql_query($query_meses, $base) or die(mysql_error());
			$meses = array();
			while ($row = mysql_fetch_assoc($exe_q_meses)) {
			    $row['extenso'] = ucfirst($row['extenso']);
			    $meses[] = $row; 
			}
            setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
            $meses[]= array('numerico'=>$data_atual,'extenso'=>ucfirst(strftime('%B',time())).' / '.strftime('%Y',time()));
?>
                    
                        <label for="DataInicialEvolucaoMensal">De</label>
                        <select id="DataInicialEvolucaoMensal" name="DataInicialEvolucaoMensal" class="wdt-15-12">
<?php
                        $numMeses = sizeof($meses);
                        $contador = 0;
			foreach ($meses as $mes) {
			    $contador++;
			    $selecionado = $numMeses >= 12 ? $numMeses-11 == $contador : $contador == 1;
?>
                        <option value="<?php echo $mes['numerico']; ?>" <?php echo ($selecionado) ? 'selected' : null; ?>><?php echo $mes['extenso']; ?></option>
<?php
                        }
?>
                            <!-- Carregamento pela DataInicialEvolucaoMensal -->
                        </select>
                    <label for="DataFinalEvolucaoMensal"> At&eacute; </label>
                        <select id="DataFinalEvolucaoMensal" name="DataFinalEvolucaoMensal" class="wdt-15-12">
<?php
                        $contador = 0;
			foreach ($meses as $mes) {
			    $contador++;
			    $selecionado = $contador == $numMeses;
?>
                        <option value="<?php echo $mes['numerico']; ?>" <?php echo ($selecionado) ? 'selected' : null; ?>><?php echo $mes['extenso']; ?></option>
<?php
                        }
?>
                        </select>

                    </fieldset>
		
                    <br/>
                    <div id="dvEvolucaoMensal-div_graficoResultado" class="div_graficoResultado">
                        <!--Carregamento AJAX-->
                    </div>
                    <div id="dvEvolucaoMensal-div_tabelaResultado" class="div_tabelaResultado">
                        <table id="dvEvolucaoMensal-buscaSituacaoUnidade" class="tabelaResultado" cellpadding="1" style="font-size:0.9em;">
                            <!--Carregamento AJAX-->
                        </table>
                    </div>
                    <div id="dvEvolucaoMensal-div_erroResultado" class="div_erroResultado">
                        <!--Erro de Resultado-->
                    </div>
<?php 
                } else {
?>
                    <center>N&atilde;o existem Unidades Fabris para o usu&aacute;rio logado ou Situa&ccedil;&otilde;es cadastradas no sistema.</center>
<?php 
                }
?>
            </div> 
        </div>
        <div class="clear"></div>
    </div>
    </div>
</div>
    <!-- FIM GERAL -->
<?php 
    }
?>
</body>
</html>