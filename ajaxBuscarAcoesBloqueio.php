<?php
session_start(); // inicio a sessão
if($_SESSION['usuario']){ // verifico se usuario esta logado
    require("conexao.php");
    conexao();
    $apontador = $_SESSION['apontador'];
    $busca = isset($_GET['busca']) ? $_GET['busca'] : null;
    $acao = isset($_GET['acao']) ? $_GET['acao'] : null;
    $name_responsavel = $_SESSION['nome'];

    $situacao_sql = "SELECT id_situacao, descricao FROM situacao WHERE status = 'A' AND id_situacao IN(1,3,5,6) order by id_situacao ASC";
    $situacao_exe = mysql_query($situacao_sql, $base) or die(mysql_error());

    while ($row = mysql_fetch_assoc($situacao_exe)) {
        $grafico[$row['id_situacao']] = new stdClass();
        $grafico[$row['id_situacao']]->descricao = $row['descricao'];
        $grafico[$row['id_situacao']]->valor     = 0;
    }

    $sql = "SELECT * FROM v_acao_bloqueio WHERE acao_apontador = '$apontador'"; // SELECT PADRAO

    $sql .= (empty($_POST['unidade_fabril']))    ? NULL : ' AND unidade_id = '.$_POST['unidade_fabril'];
    $sql .= (empty($_POST['linha_producao']))    ? NULL : ' AND linha_producao_descricao = \''.mysql_real_escape_string($_POST['linha_producao']).'\'';
    $sql .= (empty($_POST['equipamento']))       ? NULL : ' AND equipamento_descricao = \''.mysql_real_escape_string($_POST['equipamento']).'\'';
    $sql .= (empty($_POST['titulo']))            ? NULL : ' AND analise_titulo = \''.mysql_real_escape_string($_POST['titulo']).'\'';
    $sql .= (empty($_POST['situacao']))          ? NULL : ' AND situacao_id = '.$_POST['situacao'];
    $sql .= (empty($_SESSION['apontador']))      ? NULL : ' AND acao_apontador = '.$_SESSION['apontador'];
    $sql .= (empty($_POST['responsavel']))       ? NULL : ' AND acao_responsavel = \''.mysql_real_escape_string($_POST['responsavel']).'\'';
    $sql .= (empty($_POST['descricao']))         ? NULL : ' AND acao_descricao like \'%'.mysql_real_escape_string($_POST['descricao']).'%\'';
    $sql .= " ORDER BY acao_id DESC";

    $exe = mysql_query($sql, $base) or die(mysql_error());

    $r['qtd'] = mysql_num_rows($exe);    
    while ($row = mysql_fetch_assoc($exe)) {
        $r['res'][] = $row;
        $grafico[$row['situacao_id']]->valor += 1;
    }

    foreach ($grafico as $k=>$v){
        $r['grafico'][] = array($v->descricao, $v->valor);
    }

    $r['nivel_usuario'] = $_SESSION['nivel'];

    echo json_encode($r);
}
?>