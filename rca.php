<?php session_start(); // Inicio a Sessão?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="análise de falhas, indústria, RCA - Root Cause Analisys, ações de bloqueio, causa raiz da falha
" />
		<meta name="description" content="Realize análises de falhas de maneira descomplicada e contribua para a melhoria nos indicadores da indústria." />
<meta name="rating" content="General" />
<meta name="author" content="Agência GH" />
<meta name="language" content="pt-br" />
<meta name="ROBOTS" content="index,follow" />
<title>TELIOS</title>
<link href="../favicon.ico" rel="shortcut icon" />
<link href="css/home.css" rel="stylesheet" type="text/css" media="screen" />
<script language="javascript" src="js/post.js"></script>
<script language="javascript" src="js/mascara.js"></script>
<script type="text/javascript" src="swfobject/swfobject.js"></script>

<link rel="stylesheet" href="css/colorbox.css" />
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.colorbox.js"></script>
		<script>
			$(document).ready(function(){
				
				$(".iframe").colorbox({iframe:true, width:"600px", height:"300px"});
			});
		</script>

</head>
<body>
<?php if (isset($_SESSION['usuario'])) { // Verifico se o Usuário ja esta logado?>
<script language="JavaScript">
			window.location.href = "logado.php";
		</script>
<?php } else { // Se o Usuário não estiver logado mostro form de login ?>
<div id="tudo">
  <div id="topo">
    <div id="topoInterno">
              <div id="linkHome"><a href="index.php"><img src="images/site/linkHome.gif" width="192" height="41" /></a></div>
              <a href="ferramenta.php"><div id="linkFerramenta"></div></a>
              <a href="rca.php"><div id="linkRca"></div></a>
      		  <a href="vantagens.php"><div id="linkVantagens"></div></a>
      		  <a href="artigos.php"><div id="linkArtigos"></div></a>
	  <a href="estatisticas.php" title="Estatísticas"><div id="linkEstatisticas"></div></a>
              <a href="busca_publicadas.php"><div id="linkBusca"></div></a>
              <a href="contato.php"><div id="linkContato"></div></a>
              <div id="formLogin">
        <div id="formLoginInterna">
                  <form id="log" name="log" method="post" action="logado.php" >
            <div id="inputFormLogin">
                      <input name="login" type="text" id="login"/>
                    </div>
            <div id="inputFormSenha">
                      <input name="senha" type="password" id="senha"/>
                    </div>
            <div id="inputFormEntrar">
                      <input name="envia" type="submit" id="envia" class="btnLogar" value=""/>
                    </div>
          </form>
                  <?php if(isset($_GET['deslogado'])){?>
                  <div id="erroLogin">Login ou Senha Inválido</div>
                  <?php }?>
<div id="esqueceu_senha"><a href="esqueceu_senha.php" class="group2 iframe">Esqueceu sua senha ? </a></div>
                </div>
      </div>
            </div>
  </div>
  <div id="fundoSite"> </div>
  <div id="conteudo2">
    <div id="rca1">
      <p class="fonte28">RCA  root cause analysis</p>
      <p>A Análise de Causa Raiz (Root Cause Analysis - RCA) é uma metodologia de resolução de problemas que proporciona valiosa contribuição na melhoria da disponibilidade dos ativos industriais, diminuição dos custos de manutenção e na formação de equipes de alta performance. </p>
      <p>Por ser um processo reativo, pois foca nas falhas que já ocorreram, necessita de dados para confirmar as hipóteses levantadas pela equipe de análise. Fatos e evidências são fundamentais. </p>
      <p>A análise deve estabelecer todas as relações entre o problema e a(s) causa(s) raiz(es) e um roteiro padrão proporciona auxílio e simplicidade nos passos a serem dados. Simplificadamente pode-se enumerar as seguintes etapas:</p>
    </div>
  </div>
  <div id="linhaRCA">
    <div id="conteudo3">
      <div id="chamada_1">
        <p class="fonte14Azul">Defina o<br />
          problema</p>
      </div>
      <div id="texto_1">
        <p>Explicite o fato/falha que se quer analisar, foque os esforços. </p>
      </div>
      <div id="chamada_2">
        <p class="fonte14Azul">Identifique as<br />
          relações causais</p>
      </div>
      <div id="texto_2">
        <p>Busque as informações, traga dados, converse com as pessoas, acesse o histórico do equipamento, desenhos de projeto, relatórios
          de manutenção, modificações etc. </p>
      </div>
      <div id="chamada_3">
        <p class="fonte14Azul">Reuna fatos<br />
          e evidências</p>
      </div>
      <div id="texto_3">
        <p>Pergunte "Por que?", aguce a curiosidade sobre os fatos e
          dados.</p>
      </div>
      <div id="chamada_4">
        <p class="fonte14Azul">Identifique<br />soluções eficazes</p>
      </div>
      <div id="texto_4">
        <p>O que poderia ser feito para quebrar a corrente entre a(s) causa(s) e a falha? Como prevenir a recorrência? As causas estão sob controle? Vão ao encontro do objetivo? Não causam outros transtornos? </p>
      </div>
      
       <div id="chamada_5">
        <p class="fonte14Azul">Implemente as<br />Recomendações</p>
      </div>
      <div id="texto_5">
        <p>Se fazemos o mesmo, teremos os mesmos resultados. </p>
      </div>
      
      <div id="chamada_6">
        <p class="fonte14Azul">Acompanhe a<br />Implementação</p>
      </div>
      <div id="texto_6">
        <p>Acompanhe a implementação das recomendações para
assegurar a eficácia.  </p>
      </div>
      
      
      
    </div>
  </div>
  <div id="rca3">
    <p>A eficácia da investigação é sempre impactada pela experiência do grupo neste processo. A sistematização do RCA desenvolve senso crítico para avaliar se certas recomendações realmente são pertinentes e serão implementadas. </p>
  </div>
  <div id="rodape">
  	<div id="rodape_interno">
    	<a href="http://www.agenciagh.com.br" title="Agência GH" target="_blank"><div id="marca_gh"></div></a>
        <div id="menu_inferior">
        	<div class="email_contato"><span class="negrito">E-mail:</span><a href="mailto:contato@telios.eng.br" class="group2">contato@telios.eng.br</a></div>
            
            <div class="menu_inferior_link"><a href="contato.php" title="CONTATO" class="group2">Contato</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="busca_publicadas.php" title="BUSCA ANÁLISE" class="group2">Busca Análises</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="estatisticas.php" title="RANKING DO CONHECIMENTO" class="group2">Ranking do Conhecimento</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="artigos.php" title="ARTIGOS" class="group2">Artigos</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="rca.php" title="RCA" class="group2">RCA</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="vantagens.php" title="VANTAGENS" class="group2">Vantagens</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="ferramenta.php" title="A FERRAMENTA" class="group2">A Ferramenta</a></div>
        </div>
        
        <div class="clear"></div>
    </div>
  </div>
</div>

<!--<form id="enviaLogin" name="enviaLogin" method="post" action="logado.php" >
  <table cellspacing="0" id="tabGeral">
    <tr>
      <td align="right" valign="middle">Usuário:</td>
      <td><input name="login" type="text" id="login" size="30"/></td>
    </tr>
    <tr>
      <td align="right" valign="middle">Senha:</td>
      <td><input name="senha" type="password" id="senha" size="30"/></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><a href="#" onclick="recuperaSenha('senha/recuperaSenha.php');">Esqueci Minha Senha.</a></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input name="envia" type="submit" id="envia" value="Logar" class="btnLogar"/></td>
    </tr>
  </table>
</form>-->
<?php }?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29818024-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>