// JavaScript Document
/*
1. frmNome - Nome do formulário
2. url - arquivo para onde será enviado o POST
3. destino (opcional) - conteiner para onde ira a resposta
vinda do arquivo "url"
*/
function geral(frmNome, url, destino){ // Função para o Login

	f = document.getElementById(frmNome);
	query="";
	j=0;
	for(i=0;i<f.elements.length;i++){
		if((f.elements[i].type=='radio' || f.elements[i].type=='checkbox' )&& !(f.elements[i].checked)){
			f.elements[i].value='';
			
			if(f.elements[i].value!=''){
				query += j==0 ? '' : '&';
				query += f.elements[i].name + '=' + f.elements[i].value;
				j++;
			}
		}
		else
		{
			query += j==0 ? '' : '&';
			query += f.elements[i].name + '=' + f.elements[i].value;
			j++;
		}
	}
	//chama a função postajax() para enviar os dados
	//ao arquivo .php que fará o processamento
	postajax(url, query, destino);
}


function envia_cadastro(frmNome, url, destino){ // Função para o Login

	if (document.enviaDados2.nome_cadastra.value == "" || document.enviaDados2.nome_cadastra.value == "NOME"){
				alert ("DIGITE O NOME!");
				document.enviaDados2.nome_cadastra.focus();
				return false;
	}
	/*if (document.enviaDados2.cnpj_cadastra.value == "" || document.enviaDados2.cnpj_cadastra.value == "CNPJ" ){
				alert ("DIGITE O CNPJ!");
				document.enviaDados2.cnpj_cadastra.focus();
				return false;
	}*/
	
	if (document.enviaDados2.email_cadastra.value == "" || document.enviaDados2.email_cadastra.value == "E-MAIL"){
				alert ("DIGITE O E-MAIL!");
				document.enviaDados2.email_cadastra.focus();
				return false;
	}
	if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.enviaDados2.email_cadastra.value))) {
		alert("E-MAIL INVÁLIDO.");
		document.enviaDados2.email_cadastra.focus();
		return false;
	} 
	
	f = document.getElementById(frmNome);
	query="";
	j=0;
	for(i=0;i<f.elements.length;i++){
		if((f.elements[i].type=='radio' || f.elements[i].type=='checkbox' )&& !(f.elements[i].checked)){
			f.elements[i].value='';
			
			if(f.elements[i].value!=''){
				query += j==0 ? '' : '&';
				query += f.elements[i].name + '=' + f.elements[i].value;
				j++;
			}
		}
		else
		{
			query += j==0 ? '' : '&';
			query += f.elements[i].name + '=' + f.elements[i].value;
			j++;
		}
	}
	//chama a função postajax() para enviar os dados
	//ao arquivo .php que fará o processamento
	postajax(url, query, destino);
}

/* PASSO 1 DE CADASTRO DE CLIENTES*/
function cliente(frmNome, url, destino){ // Função para o Login

	if (document.enviaDados.razao_social.value == ""){
				alert ("DIGITE A RAZÃO SOCIAL!");
				document.enviaDados.razao_social.focus();
				return false;
	}
	if (document.enviaDados.cnpj.value == ""){
				alert ("DIGITE O CNPJ!");
				document.enviaDados.cnpj.focus();
				return false;
	}
	if (document.enviaDados.ins_estadual.value == ""){
				alert ("DIGITE A INSCRIÇÃO ESTADUAL!");
				document.enviaDados.ins_estadual.focus();
				return false;
	}
	if (document.enviaDados.endereco.value == ""){
				alert ("DIGITE O ENDEREÇO!");
				document.enviaDados.endereco.focus();
				return false;
	}
	if (document.enviaDados.telefone.value == ""){
				alert ("DIGITE O TELEFONE!");
				document.enviaDados.telefone.focus();
				return false;
	}
	if (document.enviaDados.email_empresa.value == ""){
				alert ("DIGITE O E-MAIL!");
				document.enviaDados.email_empresa.focus();
				return false;
	}
	if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.enviaDados.email_empresa.value))) {
		alert("E-MAIL INVÁLIDO.");
		document.enviaDados.email_empresa.focus();
		return false;
	} 
	if (document.enviaDados.ramo_atividade.value == ""){
				alert ("DIGITE O RAMO DE ATIVIDADE!");
				document.enviaDados.ramo_atividade.focus();
				return false;
	}
	if (document.enviaDados.responsavel.value == ""){
				alert ("DIGITE O RESPONSAVEL TECNICO!");
				document.enviaDados.responsavel.focus();
				return false;
	}
	if (document.enviaDados.cpf_responsavel.value == ""){
				alert ("DIGITE O CPF DO RESPONSAVEL!");
				document.enviaDados.cpf_responsavel.focus();
				return false;
	}
	
	f = document.getElementById(frmNome);
	query="";
	j=0;
	for(i=0;i<f.elements.length;i++){
		if((f.elements[i].type=='radio' || f.elements[i].type=='checkbox' )&& !(f.elements[i].checked)){
			f.elements[i].value='';
			
			if(f.elements[i].value!=''){
				query += j==0 ? '' : '&';
				query += f.elements[i].name + '=' + f.elements[i].value;
				j++;
			}
		}
		else
		{
			query += j==0 ? '' : '&';
			query += f.elements[i].name + '=' + f.elements[i].value;
			j++;
		}
	}
	//chama a função postajax() para enviar os dados
	//ao arquivo .php que fará o processamento
	postajax(url, query, destino);
}

function cliente_cadastra(frmNome, url, destino){ // Função para o Login

	if (document.enviaDados.responsavel.value == ""){
				alert ("DIGITE O RESPONSÁVEL TECNICO!");
				document.enviaDados.responsavel.focus();
				return false;
	}
	if (document.enviaDados.email_empresa.value == ""){
				alert ("DIGITE O E-MAIL!");
				document.enviaDados.email_empresa.focus();
				return false;
	}
	if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.enviaDados.email_empresa.value))) {
		alert("E-MAIL INVÁLIDO.");
		document.enviaDados.email_empresa.focus();
		return false;
	} 
	
	if (document.enviaDados.ramo_atividade.value == ""){
				alert ("DIGITE O RAMO DE ATIVIDADE!");
				document.enviaDados.ramo_atividade.focus();
				return false;
	}	
	
	
	
	f = document.getElementById(frmNome);
	query="";
	j=0;
	for(i=0;i<f.elements.length;i++){
		if((f.elements[i].type=='radio' || f.elements[i].type=='checkbox' )&& !(f.elements[i].checked)){
			f.elements[i].value='';
			
			if(f.elements[i].value!=''){
				query += j==0 ? '' : '&';
				query += f.elements[i].name + '=' + f.elements[i].value;
				j++;
			}
		}
		else
		{
			query += j==0 ? '' : '&';
			query += f.elements[i].name + '=' + f.elements[i].value;
			j++;
		}
	}
	//chama a função postajax() para enviar os dados
	//ao arquivo .php que fará o processamento
	postajax(url, query, destino);
}


/*function envia_busca_analise(frmNome, url, destino){ // Função para o Login

	if (document.enviaDados.input_busca.value == ""){
				alert ("DIGITE SUA BUSCA!");
				document.enviaDados.input_busca.focus();
				return false;
	}	
	f = document.getElementById(frmNome);
	query="";
	j=0;
	for(i=0;i<f.elements.length;i++){
		if((f.elements[i].type=='radio' || f.elements[i].type=='checkbox' )&& !(f.elements[i].checked)){
			f.elements[i].value='';
			
			if(f.elements[i].value!=''){
				query += j==0 ? '' : '&';
				query += f.elements[i].name + '=' + f.elements[i].value;
				j++;
			}
		}
		else
		{
			query += j==0 ? '' : '&';
			query += f.elements[i].name + '=' + f.elements[i].value;
			j++;
		}
	}
	//chama a função postajax() para enviar os dados
	//ao arquivo .php que fará o processamento
	postajax(url, query, destino);
}*/




function envia_pesquisa(frmNome, url, destino){ // Função para o Login

	if (document.enviaPesquisa.busca_analise.value == ""){
				alert ("CAMPO DA BUSCA VAZIO!");
				document.enviaPesquisa.busca_analise.focus();
				return false;
	}
	
	
	f = document.getElementById(frmNome);
	query="";
	j=0;
	for(i=0;i<f.elements.length;i++){
		if((f.elements[i].type=='radio' || f.elements[i].type=='checkbox' )&& !(f.elements[i].checked)){
			f.elements[i].value='';
			
			if(f.elements[i].value!=''){
				query += j==0 ? '' : '&';
				query += f.elements[i].name + '=' + f.elements[i].value;
				j++;
			}
		}
		else
		{
			query += j==0 ? '' : '&';
			query += f.elements[i].name + '=' + f.elements[i].value;
			j++;
		}
	}
	//chama a função postajax() para enviar os dados
	//ao arquivo .php que fará o processamento
	postajax(url, query, destino);
}






/* SENHA CONFIRMAR*/
function validarSenha(){
	senha1 = document.enviaDados.senha1.value
	senha2 = document.enviaDados.senha2.value

	if (senha1 == senha2){
		//alert("SENHAS IGUAIS")
	} else
		alert("SENHAS DIFERENTES")
}
/* FIM CONFIRMAR SENHA*/
/* PASSO 2 DE CADASTRO DE CLIENTES*/
function senha_cliente(frmNome, url, destino){ 
	
	if (document.enviaDados.email.value == ""){
				alert ("DIGITE O E-MAIL!");
				document.enviaDados.email.focus();
				return false;
	}
	if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.enviaDados.email.value))) {
		alert("E-MAIL INVÁLIDO.");
		document.enviaDados.email.focus();
		return false;
	} 
	if (document.enviaDados.senha1.value == ""){
				alert ("DIGITE A SENHA!");
				document.enviaDados.senha1.focus();
				return false;
	}
	if (document.enviaDados.senha2.value == ""){
				alert ("CONFIRME SUA SENHA!");
				document.enviaDados.senha2.focus();
				return false;
	}
	f = document.getElementById(frmNome);
	query="";
	j=0;
	for(i=0;i<f.elements.length;i++){
		if((f.elements[i].type=='radio' || f.elements[i].type=='checkbox' )&& !(f.elements[i].checked)){
			f.elements[i].value='';
			
			if(f.elements[i].value!=''){
				query += j==0 ? '' : '&';
				query += f.elements[i].name + '=' + f.elements[i].value;
				j++;
			}
		}
		else
		{
			query += j==0 ? '' : '&';
			query += f.elements[i].name + '=' + f.elements[i].value;
			j++;
		}
	}
	//chama a função postajax() para enviar os dados
	//ao arquivo .php que fará o processamento
	postajax(url, query, destino);
}

/* CADASTRO DE USUÁRIOS */
function usuario(frmNome, url, destino){ 

	if (document.enviaDados.nome.value == ""){
				alert ("DIGITE O NOME!");
				document.enviaDados.nome.focus();
				return false;
	}
	if (document.enviaDados.area_trabalho.value == ""){
				alert ("DIGITE A ÁREA DE TRABALHO!");
				document.enviaDados.area_trabalho.focus();
				return false;
	}
	if (document.enviaDados.email.value == ""){
				alert ("DIGITE O E-MAIL!");
				document.enviaDados.email.focus();
				return false;
	}
	if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.enviaDados.email.value))) {
		alert("E-MAIL INVÁLIDO.");
		document.enviaDados.email.focus();
		return false;
	} 
	if (document.enviaDados.senha1.value == ""){
				alert ("DIGITE A SENHA!");
				document.enviaDados.senha1.focus();
				return false;
	}
	if (document.enviaDados.senha2.value == ""){
				alert ("CONFIRME SUA SENHA!");
				document.enviaDados.senha2.focus();
				return false;
	}
	f = document.getElementById(frmNome);
	query="";
	j=0;
	for(i=0;i<f.elements.length;i++){
		if((f.elements[i].type=='radio' || f.elements[i].type=='checkbox' )&& !(f.elements[i].checked)){
			f.elements[i].value='';
			
			if(f.elements[i].value!=''){
				query += j==0 ? '' : '&';
				query += f.elements[i].name + '=' + f.elements[i].value;
				j++;
			}
		}
		else
		{
			query += j==0 ? '' : '&';
			query += f.elements[i].name + '=' + f.elements[i].value;
			j++;
		}
	}
	//chama a função postajax() para enviar os dados
	//ao arquivo .php que fará o processamento
	postajax(url, query, destino);
}
/* EDITAR USUÁRIOS*/
function usuario_edita(frmNome, url, destino){ 

	if (document.enviaDados.nome.value == ""){
				alert ("DIGITE O NOME!");
				document.enviaDados.nome.focus();
				return false;
	}
	if (document.enviaDados.area_trabalho.value == ""){
				alert ("DIGITE A ÁREA DE TRABALHO!");
				document.enviaDados.area_trabalho.focus();
				return false;
	}
	if (document.enviaDados.email.value == ""){
				alert ("DIGITE O E-MAIL!");
				document.enviaDados.email.focus();
				return false;
	}
	if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.enviaDados.email.value))) {
		alert("E-MAIL INVÁLIDO.");
		document.enviaDados.email.focus();
		return false;
	} 
	
	f = document.getElementById(frmNome);
	query="";
	j=0;
	for(i=0;i<f.elements.length;i++){
		if((f.elements[i].type=='radio' || f.elements[i].type=='checkbox' )&& !(f.elements[i].checked)){
			f.elements[i].value='';
			
			if(f.elements[i].value!=''){
				query += j==0 ? '' : '&';
				query += f.elements[i].name + '=' + f.elements[i].value;
				j++;
			}
		}
		else
		{
			query += j==0 ? '' : '&';
			query += f.elements[i].name + '=' + f.elements[i].value;
			j++;
		}
	}
	//chama a função postajax() para enviar os dados
	//ao arquivo .php que fará o processamento
	postajax(url, query, destino);
}
/* MENSAGEM */
function mensagem_user(frmNome, url, destino){ 

	if (document.enviaDados.titulo.value == ""){
				alert ("DIGITE O TÍTULO!");
				document.enviaDados.titulo.focus();
				return false;
	}
	if (document.enviaDados.mensagem.value == ""){
				alert ("DIGITE A MENSAGEM!");
				document.enviaDados.mensagem.focus();
				return false;
	}	
	f = document.getElementById(frmNome);
	query="";
	j=0;
	for(i=0;i<f.elements.length;i++){
		if((f.elements[i].type=='radio' || f.elements[i].type=='checkbox' )&& !(f.elements[i].checked)){
			f.elements[i].value='';
			
			if(f.elements[i].value!=''){
				query += j==0 ? '' : '&';
				query += f.elements[i].name + '=' + f.elements[i].value;
				j++;
			}
		}
		else
		{
			query += j==0 ? '' : '&';
			query += f.elements[i].name + '=' + f.elements[i].value;
			j++;
		}
	}
	//chama a função postajax() para enviar os dados
	//ao arquivo .php que fará o processamento
	postajax(url, query, destino);
}
/* E-MAIL DO CLIENTE */
function email_cliente(frmNome, url, destino){ 
	
	if (document.enviaDados.email.value == ""){
				alert ("DIGITE O E-MAIL!");
				document.enviaDados.email.focus();
				return false;
	}
	if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.enviaDados.email.value))) {
		alert("E-MAIL INVÁLIDO.");
		document.enviaDados.email.focus();
		return false;
	} 

	f = document.getElementById(frmNome);
	query="";
	j=0;
	for(i=0;i<f.elements.length;i++){
		if((f.elements[i].type=='radio' || f.elements[i].type=='checkbox' )&& !(f.elements[i].checked)){
			f.elements[i].value='';
			
			if(f.elements[i].value!=''){
				query += j==0 ? '' : '&';
				query += f.elements[i].name + '=' + f.elements[i].value;
				j++;
			}
		}
		else
		{
			query += j==0 ? '' : '&';
			query += f.elements[i].name + '=' + f.elements[i].value;
			j++;
		}
	}
	//chama a função postajax() para enviar os dados
	//ao arquivo .php que fará o processamento
	postajax(url, query, destino);
}
/* UNIDADE FABRIL */
function cadastra_unidade(frmNome, url, destino){ 
	
	if (document.enviaDados.nome.value == ""){
				alert ("DIGITE O NOME!");
				document.enviaDados.nome.focus();
				return false;
	}
	
	f = document.getElementById(frmNome);
	query="";
	j=0;
	for(i=0;i<f.elements.length;i++){
		if((f.elements[i].type=='radio' || f.elements[i].type=='checkbox' )&& !(f.elements[i].checked)){
			f.elements[i].value='';
			
			if(f.elements[i].value!=''){
				query += j==0 ? '' : '&';
				query += f.elements[i].name + '=' + f.elements[i].value;
				j++;
			}
		}
		else
		{
			query += j==0 ? '' : '&';
			query += f.elements[i].name + '=' + f.elements[i].value;
			j++;
		}
	}
	//chama a função postajax() para enviar os dados
	//ao arquivo .php que fará o processamento
	postajax(url, query, destino);
}

/* CADASTRO EQUIPAMENTOS */
function cadastra_equipamentos(frmNome, url, destino){ 
	
	if (document.enviaDados.nome.value == ""){
				alert ("DIGITE O NOME!");
				document.enviaDados.nome.focus();
				return false;
	}
	if (document.enviaDados.familia.value == ""){
				alert ("ESCOLHA UMA FAMILIA!");
				document.enviaDados.familia.focus();
				return false;
	}
	
	f = document.getElementById(frmNome);
	query="";
	j=0;
	for(i=0;i<f.elements.length;i++){
		if((f.elements[i].type=='radio' || f.elements[i].type=='checkbox' )&& !(f.elements[i].checked)){
			f.elements[i].value='';
			
			if(f.elements[i].value!=''){
				query += j==0 ? '' : '&';
				query += f.elements[i].name + '=' + f.elements[i].value;
				j++;
			}
		}
		else
		{
			query += j==0 ? '' : '&';
			query += f.elements[i].name + '=' + f.elements[i].value;
			j++;
		}
	}
	//chama a função postajax() para enviar os dados
	//ao arquivo .php que fará o processamento
	postajax(url, query, destino);
}


/* DADOS BASICOS FALHAS */
function basico_falha(frmNome, url, destino){ 
	
	if (document.enviaDados.titulo.value == ""){
				alert ("DIGITE O TÍTULO!");
				document.enviaDados.titulo.focus();
				return false;
	}
	if (document.enviaDados.data_evento.value == ""){
				alert ("DIGITE A DATA DO EVENTO!");
				document.enviaDados.data_evento.focus();
				return false;
	}
	if (document.enviaDados.equipamento.value == ""){
				alert ("ESCOLHA UM EQUIPAMENTO!");
				document.enviaDados.equipamento.focus();
				return false;
	}
	if (document.enviaDados.linha_producao.value == ""){
				alert ("ESCOLHA UMA LINHA DE PRODUÇÃO!");
				document.enviaDados.linha_producao.focus();
				return false;
	}
	if (document.enviaDados.id_unidade.value == ""){
				alert ("ESCOLHA UMA UNIDADE FABRIL!");
				document.enviaDados.id_unidade.focus();
				return false;
	}
	
	f = document.getElementById(frmNome);
	query="";
	j=0;
	for(i=0;i<f.elements.length;i++){
		if((f.elements[i].type=='radio' || f.elements[i].type=='checkbox' )&& !(f.elements[i].checked)){
			f.elements[i].value='';
			
			if(f.elements[i].value!=''){
				query += j==0 ? '' : '&';
				query += f.elements[i].name + '=' + f.elements[i].value;
				j++;
			}
		}
		else
		{
			query += j==0 ? '' : '&';
			query += f.elements[i].name + '=' + f.elements[i].value;
			j++;
		}
	}
	//chama a função postajax() para enviar os dados
	//ao arquivo .php que fará o processamento
	postajax(url, query, destino);
}

/* CADASTRA AÇÕES */
function cadastra_acoes(frmNome, url, destino){ 
	
	if (document.enviaDados.prazo.value == ""){
				alert ("DIGITE O PRAZO!");
				document.enviaDados.prazo_1.focus();
				return false;
	}
	if (document.enviaDados.descricao.value == ""){
				alert ("DIGITE A DESCRIÇÃO!");
				document.enviaDados.descricao_1.focus();
				return false;
	}
	if (document.enviaDados.responsavel.value == ""){
				alert ("ESCOLHA UM RESPONSÁVEL!");
				document.enviaDados.responsavel_1.focus();
				return false;
	}
	
	/*if (document.enviaDados.resposta.value == ""){
				alert ("ESCOLHA QUEM RESPONDEU!");
				document.enviaDados.responsavel_1.focus();
				return false;
	}*/
	
	f = document.getElementById(frmNome);
	query="";
	j=0;
	for(i=0;i<f.elements.length;i++){
		if((f.elements[i].type=='radio' || f.elements[i].type=='checkbox' )&& !(f.elements[i].checked)){
			f.elements[i].value='';
			
			if(f.elements[i].value!=''){
				query += j==0 ? '' : '&';
				query += f.elements[i].name + '=' + f.elements[i].value;
				j++;
			}
		}
		else
		{
			query += j==0 ? '' : '&';
			query += f.elements[i].name + '=' + f.elements[i].value;
			j++;
		}
	}
	//chama a função postajax() para enviar os dados
	//ao arquivo .php que fará o processamento
	postajax(url, query, destino);
}

// FUNÇÃO postajax que é usada nas acimas
function postajax(url, query, destino){
    	
//var tecla=window.event.keyCode;
	var status = false;
    var tipo_conteudo = "application/x-www-form-urlencoded; charset=UTF-8";
	
	if(typeof destino !=='undefined')
		div_resposta = document.getElementById(destino);

    // Tenta criar objeto XMLHttpRequest NATIVO
    if (window.XMLHttpRequest) {
        requisicao = new XMLHttpRequest();
		requisicao.onreadystatechange=function(){
			if(requisicao.readyState==4){
				if(typeof destino !=='undefined'){
					//Aqui vai o processamento a ser feito
					//quando o arquivo .php retornar o resultado
					/*
						Recebe a resposta e coloca ela no conteiner
						pêgo pela variável "destino"
					*/
					texto=unescape(requisicao.responseText.replace(/\+/g," "));
					div_resposta.innerHTML=texto;
					div_resposta.style.visibility="visible";
					//exibe mensagem de sucesso na tela		
				}
			}
		}
		//Abre a conexão
		requisicao.open("post", url, true);
		//Envia os cabeçalhos
		requisicao.setRequestHeader("Content-Type", tipo_conteudo);
		//Envia a requisicao
		requisicao.send(query);
		//Status da requisicao
		status = true;
    } //Senão, tenta criar objeto XMLHttpRequest ActiveX (Internet Explorer)
	else if (window.ActiveXObject){
        requisicao = new ActiveXObject("Microsoft.XMLHTTP");
        if (requisicao){
            requisicao.onreadystatechange=function(){
				if(requisicao.readyState==4){
					if(typeof destino !='undefined'){
						//Aqui vai o processamento a ser feito
						//quando o arquivo .php retornar o resultado
						/*
							Recebe a resposta e coloca ela no conteiner
							pêgo pela variável "destino"
						*/
						texto=unescape(requisicao.responseText.replace(/\+/g," "));
						div_resposta.innerHTML=texto;
						div_resposta.style.visibility="visible";
						//exibe mensagem de sucesso na tela
						
					}
				}
			}
			//Abre a conexão
            requisicao.open("post", url, true);
			//Envia os cabeçalhos
            requisicao.setRequestHeader("Content-Type", tipo_conteudo);
			//Envia a requisicao
            requisicao.send(query);
			//Status da requisicao
            status = true;
        }
    }

    return status;
	
	
}

function enviar(evento){
        tecla = evento.keyCode;
        if(tecla == 0){tecla = evento.charCode;}
        if(tecla == 13){cadastra_equipamentos('enviaDados', 'equipamentos/gerencia.php?acao=2', 'alterar');}
}
function enviar2(evento){
        tecla = evento.keyCode;
        if(tecla == 0){tecla = evento.charCode;}
        if(tecla == 13){cadastra_unidade('enviaDados', 'equipamentos/familia/gerencia.php?acao=2', 'alterar');}
}
function enviar3(evento){
        tecla = evento.keyCode;
        if(tecla == 0){tecla = evento.charCode;}
        if(tecla == 13){cadastra_unidade('enviaDados', 'linhaProducao/gerencia.php?acao=2', 'alterar');}
}
function enviar4(evento){
        tecla = evento.keyCode;
        if(tecla == 0){tecla = evento.charCode;}
        if(tecla == 13){cadastra_unidade('enviaDados', 'unidadesFabris/gerencia.php?acao=2', 'alterar');}
}
function enviar5(evento){
        tecla = evento.keyCode;
        if(tecla == 0){tecla = evento.charCode;}
        if(tecla == 13){usuario('enviaDados', 'cliente/usuario/nivel2/gerencia.php?acao=1', 'alterar');}
}
function enviar6(evento){
        tecla = evento.keyCode;
        if(tecla == 0){tecla = evento.charCode;}
        if(tecla == 13){}
}
function enviar7(evento){
        tecla = evento.keyCode;
        if(tecla == 0){tecla = evento.charCode;}
        if(tecla == 13){cliente_cadastra('enviaDados', 'cadastra3.php', 'conteudo2');}
}
function enviar8(evento){
        tecla = evento.keyCode;
        if(tecla == 0){tecla = evento.charCode;}
        if(tecla == 13){envia_cadastro('enviaDados2', 'cadastra.php?acao=1','tudo');}
}


