// JavaScript Document
function GetXMLHttp() {
    if(navigator.appName == "Microsoft Internet Explorer") {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		
    }
    else {
        xmlHttp = new XMLHttpRequest();
		 
    }
    return xmlHttp;
}
var xmlRequest = GetXMLHttp();
/* MENU */
function menuTop(valor, scripts){
    var url = valor;
    if (scripts && scripts.length > 0){
        scripts_global = scripts;
    }
    xmlRequest.open("GET",url,true);
    xmlRequest.onreadystatechange = function() {mudancaEstadoMenuTop(scripts);};
    xmlRequest.send(null);

    if (xmlRequest.readyState == 1) {
        document.getElementById("alterar").innerHTML = "<div class='loader'></div>";
    }

    scripts_global = Array();
    return url;
}

function mudancaEstadoMenuTop(scripts){
    if (xmlRequest.readyState == 4){
        document.getElementById("alterar").innerHTML = xmlRequest.responseText;
        if (scripts && scripts.length > 0){
            LoadMyJs(scripts);
        }
    }
}

function carregaOpcaoMenu(url) {
    $.get(url, function(data) {
        $("#alterar").html(data);        
        $("#alterar").find("script").each(function() {
            eval($(this).html());
        });
    });
}

/* FIM MENU */

function menuFiltro(valor){
	var url = valor;

	xmlRequest.open("GET",url,true);
	xmlRequest.onreadystatechange = mudancaEstadoMenuFiltro;
	xmlRequest.send(null);

	if (xmlRequest.readyState == 1) {
		document.getElementById("lista_geral_ranking").innerHTML = "<div class='loader'></div>";
	}

		return url;
	}
function mudancaEstadoMenuFiltro(){
	if (xmlRequest.readyState == 4){
		document.getElementById("lista_geral_ranking").innerHTML = xmlRequest.responseText;
	}
}




function deleta_analise(valor){
	var url = valor;

	xmlRequest.open("GET",url,true);
	xmlRequest.onreadystatechange = mudancaEstadoDeleta_analise;
	xmlRequest.send(null);

	if (xmlRequest.readyState == 1) {
		document.getElementById("formAnaliseDir").innerHTML = "<div class='loader'></div>";
	}

		return url;
	}
function mudancaEstadoDeleta_analise(){
	if (xmlRequest.readyState == 4){
		document.getElementById("formAnaliseDir").innerHTML = xmlRequest.responseText;
	}
}

function mudaImg(valor){
	var url = valor;

	xmlRequest.open("GET",url,true);
	xmlRequest.onreadystatechange = mudancaEstadoMudaImg;
	xmlRequest.send(null);

	if (xmlRequest.readyState == 1) {
		document.getElementById("marcaEmpresa").innerHTML = "<div class='loader'></div>";
	}

		return url;
	}
function mudancaEstadoMudaImg(){
	if (xmlRequest.readyState == 4){
		document.getElementById("marcaEmpresa").innerHTML = xmlRequest.responseText;
	}
}



function recuperaSenha(valor){
	var url = valor;

	xmlRequest.open("GET",url,true);
	xmlRequest.onreadystatechange = mudancaEstadoSenha;
	xmlRequest.send(null);

	if (xmlRequest.readyState == 1) {
		document.getElementById("login").innerHTML = "<div class='loader'></div>";
	}

		return url;
	}
function mudancaEstadoSenha(){
	if (xmlRequest.readyState == 4){
		document.getElementById("login").innerHTML = xmlRequest.responseText;
	}
}

function menu(valor){
	var url = valor;

	xmlRequest.open("GET",url,true);
	xmlRequest.onreadystatechange = mudancaEstadoMenu;
	xmlRequest.send(null);

	if (xmlRequest.readyState == 1) {
		document.getElementById("formAnaliseDir").innerHTML = "<div class='loader'></div>";
	}

		return url;
	}
function mudancaEstadoMenu(){
	if (xmlRequest.readyState == 4){
		document.getElementById("formAnaliseDir").innerHTML = xmlRequest.responseText;
	}
}

function deleta_acao(valor){
	var url = valor;

	xmlRequest.open("GET",url,true);
	xmlRequest.onreadystatechange = mudancaEstadoDeleta;
	xmlRequest.send(null);

	if (xmlRequest.readyState == 1) {
		document.getElementById("listaAcoes").innerHTML = "<div class='loader'></div>";
	}

		return url;
	}
function mudancaEstadoDeleta(){
	if (xmlRequest.readyState == 4){
		document.getElementById("listaAcoes").innerHTML = xmlRequest.responseText;
	}
}

function edita_acao(valor){
	var url = valor;

	xmlRequest.open("GET",url,true);
	xmlRequest.onreadystatechange = mudancaEstadoEdita;
	xmlRequest.send(null);

	if (xmlRequest.readyState == 1) {
		document.getElementById("formAnalise").innerHTML = "<div class='loader'></div>";
	}

		return url;
	}
function mudancaEstadoEdita(){
	if (xmlRequest.readyState == 4){
		document.getElementById("formAnalise").innerHTML = xmlRequest.responseText;
	}
}

function deleta_acao(valor){
	var url = valor;

	xmlRequest.open("GET",url,true);
	xmlRequest.onreadystatechange = mudancaEstadoDeletaAcao;
	xmlRequest.send(null);

	if (xmlRequest.readyState == 1) {
		document.getElementById("loadingAcao").innerHTML = "<div class='loader'></div>";
	}

		return url;
	}
function mudancaEstadoDeletaAcao(){
	if (xmlRequest.readyState == 4){
		document.getElementById("loadingAcao").innerHTML = xmlRequest.responseText;
	}
}

function busca_analise(valor){
	var url = valor;

	xmlRequest.open("GET",url,true);
	xmlRequest.onreadystatechange = mudancaEstadoBusca;
	xmlRequest.send(null);

	if (xmlRequest.readyState == 1) {
		document.getElementById("alteraBuscaAnalises").innerHTML = "<div class='loader'></div>";
	}

		return url;
	}
function mudancaEstadoBusca(){
	if (xmlRequest.readyState == 4){
		document.getElementById("alteraBuscaAnalises").innerHTML = xmlRequest.responseText;
	}
}

function menuLat(valor){
	var url = valor;

	xmlRequest.open("GET",url,true);
	xmlRequest.onreadystatechange = mudancaEstadoMenuLat;
	xmlRequest.send(null);

	if (xmlRequest.readyState == 1) {
		document.getElementById("internaMenuPassos").innerHTML = "<div class='loader'></div>";
	}

		return url;
	}
function mudancaEstadoMenuLat(){
	if (xmlRequest.readyState == 4){
		document.getElementById("internaMenuPassos").innerHTML = xmlRequest.responseText;
	}
}


function passo_link(valor){
	var url = valor;

	xmlRequest.open("GET",url,true);
	xmlRequest.onreadystatechange = mudancaEstadoPasso;
	xmlRequest.send(null);

	if (xmlRequest.readyState == 1) {
		document.getElementById("alterar").innerHTML = "<div class='loader'></div>";
	}

		return url;
	}
function mudancaEstadoPasso(){
	if (xmlRequest.readyState == 4){
		document.getElementById("alterar").innerHTML = xmlRequest.responseText;
	}
}



// Replace the normal jQuery getScript function with one that supports
// debugging and which references the script files as external resources
// rather than inline.
jQuery.extend({
   getScript: function(url, callback) {
      var head = document.getElementsByTagName("head")[0];
      var script = document.createElement("script");
      script.src = url;

      // Handle Script loading
      {
         var done = false;

         // Attach handlers for all browsers
         script.onload = script.onreadystatechange = function(){
            if ( !done && (!this.readyState ||
                  this.readyState == "loaded" || this.readyState == "complete") ) {
               done = true;
               if (callback)
                  callback();

               // Handle memory leak in IE
               script.onload = script.onreadystatechange = null;
            }
         };
      }

      head.appendChild(script);

      // We handle everything using the script element injection
      return undefined;
   },
});
