AcoesBloqueio = {
    ajax: function() {
        var dados = {
            'unidade_fabril'    : $('#ddlUnidadeFabril').val(),
            'linha_producao'    : $('#ddlLinhaProducao').val(),
            'equipamento'       : $('#ddlEquipamento').val(),
            'titulo'            : $('#ddlTitulo').val(),
            'responsavel'       : $('#ddlResponsavel').val(),
            'situacao'          : $('#ddlSituacao').val(),
            'descricao'         : $('#txtDescAcao').val(),
        };
        
        $.ajax({
            url: 'ajaxBuscarAcoesBloqueio.php',
            type: 'post',
            dataType: 'json',
            data: dados,
            success: function(r) {
                $('#resultadoTotalQtd').text(r['qtd']);
                AcoesBloqueio.tabela(r);
                if (parseInt(r['qtd'])>0){
                    AcoesBloqueio.grafico([r['grafico']]);
                }else{
                    $('#graficoAcoesBloqueio').html('');
                }
            }
        });
        
    },
    tabela: function(dados){
        var html = '';

        if (dados['qtd'] > 0){
            html += '<table width="100%" cellspacing="0" id="tabAnalise">'+
                        '<tr>'+
                            '<td width="250" bgcolor="#efefef" class="negrito">An&aacute;lise</td>';
            if (parseInt(dados['nivel_usuario']) === 1) {
                html +=     '<td width="200" bgcolor="#efefef" class="negrito">Respons&aacute;vel</td>';
            }
            html +=         '<td width="250" bgcolor="#efefef" class="negrito">Descri&ccedil;&atilde;o</td>'+
                            '<td width="100" bgcolor="#efefef" class="negrito">E-mail <a href="#" class="dcontexto" onclick="AcoesBloqueio.email(null);" ><span>Reenvie e-mail para todos os responsáveis avisando de suas análises Pendentes ou Atrasadas.</span><img src="images/email_go.png" width="16" height="16" /></a></td>'+
                            '<td width="100" bgcolor="#efefef" class="negrito">Prazo</td>'+
                            '<td width="120" bgcolor="#efefef" class="negrito">Situa&ccedil;&atilde;o</td>'+
                        '</tr>';
                
            for (var i=0; i<dados['qtd']; i++){
                html += '<tr>'+
                            '<td>'+
                                '<a href="javascript:;" onclick="menuTop(\'analiseFalhas/edita_passo1.php?id='+dados['res'][i]['analise_id']+'&amp;acao=1\');">'+dados['res'][i]['analise_titulo']+'</a>'+
                            '</td>';
                if (parseInt(dados['nivel_usuario']) === 1) {
                    html += '<td>'+dados['res'][i]['acao_responsavel']+'</td>';
                }
                
                html +=     '<td><a href="javascript:;" onclick="menuTop(\'analiseFalhas/edita_analise.php?id='+dados['res'][i]['analise_id']+'&amp;acao=1&amp;id_acao='+dados['res'][i]['acao_id']+'\');">'+dados['res'][i]['acao_descricao']+'</a></td>'+
                            '<td>';
                if (parseInt(dados['res'][i]['situacao_id']) === 1 || parseInt(dados['res'][i]['situacao_id']) === 6) {
                    html += '<a href="javascript:;" class="dcontexto" onclick="AcoesBloqueio.email('+dados['res'][i]['acao_id']+');" ><span>Reenvie este e-mail para seu respons&aacute;vel avisando que est&aacute; a&ccedil;&atilde;o est&aacute; pendente.</span><img src="images/email_go.png" width="16" height="16" /></a>';
                }
                html +=     '</td>'+
                            '<td>'+dados['res'][i]['acao_prazo']+'</td>'+    
                            '<td>'+dados['res'][i]['situacao_descricao']+'</td>'+    
                        '<tr>';
            }
            html += '</table>';
        }else{
            html = '<p class="negrito">Nenhuma an&aacute;lise cadastrada.</p>';
        }
  
        $('#conteudoPesquisa').html(html);
    },
    grafico: function(dados) {
        //console.log(dados);
        var plot8 = $.jqplot('graficoAcoesBloqueio', dados, {
            grid: {
                drawBorder: false,
                drawGridlines: false,
                background: '#ffffff',
                shadow:false
            },
            axesDefaults: {
                tickOptions: {
                    formatString: ''
                }
            },
            seriesDefaults: {
                renderer: jQuery.jqplot.PieRenderer, 
                rendererOptions: {
                    showDataLabels: true,
                    dataLabels: 'value',
                    sliceMargin: 5,
                    fill: false,
                    shadowAlpha: 0,
                    pointLabels: {show: true}
                }
            },
            legend: {
                show: true,
                location: 's'
            }
        }); 
    }, 
    email: function(id_acao) {
        var msg_alert = '';
        if (id_acao !== null){
            dados = {'acao_email':id_acao};
        }else{
            dados = null;
        }
        $.ajax({
            url: 'ajaxEmailAcoesBloqueio.php',
            type: 'post',
            dataType: 'json',
            data: dados,
            success: function(msg) {
                alert(msg);
            },
	    error: function(msg) {
		alert("Erro ao enviar e-mails!");
	    }
        });
    }
};


$(function() {
    AcoesBloqueio.ajax();
    $('#ddlUnidadeFabril,#ddlLinhaProducao,#ddlEquipamento,#ddlTitulo,#ddlResponsavel,#ddlSituacao,#txtDescAcao').change(function(){
        AcoesBloqueio.ajax();
    });
});