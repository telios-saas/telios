GerenciarAnalises = {
    ajax: function() {
        var dados = {
            'unidade_fabril'    : $('#ddlUnidadeFabril').val(),
            'linha_producao'    : $('#ddlLinhaProducao').val(),
            'equipamento'       : $('#ddlEquipamento').val(),
            'situacao'          : $('#ddlSituacao').val(),
            'prioridade'        : $('#ddlPrioridade').val(),
            'titulo'            : $('#txtTituloAnalise').val()
        };
        
        $.ajax({
            url: 'analiseFalhas/ajaxGerenciarAnalises.php',
            type: 'post',
            dataType: 'json',
            data: dados,
            success: function(r) {
                $('#resultadoTotalQtd').text(r['qtd']);
                GerenciarAnalises.tabela(r);
                if (parseInt(r['qtd'])>0){
                    GerenciarAnalises.grafico([r['grafico']]);
                }else{
                    $('#graficoTodasAnalises').html('');
                }
            }
        });
        
    },
    tabela: function(dados){
        var html = '';

        if (dados['qtd'] > 0){
            html += '<table width="99%" cellspacing="0" id="tabAnalise">'+
                        '<tr>'+
                            '<td width="75" align="center" bgcolor="#dfe8fa" class="negrito">Prioridade</td>'+
                            '<td width="75" align="center" bgcolor="#dfe8fa" class="negrito">Data da Falha</td>'+
                            '<td bgcolor="#dfe8fa" class="negrito">T&iacute;tulo da An&aacute;lise</td>'+
                            '<td width="75" align="center" bgcolor="#dfe8fa" class="negrito">Situa&ccedil;&atilde;o</td>'+
                            
                        '</tr>';
                
            for (var i=0; i<dados['qtd']; i++){
                html += '<tr>'+
                            '<td align="center"><img src="images/Prioridade'+dados['res'][i]['prioridade']+'.png" width="18" height="18" /></td>'+    
                            '<td align="center">'+dados['res'][i]['data_evento']+'</td>'+    
                            '<td><a href="javascript:;" onclick="menuTop(\'analiseFalhas/edita_passo1.php?id='+dados['res'][i]['basico_id']+'&amp;acao=1\');">'+dados['res'][i]['titulo']+'</a></td>'+
                            '<td align="center">'+dados['res'][i]['situacao_descricao']+'</td>'+
                        '<tr>';
            }
            html += '</table>';
        }else{
            html = '<p class="negrito">Nenhuma an&aacute;lise cadastrada.</p>';
        }
  
        $('#conteudoPesquisa').html(html);
    },
    grafico: function(dados) {
        var plot8 = $.jqplot('graficoTodasAnalises', dados, {
            grid: {
                drawBorder: false,
                drawGridlines: false,
                background: '#ffffff',
                shadow:false
            },
            axesDefaults: {
                tickOptions: {
                    formatString: ''
                }
            },
            seriesDefaults: {
                renderer: jQuery.jqplot.PieRenderer, 
                rendererOptions: {
                    showDataLabels: true,
                    dataLabels: 'value',
                    sliceMargin: 5,
                    fill: false,
                    shadowAlpha: 0,
                    pointLabels: {show: true}
                }
            },
            legend: {
                show: true,
                location: 's'
            }
        }); 
    }
};


$(function() {
    GerenciarAnalises.ajax();
    $('#ddlUnidadeFabril,#ddlLinhaProducao,#ddlEquipamento,#ddlSituacao,#ddlPrioridade,#txtTituloAnalise').change(function(){
        GerenciarAnalises.ajax();
    });
});