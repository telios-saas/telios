function htmlspecialchars(str) {
 if (typeof(str) == "string") {
  str = str.replace(/&/g, "&amp;"); /* must do &amp; first */
  str = str.replace(/"/g, "&quot;");
  str = str.replace(/'/g, "&#039;");
  str = str.replace(/</g, "&lt;");
  str = str.replace(/>/g, "&gt;");
  }
 return str;
 }
 
var TeliosJS = {
    
    showResponse : function(resp)
    {
        var nodes = $(resp);
        for (var i = 0; i < nodes.length; i++)
        {
            var nodeName = nodes[i].nodeName;
            var nodeId = nodes[i].id;
            var nodeHTML = nodes[i].innerHTML;

            if (nodeName === "DIV" || nodeName === "SPAN")
                $('#' + nodeId).html(nodeHTML);
            else if (nodeName === "SCRIPT")
                eval(nodeHTML);
        }
    },
            
    post : function(url, data, callback)
    {
        $.blockUI();
        $.post(url, data, function(resp, status, req) {
            TeliosJS.showResponse(resp);
            if (callback != undefined) {
                callback();
            }
        });
    },
            
    get : function(url, callback)
    {
        $.blockUI();
        $.get(url, function(resp, status, req) {
            TeliosJS.showResponse(resp);
            if (callback != undefined) {
                callback();
            }
        });
    },
            
    getCSS : function(cssPath) 
    {
        $("head").append("<link>");
        css = $("head").children(":last");
        css.attr({rel: "stylesheet", type: "text/css", href: cssPath});
    }
};

$(document).ready(function()
{                      
    $(document).ajaxStop($.unblockUI);
    
});
function LoadMyJs(scripts) {
    if (scripts){
        var docHeadObj = document.getElementsByTagName("head")[0];
        for (var i=0;i<scripts.length;i++){
            var dynamicScript = document.createElement("script");
            dynamicScript.type = "text/javascript";
            dynamicScript.src = scripts[i];
            docHeadObj.appendChild(dynamicScript);
        }
    }
}