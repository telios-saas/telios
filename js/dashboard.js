SituacaoAtual = {
    gerarTabela: function(id) {
        $('#'+id+'-buscaSituacaoUnidade, #'+id+'-div_graficoResultado').html('');
        var unidades = [];
        var situacoes = [];

        $('#' + id + '-fsSituacao input[type=checkbox]:checked').each(function() {
            situacoes.push($(this).val());
        });
        $('#' + id + '-fsUnidade input[type=checkbox]:checked').each(function() {
            unidades.push($(this).val());
        });

        var dados = {'situacoes': situacoes, 'unidades': unidades};

        $.ajax({
            url: 'ajaxBuscarSituacaoAtual.php',
            type: 'post',
            dataType: 'json',
            data: dados,
            success: function(r) {
                if (parseInt(r['qtd']) > 0) {
                    $('#dvSituacaoAtual-div_graficoResultado, #dvSituacaoAtual-div_tabelaResultado').show();
                    $('#dvSituacaoAtual-div_erroResultado').html('').show();
                    var index_start_situacao = 1;
                    //thead
                    var conteudo_thead = '<thead><tr>';
                    var totais = [];
                    for (var i = 0; i < r['col'].length; i++) {
                        if (r['col'][i]['label']) {
                            conteudo_thead += '<th align="' + r['col'][i]['align'] + '">' + r['col'][i]['label'] + '</th>';
                            if (i >= index_start_situacao) {
                                totais.push({'nome': r['col'][i]['nome'], 'valor': 0});
                            }
                        }
                    }

                    conteudo_thead += '<th align="right">Total</th>';
                    conteudo_thead += '</tr></thead>';

                    //tbody
                    var conteudo_tbody = '<tbody>';
                    var total_coluna;
                    var total_total = 0;
                    for (var i = 0; i < r['qtd']; i++) {
                        total_coluna = 0;
                        conteudo_tbody += '<tr>';
                        for (var j = 0; j < r['col'].length; j++) {

                            conteudo_tbody += '<td align="' + r['col'][j]['align'] + '">' + r['res'][i][j] + '</td>';
                            if (j >= 1) {
                                if (totais[j - index_start_situacao].nome === r['col'][j]['nome']) {
                                    totais[j - index_start_situacao].valor += parseInt(r['res'][i][j]);
                                    total_coluna += parseInt(r['res'][i][j]);
                                }
                            }
                        }
                        total_total += total_coluna;
                        conteudo_tbody += '<td align="right">' + total_coluna + '</td>';
                        conteudo_tbody += '</tr>';
                    }
                    conteudo_tbody += '</tbody>';

                    //tfoot
                    var situacoes = [], itens = [];
                    var conteudo_tfoot = '<tfoot><tr><td align="left" colspan="1">Total Geral</td>';
                    for (var i = 0; i < totais.length; i++) {
                        conteudo_tfoot += '<td align="right">' + totais[i].valor + '</td>';

                        //grafico
                        eval('var s' + i + ' = Array();');
                        var j = i + 1;
                        situacoes.push({'label': r['col'][j]['label']});
                        for (var u = 0; u < r['res'].length; u++) {
                            eval('s' + i + '.push(' + r['res'][u][i + 1] + ')');
                        }
                        itens.push(eval('s' + i));
                    }

                    conteudo_tfoot += '<td align="right">' + total_total + '</td>';
                    conteudo_tfoot += '</tr></tfoot>';

                    $('#dvSituacaoAtual-buscaSituacaoUnidade').html(conteudo_thead + conteudo_tbody + conteudo_tfoot);

                    var unidades = []
                    for (var u = 0; u < r['res'].length; u++) {
                        unidades.push(r['res'][u][0]);
                    }

                    SituacaoAtual.gerarGrafico(id, unidades, situacoes, itens);
                } else {
                    $('#dvSituacaoAtual-div_graficoResultado, #dvSituacaoAtual-div_tabelaResultado').hide();
                    $('#dvSituacaoAtual-div_erroResultado').html('<center>Nenhum registro encontrado.</center>').show();
                }
            }
        });
    },
    gerarGrafico: function(id, unidades, situacoes, itens) {
        plot3 = $.jqplot(id + '-div_graficoResultado', itens, {
            stackSeries: true,
            captureRightClick: true,
            seriesDefaults: {
                renderer: $.jqplot.BarRenderer,
                rendererOptions: {
                    barMargin: 30,
                    barWidth: 50,
                    highlightMouseOver: true
                },
                pointLabels: {show: true, hideZeros: true}
            },
            series: situacoes,
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: unidades,
                    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                    tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                    tickOptions: {
                        angle: -30,
                        fontSize: '11px'
                    }
                },
                yaxis: {
                    padMin: 0,
                    min: 0
                }
            },
            legend: {
                show: true,
                placement: 'outsideGrid'
            }
        });
    }
};
SituacaoMensal = {
    gerarTabela: function(id) {
        $('#'+id+'-buscaSituacaoUnidade, #'+id+'-div_graficoResultado').html('');
        var unidades = [];
        var situacoes = [];

        $('#' + id + '-fsSituacao input[type=checkbox]:checked').each(function() {
            situacoes.push($(this).val());
        });

        $('#' + id + '-fsUnidade input[type=checkbox]:checked').each(function() {
            unidades.push($(this).val());
        });

        var dados = {'situacoes': situacoes,
                    'unidades': unidades, 
                    'mes_inicial': $('#DataInicialSituacaoMensal').val(), 
                    'mes_final': $('#DataFinalSituacaoMensal').val()};

        $.ajax({
            url: 'ajaxBuscarSituacaoMensal.php',
            type: 'post',
            dataType: 'json',
            data: dados,
            success: function(r) {
                if (parseInt(r['qtd']) > 0) {
                    $('#dvSituacaoMensal-div_graficoResultado, #dvSituacaoMensal-div_tabelaResultado').show();
                    $('#dvSituacaoMensal-div_erroResultado').html('').hide();
                    var index_start_situacao = 1;
                    //thead
                    var conteudo_thead = '<thead><tr>';
                    var totais = [];
                    for (var i = 0; i < r['col'].length; i++) {
                        if (r['col'][i]['label']) {
                            conteudo_thead += '<th align="' + r['col'][i]['align'] + '">' + r['col'][i]['label'] + '</th>';
                            if (i >= index_start_situacao) {
                                totais.push({'nome': r['col'][i]['nome'], 'valor': 0});
                            }
                        }
                    }

                    conteudo_thead += '<th align="right">Total</th>';
                    conteudo_thead += '</tr></thead>';

                    //tbody
                    var conteudo_tbody = '<tbody>';
                    var total_coluna,total_variacao_mensal;              
                    var total_total = 0;
                     //loop linhas
                    for (var i = 0; i < r['qtd']; i++) {
                        total_coluna = 0;
                        total_variacao_mensal=0;
                        conteudo_tbody += '<tr>';
                        //loop colunas
                        for (var j = 0; j < r['col'].length; j++) {

                            conteudo_tbody += '<td align="' + r['col'][j]['align'] + '">' + r['res'][i][j] + '</td>';
                            if (j >= index_start_situacao) {
                                if (totais[j - index_start_situacao].nome === r['col'][j]['nome']) {
                                    
                                    //se  existir mais de 1 linha                                         
                                    if ((i== r['qtd']-1) && (r['qtd']>1)){
                                        //pega a diferença das ultimas duas linhas
                                        totais[j - index_start_situacao].valor = parseInt(r['res'][i][j])-parseInt(r['res'][i-1][j]);
                                    }
                                    total_coluna += parseInt(r['res'][i][j]);
                                    total_variacao_mensal+=totais[j - index_start_situacao].valor;
                                }
                            }
                        }
                        total_total += total_variacao_mensal;
                        conteudo_tbody += '<td align="right">' + total_coluna + '</td>';
                        conteudo_tbody += '</tr>';
                    }
                    conteudo_tbody += '</tbody>';

                    //tfoot Evolução Mensal das Analises
                    var situacoes = [], itens = [];
                    var conteudo_tfoot = '<tfoot><tr><td align="left">Variação Mensal</td>';
                    for (var i = 0; i < totais.length; i++) {
                        conteudo_tfoot += '<td align="right">' + totais[i].valor + '</td>';

                        //grafico
                        eval('var s' + i + ' = Array();');
                        var j = i + index_start_situacao;
                        situacoes.push({'label': r['col'][j]['label']});
                        for (var u = 0; u < r['res'].length; u++) {
                            eval('s' + i + '.push(' + r['res'][u][i + index_start_situacao] + ')');
                        }
                        itens.push(eval('s' + i));
                    }

                    conteudo_tfoot += '<td align="right">' + total_total + '</td>';
                    conteudo_tfoot += '</tr></tfoot>';

                    $('#dvSituacaoMensal-buscaSituacaoUnidade').html(conteudo_thead + conteudo_tbody + conteudo_tfoot);

                    var datas = []
                    for (var u = 0; u < r['res'].length; u++) {
                        datas.push(r['res'][u][0]);
                    }

                    SituacaoMensal.gerarGrafico(id, datas, situacoes, itens);
                } else {
                    $('#dvSituacaoMensal-div_graficoResultado, #dvSituacaoMensal-div_tabelaResultado').hide();
                    $('#dvSituacaoMensal-div_erroResultado').show().html('<center>Nenhum registro encontrado.</center>');
                }
            }
        });
    },
    gerarGrafico: function(id, datas, situacoes, itens) {
        plot3 = $.jqplot(id + '-div_graficoResultado', itens, {
            stackSeries: true,
            captureRightClick: true,
            seriesDefaults: {
                renderer: $.jqplot.BarRenderer,
                rendererOptions: {
                    barMargin: 30,
                    barWidth: 50,
                    highlightMouseOver: true
                },
                pointLabels: {show: true, hideZeros: true}
            },
            series: situacoes,
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: datas,
                    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                    tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                    tickOptions: {
                        angle: -30,
                        fontSize: '11px'
                    }
                },
                yaxis: {
                    padMin: 0,
                    min: 0
                }
            },
            legend: {
                show: true,
                placement: 'outsideGrid'
            }
        });
    }
};
EvolucaoMensal = {
    gerarTabela: function(id) {
        $('#'+id+'-buscaSituacaoUnidade, #'+id+'-div_graficoResultado').html('');
        var unidades = [];
        var situacoes = [];

        $('#' + id + '-fsSituacao input[type=checkbox]:checked').each(function() {
            situacoes.push($(this).val());
        });
        $('#' + id + '-fsUnidade input[type=checkbox]:checked').each(function() {
            unidades.push($(this).val());
        });

        var dados = {'situacoes': situacoes, 'unidades': unidades, 'mes_inicial': $('#DataInicialEvolucaoMensal').val(), 'mes_final': $('#DataFinalEvolucaoMensal').val()};

        $.ajax({
            url: 'ajaxBuscarEvolucaoMensal.php',
            type: 'post',
            dataType: 'json',
            data: dados,
            success: function(r) {
                if (parseInt(r['qtd']) > 0) {
                    $('#dvEvolucaoMensal-div_graficoResultado, #dvEvolucaoMensal-div_tabelaResultado').show();
                    $('#dvEvolucaoMensal-div_erroResultado').html('').hide();
                    var index_start_situacao = 1;
                    //thead
                    var conteudo_thead = '<thead><tr>';
                    var totais = [];
                    for (var i = 0; i < r['col'].length; i++) {
                        
                        if (r['col'][i]['label']) {
                            
                            conteudo_thead += '<th align="' + r['col'][i]['align'] + '">' + r['col'][i]['label'] + '</th>';
                            if (i >= index_start_situacao) {
                                totais.push({'nome': r['col'][i]['nome'], 'valor': 0});
                            }
                        }
                    }

                    conteudo_thead += '<th align="right">Total</th>';
                    conteudo_thead += '</tr></thead>';

                    //tbody
                    var conteudo_tbody = '<tbody>';
                    var total_coluna;
                    var total_total = 0;
                    for (var i = 0; i < r['qtd']; i++) {
                        total_coluna = 0;
                        total_variacao_mensal=0;
                        conteudo_tbody += '<tr>';
                        for (var j = 0; j < r['col'].length; j++) {
                            conteudo_tbody += '<td align="' + r['col'][j]['align'] + '">' + r['res'][i][j] + '</td>';
                            if (j >= index_start_situacao) {
                                if (totais[j - index_start_situacao].nome === r['col'][j]['nome']) {
                                    
                                    //se  existir mais de 1 linha                                         
                                    if ((i== r['qtd']-1) && (r['qtd']>1)){
                                        //pega a diferença das ultimas duas linhas
                                        totais[j - index_start_situacao].valor = parseInt(r['res'][i][j])-parseInt(r['res'][i-1][j]);
                                    }
                                    total_coluna += parseInt(r['res'][i][j]);
                                    total_variacao_mensal+=totais[j - index_start_situacao].valor;
                                }
                            }
                        }
                        total_total += total_variacao_mensal;
                        conteudo_tbody += '<td align="right">' + total_coluna + '</td>';
                        conteudo_tbody += '</tr>';
                    }
                    conteudo_tbody += '</tbody>';

                    //tfoot Evolução Mensal das Ações de bloqueio
                    var conteudo_tfoot = '<tfoot><tr><td align="left">Variação Mensal</td>';
                    //Situacoes
                    for (var i = 0; i < totais.length; i++) {
                        conteudo_tfoot += '<td align="right">' + totais[i].valor + '</td>';
                    }

                    conteudo_tfoot += '<td align="right">' + total_total + '</td>';
                    conteudo_tfoot += '</tr></tfoot>';

                    $('#dvEvolucaoMensal-buscaSituacaoUnidade').html(conteudo_thead + conteudo_tbody + conteudo_tfoot);
                    
                    /* GRAFICO */
                    var itens = [];
                    var situacoes = [];
                    var total_situacoes = r['col'].length-1;
                    var total_registros = r['qtd'];
                    for (var j = 1; j <= total_situacoes; j++) {
                        eval('var linha' + j + ' = [];');
                        situacoes.push({'label': r['col'][j]['label']});
                    }

                    for (var i = 0; i < total_registros; i++) {
                        for (var j = 1; j <= total_situacoes; j++) {
                            eval('linha' + j + '.push([\''+r['res'][i][0]+'\','+r['res'][i][j]+']);');
                        }
                    }
                    
                    for (var j = 1; j <= total_situacoes; j++) {
                        itens.push(eval('linha' + j));
                    }
                    /* GRAFICO */

                    EvolucaoMensal.gerarGrafico(id, situacoes, itens);
                } else {
                    $('#dvEvolucaoMensal-div_graficoResultado, #dvEvolucaoMensal-div_tabelaResultado').hide();
                    $('#dvEvolucaoMensal-div_erroResultado').show().html('<center>Nenhum registro encontrado.</center>');
                }
            }
        });
    },
    gerarGrafico: function(id, situacoes, itens) {
    //var months = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
    var plot1 = $.jqplot(id + '-div_graficoResultado', itens, {
      series: situacoes,
      axes:{
        xaxis:{
            renderer: $.jqplot.CategoryAxisRenderer,
            labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
            tickRenderer: $.jqplot.CanvasAxisTickRenderer,
           
            tickOptions: {
                angle: -30,
                fontSize: '11px'
            }
        },
	yaxis: {
	    min: 0
	}
      },
    legend: {
        show: true,
        placement: 'outsideGrid'
    },
    highlighter: {
        show: true,
        sizeAdjust: 7.5,
    },
    cursor: {
        show: false
    }
  });
    }
};
$(function() {
    $('#dvSituacaoAtual-fsSituacao input[type=checkbox], #dvSituacaoAtual-fsUnidade input[type=checkbox]').change(function() {
        var id_pai = $(this).parent().parent().attr('id');

        if ((id_pai === 'dvSituacaoAtual-fsUnidade' && $('#dvSituacaoAtual-fsUnidade input[type=checkbox]:checked').length === 0) ||
                (id_pai === 'dvSituacaoAtual-fsSituacao' && $('#dvSituacaoAtual-fsSituacao input[type=checkbox]:checked').length === 0)) {
            $(this).attr('checked', 'checked');
        }
        var idDiv = id_pai.split('-');

        $('#dvSituacaoAtual-buscaSituacaoUnidade, #dvSituacaoAtual-div_graficoResultado').html('');
        SituacaoAtual.gerarTabela(idDiv[0]);
    });
    $('#dvSituacaoMensal-fsSituacao input[type=checkbox], #dvSituacaoMensal-fsUnidade input[type=checkbox]').change(function() {
        var qtd_situacao = $('#dvSituacaoMensal-fsSituacao input[type=checkbox]:checked').length;
        var qtd_unidade = $('#dvSituacaoMensal-fsUnidade input[type=checkbox]:checked').length;
        var data_inicial = $('#DataInicialSituacaoMensal').val();
        var data_final = $('#DataFinalSituacaoMensal').val();

        var id_pai = $(this).parent().parent().attr('id');

        if ((id_pai === 'dvSituacaoMensal-fsUnidade' && qtd_unidade === 0) ||
                (id_pai === 'dvSituacaoMensal-fsSituacao' && qtd_situacao === 0)) {
            $(this).attr('checked', 'checked');
        }
        var idDiv = id_pai.split('-');

        if (qtd_situacao > 0 && data_inicial !== '' && data_final !== '') {
            SituacaoMensal.gerarTabela(idDiv[0]);
        }
    });
    
    $('#dvEvolucaoMensal-fsSituacao input[type=checkbox], #dvEvolucaoMensal-fsUnidade input[type=checkbox]').change(function() {
        var id_pai = $(this).parent().parent().attr('id');

        if ((id_pai === 'dvEvolucaoMensal-fsUnidade' && $('#dvEvolucaoMensal-fsUnidade input[type=checkbox]:checked').length === 0) ||
                (id_pai === 'dvEvolucaoMensal-fsSituacao' && $('#dvEvolucaoMensal-fsSituacao input[type=checkbox]:checked').length === 0)) {
            $(this).attr('checked', 'checked');
        }
        var idDiv = id_pai.split('-');

        $('#dvEvolucaoMensal-buscaSituacaoUnidade, #dvEvolucaoMensal-div_graficoResultado').html('');
        EvolucaoMensal.gerarTabela(idDiv[0]);
    });

    $('.tituloConteudoDireito').click(function() {
        if ($(this).parent().find('.conteudo').is(':visible')) {
            $(this).removeClass('bTop').addClass('bAll');
            $(this).parent().find('.conteudo').hide();
        } else {
            $(this).removeClass('bAll').addClass('bTop');
            $(this).parent().find('.conteudo').show();
        }

        if ($(this).parent().find('.conteudo').is(':visible')) {
            var idDiv = $(this).parent().attr('id');
            eval(idDiv.replace('dv','')+'.gerarTabela(idDiv)');
        }
    });
    
    $('#DataInicialSituacaoMensal').change(function() {
        var valor_inicial = $(this).val();
        var desativar = true;
        
        if (parseInt(valor_inicial) > parseInt($('#DataFinalSituacaoMensal').val())){
            $('#DataFinalSituacaoMensal').val(valor_inicial);
        }
        
        $('#DataFinalSituacaoMensal option').each(function() {
            var valor_final = $(this).val();

            if (valor_inicial === valor_final){
                desativar = false;
            }

            if (desativar) {
                $(this).attr('disabled', 'disabled');
            } else {
                $(this).removeAttr('disabled');
            }
        });
    });
    $('#DataInicialEvolucaoMensal').change(function() {
        var valor_inicial = $(this).val();
        var desativar = true;
        
        if (parseInt(valor_inicial.split('/')[0]) > parseInt($('#DataFinalEvolucaoMensal').val().split('/')[0])){
            $('#DataFinalEvolucaoMensal').val(valor_inicial);
        }
        
        $('#DataFinalEvolucaoMensal option').each(function() {
            var valor_final = $(this).val();

            if (valor_inicial === valor_final){
                desativar = false;
            }

            if (desativar) {
                $(this).attr('disabled', 'disabled');
            } else {
                $(this).removeAttr('disabled');
            }
        });
    });    
    
    
    $('#DataInicialSituacaoMensal, #DataFinalSituacaoMensal').change(function() {
        var idDiv = $(this).parent().parent().parent().attr('id').split('-');
        SituacaoMensal.gerarTabela(idDiv[0]);
    });
    $('#DataInicialEvolucaoMensal, #DataFinalEvolucaoMensal').change(function() {
        var idDiv = $(this).parent().parent().parent().attr('id').split('-');
        EvolucaoMensal.gerarTabela(idDiv[0]);
    });

    $('.tituloConteudoDireito').click();
});
