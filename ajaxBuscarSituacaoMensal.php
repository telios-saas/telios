<?php
require_once("helper_functions.php");
session_start(); // inicio a sessão

if($_SESSION['usuario']){ // verifico se usuario esta logado
    require("conexao.php");
    conexao();
    
    $post_situacoes    = isset($_POST['situacoes'])?$_POST['situacoes']:'';
    $post_unidades    = isset($_POST['unidades'])?$_POST['unidades']:null;
    $post_mes_inicial  = isset($_POST['mes_inicial'])?substr($_POST['mes_inicial'],4,2):"";
    $post_ano_inicial = isset($_POST['mes_inicial'])?substr($_POST['mes_inicial'],0,4):"";
    $post_mes_final    = isset($_POST['mes_final'])?substr($_POST['mes_final'],4,2):"";
    $post_ano_final    = isset($_POST['mes_final'])?substr($_POST['mes_final'],0,4):"";
    $date = date_create('now');
    $mes_atual=  substr(str_pad($date->format('m'),1,'0',STR_PAD_LEFT),-2,2);
    $ano_atual= $date->format('Y');
    $data_final_atual=false;
	
    if ($post_mes_final==$mes_atual && $post_ano_final==$ano_atual) $data_final_atual=true;
    $r['col'][] = array('nome' => 'DATE_FORMAT(sm.data,\'%M / %Y\') AS data',
                                  'label' => 'Data',
                                  'align' => 'left');
    $where = ' WHERE 1=1';
    if (in_array(1, $post_situacoes)) { 
        $r['col'][] = array('nome' => 'SUM(sm.pendente_qtd) AS pendente',
                            'label' => 'Pendentes',
                            'align' => 'right');
    }
    if (in_array(2, $post_situacoes)) { 
        $r['col'][] = array('nome' => 'SUM(sm.em_analise_qtd) AS em_analise',
                            'label' => 'Em an&aacute;lise',
                            'align' => 'right');
    }
    if (in_array(3, $post_situacoes)) { 
        $r['col'][] = array('nome' => 'SUM(sm.concluida_qtd) AS concluida',
                            'label' => 'Conclu&iacute;das',
                            'align' => 'right');
    }
    if (in_array(4, $post_situacoes)) { 
        $r['col'][] = array('nome' => 'SUM(sm.publicada_qtd) AS publicada',
                            'label' => 'Publicadas',
                            'align' => 'right');
    }
    if (in_array(5, $post_situacoes)) { 
        $r['col'][] = array('nome' => 'SUM(sm.cancelada_qtd) AS cancelada',
                            'label' => 'Canceladas',
                            'align' => 'right');
    }

    for($i=0;$i<sizeOf($r['col']);$i++){
        $colunas[] = $r['col'][$i]['nome'];
    }
        
    $select = 'SELECT '.implode($colunas,',');
    
    if (!empty($post_mes_inicial) && !empty($post_mes_final)){
        $where .= ' AND DATE_FORMAT(sm.data,\'%Y%m\') BETWEEN '.$_POST['mes_inicial'].' AND '.$_POST['mes_final'].' ';
    }
    
    if (sizeOf($post_unidades)>0){
        $where .= ' AND sm.id_unidade IN ('.implode($post_unidades,',').')';
    }

    $sql = $select." FROM situacao_mensal AS sm ".$where." GROUP BY DATE_FORMAT(sm.data,'%Y%m')";
    
    mysql_query("SET lc_time_names = 'pt_BR'", $base);
    $exe = mysql_query($sql, $base) or die(mysql_error());
    
    $r['qtd']= mysql_num_rows($exe); // retorna se existe alguma linha no banco
    
    while ($row = mysql_fetch_array($exe, MYSQL_NUM)) {
        $row[0] = ucfirst($row[0]);
        $r['res'][] = $row;
    }

    if ($data_final_atual) $r=situacao_mes_atual($post_situacoes, $post_unidades,$r);
    echo json_encode($r);
}



function situacao_mes_atual($post_situacoes, $post_unidades,$r){
    global $base;
    $sql="select DATE_FORMAT(CURDATE(),'%M / %Y') AS data";

    if (in_array(1, $post_situacoes)) $sql .=', sum(pendente) as pendente';

    if (in_array(2, $post_situacoes)) $sql .=', sum(em_analise) as em_analise';

    if (in_array(3, $post_situacoes)) $sql .=', sum(concluida) as concluida';

    if (in_array(4, $post_situacoes)) $sql .=', sum(publicada) as publicada';

    if (in_array(5, $post_situacoes)) $sql .=', sum(cancelada) as cancelada';

    $sql.=" FROM v_situacao_atual_analises ";
    if (sizeOf($post_unidades)>0)   $sql .= ' where unidade_id IN ('.implode($post_unidades,',').')';
    
    mysql_query("SET lc_time_names = 'pt_BR'", $base);
    $exe = mysql_query($sql, $base) or die(mysql_error());
    
    $r['qtd']+=1;
    while ($row = mysql_fetch_array($exe, MYSQL_NUM)) {
        $row[0] = ucfirst($row[0]);
        $r['res'][] = $row;
    }
    return $r;
}
