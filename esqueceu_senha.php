<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Télios</title>
<style>
body{
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	color:#000;
	line-height:18px;	
}
*{
	padding:0;
	margin:0;
	border:0;	
}
.fonte28 {
	font-size:28px;
	font-weight:bold;
	color:#05255a;
	text-transform:uppercase;
}
p{
	margin:10px 0;	
}
.btnEnviar{
	background-image:url(images/B_prosseguir.jpg);
	width:109px;
	height:30px;
	border:none;
	background-position:0 0;
	cursor:pointer;	
}
.btnEnviar:hover{
	background-image:url(images/B_prosseguir.jpg);
	width:109px;
	height:30px;
	border:none;
	background-position:0 -30px;
	cursor:pointer;	
}
input{
	border:1px solid #CCC;
	background-color:#FFF;
	padding:5px;	
}
</style>

<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="geral">
  <p class="fonte28">recuperação de senha</p>
  <p>Digite seu e-mail no campo abaixo.</p>	
  <form id="envia_form" name="envia_form" method="post" action="esqueceu_senha_verifica.php">
    <p><span id="sprytextfield1">
   
    <input name="email" type="text" id="email" size="60" /><br />
    <span class="textfieldRequiredMsg">* Obrigatório</span><span class="textfieldInvalidFormatMsg">E-mail inválido</span></span></p>
    <p><input type="submit" name="enviar" id="enviar" value="" class="btnEnviar" /></p>
  </form>
</div>
<script type="text/javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "email");
</script>
</body>
</html>