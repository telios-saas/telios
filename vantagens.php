<?php session_start(); // Inicio a Sessão?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="análise de falhas, indústria, RCA - Root Cause Analisys, ações de bloqueio, causa raiz da falha
" />
<meta name="description" content="Realize análises de falhas de maneira descomplicada e contribua para a melhoria nos indicadores da indústria." />
<meta name="rating" content="General" />
<meta name="author" content="Agência GH" />
<meta name="language" content="pt-br" />
<meta name="ROBOTS" content="index,follow" />
<title>TELIOS</title>
<link href="../favicon.ico" rel="shortcut icon" />
<link href="css/home.css" rel="stylesheet" type="text/css" media="screen" />
<script language="javascript" src="js/post.js"></script>
<script language="javascript" src="js/mascara.js"></script>
<script type="text/javascript" src="swfobject/swfobject.js"></script>

<link rel="stylesheet" href="css/colorbox.css" />
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.colorbox.js"></script>
		<script>
			$(document).ready(function(){
				
				$(".iframe").colorbox({iframe:true, width:"600px", height:"300px"});
			});
		</script>

</head>
<body>
<?php if (isset($_SESSION['usuario'])) { // Verifico se o Usuário ja esta logado?>
<script language="JavaScript">
			window.location.href = "logado.php";
		</script>
<?php } else { // Se o Usuário não estiver logado mostro form de login ?>
<div id="tudo">
  <div id="topo">
    <div id="topoInterno">
              <div id="linkHome"><a href="index.php"><img src="images/site/linkHome.gif" width="192" height="41" /></a></div>
              <a href="ferramenta.php"><div id="linkFerramenta"></div></a>
              <a href="rca.php"><div id="linkRca"></div></a>
      		  <a href="vantagens.php"><div id="linkVantagens"></div></a>
      		  <a href="artigos.php"><div id="linkArtigos"></div></a>
	  <a href="estatisticas.php" title="Estatísticas"><div id="linkEstatisticas"></div></a>
              <a href="busca_publicadas.php"><div id="linkBusca"></div></a>
              <a href="contato.php"><div id="linkContato"></div></a>
              <div id="formLogin">
        <div id="formLoginInterna">
                  <form id="log" name="log" method="post" action="logado.php" >
            <div id="inputFormLogin">
                      <input name="login" type="text" id="login"/>
                    </div>
            <div id="inputFormSenha">
                      <input name="senha" type="password" id="senha"/>
                    </div>
            <div id="inputFormEntrar">
                      <input name="envia" type="submit" id="envia" class="btnLogar" value=""/>
                    </div>
          </form>
                  <?php if(isset($_GET['deslogado'])){?>
                  <div id="erroLogin">Login ou Senha Inválido</div>
                  <?php }?>
<div id="esqueceu_senha"><a href="esqueceu_senha.php" class="group2 iframe">Esqueceu sua senha ? </a></div>
                </div>
      </div>
            </div>
  </div>
  <div id="conteudo2"> 
   <div id="chamada"><p class="fonte28">Vantagens</p></div> 
  <div id="vantagens">
  
  <div id="simplicada">
  	<p>Com dados básicos para manter a </p>
  </div>
  <div id="simplicada_2">
  	<p>rastreabilidade das informações, cadastre e priorize as suas
análises, gere ações de
bloqueio e mantenha
tudo sob controle.</p>
  </div>
  
  <div id="filtros">
  	<p>A ferramenta disponibiliza filtros</p>
  </div>
  <div id="filtros_2">
  	<p>rápidos para resgate de análises e ações cadastradas, facilitando a atualização
das informações.</p>
  </div>
  
  <div id="relatorios_click">
  	<p>Os relatórios podem ser gerados em formato pdf </p>
  </div>
  
   <div id="relatorios_click_2">
  	<p>padrão e as ações de bloqueio pendentes podem ser
enviadas por email para
os responsáveis em
apenas um click. </p>
  </div>
  
  <div id="controle_indicadores">
  	<p>Explore a
ferramenta e </p>
  </div>
  
   <div id="controle_indicadores_2">
  	<p>contribua para a constante melhoria
dos indicadores
de performance de seus equipamentos.</p>
  </div>
  
  
  
  </div>
  </div>
  <div id="rodape">
  	<div id="rodape_interno">
    	<a href="http://www.agenciagh.com.br" title="Agência GH" target="_blank"><div id="marca_gh"></div></a>
        <div id="menu_inferior">
        	<div class="email_contato"><span class="negrito">E-mail:</span><a href="mailto:contato@telios.eng.br" class="group2">contato@telios.eng.br</a></div>
            
            <div class="menu_inferior_link"><a href="contato.php" title="CONTATO" class="group2">Contato</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="busca_publicadas.php" title="BUSCA ANÁLISE" class="group2">Busca Análises</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="estatisticas.php" title="RANKING DO CONHECIMENTO" class="group2">Ranking do Conhecimento</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="artigos.php" title="ARTIGOS" class="group2">Artigos</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="rca.php" title="RCA" class="group2">RCA</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="vantagens.php" title="VANTAGENS" class="group2">Vantagens</a></div>
            <div class="menu_inferior_link">|</div>
            <div class="menu_inferior_link"><a href="ferramenta.php" title="A FERRAMENTA" class="group2">A Ferramenta</a></div>
        </div>
        
        <div class="clear"></div>
    </div>
  </div>
</div>

<!--<form id="enviaLogin" name="enviaLogin" method="post" action="logado.php" >
  <table cellspacing="0" id="tabGeral">
    <tr>
      <td align="right" valign="middle">Usuário:</td>
      <td><input name="login" type="text" id="login" size="30"/></td>
    </tr>
    <tr>
      <td align="right" valign="middle">Senha:</td>
      <td><input name="senha" type="password" id="senha" size="30"/></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><a href="#" onclick="recuperaSenha('senha/recuperaSenha.php');">Esqueci Minha Senha.</a></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input name="envia" type="submit" id="envia" value="Logar" class="btnLogar"/></td>
    </tr>
  </table>
</form>-->
<?php }?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29818024-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>