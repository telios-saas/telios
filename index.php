<?php session_start(); // Inicio a Sessão ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="keywords" content="análise de falhas, indústria, RCA - Root Cause Analisys, ações de bloqueio, causa raiz da falha
                  " />
            <meta name="description" content="Realize análises de falhas de maneira descomplicada e contribua para a melhoria nos indicadores da indústria." />
            <meta name="rating" content="General" />
            <meta name="author" content="Agência GH" />
            <meta name="language" content="pt-br" />
            <meta name="ROBOTS" content="index,follow" />
            <title>TÉLIOS</title>

            <link href="css/home.css" rel="stylesheet" type="text/css" media="screen" />
            <!--[if IE]>
            <link href="css/ie.css" rel="stylesheet" type="text/css" media="screen" />
                    <![endif]-->
            <!-- [if gte IE 8]
            <link rel="stylesheet" href="css/ie8.css" type="text/css" />
            <! [endif] -->

            <script language="javascript" src="js/post.js"></script>
            <script language="javascript" src="js/mascara.js"></script>
            <script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>

            <link rel="stylesheet" href="css/colorbox.css" />
            <link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
            <script src="js/jquery.min.js"></script>
            <script src="js/jquery.colorbox.js"></script>
            <script>
                $(document).ready(function() {

                    $(".iframe").colorbox({iframe: true, width: "600px", height: "300px"});
                    //$(".group2").colorbox({rel:'group2'});
                });
            </script>
    </head>
    <body>
        <?php
        if (isset($_SESSION['usuario'])) {

            if (isset($_GET['deslogado'])) {
                session_start();
                session_unset();
                session_destroy();
                ?>	
                <script language="JavaScript">
                    window.location.href = "index.php";
                </script>
            <?php }
            // Verifico se o Usuário ja esta logado
            ?>
            <script language="JavaScript">
                window.location.href = "logado.php";
            </script>
<?php } else { // Se o Usuário não estiver logado mostro form de login  ?>
            <div id="tudo">
                <div id="topo">
                    <div id="topoInterno">
                        <div id="linkHome"><a href="index.php"><img src="images/site/linkHome.gif" width="192" height="41" /></a></div>
                        <a href="ferramenta.php"><div id="linkFerramenta"></div></a>
                        <a href="rca.php"><div id="linkRca"></div></a>
                        <a href="vantagens.php"><div id="linkVantagens"></div></a>
                        <a href="artigos.php"><div id="linkArtigos"></div></a>
                        <a href="estatisticas.php" title="Estatísticas"><div id="linkEstatisticas"></div></a>
                        <a href="busca_publicadas.php"><div id="linkBusca"></div></a>
                        <a href="contato.php"><div id="linkContato"></div></a>
                        <div id="formLogin">
                            <div id="formLoginInterna">
                                <form id="log" name="log" method="post" action="logado.php" >
                                    <div id="inputFormLogin">
                                        <input name="login" type="text" id="login"/>
                                    </div>
                                    <div id="inputFormSenha">
                                        <input name="senha" type="password" id="senha"/>
                                    </div>
                                    <div id="inputFormEntrar">
                                        <input name="envia" type="submit" id="envia" class="btnLogar" value=""/>
                                    </div>
                                </form>
                                <?php if (isset($_GET['deslogado'])) { ?>
                                    <div id="erroLogin">Login ou Senha Inválido</div>
    <?php } ?>
                                <div id="esqueceu_senha"><a href="esqueceu_senha.php" class="group2 iframe">Recuperar / Alterar
senha    </a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="banner">
		    <a href="ferramenta.php">
			<img src="images/conheca_ferramenta.png" />
		    </a>
                </div>
                <div id="fundoSite">
                    <div id="fundoSiteInterna">
                        <div class="ladoEsq">
                            <p><img src="images/site/layoute_03.png" width="402" height="27" /></p>
                            <div id="textoEsq">
                                <ul>
                                    <li>Com dados básicos para manter a rastreabilidade das informações, cadastre e priorize suas análises, gere ações de bloqueio e mantenha tudo sob controle.</li>
                                    <li>A ferramenta disponibiliza filtros rápidos para resgate de análises e ações cadastradas, facilitando a atualização das informações.</li>
                                </ul>
                            </div>
                            <div id="textoDir">
                                <ul>
                                    <li>Os relatórios podem ser gerados em formato pdf padrão e as ações de bloqueio pendentes podem ser enviadas por email para os responsáveis em apenas um clique.</li>
                                    <li>Explore a ferramenta e contribua para a constante melhoria dos indicadores de manutenção de sua empresa. </li>
                                </ul>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div id="ladoDir" style="position:relative;">
                            <div style="left: 23px; position: absolute; top: 50px; width: 210px;">Utilize a Télios gratuitamente pelo período de avaliação de 90 dias.</div>
                        <!--<div id="chamadaCadastro"><img src="images/site/layoute_06.png" width="86" height="14" /></div>-->
                            <form id="enviaDados2" name="enviaDados2" method="post" onsubmit="return false;">
                                <div id="inputNome">
                                    <input type="text" name="nome_cadastra" id="nome_cadastra" onfocus="this.value = ''" value="NOME" onkeypress="enviar8(event);"/>
                                </div>

                                <div id="inputEmail">
                                    <input type="text" name="email_cadastra" id="email_cadastra" onfocus="this.value = ''" value="E-MAIL" onkeypress="enviar8(event);"/>
                                </div>
                                <div id="prosseguir">
                                    <input name="envia" type="button" id="envia" onclick="envia_cadastro('enviaDados2', 'cadastra.php?acao=1', 'tudo');" value="" class="btnProsseguir"/>
                                </div>
                            </form>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div id="conteudo2">
                    <div id="dadosConteudoEsq">
                        <div id="conteudoDadosEsq">
                            <p><img src="images/site/layoute_11.png" width="35" height="15" /></p>
                            <p class="fonteCinza">A Análise de Causa Raiz (Root Cause Analysis - RCA) é uma metodologia de resolução de problemas que proporciona valiosa contribuição na melhoria da disponibilidade dos ativos industriais, diminuição dos custos de manutenção e na formação de equipes de alta performance. </p>
                            <p><a href="rca.php"><img src="images/site/btnLeiaMais.gif" width="79" height="32" /></a></p>
                        </div>
                        <div id="conteudoDadosDir">
                            <p><img src="images/site/layoute_13.png" width="63" height="20" /></p>
                            <ul>
                                <li class="fonteCinza">Justifying Root Cause Analisys<br />
                                    <span class="italico">By Robert J. Latino -  Reliability Center, Inc.</span></li>
                                <li class="fonteCinza">Getting Root Cause Analysis to Work for You<br />
                                    <span class="italico">Alexander Dunn - Assetivity Pty Ltd</span></li>
                                <!--<li class="fonteCinza">Manutenção em Tempo de Crise<br />
                          <span class="italico">Mario Filho</span> </li>-->
                            </ul>
                            <div id="btn_1">
                                <p><a href="artigos.php"><img src="images/site/btnLeiaMais.gif" width="79" height="32" /></a></p>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div id="dadosConteudoDir">
                        <p><img src="images/site/chama_busca.jpg"/></p>
                        <p>Digite um termo de referência para encontrar a análise da falha desejada.<br />
                            <span class="italico">Exemplo: Correia da extrusora.</span></p>
                        <form id="form1" name="form1" method="post" action="busca_publicadas.php">
                            <div id="input_busca">
                                <span id="sprytextfield1">

                                    <input type="text" name="busca_analise" id="busca_analise" />

                                    <span class="textfieldRequiredMsg">

                                        <div class="aviso_busca">Campo Obrigatório</div>

                                    </span>

                                </span> 

                            </div>
                            <div id="envia_busca">
                                <input type="submit" name="envia_busca2" id="envia_busca2" value="" />
                            </div>
                        </form>
                            <!--<p><img src="images/site/layoute_15.png" width="70" height="15" /></p>
                                <p>Em caso de dúvidas, críticas ou sujestões, entre em contato.</p>
                                <p>Telefone: <span class="negrito">+ 51 (55) 3333 - 3333</span><br />
                          Fax: <span class="negrito">+ 51 (55) 3333 - 3331</span><br />
                          E-mail: <span class="negrito">contato@telios.com.br</span><br />
                          MSN: <span class="negrito">contato@hotmail.com</span><br />
                          Skipe: <span class="negrito">telios</span></p>-->
                    </div>
                    <div class="clear"></div>
                </div>
                <div id="rodape">
                    <div id="rodape_interno">
                        <a href="http://www.agenciagh.com.br" title="Agência GH" target="_blank"><div id="marca_gh"></div></a>
                        <div id="menu_inferior">
                            <div class="email_contato"><span class="negrito">E-mail:</span><a href="mailto:contato@telios.eng.br" class="group2">contato@telios.eng.br</a></div>

                            <div class="menu_inferior_link"><a href="contato.php" title="CONTATO" class="group2">Contato</a></div>
                            <div class="menu_inferior_link">|</div>
                            <div class="menu_inferior_link"><a href="busca_publicadas.php" title="BUSCA ANÁLISE" class="group2">Busca Análises</a></div>
                            <div class="menu_inferior_link">|</div>
                            <div class="menu_inferior_link"><a href="estatisticas.php" title="RANKING DO CONHECIMENTO" class="group2">Ranking do Conhecimento</a></div>
                            <div class="menu_inferior_link">|</div>
                            <div class="menu_inferior_link"><a href="artigos.php" title="ARTIGOS" class="group2">Artigos</a></div>
                            <div class="menu_inferior_link">|</div>
                            <div class="menu_inferior_link"><a href="rca.php" title="RCA" class="group2">RCA</a></div>
                            <div class="menu_inferior_link">|</div>
                            <div class="menu_inferior_link"><a href="vantagens.php" title="VANTAGENS" class="group2">Vantagens</a></div>
                            <div class="menu_inferior_link">|</div>
                            <div class="menu_inferior_link"><a href="ferramenta.php" title="A FERRAMENTA" class="group2">A Ferramenta</a></div>
                        </div>

                        <div class="clear"></div>
                    </div>
                </div>


            </div>

            <!--<form id="enviaLogin" name="enviaLogin" method="post" action="logado.php" >
              <table cellspacing="0" id="tabGeral">
                <tr>
                  <td align="right" valign="middle">Usuário:</td>
                  <td><input name="login" type="text" id="login" size="30"/></td>
                </tr>
                <tr>
                  <td align="right" valign="middle">Senha:</td>
                  <td><input name="senha" type="password" id="senha" size="30"/></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td><a href="#" onclick="recuperaSenha('senha/recuperaSenha.php');">Esqueci Minha Senha.</a></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td><input name="envia" type="submit" id="envia" value="Logar" class="btnLogar"/></td>
                </tr>
              </table>
            </form>-->
<?php } ?>

        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-29818024-1']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();
            var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
        </script>
    </body>
</html>