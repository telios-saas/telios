<?php 
require('conexao.php');
conexao();
if(isset($_GET['apontador']) and isset($_GET['id_analise'])){
	$id_autor = mysql_escape_string(addslashes($_GET['apontador']));
	$id_analise = mysql_escape_string(addslashes($_GET['id_analise']));

	$select = "SELECT * FROM  dados_basico_analise WHERE id_analise = '$id_analise'";
	$query = mysql_query($select, $base) or die(mysql_error()); 
	$total_resultados = mysql_num_rows($query);
	$reg = mysql_fetch_assoc($query);
	
	$select_user = "SELECT * FROM  usuarios WHERE id_usuario = '$id_autor'";
	$query_user = mysql_query($select_user, $base) or die(mysql_error()); 
	$reg_user = mysql_fetch_assoc($query_user);
	
}else{
	$total_resultados = 0;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TELIOS</title>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="js/jquery.mask.min.js"></script>
<script>
$(document).ready(function(e) {
	var obj = $('.msk-celular');
		
		$(obj).mask(($(obj).val().length > 13) ? '(00)00000-0000' : '(00)0000-0000', 
		{onKeyPress: function(phone, e, currentField, options){
		 var new_sp_phone = phone.match(/^(\(11\)9(5[0-9]|6[0-9]|7[01234569]|8[0-9]|9[0-9])[0-9]{1})/g);
		 new_sp_phone ? $(currentField).mask('(00)00000-0000', options) : $(currentField).mask('(00)0000-0000', options)}});
});
</script>
<style>
*{
	padding:0; margin:0; border:0;	
}
body{
	font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; line-height:18px; background-color:#eaebf1;	
}
.fonte21{
	font-size:21px; color:#072e70; font-weight:bold; letter-spacing:-1px;	
}
.fonte18{
	font-size:21px; color:#072e70; font-weight:bold; 
}
.fonte15{
	font-size:15px; 
}
.negrito{
	font-weight:bold;	
}
#chamada{
	margin:25px;	
}
#dados_analise{
	margin:0 25px 25px 25px;	
}
#dados_analise p{
	padding:6px 0;	
}
#envia_form{
	margin-top:20px;	
}
#envia_form input{
	border:1px solid #b8becd;
	padding:5px;
	width:255px;
	background-color:#FFF;
	margin-right:10px;	
}
#envia_form textarea{
	width:413px;
	height:134px;
	background-color:#FFF;
	border:1px solid #b8becd;
	padding:5px;
	font-size:12px;
	color:#000;
	font-family:Arial, Helvetica, sans-serif;
	margin-right:10px;		
}
.btnEnviar{
	background-image:url(images/btn_enviar.jpg);
	width:80px;
	height:32px;
	border:none;
	cursor:pointer;	
}
</style>
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
</head>

<body>
<div id="chamada">
	<p class="fonte21">Enviar Comentário</p>
</div>
<div id="dados_analise">
	<p class="fonte18">Análise "<?php echo $reg['titulo'];?>"</p>
    <p class="fonte15"><span class="negrito">Autor:</span> <?php echo $reg_user['nome'];?>. <span class="negrito">Familia Equipamento:</span> <?php echo $reg['familia_equipamento'];?>. <span class="negrito">Causa Raiz:</span> <?php echo $reg['causa_raiz'];?>.</p>
    <form id="form1" name="form1" method="post" action="envia_email_publico2.php?user=<?php echo $reg_user['usuario'];?>">
        <div id="envia_form">
          <p>
            <label for="nome"></label>
            <span id="sprytextfield1">
            
            <input type="text" name="nome" id="nome2" />
</span>          
            <input name="envia" type="hidden" id="envia" value="1" />
          </p>

          
          <p><span id="sprytextfield4"><input type="text" name="email" id="email" /><span class="textfieldRequiredMsg">* Obrigatório</span><span class="textfieldInvalidFormatMsg">* Inválido</span></span></p>
          <p>
            <label for="comentario"></label>
            <span id="sprytextarea1">
            <label for="comentario2"></label>
            <textarea name="comentario" id="comentario" cols="45" rows="5"></textarea>
</span>          </p> 
      </div>
        <p><input type="submit" name="enviar" id="enviar" value="" class="btnEnviar" /></p>
    </form>
</div>
<script type="text/javascript">
var sprytextfield4 = new Spry.Widget.ValidationTextField("sprytextfield4", "email", {useCharacterMasking:true, validateOn:["blur"], hint:"E-mail:"});
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none", {isRequired:false, hint:"Nome"});
var sprytextarea1 = new Spry.Widget.ValidationTextarea("sprytextarea1", {isRequired:false, hint:"Coment\xE1rio"});
</script>
</body>
</html>