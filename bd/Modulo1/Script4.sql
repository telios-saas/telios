CREATE OR REPLACE VIEW v_situacao_atual_analises AS
SELECT
	u.id_unidade AS unidade_id,
	u.nome AS unidade_nome,
	COUNT(DISTINCT(pe.id_basico)) AS pendente,
	COUNT(DISTINCT(ea.id_basico)) AS em_analise,
	COUNT(DISTINCT(co.id_basico)) AS concluida,
	COUNT(DISTINCT(pu.id_basico)) AS publicada,
	COUNT(DISTINCT(ca.id_basico)) AS cancelada
FROM
	unidade_fabril AS u
LEFT JOIN dados_basico_analise AS pe
	ON pe.id_unidade = u.id_unidade
	AND pe.id_situacao = 1
LEFT JOIN dados_basico_analise AS ea
	ON ea.id_unidade = u.id_unidade
	AND ea.id_situacao = 2
LEFT JOIN dados_basico_analise AS co
	ON co.id_unidade = u.id_unidade
	AND co.id_situacao = 3
LEFT JOIN dados_basico_analise AS pu
	ON pu.id_unidade = u.id_unidade
	AND pu.id_situacao = 4
LEFT JOIN dados_basico_analise AS ca
	ON ca.id_unidade = u.id_unidade	
	AND ca.id_situacao = 5
GROUP BY u.id_unidade;

DROP TABLE IF EXISTS situacao_mensal;
CREATE TABLE situacao_mensal(
	id int(11),
        id_unidade int(11) NOT NULL,
	pendente_qtd int(11) NOT NULL,
	em_analise_qtd int(11) NOT NULL,
	concluida_qtd int(11) NOT NULL,
	publicada_qtd int(11) NOT NULL,
	cancelada_qtd int(11) NOT NULL,
	data DATETIME NOT NULL
) Engine=InnoDB;
ALTER TABLE situacao_mensal ADD CONSTRAINT pk_situacao_mensal PRIMARY KEY (id);
ALTER TABLE situacao_mensal CHANGE id id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE situacao_mensal ADD CONSTRAINT fk_situacao_mensal_unidade FOREIGN KEY (id_unidade) REFERENCES unidade_fabril (id_unidade);