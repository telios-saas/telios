DROP TABLE IF EXISTS situacao_mensal;
CREATE TABLE situacao_mensal(
	id int(11),
	pendente_qtd int(11) NOT NULL,
	em_analise_qtd int(11) NOT NULL,
	concluida_qtd int(11) NOT NULL,
	publicada_qtd int(11) NOT NULL,
	cancelada_qtd int(11) NOT NULL,
	data DATETIME NOT NULL
) Engine=InnoDB;
ALTER TABLE situacao_mensal ADD CONSTRAINT pk_situacao_mensal PRIMARY KEY (id);
ALTER TABLE situacao_mensal CHANGE id id int(11) NOT NULL AUTO_INCREMENT;