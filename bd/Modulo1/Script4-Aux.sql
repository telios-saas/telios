INSERT INTO situacao_mensal (id, id_unidade, pendente_qtd, em_analise_qtd, concluida_qtd, publicada_qtd, cancelada_qtd, data) VALUES 
(NULL, 28, '4', '6', '2', '12', '5', '2013-04-30 00:00:00'),
(NULL, 28, '6', '2', '6', '9', '2', '2013-05-30 00:00:00'),
(NULL, 28, '4', '5', '3', '12', '5', '2013-06-30 00:00:00'),
(NULL, 28, '3', '6', '8', '7', '7', '2013-07-30 00:00:00'),
(NULL, 28, '4', '8', '2', '5', '9', '2013-08-30 00:00:00'),
(NULL, 28, '7', '12', '2', '1', '1', '2013-09-30 00:00:00'),
(NULL, 28, '4', '4', '4', '2', '2', '2013-010-30 00:00:00'),

(NULL, 22,'4', '6', '2', '12', '5', '2013-10-30 00:00:00'),
(NULL, 22,'6', '2', '6', '9', '2', '2013-09-30 00:00:00'),
(NULL, 22,'4', '5', '3', '12', '5', '2013-08-30 00:00:00'),
(NULL, 22,'3', '6', '8', '7', '7', '2013-07-30 00:00:00'),
(NULL, 22,'4', '8', '2', '5', '9', '2013-06-30 00:00:00'),
(NULL, 22,'7', '12', '2', '1', '1', '2013-05-30 00:00:00'),
(NULL, 22,'4', '4', '4', '2', '2', '2013-04-30 00:00:00');

SELECT 
    MONTH(sm.data) AS data,
    SUM(sm.pendente_qtd) AS pendente,
    SUM(sm.em_analise_qtd) AS em_analise,
    SUM(sm.concluida_qtd) AS concluida,
    SUM(sm.publicada_qtd) AS publicada,
    SUM(sm.cancelada_qtd) AS cancelada 
FROM 
    situacao_mensal AS sm  
WHERE 1=1 
    AND YEAR(sm.data) = YEAR(CURDATE()) 
    AND MONTH(sm.data) BETWEEN 4 AND 10  
    AND sm.id_unidade IN(28,22)
GROUP BY MONTH(sm.data);