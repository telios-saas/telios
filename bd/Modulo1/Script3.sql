ALTER TABLE causa_efeito DROP FOREIGN KEY fk_causa_efeito_analise;
ALTER TABLE causa_efeito ADD CONSTRAINT fk_causa_efeito_analise FOREIGN KEY (id_analise) REFERENCES analise_falhas(id_analise) ON DELETE CASCADE;

ALTER TABLE dados_basico_analise DROP FOREIGN KEY fk_dados_basico_analise_analise;
ALTER TABLE dados_basico_analise ADD CONSTRAINT fk_dados_basico_analise_analise FOREIGN KEY (id_analise) REFERENCES analise_falhas(id_analise) ON DELETE CASCADE;

ALTER TABLE arquivos_analise DROP FOREIGN KEY fk_arquivos_analise_analise;
ALTER TABLE arquivos_analise ADD CONSTRAINT fk_arquivos_analise_analise FOREIGN KEY (id_analise) REFERENCES analise_falhas(id_analise) ON DELETE CASCADE;

ALTER TABLE priorizacao_falhas DROP FOREIGN KEY fk_priorizacao_falhas_analise;
ALTER TABLE priorizacao_falhas ADD CONSTRAINT fk_priorizacao_falhas_analise FOREIGN KEY (id_analise) REFERENCES analise_falhas(id_analise) ON DELETE CASCADE;

ALTER TABLE 5_porques DROP FOREIGN KEY fk_5_porques_analise;
ALTER TABLE 5_porques ADD CONSTRAINT fk_5_porques_analise FOREIGN KEY (id_analise) REFERENCES analise_falhas(id_analise) ON DELETE CASCADE;
