DROP TABLE IF EXISTS situacao;
CREATE TABLE situacao (
	id_situacao INT(50) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	descricao VARCHAR(50) NOT NULL,
	status ENUM('A','I') DEFAULT 'A'
) Engine=InnoDB;

INSERT INTO situacao (id_situacao,descricao) VALUES
(1,'Pendente'),
(2,'Em Análise'),
(3,'Concluída'),
(4,'Publicada'),
(5,'Cancelada'),
(6,'Atrasada'),
(7,'Aberta');

UPDATE acoes SET situacao = 1 where situacao = 'Pendente';
UPDATE acoes SET situacao = 3 where situacao = 'Concluída';
UPDATE acoes SET situacao = 6 where situacao = 'Atrasada';
UPDATE acoes SET situacao = 5 where situacao = 'Cancelada';

UPDATE dados_basico_analise SET situacao = 1 where situacao = 'Pendente';
UPDATE dados_basico_analise SET situacao = 3 where situacao = 'Concluída';
UPDATE dados_basico_analise SET situacao = 4 where situacao = 'Publicada';

ALTER TABLE acoes CHANGE situacao id_situacao INT( 50 ) NOT NULL ;
ALTER TABLE dados_basico_analise CHANGE situacao id_situacao INT( 50 ) NOT NULL ;

ALTER TABLE acoes ADD CONSTRAINT fk_acoes_situacao FOREIGN KEY (id_situacao) REFERENCES situacao(id_situacao);
ALTER TABLE dados_basico_analise ADD CONSTRAINT fk_dados_basico_analise_situacao FOREIGN KEY (id_situacao) REFERENCES situacao(id_situacao);

DELETE FROM causa_efeito WHERE id_analise NOT IN (SELECT DISTINCT(id_analise) FROM analise_falhas);
ALTER TABLE causa_efeito ADD CONSTRAINT fk_causa_efeito_analise FOREIGN KEY (id_analise) REFERENCES analise_falhas(id_analise);
DELETE FROM dados_basico_analise WHERE id_analise NOT IN (SELECT DISTINCT(id_analise) FROM analise_falhas);
ALTER TABLE dados_basico_analise ADD CONSTRAINT fk_dados_basico_analise_analise FOREIGN KEY (id_analise) REFERENCES analise_falhas(id_analise);
DELETE FROM arquivos_analise WHERE id_analise NOT IN (SELECT DISTINCT(id_analise) FROM analise_falhas);
ALTER TABLE arquivos_analise ADD CONSTRAINT fk_arquivos_analise_analise FOREIGN KEY (id_analise) REFERENCES analise_falhas(id_analise);
DELETE FROM priorizacao_falhas WHERE id_analise NOT IN (SELECT DISTINCT(id_analise) FROM analise_falhas);
ALTER TABLE priorizacao_falhas ADD CONSTRAINT fk_priorizacao_falhas_analise FOREIGN KEY (id_analise) REFERENCES analise_falhas(id_analise);
DELETE FROM 5_porques WHERE id_analise NOT IN (SELECT DISTINCT(id_analise) FROM analise_falhas);
ALTER TABLE 5_porques ADD CONSTRAINT fk_5_porques_analise FOREIGN KEY (id_analise) REFERENCES analise_falhas(id_analise);

DELETE FROM dados_basico_analise WHERE unidade_fabril NOT IN (SELECT nome FROM unidade_fabril);
DROP PROCEDURE IF EXISTS DE_PARA_UNIDADE_ANALISE;
DELIMITER $$
CREATE PROCEDURE DE_PARA_UNIDADE_ANALISE()
	BEGIN
		DECLARE done INT DEFAULT FALSE;
		DECLARE p_id INT(50);
		DECLARE p_nome VARCHAR(255);
		DECLARE cur1 CURSOR FOR SELECT id_unidade, nome FROM unidade_fabril;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
		
		OPEN cur1;
		REPEAT
				FETCH cur1 INTO p_id, p_nome;
				UPDATE dados_basico_analise SET unidade_fabril = p_id WHERE unidade_fabril = p_nome;
		UNTIL done END REPEAT;
		CLOSE cur1;
	END$$
DELIMITER ;
CALL DE_PARA_UNIDADE_ANALISE();
DROP PROCEDURE IF EXISTS DE_PARA_UNIDADE_ANALISE;

ALTER TABLE dados_basico_analise CHANGE unidade_fabril id_unidade INT( 50 ) NOT NULL;
ALTER TABLE dados_basico_analise ADD CONSTRAINT fk_dados_basico_analise_unidade FOREIGN KEY (id_unidade) REFERENCES unidade_fabril(id_unidade);

CREATE OR REPLACE VIEW v_situacao_atual_analises AS
SELECT
	u.id_unidade AS unidade_id,
	u.nome AS unidade_nome,
	COUNT(pe.id_basico) AS pendente,
	COUNT(ea.id_basico) AS em_analise,
	COUNT(co.id_basico) AS concluida,
	COUNT(pu.id_basico) AS publicada,
	COUNT(ca.id_basico) AS cancelada
FROM
	unidade_fabril AS u
LEFT JOIN dados_basico_analise AS pe
	ON pe.id_unidade = u.id_unidade
	AND pe.id_situacao = 1
LEFT JOIN dados_basico_analise AS ea
	ON ea.id_unidade = u.id_unidade
	AND ea.id_situacao = 2
LEFT JOIN dados_basico_analise AS co
	ON co.id_unidade = u.id_unidade
	AND co.id_situacao = 3
LEFT JOIN dados_basico_analise AS pu
	ON pu.id_unidade = u.id_unidade
	AND pu.id_situacao = 4
LEFT JOIN dados_basico_analise AS ca
	ON ca.id_unidade = u.id_unidade	
	AND ca.id_situacao = 5
GROUP BY u.id_unidade;

ALTER TABLE user_clientes ADD CONSTRAINT fk_user_clientes_empresa FOREIGN KEY (id_empresa) REFERENCES clientes(id_clientes);
ALTER TABLE user_clientes ADD CONSTRAINT fk_user_clientes_login FOREIGN KEY (id_login) REFERENCES usuarios(id_usuario);

ALTER TABLE usuarios ADD CONSTRAINT fk_usuarios_apontador FOREIGN KEY (apontador) REFERENCES clientes(id_clientes);