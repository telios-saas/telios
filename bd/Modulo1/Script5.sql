DROP TABLE IF EXISTS evolucao_mensal;
CREATE TABLE evolucao_mensal(
	id int(11),
        id_unidade int(11) NOT NULL,
	pendente_qtd int(11) NOT NULL,
	concluida_qtd int(11) NOT NULL,
	cancelada_qtd int(11) NOT NULL,
	atrasada_qtd int(11) NOT NULL,
	data DATETIME NOT NULL
) Engine=InnoDB;
ALTER TABLE evolucao_mensal ADD CONSTRAINT pk_evolucao_mensal PRIMARY KEY (id);
ALTER TABLE evolucao_mensal CHANGE id id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE evolucao_mensal ADD CONSTRAINT fk_evolucao_mensal_unidade FOREIGN KEY (id_unidade) REFERENCES unidade_fabril (id_unidade);


CREATE OR REPLACE VIEW v_situacao_atual_acoes AS
SELECT
	un.id_unidade AS unidade_id,
	un.nome AS unidade_nome,
	COUNT(DISTINCT(pe.acao_id)) AS pendente,
	COUNT(DISTINCT(co.acao_id)) AS concluida,
	COUNT(DISTINCT(ca.acao_id)) AS cancelada,
        COUNT(DISTINCT(at.acao_id)) AS atrasada
FROM
	unidade_fabril AS un
LEFT JOIN v_acao_bloqueio AS pe
	ON pe.unidade_id = un.id_unidade
	AND pe.situacao_id = 1
LEFT JOIN v_acao_bloqueio AS co
	ON co.unidade_id = un.id_unidade
	AND co.situacao_id = 3
LEFT JOIN v_acao_bloqueio AS ca
	ON ca.unidade_id = un.id_unidade	
	AND ca.situacao_id = 5
LEFT JOIN v_acao_bloqueio AS at
	ON at.unidade_id = un.id_unidade
	AND at.situacao_id = 6
GROUP BY un.id_unidade;