ALTER TABLE dados_basico_analise CHANGE quem_cadastro quem_cadastro INT( 50 ) NOT NULL ;
ALTER TABLE dados_basico_analise ADD CONSTRAINT fk_dados_basico_analise_criador FOREIGN KEY (quem_cadastro) REFERENCES usuarios (id_usuario);

CREATE OR REPLACE VIEW v_email_acoes_atrasadas AS
	SELECT 
            dba.id_analise AS analise_id,
            dba.titulo AS analise_titulo, 
            a.id_acao AS acao_id,
            a.descricao AS acao_descricao, 
            a.prazo AS acao_prazo, 
            a.resposta AS acao_resposta,
            c.id_usuario AS criador_id,
            c.nome AS criador_nome,
            r.id_usuario AS responsavel_id,
            r.nome AS responsavel_nome,
            r.usuario AS responsavel_email
	FROM
            dados_basico_analise AS dba
	INNER JOIN acoes AS a
            ON dba.id_analise = a.id_analise
        INNER JOIN usuarios AS r
            ON r.nome = a.responsavel
        INNER JOIN usuarios AS c
            ON c.id_usuario = dba.quem_cadastro
	ORDER BY a.id_acao ASC;