CREATE OR REPLACE VIEW v_gerenciar_analises AS
    SELECT 
        d.id_basico AS basico_id,
        u.id_unidade AS unidade_id,
        u.nome AS unidade_descricao,
        d.linha_producao AS linha_producao_descricao,
        d.equipamento AS equipamento_descricao,
        s.id_situacao AS situacao_id,
        s.descricao AS situacao_descricao,
        af.titulo AS titulo,
        af.prioridade AS prioridade,
        af.apontador AS apontador,
        af.data_evento AS data_evento
    FROM 
        dados_basico_analise AS d
    LEFT JOIN unidade_fabril AS u
        ON u.id_unidade = d.id_unidade
    LEFT JOIN situacao AS s
        ON s.id_situacao = d.id_situacao
    LEFT JOIN analise_falhas AS af
        ON d.id_analise = af.id_analise
    ORDER BY af.prioridade ASC, af.data_evento DESC;

