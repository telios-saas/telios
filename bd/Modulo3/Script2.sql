CREATE OR REPLACE VIEW v_acao_bloqueio AS
	SELECT 
            dba.id_analise AS analise_id, 
            dba.titulo AS analise_titulo, 
            dba.id_unidade AS unidade_id,
            dba.linha_producao AS linha_producao_descricao,
            dba.equipamento AS equipamento_descricao,
            a.id_acao AS acao_id,
            a.apontador AS acao_apontador, 
            a.responsavel AS acao_responsavel, 
            a.descricao AS acao_descricao, 
            a.prazo AS acao_prazo, 
            s.id_situacao AS situacao_id, 
            s.descricao AS situacao_descricao
	FROM
            dados_basico_analise AS dba
	INNER JOIN acoes AS a
            ON dba.id_analise = a.id_analise
        INNER JOIN situacao AS s
            ON a.id_situacao = s.id_situacao
	ORDER BY a.id_acao ASC;