SET foreign_key_checks = 0;
DROP TABLE IF EXISTS `5_porques`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `5_porques` (
  `id_pq` int(50) NOT NULL AUTO_INCREMENT,
  `id_analise` int(50) NOT NULL,
  `descricao_desvio` mediumtext,
  `1_round` mediumtext,
  `hipotese_1_1` mediumtext,
  `hipotese_v_1_1` mediumtext,
  `hipotese_1_2` mediumtext,
  `hipotese_v_1_2` mediumtext,
  `hipotese_1_3` mediumtext,
  `hipotese_v_1_3` mediumtext,
  `2_round` mediumtext,
  `hipotese_2_1` mediumtext,
  `hipotese_v_2_1` mediumtext,
  `hipotese_2_2` mediumtext,
  `hipotese_v_2_2` mediumtext,
  `hipotese_2_3` mediumtext,
  `hipotese_v_2_3` mediumtext,
  `3_round` mediumtext,
  `hipotese_3_1` mediumtext,
  `hipotese_v_3_1` mediumtext,
  `hipotese_3_2` mediumtext,
  `hipotese_v_3_2` mediumtext,
  `hipotese_3_3` mediumtext,
  `hipotese_v_3_3` mediumtext,
  `4_round` mediumtext,
  `hipotese_4_1` mediumtext,
  `hipotese_v_4_1` mediumtext,
  `hipotese_4_2` mediumtext,
  `hipotese_v_4_2` mediumtext,
  `hipotese_4_3` mediumtext,
  `hipotese_v_4_3` mediumtext,
  `5_round` mediumtext,
  `hipotese_5_1` mediumtext,
  `hipotese_v_5_1` mediumtext,
  `hipotese_5_2` mediumtext,
  `hipotese_v_5_2` mediumtext,
  `hipotese_5_3` mediumtext,
  `hipotese_v_5_3` mediumtext,
  `ideia_melhora` mediumtext,
  PRIMARY KEY (`id_pq`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `5_porques`
--

LOCK TABLES `5_porques` WRITE;
/*!40000 ALTER TABLE `5_porques` DISABLE KEYS */;
INSERT INTO `5_porques` VALUES (8,8,'Teste','Teste','Teste','S','Teste','N','','N','','','N','','N','','N','','','N','teste','N','','N','','','N','','N','','','','','N','','N','','N',''),(9,9,'','','','N','','N','','N','','','N','','N','','N','','','N','','N','','N','','','N','','N','','','','','N','','N','','N',''),(10,10,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(12,12,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(27,27,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(28,28,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(39,39,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(40,40,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(41,41,'Presença de ponto preto na embalgem','Por que exitem pontos pretos na embalagem?','Material degradado no cabeçote','S','Excesso de temperatura do cabeçote','N','Design no cabeçote','N','Por que ha material degradado no cabeçote?','Existencia de zonas mortas no cabeçote','S','Termopar descalibrado','N','','N','Por que ha zonas mortas no cabeçote','Design não aproriado para o processo','N','cabeçote muito velho','S','','N','Por que o cabeçote é muito velho?','Restrição nos investimento de manutenção','S','','N','','','Por que estamos restringindo o orçamento de manutenção?','Falta de planejamento para a renovação do parque fabril.','S','','N','','N','1) Avaliar os custo para manutenção geral nos equipamentos da fábrica'),(43,43,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(52,52,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(54,54,'Teste Atualização','Teste Atualização','Teste Atualização','N','Teste Atualização','N','Teste Atualização','S','Teste Atualização','Teste Atualização','N','Teste Atualização','N','Teste Atualização','S','Teste Atualização','Teste Atualização','S','Teste Atualização','N','Teste Atualização','N','Teste Atualização','Teste Atualização','N','Teste Atualização','N','Teste Atualização','Teste Atualização','Teste Atualização','Teste Atualização','S','Teste Atualização','N','Teste Atualização','N','Teste Atualização'),(59,59,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(60,60,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(61,61,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(73,73,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(75,75,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(83,83,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(87,87,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','');
/*!40000 ALTER TABLE `5_porques` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acoes`
--

DROP TABLE IF EXISTS `acoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acoes` (
  `id_acao` int(50) NOT NULL AUTO_INCREMENT,
  `id_analise` int(50) NOT NULL,
  `responsavel` varchar(255) NOT NULL,
  `descricao` longtext NOT NULL,
  `prazo` varchar(255) NOT NULL,
  `situacao` varchar(255) NOT NULL,
  `apontador` int(50) NOT NULL,
  `resposta` longtext NOT NULL,
  PRIMARY KEY (`id_acao`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acoes`
--

LOCK TABLES `acoes` WRITE;
/*!40000 ALTER TABLE `acoes` DISABLE KEYS */;
INSERT INTO `acoes` VALUES (7,7,'Marcos Duda Telios8 Equipe1','teste usuário nivel2','10/03/2012','Pendente',13,''),(9,8,'Teste','dafdsf','10/02/2012','Pendente',14,''),(10,9,'Kodo','Teste','14/10/2012','Pendente',15,''),(11,9,'Kodo','Teste 2','12/10/2012','Pendente',15,''),(12,9,'Kodo','aaaaa','14/10/2012','Concluída',15,''),(15,10,'Teste','aaaa','14/10/2101','Pendente',16,''),(19,12,'aaaa','teste','12/09/2012','Pendente',18,''),(50,39,'Huguinho','Elaborar Fluxo de Diagnóstico para minimizar o impacto do tempo de parada','30/04/2013','Pendente',25,''),(52,41,'MDT2E','Avaliar melhorias do cabeçote junto ao fabricante do equipamento','19/04/2013','Pendente',17,''),(53,41,'MDT2E','Verificar a influência da temperatura ambiente com a presença de pontos pretos.','19/04/2013','Concluída',17,'A ocorrência de pontos pretos é na primeira hora da manha, independente se está frio ou quente. Não identificamos influência da temperatura ambiente com a presença de pontos pretos.'),(54,41,'MBKT1','Reavaliar procedimento de parada/partida, verificando quais variáveis podem interferir no aumento ou diminuição do tempo de presença de pontos pretos.','27/04/2013','Pendente',17,''),(55,41,'MDT2E','Verificar dados da última calibração, frequencia de calibrações dos termopares das sopradoras e avaliar se devemos alterar plano.','25/04/2013','Pendente',17,''),(56,41,'MBKT1','Contatar fornecedor da resina e agendar treinamento para a equipe de operação.','19/04/2013','Atrasada',17,''),(57,41,'MBKT1','Verificar a presença de finos no funil ou no tambor de moido.','16/04/2013','Atrasada',17,''),(61,52,'MBKT1','Veriifcar a Temperatura de Extrusão do processo de sopro.\n','23/04/2013','Concluída',17,'Temperatura Ok. Perfil de temperatura: 180 - 200 °C.'),(62,52,'MBKT1','Verificar problemas no master juntamente com o fornecedor.','25/04/2013','Concluída',17,'Segundo o fornecedor, o master não apresentou nenhuma alteração de formulação. O fornecedor retirou 100 g de amostra para análise.'),(63,52,'MBKT1','Verificar problemas com a resina juntamente com o fornecedor.','25/04/2013','Concluída',17,'A resina é indicada para esse tipo de aplicação e possui a aditivação correta, segundo o fornecedor. Como o problema foi confirmado em vários lotes da resina, essa hipotese foi descartada.'),(64,52,'MBKT1','Verificar se há bicos de saída de gás entupido.','25/04/2013','Concluída',17,'Os bicos estão Ok.'),(65,52,'MBKT1','Verificar se o tempo de exposição do frascos à chama é adequado. Não pode ser inferior à 1 s.','25/04/2013','Concluída',17,'Tempo de exposição oscila entre 3 e 4s. Tempo Ok.'),(66,52,'MBKT1','Verificar se a distância entre a chama e o frasco é adequada.','25/04/2013','Concluída',17,'Segundo o assistente técnico da resina, a distância entre a chama e o frasco é adequada para garantir qualidade de flambagem.'),(67,52,'MBKT1','Estabelecer um controle de qualidade da flambagem na área de serigrafia.','21/05/2013','Concluída',17,'A partir de 30/04 a cada 100 frascos produzidos, 5 serão avaliados. O método de avaliação basicamente consiste na imersão em água e verificação visual da tensão superficial da água sobre o frasco.'),(68,52,'MBKT1','Verificar a qualidade da tinta juntamente com o fabricante.','25/04/2013','Concluída',17,'Segundo o fornecedor, a qualidade da tinta está ok.'),(69,52,'MBKT1','Verificar a eficiência do processo de cura da tinta.','25/04/2013','Concluída',17,'O cliente visitou a nossa planta e acompanhou todo o processo. Segundo o técnico, o procedimento está ok.'),(70,52,'MBKT1','Verificar a qualidade da superfície da cavidade do molde.','25/04/2013','Concluída',17,'A superfície do molde está muito lisa. Necessário realizar o jateamento da superfície do molde para promover uma superfície mais áspera, o que auxilia a aderência física da tinta no frasco.'),(71,54,'Marciane Reiter','Teste ','30/04/2013','Pendente',1,'Teste'),(72,54,'Marciane Reiter','Teste AHEUEAHUEAHUEA HAEUAHEUAEHUAE','30/04/2013','Concluída',1,'teste'),(73,54,'Marciane Reiter','fadsfasd','30/04/2013','Pendente',1,'fasdfasdfas'),(74,54,'Mauricio Delajustine Kodo','adsfds','11/11/1111','Concluída',1,'fasdfasd'),(75,54,'Marciane Reiter','Alteração ','30/04/2013','Concluída',1,'Alteração '),(76,54,'Mauricio Delajustine Kodo','fasd','20/02/2012','Pendente',1,'fasdfasdfsda'),(77,54,'Marciane Reiter','sdfasf','10/08/2013','Atrasada',1,'asdfadsfdas'),(78,54,'Marciane Reiter','fasdfasdfasd','20/10/2013','Pendente',1,'sfsfasd'),(79,54,'Mauricio Delajustine Kodo','dfasfadsf','20/03/2013','Cancelada',1,'asdfasdfasd'),(80,54,'Jacson','Teste Jack Alteracao','30/04/2013','Pendente',1,'Teste Jack'),(88,60,'MBKT1','Verificar juntamente com o fabricante da resina se o perfil de temperatura utilizado atualmente é o mais indicado.','10/05/2013','Concluída',17,'O perfil de temperatura utilizado está entre 180°C e 200°C. Foi relatado pelo fabricante de resina que temperaturas baixas podem causar o fenômeno obervado no parison devido ao aumento do cisalhamento no material fundido.'),(89,60,'MBKT1','Verificar juntamente com o responsável da manutenção sobre o que foi feito no equipamento. Por que há zonas de aquecimento desligadas?','08/05/2013','Concluída',17,'Foi relatado um desvio durante a manutenção. As zonas de aquecimento foram desativadas por acidente durante a manutenção elétrica preventiva. As zonas de aquecimento foram religadas e com a temperatura de canhão estabilizada, foi possível notar uma'),(90,60,'MBKT1','Verificar o grau de polimento dos dispositivos da trefila. Comparar com outros equipamentos.','10/05/2013','Concluída',17,'O polimento da trefila foi realizado. Quando comparado à outras sopradoras mais novas, notamos a qualidade inferior da superfície dos dispositivos. Polimento realizado e equipamento já em operação.'),(107,73,'MBKT1','Diminuição da temperatura de massa.Verificar a funcionalidade das zonas de aquecimento e termopares.','30/06/2013','Concluída',17,'A temperatura de massa está entre 210 - 220°C, temperatura sugerida pelo fornecedor de matéria-prima.\nFoi encontrada uma zona de aquecimento ineficiente. Os dispositivos foram trocados e o controle de temperatura está Ok!'),(108,73,'MBKT1','Aumentar a pressão de recalque da peça.','28/06/2013','Concluída',17,'Foram realizados testes em 3 patamares superiores ao determinado pela ficha de produção. Não houve percepção de melhoria significativa no empenamento da peça final.'),(109,73,'MBKT1','Verificar a eficiência do resfriamento do chiller.\nVerificar a temperatura do molde.','01/07/2013','Concluída',17,'Eficiência do Chiller dentro dos parâmetros de especificação.\nTemperatura do molde trabalhando entre 15 e 20°C. Ok!'),(110,73,'MBKT1','Verificar o dimensional dos pontos de injeção e do canal de alimentação de injeção. Estão dentro da especificação?','01/07/2013','Concluída',17,'Os dimensionais estão dentro da especificação estabelecida no projeto.'),(111,73,'MDT2E','Supervisionar nos turnos o perfil de temperatura e os parâmetros de processo aplicados pelos operadores. Ficar atento para possíveis alterações de tempo de ciclo.','05/07/2013','Pendente',17,''),(112,73,'MBKT1','Verificar cotas do molde. O dimensional do molde ainda se encontra dentro do range especificado pelo projeto?','01/07/2013','Concluída',17,'O dimensional está Ok. As cotas foram medidas e verificadas com o determinado no projeto.'),(113,73,'MBKT1','Consultar o fornecedor para utilização de uma resina com menor fluidez. OBS.: Realizar essa ação assim que as outras ações forem concluídas.','10/07/2013','Concluída',17,'O fornecedor sugeriu a utilização de outra resina, de maior fluidez, distribuição de massa molar mais estreita e menor densidade. A utilização do material diminui a pressão de injeção, forçando menos o material e diminuindo significativamente o ');
/*!40000 ALTER TABLE `acoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `analise_falhas`
--

DROP TABLE IF EXISTS `analise_falhas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analise_falhas` (
  `id_analise` int(50) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `data_evento` varchar(255) NOT NULL,
  `prioridade` varchar(255) DEFAULT NULL,
  `apontador` int(50) NOT NULL,
  PRIMARY KEY (`id_analise`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `analise_falhas`
--

LOCK TABLES `analise_falhas` WRITE;
/*!40000 ALTER TABLE `analise_falhas` DISABLE KEYS */;
INSERT INTO `analise_falhas` VALUES (8,'Teste','12/03/2012','',14),(9,'Teste Análise','12/03/2012','A',15),(10,'Teste','13/03/2012','',16),(12,'teste','19/03/2012','',18),(27,'Teste com Enfardadeira Seladora','14/06/2012','A',20),(28,'Analise 1','27/07/2012','C',21),(39,'QUEBRA DA ROSCA SEM FIM','08/04/2013','B',25),(40,'3434','08/04/2013','',25),(41,'Contaminação por pontos pretos','15/04/2013','C',17),(43,'Análise Mauricio ','18/04/2013','E',1),(52,'Baixa aderência na Serigrafia','21/04/2013','E',17),(54,'Analíse User Mauricio','24/04/2013','E',1),(59,'Parison Rugoso (fratura do fundido)','29/04/2013','B',17),(60,'Olho de Peixe (Géis)','29/04/2013','A',17),(61,'Empenamento','29/04/2013','C',17),(73,'Contração muito alta da peça final injetada','24/06/2013','B',17),(75,'DASSDFG SDGD FGSDFGSDF','01/07/2013','A',1),(83,'teste','20/07/2013','E',10),(87,'Falha no empurrador','29/08/2013','B',34);
/*!40000 ALTER TABLE `analise_falhas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `arquivos_analise`
--

DROP TABLE IF EXISTS `arquivos_analise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arquivos_analise` (
  `id_arquivo` int(50) NOT NULL AUTO_INCREMENT,
  `id_analise` int(50) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `data` varchar(255) NOT NULL,
  `descricao` longtext NOT NULL,
  PRIMARY KEY (`id_arquivo`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `arquivos_analise`
--

LOCK TABLES `arquivos_analise` WRITE;
/*!40000 ALTER TABLE `arquivos_analise` DISABLE KEYS */;
INSERT INTO `arquivos_analise` VALUES (44,12,'solucao.jpg','19/03/2012','Teste'),(45,12,'celito sawitzki.jpg','19/03/2012','teste'),(136,29,'1352495782.jpg','09/11/2012','Anexe figuras/fotos da falha e das evidencias. Anexe artigos e documentos para compor o dossie de dados relevantes.'),(138,41,'1366073358.jpg','15/04/2013','Fotos ponto preto embalagem 250ml'),(140,73,'1372129127.jpg','24/06/2013','Empenamento observado na caixaria');
/*!40000 ALTER TABLE `arquivos_analise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `causa_efeito`
--

DROP TABLE IF EXISTS `causa_efeito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `causa_efeito` (
  `id_causa` int(50) NOT NULL AUTO_INCREMENT,
  `id_analise` int(50) NOT NULL,
  `maquina` longtext,
  `meio_ambiente` longtext,
  `metodo` longtext,
  `medida` longtext,
  `mao_obra` longtext,
  `materia_prima` longtext,
  `falha` longtext,
  PRIMARY KEY (`id_causa`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `causa_efeito`
--

LOCK TABLES `causa_efeito` WRITE;
/*!40000 ALTER TABLE `causa_efeito` DISABLE KEYS */;
INSERT INTO `causa_efeito` VALUES (8,8,'teste','','','','','',''),(9,9,'','','','','','',''),(10,10,'','','','','','',''),(12,12,'teste','teste','teste','teste','teste','teste','teste'),(27,27,'','','','','','',''),(28,28,'','','','','','',''),(39,39,'Desgaste na Chaveta\nEnrosco de latas na máquina','umidade.\n','Falta de um procedimento de diagnóstico.\n\nfluxo de reparo','','','material empregado na construção da chaveta','Amassamento de latas na entrada'),(40,40,'','','','','','',''),(41,41,'- Design do cabeçote;','- Temperatura elevada do parque industrial.','- Procedimento de partida/parada não adequado de forma a reduzir tempo de ajuste.','- Calibração dos termopares;','- Falta de treinamento dos operadores do Sopro;','- Presença de finos no funil de alimentação;','Contaminação por pontos pretos'),(43,43,'','','','','','',''),(52,52,'Bico de saída de gás entupido;\r\nQualidade da superfície da cavidade do molde.\r\nExcesso de temperatura de extrusão.','Temperatura no transporte.','Baixo tempo de flambagem;','Falta de procedimento de qualidade na área de serigrafia.','Posição do frasco colocado em máquina pelo operador.','Aditivação/N.C. da resina;\r\nN.C. do master branco;\r\n','Baixa aderência na serigrafia'),(54,54,'Teste Atualização','Teste Atualização','Teste Atualização','Teste Atualização','Teste Atualização','Teste Atualização','Teste Atualização'),(59,59,'','','','','','',''),(60,60,'Perfil de temperatura;\nZonas de aquecimento desligadas;','Oscilação da temperatura ambiente entre dia e noite','Manutenção preventiva em máquina - todos os procedimentos foram realizados?','Análise das condições de matéria-prima antes de entrada em máquina;\nLiberação de produtos pela qualidade.','Cumprimento dos parâmetros de processo de produção;','Qualidade da matéria-prima recebida;\nPresença de umidade ou contaminação.','Rugosidade do parison'),(61,61,'','','','','','',''),(73,73,'Molde com muito tempo de uso;\nDimensões do ponto de injeçãoe do canal de alimentação incorretos;\nPerfil de aquecimento da canhão não eficiente;\nResfriamento da água ineficiente (chiller).\n','Alta temperatura do ambiente prejudica o resfriamento da peça;\n','Alta pressão de injeção;\nBaixo tempo de recalque da peça;\nBaixa velocidade de injeção;','','Ajuste de perfil de temperatura incorreto;\nVariação do tempo de ciclo;\nUtilização de mais material moído do que o determinado pela ficha de produção.','Resina com baixa fluidez;\nResina com alta rigidez;','Empenamento da peça'),(75,75,'','','','','','',''),(83,83,'','','','','','',''),(87,87,'','','','','','','');
/*!40000 ALTER TABLE `causa_efeito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id_clientes` int(50) NOT NULL AUTO_INCREMENT,
  `razao_social` varchar(255) NOT NULL,
  `cnpj` varchar(255) DEFAULT NULL,
  `ins_estadual` varchar(255) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  `email_empresa` varchar(255) DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL,
  `pais` varchar(255) DEFAULT NULL,
  `ramo_atividade` varchar(255) DEFAULT NULL,
  `cep` varchar(255) DEFAULT NULL,
  `marca_empresa` varchar(255) DEFAULT 'padrao.jpg',
  `responsavel_tecnico` varchar(255) DEFAULT NULL,
  `cpf` varchar(255) DEFAULT NULL,
  `data` varchar(255) NOT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_clientes`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'Agência GH Alteração','','','Rua São José 27','55 3512 3046','contato@agenciagh.com.brd','RS','Brasil','Desenvolvimento Web','99999-999','150312-gh new 1050.jpg','Agência GH','','01/03/2012',NULL),(2,'Agência GH','','','Rua São José 27','(55) 3512-3046','contato@agenciagh.com.br','RS','Brasil','TEste','98900-000','padrao.jpg','Agência GH 2','','02/03/2012',NULL),(3,'GH',NULL,NULL,'Rua america','(55) 3512-3046','conred.lenz@gmail.com','RS','Brasil','Design','98900-000','padrao.jpg','Conrado Lenz',NULL,'06/03/2012',NULL),(4,'design design',NULL,NULL,'Rua São José','(55) 3333-3333','conred.lenz@gmail.com','RS','Brasil','design','98900-000','padrao.jpg','Conrado Lenz',NULL,'06/03/2012',NULL),(7,'',NULL,NULL,'','','natibaumgart@hotmail.com','RS','Brasil','Teste','','padrao.jpg','Natália Caroline Baumgart',NULL,'08/03/2012',NULL),(8,'',NULL,NULL,'','','mauricio.delajustine@gmail.com','RS','Brasil','Teste','','padrao.jpg','Mauricio Delajustine',NULL,'08/03/2012',NULL),(9,'',NULL,NULL,'','','kodo_u@hotmail.com','RS','Brasil','Teste','','padrao.jpg','Mauricio Delajustine',NULL,'08/03/2012',NULL),(10,'',NULL,NULL,'','','marcos.duda.telios1@gmail.com','RS','Brasil','teste','','padrao.jpg','MDT1',NULL,'08/03/2012',NULL),(14,'',NULL,NULL,'','','mauricio@agenciagh.com.br','RS','Brasil','Teste','','padrao.jpg','mauricio',NULL,'12/03/2012',NULL),(15,'',NULL,NULL,'','','mauricio@agenciagh.com.br','RS','Brasil','Teste','','padrao.jpg','Mauricio Delajustine',NULL,'12/03/2012',NULL),(16,'',NULL,NULL,'','','mauricio.delajustine@gmail.com','RS','Brasil','Teste Final','','padrao.jpg','Mauricio ',NULL,'13/03/2012',NULL),(17,'ACME','','','','','marcos.duda.telios2@gmail.com','RS','Brasil','teste','','240312-ACME Logo.jpg','MDT2','','13/03/2012',''),(18,'Teste rorato',NULL,NULL,'','','rorato@agenciagh.com.br','RS','Brasil','Programacao','','padrao.jpg','Gustavo Rorato',NULL,'19/03/2012',NULL),(20,'Greco ',NULL,NULL,'Rua Albano donizete Miano, 91','(11) 4114-4400','sergio@grecoeguerreiro.com.br','SP','Brasil','Transformação Plásticos','13255-170','padrao.jpg','Sérgio Luiz da Silva',NULL,'13/06/2012',NULL),(21,'Costa Franco e Valduga',NULL,NULL,'','','elimombelli@gmail.com','RS','Brasil','Jurídico','','padrao.jpg','Elisa Mombelli',NULL,'27/07/2012',NULL),(22,'Grupo GG',NULL,NULL,'','','sassouza1975@gmail.com','SP','Brasil','Transformação Plastico','','padrao.jpg','Sandro A Souza',NULL,'13/11/2012',NULL),(23,'Costa Franco Valduga Advocacia',NULL,NULL,'Rua Cel. Bordini, 689/601, Porto Alegre','(51) 3084-6689','helena.franco@yahoo.com.br','RS','Brasil','advocacia','90440001','padrao.jpg','Helena Franco',NULL,'13/11/2012',NULL),(24,'',NULL,NULL,'','','arturmuller@globo.com','MG','Brasil','Manutenção Industrial','','padrao.jpg','Artur Muller',NULL,'06/03/2013',NULL),(25,'Grupo Petropolis SA',NULL,NULL,'','','lroveri@grupopetropolis.com.br','SP','Brasil','Bebidas','','padrao.jpg','Luciano Roveri',NULL,'08/04/2013',NULL),(26,'Braskem S.A.',NULL,NULL,'Rua Capote Valente, 188','(11) 9813-7539','murilo.klen@braskem.com','SP','Brasil','Engenharia','05409-000','padrao.jpg','Murilo Klen',NULL,'21/04/2013',NULL),(27,'',NULL,NULL,'','','marcos.duda.telios3@gmail.com','RS','Brasil','Transformação plastica','','padrao.jpg','MDT3',NULL,'15/05/2013',NULL),(28,'Agência GH',NULL,NULL,'','','joabel@agenciagh.com.br','RS','Brasil','Teste','','padrao.jpg','Joabel',NULL,'21/06/2013',NULL),(29,'',NULL,NULL,'','','joabel@agenciagh.com.br','RS','Brasil','Teste','','padrao.jpg','Joabel',NULL,'21/06/2013',NULL),(30,'Agência GH Teste',NULL,NULL,'','','mauricio.delajustine@gmail.com','RS','Brasil','teste','','padrao.jpg','Mauricio Delajustine',NULL,'21/06/2013',NULL),(31,'',NULL,NULL,'','','marcos.duda.telios4@gmail.com','RS','Brasil','teste','','padrao.jpg','Marcos Duda Telios4',NULL,'24/06/2013',NULL),(32,'Greco ',NULL,NULL,'Rua Albano Donizete Miano, 91 - Bairro São Benedito','(11) 4014-4400','diogenes@grecoeguerreiro.com.br','SP','Brasil','Industria de embalagens plasticas','13260-000','120713-LOGO PNG.png','Diógenes G. dos Santos Neto',NULL,'12/07/2013',NULL),(33,'',NULL,NULL,'','','f.montezuma@hotmail.com','RS','Brasil','TI','','padrao.jpg','fabio',NULL,'10/08/2013',NULL),(34,'Loreal',NULL,NULL,'Av. Manoel Monteiro Araujo','(11) 3622-1208','jose.vieira@br.loreal.com','SP','Brasil','COSMETICA ','','290813-ze.bmp','José Eduardo Vieira',NULL,'29/08/2013',NULL),(35,'',NULL,NULL,'','','marcos.duda.telios1@gmail.com','RS','Brasil','teste','','padrao.jpg','marcos.duda.telios1@gmail.com',NULL,'09/09/2013',NULL);
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contratos`
--

DROP TABLE IF EXISTS `contratos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contratos` (
  `id_contrato` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(50) NOT NULL,
  `id_tipo_contrato` int(11) NOT NULL,
  `dt_inicio` date DEFAULT NULL,
  `dt_fim` date DEFAULT NULL,
  `id_status_contrato` varchar(10) NOT NULL,
  `vr_preco` decimal(10,2) NOT NULL,
  `ds_chave` varchar(20) NOT NULL,
  PRIMARY KEY (`id_contrato`),
  KEY `fk_usuario_contratos_usuarios_idx` (`id_usuario`),
  KEY `fk_usuario_contratos_tipo_contrato1_idx` (`id_tipo_contrato`),
  KEY `fk_usuario_contratos_status_contrato1_idx` (`id_status_contrato`),
  CONSTRAINT `fk_usuario_contratos_status_contrato1` FOREIGN KEY (`id_status_contrato`) REFERENCES `status_contrato` (`id_status_contrato`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_contratos_tipo_contrato1` FOREIGN KEY (`id_tipo_contrato`) REFERENCES `tipo_contrato` (`id_tipo_contrato`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_contratos_usuarios` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contratos`
--

LOCK TABLES `contratos` WRITE;
/*!40000 ALTER TABLE `contratos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contratos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dados_basico_analise`
--

DROP TABLE IF EXISTS `dados_basico_analise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dados_basico_analise` (
  `id_basico` int(50) NOT NULL AUTO_INCREMENT,
  `id_analise` int(50) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `data` varchar(255) DEFAULT NULL,
  `equipamento` varchar(255) DEFAULT NULL,
  `linha_producao` varchar(255) DEFAULT NULL,
  `unidade_fabril` varchar(255) DEFAULT NULL,
  `data_avaliacao` varchar(255) DEFAULT NULL,
  `equipe_avaliacao` varchar(255) DEFAULT NULL,
  `descricao_evento` longtext,
  `evidencias` longtext,
  `causa_basica` longtext,
  `componente_falhou` varchar(255) DEFAULT NULL,
  `funcao` varchar(255) DEFAULT NULL,
  `causa_raiz` longtext,
  `observacoes` longtext,
  `situacao` varchar(255) NOT NULL,
  `modo_falha` longtext,
  `prioridade` varchar(255) DEFAULT NULL,
  `apontador` int(50) NOT NULL,
  `publicador` varchar(255) NOT NULL,
  `familia_equipamento` varchar(255) NOT NULL,
  `quem_cadastro` varchar(50) NOT NULL,
  `solucao` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_basico`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dados_basico_analise`
--

LOCK TABLES `dados_basico_analise` WRITE;
/*!40000 ALTER TABLE `dados_basico_analise` DISABLE KEYS */;
INSERT INTO `dados_basico_analise` VALUES (8,8,'Teste','12/03/2012','Tesssssssssssssete','Teste','Alterado',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Pendente',NULL,NULL,15,'','','',NULL),(9,9,'Teste Análise','12/03/2012','Teste Equipamento','Teste linha produção','Teste Unidade',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Pendente',NULL,'A',15,'','','',NULL),(10,10,'Teste','13/03/2012','Teste','Teste','Alterado',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Pendente',NULL,NULL,16,'','','',NULL),(12,12,'teste','19/03/2012','TESTE','teste','Alterado','12/02/2012','teste','teste','teste','teste','teste','teste','teste','teste','Pendente','teste',NULL,18,'','','',NULL),(27,27,'Teste com Enfardadeira Seladora','14/06/2012','S#39','Sopro 2','Morungaba','','','','','','','','','','Pendente','',NULL,20,'','','',NULL),(28,28,'Analise 1','27/07/2012','Samsung','Direito Digital','Porto Alegre 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Pendente',NULL,NULL,21,'','','',NULL),(39,39,'QUEBRA DA ROSCA SEM FIM','08/04/2013','ENCHEDORA DE LATAS','02','BOITUVA','08/04/2013','Huguinho, Zezinho; Pedrinho','Foi  verificado o amassamento de latas constantes na máquina. verificado que o sincronismo da entrega de latas da estrela para a cúpula estava normal e ao verificar a entrega da rosca para  a estrela, constatou-se folga na rosca.','Aparecimento de latas amassadas na esteira para a recravadora.','Folga na rosca sem fim ocasionando o aparecimento de latas amassadas','Chaveta da Rosca Sem fim danificada','fixar a rosca no eixo de transmissão ','devido o travamento de latas exporádico na entrada de latas causava o travamento da rosca ','','Concluída','Desgaste anormal na chaveta da rosca','B',25,'','LINHA DE LATAS','55',NULL),(40,40,'3434','08/04/2013','ENCHEDORA DE LATAS','02','BOITUVA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Pendente',NULL,NULL,25,'','LINHA DE LATAS','55',NULL),(41,41,'Contaminação por pontos pretos','15/04/2013','SO-01','LI-01','UN-01','15/04/2013','MDT1 e MBKT1','Ao partir a extrusora todas as 2as, as embalagens sopradas nas primeiras 2h apresentam pontos pretos. Isto ocorre independente da resina que estamos utilizando. Os pontos estão dispersos por todo o frasco. Todos os operadores que partiram o equipamento encontraram o mesmo problema','Amostras das embalagens sopradas durante a partida do dia 15/04, retiradas as 6h e as 8h estão disponiveis no LCQ e cfme fotos anexo1;','Presença de material carbonizado no cabeçote;','Cabeçote','Comprimir e transportar o material fundido','Falta de investimento no plano de renovação dos equipamentos da empresa.','','Publicada','Permitiu a degradação do material','C',17,'','Sopradora','41',NULL),(43,43,'Análise Mauricio ','18/04/2013','Teste Mauricio Equipamento','Linha de Produção Mauricio','Unidade Fabril Mauricio',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Publicada',NULL,'E',1,'','Teste 2 Mauricio 18-04','1',NULL),(52,52,'Baixa aderência na Serigrafia','21/04/2013','SO-01','Linha 1','UN-03','21/04/2013','MBKT1 e MDT1','Frascos \"borrados\" recebido pelos clientes. O fato foi percebido assim que os frascos foram retirados da embalagem de transporte. A tinta solta do frasco e com a movimentação do transporte e atrito os frascos são manchados.','Reclamação de clientes e produto devolvidos','Problemas com a resina ou resina não indicada para essa aplicação;\nProblemas com o master;\nBico de gás entupido na serigrafia.','Frasco','Embalagem de acetona','Superfície da cavidade do molde está com polimento excessivo.','','Publicada','A tinta solta-se do frasco e mancha os frascos durante o transporte.','E',17,'','Sopradora','57',''),(54,54,'Analíse User Mauricio','24/04/2013','Teste Mauricio Equipamento','Linha de Produção Mauricio','Unidade Fabril Mauricio','24/04/2013','Teste Atualização','Teste Atualização','Teste Atualização','Teste Atualização','Teste Atualização','Teste Atualização','Teste Atualização','Teste Atualização','Concluída','Teste Atualização','A',1,'','Teste 2 Mauricio 18-04','2','Teste Atualização'),(59,59,'Parison Rugoso (fratura do fundido)','29/04/2013','SO-02','Linha 1','UN-03',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Pendente',NULL,'B',17,'','Sopradora','57',NULL),(60,60,'Olho de Peixe (Géis)','29/04/2013','SO-01','Linha 1','UN-03','05/05/2013','MBKT1 e MDT2','Marcas de rugosidade no Parison e consequentemente no produto final - frascos 2 l. A qualidade passou a reprovar produtos produzidos na SO-01 no dia 01/05/2013. Os operadores observaram as marcas de rugosidade (\"casca de laranja\") no próprio parison. Operadores de todos os turnos observaram esse defeito no parison. A qualidade continua a reprovar os materiais e a máquina foi parada.','O equipamento passou por manutenção preventiva do dia 30/04/2013 que não teve acompanhamento do supervisor. \nO efeito no parison foi notado em 5 lotes diferentes de resinas. \nNão foram feitas alterações de processo.\nFoi necessário desligar duas zonas de aquecimento devido a falta de reposição de termopares queimados.','Perfil de temperatura inadequado;\nFalta de aquecimento de zonas desligadas;\nPolimento ineficiente das arestas das trefilas.\nÂngulo entre a bucha e o macho muito agudo;','Frasco 2 l.','Embalagem de água sanitária.','Foi relatado um desvio durante a manutenção. As zonas de aquecimento foram desativadas por acidente durante a manutenção elétrica preventiva. As zonas de aquecimento foram religadas e com a temperatura de canhão estabilizada, foi possível notar uma significativa redução na quantidade de géis no produto final. Após 1 h de produção tivemos a aprovação da qualidade.','','Concluída','Marcas de rugosidade, relatado como \"casca de laranja\", que prejudica a estética do produto. ','A',17,'','Sopradora','57',''),(61,61,'Empenamento','29/04/2013','SO-01','Linha 1','UN-03',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Pendente',NULL,'C',17,'','Sopradora','57',NULL),(73,73,'Contração muito alta da peça final injetada','24/06/2013','SO-02','LI-02','UN-01','24/06/2013','Murilo Klen, Marcos Duda','As peças de caixaria estão empenando após o resfriamento. A deformação é bem significativa, sendo necessário o reaproveitamento da peça como moído. O desvio vem ocorrendo com todas as peças do equipamento citado. Até o momento não foi possível retirar um peça sem o empenamento.','Relatos dos operadores sobre a alta temperatura de saída da peça;\nMolde com muito tempo de uso;\nOscilação da temperatura da água;\nAlta pressão para injeção da peça;','Resfriamento do molde ineficiente;\nResina de baixa fluidez;\nMolde com dimensional inadequado;\nDesign do molde inadequado (são necessários nervuras?)','Caixaria ','Produto final','Resina utilizada apresenta IF muito baixo, obrigando a utilização de uma pressão de injeção maior. Quanto maior a pressão de injeção maior a possibilidade de empenar o material devido ao maior orientação das moléculas.','','Pendente','Empenamento de um dos quatro lados da caixa. A área de empenamento não se repete no mesmo ponto em todas as produções.','B',17,'','Sopradora','57','Troca da resina.\nResina com maior fluidez, distribuição de massa molar mais estreita e redução da densidade.'),(75,75,'DASSDFG SDGD FGSDFGSDF','01/07/2013','Teste Mauricio Equipamento','Linha de Produção Mauricio','Unidade Fabril Mauricio',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Publicada',NULL,'A',1,'','Teste 2 Mauricio 18-04','1',NULL),(83,83,'teste','20/07/2013','EQ-01','LI-01','UN-01','','','fdgqertveqr','','','','','','','Pendente','','E',10,'','F1','12',''),(87,87,'Falha no empurrador','29/08/2013','ENCARTUCHADORA','110','UP1','27/08/13','Manutenção','Falhas','NA','Posicionamento das caçambas do robô','Epurrador','Empurrar esmaltes dentro do cartucho','','','Pendente','Travamento',NULL,34,'','BLISTERS','75','');
/*!40000 ALTER TABLE `dados_basico_analise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipamento`
--

DROP TABLE IF EXISTS `equipamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipamento` (
  `id_equipamento` int(50) NOT NULL AUTO_INCREMENT,
  `familia` varchar(255) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `apontador` varchar(255) NOT NULL,
  PRIMARY KEY (`id_equipamento`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipamento`
--

LOCK TABLES `equipamento` WRITE;
/*!40000 ALTER TABLE `equipamento` DISABLE KEYS */;
INSERT INTO `equipamento` VALUES (10,'Motor Elétrico','ME-01','17'),(15,'Sopradora','SO-02','17'),(21,'Sopradora','SO-01','17'),(23,'Sopradora','S#39','20'),(24,'Impressora','Samsung','21'),(28,'LINHA DE LATAS','ENCHEDORA DE LATAS','25'),(29,'LINHA DE LATAS','RECRAVADORA','25'),(32,'Teste 2 Mauricio 18-04','Teste Mauricio Equipamento','1'),(43,'afsdfasdfasd','asdfasdfasd','28'),(47,'F1','EQ-01','10'),(49,'BLISTERS','ENCARTUCHADORA','34');
/*!40000 ALTER TABLE `equipamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `familia_equipamento`
--

DROP TABLE IF EXISTS `familia_equipamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `familia_equipamento` (
  `id_familia` int(50) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `apontador` int(50) NOT NULL,
  PRIMARY KEY (`id_familia`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `familia_equipamento`
--

LOCK TABLES `familia_equipamento` WRITE;
/*!40000 ALTER TABLE `familia_equipamento` DISABLE KEYS */;
INSERT INTO `familia_equipamento` VALUES (9,'Motor Elétrico',17),(11,'Sopradora',17),(12,'Extrusora',17),(19,'Extrusora Plana',17),(20,'Injetora',17),(22,'Sopradora',20),(23,'Rotuladora',20),(25,'Enfardadeira',20),(26,'Impressora',21),(29,'LINHA DE LATAS',25),(32,'Testes Mauricio 18-04',1),(33,'Teste 2 Mauricio 18-04',1),(37,'afsdfasdfasd',28),(39,'Sopradoras',32),(40,'Alimentadores',32),(41,'Rotuladeiras',32),(42,'Enfardadeiras',32),(43,'Moinhos',32),(44,'Esteiras',32),(45,'Dosadores de Pigmento',32),(46,'Compressores',32),(47,'Resfriadores',32),(48,'Silos misturadores',32),(50,'F1',10),(52,'BLISTERS',34);
/*!40000 ALTER TABLE `familia_equipamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `insert_testes`
--

DROP TABLE IF EXISTS `insert_testes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `insert_testes` (
  `cod_teste` int(50) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`cod_teste`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insert_testes`
--

LOCK TABLES `insert_testes` WRITE;
/*!40000 ALTER TABLE `insert_testes` DISABLE KEYS */;
/*!40000 ALTER TABLE `insert_testes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `linha_producao`
--

DROP TABLE IF EXISTS `linha_producao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linha_producao` (
  `id_linha` int(50) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `apontador` int(50) NOT NULL,
  PRIMARY KEY (`id_linha`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `linha_producao`
--

LOCK TABLES `linha_producao` WRITE;
/*!40000 ALTER TABLE `linha_producao` DISABLE KEYS */;
INSERT INTO `linha_producao` VALUES (7,'Teste linha produção',15),(9,'Linha 1',17),(10,'teste',18),(11,'LI-01',17),(12,'LI-02',17),(15,'LI-03',17),(17,'Sopro 2',20),(18,'Direito Digital',21),(20,'Linha de Produção Mauricio',1),(21,'02',25),(22,'02',25),(24,'LI-01',10),(26,'sadfasdfasdfasd',28),(28,'Sopro 1',32),(29,'Sopro 2',32),(30,'Sopro 3',32),(31,'Serigrafia',32),(32,'PET',32),(34,'110',34);
/*!40000 ALTER TABLE `linha_producao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mensagem`
--

DROP TABLE IF EXISTS `mensagem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mensagem` (
  `id_mensagem` int(50) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `mensagem` longtext NOT NULL,
  `data` varchar(255) NOT NULL,
  `ativa` varchar(255) NOT NULL DEFAULT 'S',
  PRIMARY KEY (`id_mensagem`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mensagem`
--

LOCK TABLES `mensagem` WRITE;
/*!40000 ALTER TABLE `mensagem` DISABLE KEYS */;
/*!40000 ALTER TABLE `mensagem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `priorizacao_falhas`
--

DROP TABLE IF EXISTS `priorizacao_falhas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `priorizacao_falhas` (
  `id_priorizacao` int(50) NOT NULL AUTO_INCREMENT,
  `id_analise` int(50) NOT NULL,
  `saude_seguranca` int(5) NOT NULL,
  `meio_ambiente` int(5) NOT NULL,
  `producao` int(5) NOT NULL,
  `custo_manutencao` int(5) NOT NULL,
  `frequencia_evento` int(5) NOT NULL,
  PRIMARY KEY (`id_priorizacao`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `priorizacao_falhas`
--

LOCK TABLES `priorizacao_falhas` WRITE;
/*!40000 ALTER TABLE `priorizacao_falhas` DISABLE KEYS */;
INSERT INTO `priorizacao_falhas` VALUES (1,1,30,0,10,5,4),(2,2,30,30,0,10,10),(3,3,0,0,0,0,0),(8,8,0,0,0,0,0),(9,9,30,0,0,0,10),(10,10,0,0,0,0,0),(11,11,0,0,0,5,10),(12,12,0,0,0,0,0),(14,14,0,0,0,5,1),(15,15,0,0,0,0,0),(16,16,0,0,0,0,0),(18,18,0,0,0,0,0),(19,19,0,0,0,0,0),(23,23,0,0,0,0,0),(26,26,40,40,30,10,6),(27,27,0,20,30,0,10),(28,28,0,0,10,5,4),(30,30,40,40,30,5,1),(31,31,30,30,20,10,8),(32,32,10,30,10,10,10),(33,33,40,40,30,20,10),(34,34,40,40,30,20,10),(35,35,40,40,30,20,10),(36,36,0,0,0,0,0),(37,37,0,0,10,10,2),(38,38,0,0,0,0,10),(39,39,0,0,20,5,4),(40,40,0,0,0,0,0),(41,41,0,0,10,0,10),(43,43,0,0,0,0,10),(52,52,0,0,0,0,0),(54,54,0,0,0,0,1),(59,59,0,0,20,10,10),(60,60,20,0,20,20,10),(61,61,0,0,10,0,8),(73,73,0,0,10,10,10),(75,75,40,40,30,20,10),(83,83,0,0,0,0,10),(87,87,0,0,20,10,10);
/*!40000 ALTER TABLE `priorizacao_falhas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_contrato`
--

DROP TABLE IF EXISTS `status_contrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_contrato` (
  `id_status_contrato` varchar(10) NOT NULL,
  `ds_status_contrato` varchar(20) NOT NULL,
  `st_status_contrato` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_status_contrato`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_contrato`
--

LOCK TABLES `status_contrato` WRITE;
/*!40000 ALTER TABLE `status_contrato` DISABLE KEYS */;
/*!40000 ALTER TABLE `status_contrato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_contrato`
--

DROP TABLE IF EXISTS `tipo_contrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_contrato` (
  `id_tipo_contrato` int(11) NOT NULL AUTO_INCREMENT,
  `ds_tipo_contrato` varchar(50) NOT NULL,
  `num_dias_atividade` int(11) NOT NULL,
  `ds_comentarios` varchar(255) DEFAULT NULL,
  `vr_preco` decimal(10,2) NOT NULL,
  `st_tipo_contrato` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_tipo_contrato`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_contrato`
--

LOCK TABLES `tipo_contrato` WRITE;
/*!40000 ALTER TABLE `tipo_contrato` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_contrato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unidade_fabril`
--

DROP TABLE IF EXISTS `unidade_fabril`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidade_fabril` (
  `id_unidade` int(50) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `apontador` int(50) NOT NULL,
  PRIMARY KEY (`id_unidade`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unidade_fabril`
--

LOCK TABLES `unidade_fabril` WRITE;
/*!40000 ALTER TABLE `unidade_fabril` DISABLE KEYS */;
INSERT INTO `unidade_fabril` VALUES (9,'Unidade 1',17),(11,'UN-01',17),(12,'UN-02',17),(22,'UN-03',17),(25,'Morungaba',20),(26,'Porto Alegre 1',21),(28,'Unidade Fabril Mauricio',1),(29,'BOITUVA',25),(30,'UN-01',10),(34,'Greco - Morungaba',32),(36,'Polisopro - Itatiba',32),(38,'UP1',34);
/*!40000 ALTER TABLE `unidade_fabril` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_clientes`
--

DROP TABLE IF EXISTS `user_clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_clientes` (
  `id_user` int(50) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `area` varchar(255) NOT NULL,
  `empresa` varchar(255) NOT NULL,
  `id_login` int(50) NOT NULL,
  `id_empresa` int(50) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_clientes`
--

LOCK TABLES `user_clientes` WRITE;
/*!40000 ALTER TABLE `user_clientes` DISABLE KEYS */;
INSERT INTO `user_clientes` VALUES (1,'Mauricio Delajustine Kodo','Gerente de TI e Analista de Sistemas','Agência GH Alteração',2,1),(27,'MDT2E','teste','ACME',43,17),(30,'Sérgio Luiz da Silva','Garantia da Qualidade','Greco ',49,20),(31,'MBK','Eng Aplicação','',54,10),(32,'Huguinho','Manutenção Mecânica','Grupo Petropolis SA',56,25),(33,'MBKT1','Eng Aplicação','ACME',57,17),(34,'Marciane Reiter','Testes Sistema','Agência GH Alteração',58,1),(35,'Jacson','Desing','Agência GH Alteração',60,1),(36,'MDT1E','teste','',61,10),(40,'Mauricio Delajustine','Administrador','Agência GH Teste',68,30),(41,'Marcos Duda Telios4','Administrador','',69,31),(42,'Marcos Duda Telios4 Equipe','Testes','',70,31),(44,'Diógenes G. dos Santos Neto','Administrador','Greco ',72,32),(45,'Mauricio Massao Hirano','Manutenção','Greco ',73,32),(46,'fabio','Administrador','',74,33),(47,'José Eduardo Vieira','Administrador','Loreal',75,34),(48,'marcos.duda.telios1@gmail.com','Administrador','',76,35);
/*!40000 ALTER TABLE `user_clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id_usuario` int(50) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `bloqueado` varchar(255) DEFAULT NULL,
  `empresa` varchar(255) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `nivel` int(10) DEFAULT NULL,
  `apontador` int(50) NOT NULL,
  `pontuacao` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_usuario`),
  KEY `fk_usuarios_clientes1_idx` (`apontador`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'contato@agenciagh.com.br','19b19ffc30caef1c9376cd2982992a59 ','N','Agência GH Alteração','Agência GH',1,1,'200'),(2,'mauricio@agenciagh.com.br','0a91a753f094b92553325c367b4bfa92','N','Agência GH Alteração','Mauricio Delajustine Kodo',2,1,'25'),(12,'marcos.duda.telios1@gmail.com','6aae2f0991616421f60d86e45ad908cc','N','','MDT1',1,10,'5'),(41,'marcos.duda.telios2@gmail.com','d41d8cd98f00b204e9800998ecf8427e','N','ACME','MDT2',1,17,'0'),(43,'marcos.duda.telios2.equipe@gmail.com','6aae2f0991616421f60d86e45ad908cc','N','ACME','MDT2E',2,17,'0'),(48,'sergio@grecoeguerreiro.com.br','b2a45e16a3d33a2b8adabe7f09f351e1','N','Greco ','Sérgio Luiz da Silva',1,20,'0'),(49,'sergio@grecoeguerreiro.com.br','fa94ea0eee231240726f6607f9c71941','N','Greco ','Sérgio Luiz da Silva',2,20,'0'),(50,'elimombelli@gmail.com','0e9afb4fd97a5254512ddfc42624f71a','N','Costa Franco e Valduga','Elisa Mombelli',1,21,'0'),(51,'sassouza1975@gmail.com','6fb9dec906ca692e021519560a9b27c4','S','Grupo GG','Sandro A Souza',1,22,'0'),(52,'helena.franco@yahoo.com.br','b121bde6ba124e53699fb61aa4c4dd46','N','Costa Franco Valduga Advocacia','Helena Franco',1,23,'0'),(53,'arturmuller@globo.com','91878677a42cf844520a0939989c13ea','N','','Artur Muller',1,24,'0'),(54,'murilo.klen@braskem.com.br','698dc19d489c4e4db73e28a713eab07b','N','','MBK',2,10,'10'),(55,'lroveri@grupopetropolis.com.br','6f9247eb06cc4a7c1c762555945b1aaf','N','Grupo Petropolis SA','Luciano Roveri',1,25,'35'),(56,'lroveri@grupopetropolis.com.br','6f9247eb06cc4a7c1c762555945b1aaf','N','Grupo Petropolis SA','Huguinho',2,25,'0'),(57,'murilo.klen@braskem.com','a88f8064e964b68bf51f84baa5549bf0','N','ACME','MBKT1',2,17,'140'),(58,'marciane@agenciagh.com.br','26cc385ea81d8856415b5283e8fbb604','N','Agência GH Alteração','Marciane Reiter',2,1,''),(59,'murilo.klen@braskem.com','a88f8064e964b68bf51f84baa5549bf0','S','Braskem S.A.','Murilo Klen',1,26,'0'),(60,'jacson@agenciagh.com.br','4ff9fc6e4e5d5f590c4f2134a8cc96d1','N','Agência GH Alteração','Jacson',2,1,'0'),(61,'marcos.duda.telios1.equipe@gmail.com','6aae2f0991616421f60d86e45ad908cc','N','','MDT1E',2,10,'0'),(62,'marcos.duda.telios3@gmail.com','6aae2f0991616421f60d86e45ad908cc','N','','MDT3',1,27,'0'),(68,'mauricio.delajustine@gmail.com','698dc19d489c4e4db73e28a713eab07b','N','Agência GH Teste','Mauricio Delajustine',1,30,'0'),(69,'marcos.duda.telios4@gmail.com','6aae2f0991616421f60d86e45ad908cc','N','','Marcos Duda Telios4',1,31,'0'),(70,'marcos.duda.telios4.equipe@gmail.com','6aae2f0991616421f60d86e45ad908cc','N','','Marcos Duda Telios4 Equipe',2,31,'0'),(72,'diogenes@grecoeguerreiro.com.br','f7552bbde34f5c8313a68fcfbd7b4273','N','Greco ','Diógenes G. dos Santos Neto',1,32,'0'),(73,'manutencao@grecoeguerreiro.com.br','834a3152b1aabbb8ad4c31ea1dc5ad04','N','Greco ','Mauricio Massao Hirano',2,32,'0'),(74,'f.montezuma@hotmail.com','cab5ca7e24c029fc9753d595ada17cc1','N','','fabio',1,33,'0'),(75,'jose.vieira@br.loreal.com','0e4215a8499305b7c7d9ead2f7f20253','N','Loreal','José Eduardo Vieira',1,34,'5'),(76,'marcos.duda.telios1@gmail.com','6aae2f0991616421f60d86e45ad908cc','N','','marcos.duda.telios1@gmail.com',1,35,'0');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
SET foreign_key_checks = 1;