<?php
    $menuInicial = "new_menu_inicial"; 
    $menuAnalise = "new_menu_cadastra_analise"; 
    $menuAnalise2 = "new_menu_todas_analises"; 
    $menuAcoes = "new_menu_acoes";
    $menuDados = "new_menu_dados";
    $menuDashboard = "new_menu_dashboard";

    $classeInicial = $classeAnalise = $classeAnalise2 = $classeAcoes = $classeDados = $classeDashboard = null;
    if ($menuAtivo == 'inicial') {
        $menuInicial = "menuInicialAtivo";
        $classeInicial = "ativo";
    }
    if ($menuAtivo == 'analise') {
        $menuAnalise = "menuAnaliseAtivo";
        $classeAnalise = "ativo";
    }
    if ($menuAtivo == 'analise2') {
        $menuAnalise2 = "menuAnaliseAtivo_2";
        $classeAnalise2 = "ativo";
    }
    
    if ($menuAtivo == 'acoes') {
        $menuAcoes = "menuAcoesAtivo";
        $classeAcoes = "ativo";
    }
        
    if ($menuAtivo == 'dados') {
        $menuDados = "menuDadosAtivo";
        $classeDados = "ativo";
    }
?>

<div id="new_menu">
  <div id="new_menu_div">
    <ul class="new_menu_ul">
        <div class="div_item_menu">
            <li id="<?php echo $menuInicial ?>" class="menu <?php echo $classeInicial;?>"><a href="index.php">Painel de Controle</a></li>
        </div>
        <div class="div_item_menu_azul">
            <li id="<?php echo $menuAnalise ?>" class="menu <?php echo $classeAnalise;?>"><a href="javascript:;" onclick="menuTop('analiseFalhas/cadastra.php');">Cadastrar Nova An&aacute;lise</a></li>
        </div>
        <div class="div_item_menu">
            <li id="<?php echo $menuAnalise2 ?>" class="menu <?php echo $classeAnalise2;?>"><a href="javascript:;" onclick="menuTop('analiseFalhas/todas_analises.php',Array('js/gerenciarAnalises.js','js/jqplot/jqplot.donutRenderer.min.js','js/jqplot/jqplot.pieRenderer.min.js'));">Gerenciar An&aacute;lises</a></li>
        </div>
        <div class="div_item_menu">
            <li id="<?php echo $menuAcoes ?>" class="menu <?php echo $classeAcoes;?>"><a href="javascript:;" onclick="menuTop('analiseFalhas/acoes.php',Array('js/acoesBloqueio.js','js/jqplot/jqplot.donutRenderer.min.js','js/jqplot/jqplot.pieRenderer.min.js'));">A&ccedil;&otilde;es de Bloqueio</a></li>
        </div>
	<?php if($_SESSION['nivel'] == "1") : ?>
	    <div class="div_item_menu">
		<li id="<?php echo $menuDados ?>" class="menu <?php echo $classeDados;?>">
		    <a href="javascript:;">Dados de Cadastro</a>
		    <ul>
			<div id="bordaSubMenu">
			  <?php if($_SESSION['nivel'] == "1") : ?>
			      <li class="sub"><a href="javascript:;" onclick="menuTop('cliente/nivel2/edita.php');">Editar Meus Dados</a></li>
			      <li class="sub"><a href="javascript:;" onclick="menuTop('cliente/usuario/nivel2/gerencia.php');">Equipe</a></li>
			      <li class="sub"><a href="javascript:;" onclick="menuTop('equipamentos/familia/gerencia.php');">Fam&iacute;lia de Equipamentos</a></li>
			      <li class="sub"><a href="javascript:;" onclick="menuTop('equipamentos/gerencia.php');">Equipamentos</a></li>
			      <li class="sub"><a href="javascript:;" onclick="menuTop('linhaProducao/gerencia.php');">Linha de Produção</a></li>
			      <li class="sub"><a href="javascript:;" onclick="menuTop('unidadesFabris/gerencia.php');">Unidades Fabris</a></li>
			  <?php endif; ?>
			  <?php if($_SESSION['nivel'] == "2") : ?>

			  <?php endif; ?>
			</div>
		  </ul>
		</li>
	    </div>
	<?php endif; ?>
      <li id="menuSair" class="menu"><a href="javascript:;" onclick="menuTop('sair.php');">Sair</a></li>
    </ul>
  </div>
</div>